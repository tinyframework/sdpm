/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.quality.dao.pojo;

import java.io.Serializable;

/**
 * <!-- begin-user-doc --> 测试用例步骤表 * <!-- end-user-doc -->
 */
public class QualityCaseStep implements Serializable{

	/**
	 * <!-- begin-user-doc --> 用例步骤编号 * <!-- end-user-doc -->
	 */
	private Integer caseStepId;

	/**
	 * <!-- begin-user-doc --> 用例编号 * <!-- end-user-doc -->
	 */
	private Integer caseId;

	/**
	 * <!-- begin-user-doc --> 关联用例版本 * <!-- end-user-doc -->
	 */
	private Integer caseVersion;

	/**
	 * <!-- begin-user-doc --> 描述 * <!-- end-user-doc -->
	 */
	private String caseStepDesc;

	/**
	 * <!-- begin-user-doc --> 用例预期 * <!-- end-user-doc -->
	 */
	private String caseStepExpect;

	/**
	 * <!-- begin-user-doc --> 用例步骤编号 * <!-- end-user-doc -->
	 */
	public void setCaseStepId(Integer caseStepId) {
		this.caseStepId = caseStepId;
	}

	public Integer getCaseStepId() {
		return caseStepId;
	}

	/**
	 * <!-- begin-user-doc --> 用例编号 * <!-- end-user-doc -->
	 */
	public void setCaseId(Integer caseId) {
		this.caseId = caseId;
	}

	public Integer getCaseId() {
		return caseId;
	}

	/**
	 * <!-- begin-user-doc --> 关联用例版本 * <!-- end-user-doc -->
	 */
	public void setCaseVersion(Integer caseVersion) {
		this.caseVersion = caseVersion;
	}

	public Integer getCaseVersion() {
		return caseVersion;
	}

	/**
	 * <!-- begin-user-doc --> 描述 * <!-- end-user-doc -->
	 */
	public void setCaseStepDesc(String caseStepDesc) {
		this.caseStepDesc = caseStepDesc;
	}

	public String getCaseStepDesc() {
		return caseStepDesc;
	}

	/**
	 * <!-- begin-user-doc --> 用例预期 * <!-- end-user-doc -->
	 */
	public void setCaseStepExpect(String caseStepExpect) {
		this.caseStepExpect = caseStepExpect;
	}

	public String getCaseStepExpect() {
		return caseStepExpect;
	}

}
