/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.quality.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 测试任务表 * <!-- end-user-doc -->
 */
public class QualityTestTask implements Serializable{

	public static final int DELETE_YES = 1;

	public static final int DELETE_NO = 1;

	/**
	 * <!-- begin-user-doc --> 测试版本编号 * <!-- end-user-doc -->
	 */
	private Integer testversionId;

	/**
	 * <!-- begin-user-doc --> 任务名称 * <!-- end-user-doc -->
	 */
	private String testtaskTitle;

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	private Integer productId;

	/**
	 * <!-- begin-user-doc --> 项目id * <!-- end-user-doc -->
	 */
	private Integer projectId;

	/**
	 * <!-- begin-user-doc --> 版本名称 * <!-- end-user-doc -->
	 */
	private String buildName;

	/**
	 * <!-- begin-user-doc --> 负责人 * <!-- end-user-doc -->
	 */
	private String testtaskOwner;

	/**
	 * <!-- begin-user-doc --> 优先级 * <!-- end-user-doc -->
	 */
	private Integer priority;

	/**
	 * <!-- begin-user-doc --> 开始日期 * <!-- end-user-doc -->
	 */
	private Date testtaskBegin;

	/**
	 * <!-- begin-user-doc --> 结束日期 * <!-- end-user-doc -->
	 */
	private Date testtaskEnd;

	/**
	 * <!-- begin-user-doc --> 描述 * <!-- end-user-doc -->
	 */
	private String testtaskDesc;

	/**
	 * <!-- begin-user-doc --> 测试总结 * <!-- end-user-doc -->
	 */
	private String testtaskReport;

	/**
	 * <!-- begin-user-doc --> 当前状态 * <!-- end-user-doc -->
	 */
	private String testtaskStatus;

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	private Integer deleted;

	private String searchBuildName;

	private String projectName;

	/**
	 * <!-- begin-user-doc --> 独立编号 * <!-- end-user-doc -->
	 */
	private Integer no;

	/**
	 * <!-- begin-user-doc --> 测试版本编号 * <!-- end-user-doc -->
	 */
	public void setTestversionId(Integer testversionId) {
		this.testversionId = testversionId;
	}

	public Integer getTestversionId() {
		return testversionId;
	}

	/**
	 * <!-- begin-user-doc --> 任务名称 * <!-- end-user-doc -->
	 */
	public void setTesttaskTitle(String testtaskTitle) {
		this.testtaskTitle = testtaskTitle;
	}

	public String getTesttaskTitle() {
		return testtaskTitle;
	}

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getProductId() {
		return productId;
	}

	/**
	 * <!-- begin-user-doc --> 项目id * <!-- end-user-doc -->
	 */
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	/**
	 * <!-- begin-user-doc --> 版本名称 * <!-- end-user-doc -->
	 */
	public void setBuildName(String buildName) {
		this.buildName = buildName;
	}

	public String getBuildName() {
		return buildName;
	}

	/**
	 * <!-- begin-user-doc --> 负责人 * <!-- end-user-doc -->
	 */
	public void setTesttaskOwner(String testtaskOwner) {
		this.testtaskOwner = testtaskOwner;
	}

	public String getTesttaskOwner() {
		return testtaskOwner;
	}

	/**
	 * <!-- begin-user-doc --> 优先级 * <!-- end-user-doc -->
	 */
	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Integer getPriority() {
		return priority;
	}

	/**
	 * <!-- begin-user-doc --> 开始日期 * <!-- end-user-doc -->
	 */
	public void setTesttaskBegin(Date testtaskBegin) {
		this.testtaskBegin = testtaskBegin;
	}

	public Date getTesttaskBegin() {
		return testtaskBegin;
	}

	/**
	 * <!-- begin-user-doc --> 结束日期 * <!-- end-user-doc -->
	 */
	public void setTesttaskEnd(Date testtaskEnd) {
		this.testtaskEnd = testtaskEnd;
	}

	public Date getTesttaskEnd() {
		return testtaskEnd;
	}

	/**
	 * <!-- begin-user-doc --> 描述 * <!-- end-user-doc -->
	 */
	public void setTesttaskDesc(String testtaskDesc) {
		this.testtaskDesc = testtaskDesc;
	}

	public String getTesttaskDesc() {
		return testtaskDesc;
	}

	/**
	 * <!-- begin-user-doc --> 测试总结 * <!-- end-user-doc -->
	 */
	public void setTesttaskReport(String testtaskReport) {
		this.testtaskReport = testtaskReport;
	}

	public String getTesttaskReport() {
		return testtaskReport;
	}

	/**
	 * <!-- begin-user-doc --> 当前状态 * <!-- end-user-doc -->
	 */
	public void setTesttaskStatus(String testtaskStatus) {
		this.testtaskStatus = testtaskStatus;
	}

	public String getTesttaskStatus() {
		return testtaskStatus;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Integer getDeleted() {
		return deleted;
	}

	/**
	 * <!-- begin-user-doc --> 独立编号 * <!-- end-user-doc -->
	 */
	public void setNo(Integer no) {
		this.no = no;
	}

	public String getSearchBuildName() {
		return searchBuildName;
	}

	public void setSearchBuildName(String searchBuildName) {
		this.searchBuildName = searchBuildName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getNo() {
		return no;
	}

}
