/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.quality.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 测试结果表 * <!-- end-user-doc -->
 */
public class QualityTestResult implements Serializable{

	/**
	 * <!-- begin-user-doc --> 测试结果 * <!-- end-user-doc -->
	 */
	private Integer testResultId;

	/**
	 * <!-- begin-user-doc --> TESTRESULT_RUN * <!-- end-user-doc -->
	 */
	private Integer testresultRun;

	/**
	 * <!-- begin-user-doc --> 相关用例 * <!-- end-user-doc -->
	 */
	private Integer linkCase;

	/**
	 * <!-- begin-user-doc --> 关联用例版本 * <!-- end-user-doc -->
	 */
	private Integer caseVersion;

	/**
	 * <!-- begin-user-doc --> 测试结果 * <!-- end-user-doc -->
	 */
	private String caseResult;

	/**
	 * <!-- begin-user-doc --> 用例步骤结果 * <!-- end-user-doc -->
	 */
	private String caseStepresults;

	/**
	 * <!-- begin-user-doc --> 最后执行人 * <!-- end-user-doc -->
	 */
	private String testResultLastRunner;

	/**
	 * <!-- begin-user-doc --> 最后执行日期 * <!-- end-user-doc -->
	 */
	private Date testResultDate;

	/**
	 * <!-- begin-user-doc --> 测试结果 * <!-- end-user-doc -->
	 */
	public void setTestResultId(Integer testResultId) {
		this.testResultId = testResultId;
	}

	public Integer getTestResultId() {
		return testResultId;
	}

	/**
	 * <!-- begin-user-doc --> TESTRESULT_RUN * <!-- end-user-doc -->
	 */
	public void setTestresultRun(Integer testresultRun) {
		this.testresultRun = testresultRun;
	}

	public Integer getTestresultRun() {
		return testresultRun;
	}

	/**
	 * <!-- begin-user-doc --> 相关用例 * <!-- end-user-doc -->
	 */
	public void setLinkCase(Integer linkCase) {
		this.linkCase = linkCase;
	}

	public Integer getLinkCase() {
		return linkCase;
	}

	/**
	 * <!-- begin-user-doc --> 关联用例版本 * <!-- end-user-doc -->
	 */
	public void setCaseVersion(Integer caseVersion) {
		this.caseVersion = caseVersion;
	}

	public Integer getCaseVersion() {
		return caseVersion;
	}

	/**
	 * <!-- begin-user-doc --> 测试结果 * <!-- end-user-doc -->
	 */
	public void setCaseResult(String caseResult) {
		this.caseResult = caseResult;
	}

	public String getCaseResult() {
		return caseResult;
	}

	/**
	 * <!-- begin-user-doc --> 用例步骤结果 * <!-- end-user-doc -->
	 */
	public void setCaseStepresults(String caseStepresults) {
		this.caseStepresults = caseStepresults;
	}

	public String getCaseStepresults() {
		return caseStepresults;
	}

	/**
	 * <!-- begin-user-doc --> 最后执行人 * <!-- end-user-doc -->
	 */
	public void setTestResultLastRunner(String testResultLastRunner) {
		this.testResultLastRunner = testResultLastRunner;
	}

	public String getTestResultLastRunner() {
		return testResultLastRunner;
	}

	/**
	 * <!-- begin-user-doc --> 最后执行日期 * <!-- end-user-doc -->
	 */
	public void setTestResultDate(Date testResultDate) {
		this.testResultDate = testResultDate;
	}

	public Date getTestResultDate() {
		return testResultDate;
	}

}
