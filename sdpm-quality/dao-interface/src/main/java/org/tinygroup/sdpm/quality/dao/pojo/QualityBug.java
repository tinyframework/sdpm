/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.quality.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 缺陷表 * <!-- end-user-doc -->
 */
public class QualityBug implements Serializable{

	public static final String STATUS_ACTIVE = "1"; // 激活

	public static final String STATUS_RESOLVED = "2"; // 已解决

	public static final String STATUS_CLOSED = "3"; // 已关闭

	private Integer releaseId;

	/**
	 * <!-- begin-user-doc --> Bug编号 * <!-- end-user-doc -->
	 */
	private Integer bugId;

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	private Integer productId;

	/**
	 * <!-- begin-user-doc --> 模块ID * <!-- end-user-doc -->
	 */
	private Integer moduleId;

	/**
	 * <!-- begin-user-doc --> 项目id * <!-- end-user-doc -->
	 */
	private Integer projectId;

	/**
	 * <!-- begin-user-doc --> 计划ID * <!-- end-user-doc -->
	 */
	private Integer planId;

	/**
	 * <!-- begin-user-doc --> 需求ID * <!-- end-user-doc -->
	 */
	private Integer storyId;

	/**
	 * <!-- begin-user-doc --> 需求版本 * <!-- end-user-doc -->
	 */
	private Integer storyVersion;

	/**
	 * <!-- begin-user-doc --> 任务id * <!-- end-user-doc -->
	 */
	private Integer taskId;

	/**
	 * <!-- begin-user-doc --> 转任务 * <!-- end-user-doc -->
	 */
	private Integer toTaskId;

	/**
	 * <!-- begin-user-doc --> 转需求 * <!-- end-user-doc -->
	 */
	private Integer toStoryId;

	/**
	 * <!-- begin-user-doc --> Bug标题 * <!-- end-user-doc -->
	 */
	private String bugTitle;

	/**
	 * <!-- begin-user-doc --> 关键词 * <!-- end-user-doc -->
	 */
	private String bugKeywords;

	/**
	 * <!-- begin-user-doc --> 严重程度 * <!-- end-user-doc -->
	 */
	private Integer bugSeverity;

	/**
	 * <!-- begin-user-doc --> 优先级 * <!-- end-user-doc -->
	 */
	private Integer priority;

	/**
	 * <!-- begin-user-doc --> Bug类型 * <!-- end-user-doc -->
	 */
	private String bugType;

	/**
	 * <!-- begin-user-doc --> 操作系统 * <!-- end-user-doc -->
	 */
	private String operatingSystem;

	/**
	 * <!-- begin-user-doc --> 浏览器 * <!-- end-user-doc -->
	 */
	private String browser;

	/**
	 * <!-- begin-user-doc --> 硬件平台 * <!-- end-user-doc -->
	 */
	private String hardware;

	/**
	 * <!-- begin-user-doc --> 如何发现 * <!-- end-user-doc -->
	 */
	private String bugFound;

	/**
	 * <!-- begin-user-doc --> 重现步骤 * <!-- end-user-doc -->
	 */
	private String bugSteps;

	/**
	 * <!-- begin-user-doc --> Bug状态 * <!-- end-user-doc -->
	 */
	private String bugStatus;

	/**
	 * <!-- begin-user-doc --> 是否确认 * <!-- end-user-doc -->
	 */
	private Integer bugConfirmed;

	/**
	 * <!-- begin-user-doc --> 激活次数 * <!-- end-user-doc -->
	 */
	private Integer bugActivatedCount;

	/**
	 * <!-- begin-user-doc --> 抄送给 * <!-- end-user-doc -->
	 */
	private String bugMailto;

	/**
	 * <!-- begin-user-doc --> 由谁创建 * <!-- end-user-doc -->
	 */
	private String bugOpenedBy;

	/**
	 * <!-- begin-user-doc --> 创建日期 * <!-- end-user-doc -->
	 */
	private Date bugOpenedDate;

	/**
	 * <!-- begin-user-doc --> 影响版本 * <!-- end-user-doc -->
	 */
	private String bugOpenedBuild;

	/**
	 * <!-- begin-user-doc --> 指派给 * <!-- end-user-doc -->
	 */
	private String bugAssignedTo;

	/**
	 * <!-- begin-user-doc --> 指派日期 * <!-- end-user-doc -->
	 */
	private Date bugAssignedDate;

	/**
	 * <!-- begin-user-doc --> 解决者 * <!-- end-user-doc -->
	 */
	private String bugResolvedBy;

	/**
	 * <!-- begin-user-doc --> 解决方案 * <!-- end-user-doc -->
	 */
	private String bugResolution;

	/**
	 * <!-- begin-user-doc --> 解决版本 * <!-- end-user-doc -->
	 */
	private String bugResolvedBuild;

	/**
	 * <!-- begin-user-doc --> 解决日期 * <!-- end-user-doc -->
	 */
	private Date bugResolvedDate;

	/**
	 * <!-- begin-user-doc --> 由谁关闭 * <!-- end-user-doc -->
	 */
	private String bugClosedBy;

	/**
	 * <!-- begin-user-doc --> 关闭日期 * <!-- end-user-doc -->
	 */
	private Date bugClosedDate;

	/**
	 * <!-- begin-user-doc --> 重复Bug的ID * <!-- end-user-doc -->
	 */
	private Integer bugDuplicateBug;

	/**
	 * <!-- begin-user-doc --> 相关Bug * <!-- end-user-doc -->
	 */
	private String linkBug;

	/**
	 * <!-- begin-user-doc --> 相关用例 * <!-- end-user-doc -->
	 */
	private Integer linkCase;

	/**
	 * <!-- begin-user-doc --> 关联用例版本 * <!-- end-user-doc -->
	 */
	private Integer caseVersion;

	/**
	 * <!-- begin-user-doc --> BUG_RESULT * <!-- end-user-doc -->
	 */
	private Integer bugResult;

	/**
	 * <!-- begin-user-doc --> BUG_REPO * <!-- end-user-doc -->
	 */
	private Integer bugRepo;

	/**
	 * <!-- begin-user-doc --> BUG_ENTRY * <!-- end-user-doc -->
	 */
	private String bugEntry;

	/**
	 * <!-- begin-user-doc --> 来源用例 * <!-- end-user-doc -->
	 */
	private Integer bugFromCase;

	/**
	 * <!-- begin-user-doc --> BUG_LINES * <!-- end-user-doc -->
	 */
	private String bugLines;

	/**
	 * <!-- begin-user-doc --> BUG_V1 * <!-- end-user-doc -->
	 */
	private String bugV1;

	/**
	 * <!-- begin-user-doc --> BUG_V2 * <!-- end-user-doc -->
	 */
	private String bugV2;

	/**
	 * <!-- begin-user-doc --> BUG_REPOTYPE * <!-- end-user-doc -->
	 */
	private String bugRepoType;

	/**
	 * <!-- begin-user-doc --> 测试任务编号 * <!-- end-user-doc -->
	 */
	private Integer testtask;

	/**
	 * <!-- begin-user-doc --> 最后修改者 * <!-- end-user-doc -->
	 */
	private String bugLastEditedBy;

	/**
	 * <!-- begin-user-doc --> 最后修改日期 * <!-- end-user-doc -->
	 */
	private Date bugLastEditedDate;

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	private Integer deleted;

	private String productName;

	private String moduleName;

	private String planName;

	private String resolveBuild;

	private String fromCase;

	private String linkStoryName;

	private String toTaskName;

	private String linkCaseTitle;

	private String linkBugTitle;

	private String projectName;

	private String linkTaskName;

	private String toStoryTitle;

	/**
	 * <!-- begin-user-doc --> 独立编号 * <!-- end-user-doc -->
	 */
	private Integer no;

	/**
	 * <!-- begin-user-doc --> Bug编号 * <!-- end-user-doc -->
	 */
	public void setBugId(Integer bugId) {
		this.bugId = bugId;
	}

	public Integer getBugId() {
		return bugId;
	}

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getProductId() {
		return productId;
	}

	/**
	 * <!-- begin-user-doc --> 模块ID * <!-- end-user-doc -->
	 */
	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	/**
	 * <!-- begin-user-doc --> 项目id * <!-- end-user-doc -->
	 */
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	/**
	 * <!-- begin-user-doc --> 计划ID * <!-- end-user-doc -->
	 */
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public Integer getPlanId() {
		return planId;
	}

	/**
	 * <!-- begin-user-doc --> 需求ID * <!-- end-user-doc -->
	 */
	public void setStoryId(Integer storyId) {
		this.storyId = storyId;
	}

	public Integer getStoryId() {
		return storyId;
	}

	/**
	 * <!-- begin-user-doc --> 需求版本 * <!-- end-user-doc -->
	 */
	public void setStoryVersion(Integer storyVersion) {
		this.storyVersion = storyVersion;
	}

	public Integer getStoryVersion() {
		return storyVersion;
	}

	/**
	 * <!-- begin-user-doc --> 任务id * <!-- end-user-doc -->
	 */
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public Integer getTaskId() {
		return taskId;
	}

	/**
	 * <!-- begin-user-doc --> 转任务 * <!-- end-user-doc -->
	 */
	public void setToTaskId(Integer toTaskId) {
		this.toTaskId = toTaskId;
	}

	public Integer getToTaskId() {
		return toTaskId;
	}

	/**
	 * <!-- begin-user-doc --> 转需求 * <!-- end-user-doc -->
	 */
	public void setToStoryId(Integer toStoryId) {
		this.toStoryId = toStoryId;
	}

	public Integer getToStoryId() {
		return toStoryId;
	}

	/**
	 * <!-- begin-user-doc --> Bug标题 * <!-- end-user-doc -->
	 */
	public void setBugTitle(String bugTitle) {
		this.bugTitle = bugTitle;
	}

	public String getBugTitle() {
		return bugTitle;
	}

	/**
	 * <!-- begin-user-doc --> 关键词 * <!-- end-user-doc -->
	 */
	public void setBugKeywords(String bugKeywords) {
		this.bugKeywords = bugKeywords;
	}

	public String getBugKeywords() {
		return bugKeywords;
	}

	/**
	 * <!-- begin-user-doc --> 严重程度 * <!-- end-user-doc -->
	 */
	public void setBugSeverity(Integer bugSeverity) {
		this.bugSeverity = bugSeverity;
	}

	public Integer getBugSeverity() {
		return bugSeverity;
	}

	/**
	 * <!-- begin-user-doc --> 优先级 * <!-- end-user-doc -->
	 */
	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Integer getPriority() {
		return priority;
	}

	/**
	 * <!-- begin-user-doc --> Bug类型 * <!-- end-user-doc -->
	 */
	public void setBugType(String bugType) {
		this.bugType = bugType;
	}

	public String getBugType() {
		return bugType;
	}

	/**
	 * <!-- begin-user-doc --> 操作系统 * <!-- end-user-doc -->
	 */
	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

	public String getOperatingSystem() {
		return operatingSystem;
	}

	/**
	 * <!-- begin-user-doc --> 浏览器 * <!-- end-user-doc -->
	 */
	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getBrowser() {
		return browser;
	}

	/**
	 * <!-- begin-user-doc --> 硬件平台 * <!-- end-user-doc -->
	 */
	public void setHardware(String hardware) {
		this.hardware = hardware;
	}

	public String getHardware() {
		return hardware;
	}

	/**
	 * <!-- begin-user-doc --> 如何发现 * <!-- end-user-doc -->
	 */
	public void setBugFound(String bugFound) {
		this.bugFound = bugFound;
	}

	public String getBugFound() {
		return bugFound;
	}

	/**
	 * <!-- begin-user-doc --> 重现步骤 * <!-- end-user-doc -->
	 */
	public void setBugSteps(String bugSteps) {
		this.bugSteps = bugSteps;
	}

	public String getBugSteps() {
		return bugSteps;
	}

	/**
	 * <!-- begin-user-doc --> Bug状态 * <!-- end-user-doc -->
	 */
	public void setBugStatus(String bugStatus) {
		this.bugStatus = bugStatus;
	}

	public String getBugStatus() {
		return bugStatus;
	}

	/**
	 * <!-- begin-user-doc --> 是否确认 * <!-- end-user-doc -->
	 */
	public void setBugConfirmed(Integer bugConfirmed) {
		this.bugConfirmed = bugConfirmed;
	}

	public Integer getBugConfirmed() {
		return bugConfirmed;
	}

	/**
	 * <!-- begin-user-doc --> 激活次数 * <!-- end-user-doc -->
	 */
	public void setBugActivatedCount(Integer bugActivatedCount) {
		this.bugActivatedCount = bugActivatedCount;
	}

	public Integer getBugActivatedCount() {
		return bugActivatedCount;
	}

	/**
	 * <!-- begin-user-doc --> 抄送给 * <!-- end-user-doc -->
	 */
	public void setBugMailto(String bugMailto) {
		this.bugMailto = bugMailto;
	}

	public String getBugMailto() {
		return bugMailto;
	}

	/**
	 * <!-- begin-user-doc --> 由谁创建 * <!-- end-user-doc -->
	 */
	public void setBugOpenedBy(String bugOpenedBy) {
		this.bugOpenedBy = bugOpenedBy;
	}

	public String getBugOpenedBy() {
		return bugOpenedBy;
	}

	/**
	 * <!-- begin-user-doc --> 创建日期 * <!-- end-user-doc -->
	 */
	public void setBugOpenedDate(Date bugOpenedDate) {
		this.bugOpenedDate = bugOpenedDate;
	}

	public Date getBugOpenedDate() {
		return bugOpenedDate;
	}

	/**
	 * <!-- begin-user-doc --> 影响版本 * <!-- end-user-doc -->
	 */
	public void setBugOpenedBuild(String bugOpenedBuild) {
		this.bugOpenedBuild = bugOpenedBuild;
	}

	public String getBugOpenedBuild() {
		return bugOpenedBuild;
	}

	/**
	 * <!-- begin-user-doc --> 指派给 * <!-- end-user-doc -->
	 */
	public void setBugAssignedTo(String bugAssignedTo) {
		this.bugAssignedTo = bugAssignedTo;
	}

	public String getBugAssignedTo() {
		return bugAssignedTo;
	}

	/**
	 * <!-- begin-user-doc --> 指派日期 * <!-- end-user-doc -->
	 */
	public void setBugAssignedDate(Date bugAssignedDate) {
		this.bugAssignedDate = bugAssignedDate;
	}

	public Date getBugAssignedDate() {
		return bugAssignedDate;
	}

	/**
	 * <!-- begin-user-doc --> 解决者 * <!-- end-user-doc -->
	 */
	public void setBugResolvedBy(String bugResolvedBy) {
		this.bugResolvedBy = bugResolvedBy;
	}

	public String getBugResolvedBy() {
		return bugResolvedBy;
	}

	/**
	 * <!-- begin-user-doc --> 解决方案 * <!-- end-user-doc -->
	 */
	public void setBugResolution(String bugResolution) {
		this.bugResolution = bugResolution;
	}

	public String getBugResolution() {
		return bugResolution;
	}

	/**
	 * <!-- begin-user-doc --> 解决版本 * <!-- end-user-doc -->
	 */
	public void setBugResolvedBuild(String bugResolvedBuild) {
		this.bugResolvedBuild = bugResolvedBuild;
	}

	public String getBugResolvedBuild() {
		return bugResolvedBuild;
	}

	/**
	 * <!-- begin-user-doc --> 解决日期 * <!-- end-user-doc -->
	 */
	public void setBugResolvedDate(Date bugResolvedDate) {
		this.bugResolvedDate = bugResolvedDate;
	}

	public Date getBugResolvedDate() {
		return bugResolvedDate;
	}

	/**
	 * <!-- begin-user-doc --> 由谁关闭 * <!-- end-user-doc -->
	 */
	public void setBugClosedBy(String bugClosedBy) {
		this.bugClosedBy = bugClosedBy;
	}

	public String getBugClosedBy() {
		return bugClosedBy;
	}

	/**
	 * <!-- begin-user-doc --> 关闭日期 * <!-- end-user-doc -->
	 */
	public void setBugClosedDate(Date bugClosedDate) {
		this.bugClosedDate = bugClosedDate;
	}

	public Date getBugClosedDate() {
		return bugClosedDate;
	}

	/**
	 * <!-- begin-user-doc --> 重复Bug的ID * <!-- end-user-doc -->
	 */
	public void setBugDuplicateBug(Integer bugDuplicateBug) {
		this.bugDuplicateBug = bugDuplicateBug;
	}

	public Integer getBugDuplicateBug() {
		return bugDuplicateBug;
	}

	/**
	 * <!-- begin-user-doc --> 相关Bug * <!-- end-user-doc -->
	 */
	public void setLinkBug(String linkBug) {
		this.linkBug = linkBug;
	}

	public String getLinkBug() {
		return linkBug;
	}

	/**
	 * <!-- begin-user-doc --> 相关用例 * <!-- end-user-doc -->
	 */
	public void setLinkCase(Integer linkCase) {
		this.linkCase = linkCase;
	}

	public Integer getLinkCase() {
		return linkCase;
	}

	/**
	 * <!-- begin-user-doc --> 关联用例版本 * <!-- end-user-doc -->
	 */
	public void setCaseVersion(Integer caseVersion) {
		this.caseVersion = caseVersion;
	}

	public Integer getCaseVersion() {
		return caseVersion;
	}

	/**
	 * <!-- begin-user-doc --> BUG_RESULT * <!-- end-user-doc -->
	 */
	public void setBugResult(Integer bugResult) {
		this.bugResult = bugResult;
	}

	public Integer getBugResult() {
		return bugResult;
	}

	/**
	 * <!-- begin-user-doc --> BUG_REPO * <!-- end-user-doc -->
	 */
	public void setBugRepo(Integer bugRepo) {
		this.bugRepo = bugRepo;
	}

	public Integer getBugRepo() {
		return bugRepo;
	}

	/**
	 * <!-- begin-user-doc --> BUG_ENTRY * <!-- end-user-doc -->
	 */
	public void setBugEntry(String bugEntry) {
		this.bugEntry = bugEntry;
	}

	public String getBugEntry() {
		return bugEntry;
	}

	/**
	 * <!-- begin-user-doc --> 来源用例 * <!-- end-user-doc -->
	 */
	public void setBugFromCase(Integer bugFromCase) {
		this.bugFromCase = bugFromCase;
	}

	public Integer getBugFromCase() {
		return bugFromCase;
	}

	/**
	 * <!-- begin-user-doc --> BUG_LINES * <!-- end-user-doc -->
	 */
	public void setBugLines(String bugLines) {
		this.bugLines = bugLines;
	}

	public String getBugLines() {
		return bugLines;
	}

	/**
	 * <!-- begin-user-doc --> BUG_V1 * <!-- end-user-doc -->
	 */
	public void setBugV1(String bugV1) {
		this.bugV1 = bugV1;
	}

	public String getBugV1() {
		return bugV1;
	}

	/**
	 * <!-- begin-user-doc --> BUG_V2 * <!-- end-user-doc -->
	 */
	public void setBugV2(String bugV2) {
		this.bugV2 = bugV2;
	}

	public String getBugV2() {
		return bugV2;
	}

	/**
	 * <!-- begin-user-doc --> BUG_REPOTYPE * <!-- end-user-doc -->
	 */
	public void setBugRepoType(String bugRepoType) {
		this.bugRepoType = bugRepoType;
	}

	public String getBugRepoType() {
		return bugRepoType;
	}

	/**
	 * <!-- begin-user-doc --> 测试任务编号 * <!-- end-user-doc -->
	 */
	public void setTesttask(Integer testtask) {
		this.testtask = testtask;
	}

	public Integer getTesttask() {
		return testtask;
	}

	/**
	 * <!-- begin-user-doc --> 最后修改者 * <!-- end-user-doc -->
	 */
	public void setBugLastEditedBy(String bugLastEditedBy) {
		this.bugLastEditedBy = bugLastEditedBy;
	}

	public String getBugLastEditedBy() {
		return bugLastEditedBy;
	}

	/**
	 * <!-- begin-user-doc --> 最后修改日期 * <!-- end-user-doc -->
	 */
	public void setBugLastEditedDate(Date bugLastEditedDate) {
		this.bugLastEditedDate = bugLastEditedDate;
	}

	public Date getBugLastEditedDate() {
		return bugLastEditedDate;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Integer getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(Integer releaseId) {
		this.releaseId = releaseId;
	}

	public Integer getDeleted() {
		return deleted;
	}

	/**
	 * <!-- begin-user-doc --> 独立编号 * <!-- end-user-doc -->
	 */
	public void setNo(Integer no) {
		this.no = no;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getResolveBuild() {
		return resolveBuild;
	}

	public void setResolveBuild(String resolveBuild) {
		this.resolveBuild = resolveBuild;
	}

	public String getFromCase() {
		return fromCase;
	}

	public void setFromCase(String fromCase) {
		this.fromCase = fromCase;
	}

	public String getLinkCaseTitle() {
		return linkCaseTitle;
	}

	public void setLinkCaseTitle(String linkCaseTitle) {
		this.linkCaseTitle = linkCaseTitle;
	}

	public String getLinkBugTitle() {
		return linkBugTitle;
	}

	public void setLinkBugTitle(String linkBugTitle) {
		this.linkBugTitle = linkBugTitle;
	}

	public String getLinkTaskName() {
		return linkTaskName;
	}

	public void setLinkTaskName(String linkTaskName) {
		this.linkTaskName = linkTaskName;
	}

	public String getToStoryTitle() {
		return toStoryTitle;
	}

	public void setToStoryTitle(String toStoryTitle) {
		this.toStoryTitle = toStoryTitle;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getLinkStoryName() {
		return linkStoryName;
	}

	public void setLinkStoryName(String linkStoryName) {
		this.linkStoryName = linkStoryName;
	}

	public String getToTaskName() {
		return toTaskName;
	}

	public void setToTaskName(String toTaskName) {
		this.toTaskName = toTaskName;
	}

	public Integer getNo() {
		return no;
	}

}
