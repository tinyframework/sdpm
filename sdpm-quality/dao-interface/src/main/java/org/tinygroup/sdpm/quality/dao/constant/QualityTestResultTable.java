/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.quality.dao.constant;

import org.tinygroup.sdpm.quality.dao.pojo.QualityTestResult;
import org.tinygroup.tinysqldsl.base.Column;
import org.tinygroup.tinysqldsl.base.Table;

/**
 * <!-- begin-user-doc --> 测试结果表 * <!-- end-user-doc -->
 */
public class QualityTestResultTable extends Table {

	public static final QualityTestResultTable QUALITY_TEST_RESULTTABLE = new QualityTestResultTable();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public static final QualityTestResultTable QUALITY_TEST_RESULT_TABLE = new QualityTestResultTable();

	/**
	 * <!-- begin-user-doc --> 测试结果 * <!-- end-user-doc -->
	 */
	public final Column TEST_RESULT_ID = new Column(this, "test_result_id");
	/**
	 * <!-- begin-user-doc --> TESTRESULT_RUN * <!-- end-user-doc -->
	 */
	public final Column TESTRESULT_RUN = new Column(this, "testresult_run");
	/**
	 * <!-- begin-user-doc --> 相关用例 * <!-- end-user-doc -->
	 */
	public final Column LINK_CASE = new Column(this, "link_case");
	/**
	 * <!-- begin-user-doc --> 关联用例版本 * <!-- end-user-doc -->
	 */
	public final Column CASE_VERSION = new Column(this, "case_version");
	/**
	 * <!-- begin-user-doc --> 测试结果 * <!-- end-user-doc -->
	 */
	public final Column CASE_RESULT = new Column(this, "case_result");
	/**
	 * <!-- begin-user-doc --> 用例步骤结果 * <!-- end-user-doc -->
	 */
	public final Column CASE_STEPRESULTS = new Column(this, "case_stepresults");
	/**
	 * <!-- begin-user-doc --> 最后执行人 * <!-- end-user-doc -->
	 */
	public final Column TEST_RESULT_LAST_RUNNER = new Column(this,
			"test_result_last_runner");
	/**
	 * <!-- begin-user-doc --> 最后执行日期 * <!-- end-user-doc -->
	 */
	public final Column TEST_RESULT_DATE = new Column(this, "test_result_date");

	public QualityTestResultTable() {
		super("quality_test_result");
	}

	public boolean isAutoGeneratedKeys() {
		return true;
	}

	public Class getPojoType() {
		return QualityTestResult.class;
	}

}
