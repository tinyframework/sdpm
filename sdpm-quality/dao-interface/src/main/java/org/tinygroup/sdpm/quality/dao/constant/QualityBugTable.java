/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.quality.dao.constant;

import org.tinygroup.sdpm.quality.dao.pojo.QualityBug;
import org.tinygroup.tinysqldsl.base.Column;
import org.tinygroup.tinysqldsl.base.Table;

/**
 * <!-- begin-user-doc --> 缺陷表 * <!-- end-user-doc -->
 */
public class QualityBugTable extends Table {

	public static final QualityBugTable QUALITY_BUGTABLE = new QualityBugTable();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public static final QualityBugTable QUALITY_BUG_TABLE = new QualityBugTable();

	/**
	 * <!-- begin-user-doc --> Bug编号 * <!-- end-user-doc -->
	 */
	public final Column BUG_ID = new Column(this, "bug_id");
	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	public final Column PRODUCT_ID = new Column(this, "product_id");
	/**
	 * <!-- begin-user-doc --> 模块ID * <!-- end-user-doc -->
	 */
	public final Column MODULE_ID = new Column(this, "module_id");
	/**
	 * <!-- begin-user-doc --> 项目id * <!-- end-user-doc -->
	 */
	public final Column PROJECT_ID = new Column(this, "project_id");
	/**
	 * <!-- begin-user-doc --> 计划ID * <!-- end-user-doc -->
	 */
	public final Column PLAN_ID = new Column(this, "plan_id");
	/**
	 * <!-- begin-user-doc --> 需求ID * <!-- end-user-doc -->
	 */
	public final Column STORY_ID = new Column(this, "story_id");
	/**
	 * <!-- begin-user-doc --> 需求版本 * <!-- end-user-doc -->
	 */
	public final Column STORY_VERSION = new Column(this, "story_version");
	/**
	 * <!-- begin-user-doc --> 任务id * <!-- end-user-doc -->
	 */
	public final Column TASK_ID = new Column(this, "task_id");
	/**
	 * <!-- begin-user-doc --> 转任务 * <!-- end-user-doc -->
	 */
	public final Column TO_TASK_ID = new Column(this, "to_task_id");
	/**
	 * <!-- begin-user-doc --> 转需求 * <!-- end-user-doc -->
	 */
	public final Column TO_STORY_ID = new Column(this, "to_story_id");
	/**
	 * <!-- begin-user-doc --> Bug标题 * <!-- end-user-doc -->
	 */
	public final Column BUG_TITLE = new Column(this, "bug_title");
	/**
	 * <!-- begin-user-doc --> 关键词 * <!-- end-user-doc -->
	 */
	public final Column BUG_KEYWORDS = new Column(this, "bug_keywords");
	/**
	 * <!-- begin-user-doc --> 严重程度 * <!-- end-user-doc -->
	 */
	public final Column BUG_SEVERITY = new Column(this, "bug_severity");
	/**
	 * <!-- begin-user-doc --> 优先级 * <!-- end-user-doc -->
	 */
	public final Column PRIORITY = new Column(this, "priority");
	/**
	 * <!-- begin-user-doc --> Bug类型 * <!-- end-user-doc -->
	 */
	public final Column BUG_TYPE = new Column(this, "bug_type");
	/**
	 * <!-- begin-user-doc --> 操作系统 * <!-- end-user-doc -->
	 */
	public final Column OPERATING_SYSTEM = new Column(this, "operating_system");
	/**
	 * <!-- begin-user-doc --> 浏览器 * <!-- end-user-doc -->
	 */
	public final Column BROWSER = new Column(this, "browser");
	/**
	 * <!-- begin-user-doc --> 硬件平台 * <!-- end-user-doc -->
	 */
	public final Column HARDWARE = new Column(this, "hardware");
	/**
	 * <!-- begin-user-doc --> 如何发现 * <!-- end-user-doc -->
	 */
	public final Column BUG_FOUND = new Column(this, "bug_found");
	/**
	 * <!-- begin-user-doc --> 重现步骤 * <!-- end-user-doc -->
	 */
	public final Column BUG_STEPS = new Column(this, "bug_steps");
	/**
	 * <!-- begin-user-doc --> Bug状态 * <!-- end-user-doc -->
	 */
	public final Column BUG_STATUS = new Column(this, "bug_status");
	/**
	 * <!-- begin-user-doc --> 是否确认 * <!-- end-user-doc -->
	 */
	public final Column BUG_CONFIRMED = new Column(this, "bug_confirmed");
	/**
	 * <!-- begin-user-doc --> 激活次数 * <!-- end-user-doc -->
	 */
	public final Column BUG_ACTIVATED_COUNT = new Column(this,
			"bug_activated_count");
	/**
	 * <!-- begin-user-doc --> 抄送给 * <!-- end-user-doc -->
	 */
	public final Column BUG_MAILTO = new Column(this, "bug_mailto");
	/**
	 * <!-- begin-user-doc --> 由谁创建 * <!-- end-user-doc -->
	 */
	public final Column BUG_OPENED_BY = new Column(this, "bug_opened_by");
	/**
	 * <!-- begin-user-doc --> 创建日期 * <!-- end-user-doc -->
	 */
	public final Column BUG_OPENED_DATE = new Column(this, "bug_opened_date");
	/**
	 * <!-- begin-user-doc --> 影响版本 * <!-- end-user-doc -->
	 */
	public final Column BUG_OPENED_BUILD = new Column(this, "bug_opened_build");
	/**
	 * <!-- begin-user-doc --> 指派给 * <!-- end-user-doc -->
	 */
	public final Column BUG_ASSIGNED_TO = new Column(this, "bug_assigned_to");
	/**
	 * <!-- begin-user-doc --> 指派日期 * <!-- end-user-doc -->
	 */
	public final Column BUG_ASSIGNED_DATE = new Column(this,
			"bug_assigned_date");
	/**
	 * <!-- begin-user-doc --> 解决者 * <!-- end-user-doc -->
	 */
	public final Column BUG_RESOLVED_BY = new Column(this, "bug_resolved_by");
	/**
	 * <!-- begin-user-doc --> 解决方案 * <!-- end-user-doc -->
	 */
	public final Column BUG_RESOLUTION = new Column(this, "bug_resolution");
	/**
	 * <!-- begin-user-doc --> 解决版本 * <!-- end-user-doc -->
	 */
	public final Column BUG_RESOLVED_BUILD = new Column(this,
			"bug_resolved_build");
	/**
	 * <!-- begin-user-doc --> 解决日期 * <!-- end-user-doc -->
	 */
	public final Column BUG_RESOLVED_DATE = new Column(this,
			"bug_resolved_date");
	/**
	 * <!-- begin-user-doc --> 由谁关闭 * <!-- end-user-doc -->
	 */
	public final Column BUG_CLOSED_BY = new Column(this, "bug_closed_by");
	/**
	 * <!-- begin-user-doc --> 关闭日期 * <!-- end-user-doc -->
	 */
	public final Column BUG_CLOSED_DATE = new Column(this, "bug_closed_date");
	/**
	 * <!-- begin-user-doc --> 重复Bug的ID * <!-- end-user-doc -->
	 */
	public final Column BUG_DUPLICATE_BUG = new Column(this,
			"bug_duplicate_bug");
	/**
	 * <!-- begin-user-doc --> 相关Bug * <!-- end-user-doc -->
	 */
	public final Column LINK_BUG = new Column(this, "link_bug");
	/**
	 * <!-- begin-user-doc --> 相关用例 * <!-- end-user-doc -->
	 */
	public final Column LINK_CASE = new Column(this, "link_case");
	/**
	 * <!-- begin-user-doc --> 关联用例版本 * <!-- end-user-doc -->
	 */
	public final Column CASE_VERSION = new Column(this, "case_version");
	/**
	 * <!-- begin-user-doc --> BUG_RESULT * <!-- end-user-doc -->
	 */
	public final Column BUG_RESULT = new Column(this, "bug_result");
	/**
	 * <!-- begin-user-doc --> BUG_REPO * <!-- end-user-doc -->
	 */
	public final Column BUG_REPO = new Column(this, "bug_repo");
	/**
	 * <!-- begin-user-doc --> BUG_ENTRY * <!-- end-user-doc -->
	 */
	public final Column BUG_ENTRY = new Column(this, "bug_entry");
	/**
	 * <!-- begin-user-doc --> 来源用例 * <!-- end-user-doc -->
	 */
	public final Column BUG_FROM_CASE = new Column(this, "bug_from_case");
	/**
	 * <!-- begin-user-doc --> BUG_LINES * <!-- end-user-doc -->
	 */
	public final Column BUG_LINES = new Column(this, "bug_lines");
	/**
	 * <!-- begin-user-doc --> BUG_V1 * <!-- end-user-doc -->
	 */
	public final Column BUG_V1 = new Column(this, "bug_v1");
	/**
	 * <!-- begin-user-doc --> BUG_V2 * <!-- end-user-doc -->
	 */
	public final Column BUG_V2 = new Column(this, "bug_v2");
	/**
	 * <!-- begin-user-doc --> BUG_REPOTYPE * <!-- end-user-doc -->
	 */
	public final Column BUG_REPO_TYPE = new Column(this, "bug_repo_type");
	/**
	 * <!-- begin-user-doc --> 测试任务编号 * <!-- end-user-doc -->
	 */
	public final Column TESTTASK = new Column(this, "testtask");
	/**
	 * <!-- begin-user-doc --> 最后修改者 * <!-- end-user-doc -->
	 */
	public final Column BUG_LAST_EDITED_BY = new Column(this,
			"bug_last_edited_by");
	/**
	 * <!-- begin-user-doc --> 最后修改日期 * <!-- end-user-doc -->
	 */
	public final Column BUG_LAST_EDITED_DATE = new Column(this,
			"bug_last_edited_date");
	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public final Column DELETED = new Column(this, "deleted");
	/**
	 * <!-- begin-user-doc --> 独立编号 * <!-- end-user-doc -->
	 */
	public final Column NO = new Column(this, "no");

	public QualityBugTable() {
		super("quality_bug");
	}

	public boolean isAutoGeneratedKeys() {
		return true;
	}

	public Class getPojoType() {
		return QualityBug.class;
	}

}
