/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.quality.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 测试计划执行表 * <!-- end-user-doc -->
 */
public class QualityTestRun implements Serializable{

	/**
	 * <!-- begin-user-doc --> 执行编号 * <!-- end-user-doc -->
	 */
	private Integer testRunId;

	/**
	 * <!-- begin-user-doc --> 任务id * <!-- end-user-doc -->
	 */
	private Integer taskId;

	/**
	 * <!-- begin-user-doc --> 用例编号 * <!-- end-user-doc -->
	 */
	private Integer caseId;

	/**
	 * <!-- begin-user-doc --> 关联用例版本 * <!-- end-user-doc -->
	 */
	private Integer caseVersion;

	/**
	 * <!-- begin-user-doc --> 指派给 * <!-- end-user-doc -->
	 */
	private String testRunAssignedTo;

	/**
	 * <!-- begin-user-doc --> 最后执行人 * <!-- end-user-doc -->
	 */
	private String testRunLastRunner;

	/**
	 * <!-- begin-user-doc --> 最后执行日期 * <!-- end-user-doc -->
	 */
	private Date testRunLastRunDate;

	/**
	 * <!-- begin-user-doc --> 最后执行结果 * <!-- end-user-doc -->
	 */
	private String testRunLastRunResult;

	/**
	 * <!-- begin-user-doc --> 测试执行状态 * <!-- end-user-doc -->
	 */
	private String testRunStatus;

	private String caseType;

	private String caseTitle;

	private String priority;

	private Integer storyId;

	private Integer no;

	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public Integer getStoryId() {
		return storyId;
	}

	public void setStoryId(Integer storyId) {
		this.storyId = storyId;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public String getCaseTitle() {
		return caseTitle;
	}

	public void setCaseTitle(String caseTitle) {
		this.caseTitle = caseTitle;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	/**
	 * <!-- begin-user-doc --> 执行编号 * <!-- end-user-doc -->
	 */
	public void setTestRunId(Integer testRunId) {
		this.testRunId = testRunId;
	}

	public Integer getTestRunId() {
		return testRunId;
	}

	/**
	 * <!-- begin-user-doc --> 任务id * <!-- end-user-doc -->
	 */
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public Integer getTaskId() {
		return taskId;
	}

	/**
	 * <!-- begin-user-doc --> 用例编号 * <!-- end-user-doc -->
	 */
	public void setCaseId(Integer caseId) {
		this.caseId = caseId;
	}

	public Integer getCaseId() {
		return caseId;
	}

	/**
	 * <!-- begin-user-doc --> 关联用例版本 * <!-- end-user-doc -->
	 */
	public void setCaseVersion(Integer caseVersion) {
		this.caseVersion = caseVersion;
	}

	public Integer getCaseVersion() {
		return caseVersion;
	}

	/**
	 * <!-- begin-user-doc --> 指派给 * <!-- end-user-doc -->
	 */
	public void setTestRunAssignedTo(String testRunAssignedTo) {
		this.testRunAssignedTo = testRunAssignedTo;
	}

	public String getTestRunAssignedTo() {
		return testRunAssignedTo;
	}

	/**
	 * <!-- begin-user-doc --> 最后执行人 * <!-- end-user-doc -->
	 */
	public void setTestRunLastRunner(String testRunLastRunner) {
		this.testRunLastRunner = testRunLastRunner;
	}

	public String getTestRunLastRunner() {
		return testRunLastRunner;
	}

	/**
	 * <!-- begin-user-doc --> 最后执行日期 * <!-- end-user-doc -->
	 */
	public void setTestRunLastRunDate(Date testRunLastRunDate) {
		this.testRunLastRunDate = testRunLastRunDate;
	}

	public Date getTestRunLastRunDate() {
		return testRunLastRunDate;
	}

	/**
	 * <!-- begin-user-doc --> 最后执行结果 * <!-- end-user-doc -->
	 */
	public void setTestRunLastRunResult(String testRunLastRunResult) {
		this.testRunLastRunResult = testRunLastRunResult;
	}

	public String getTestRunLastRunResult() {
		return testRunLastRunResult;
	}

	/**
	 * <!-- begin-user-doc --> 测试执行状态 * <!-- end-user-doc -->
	 */
	public void setTestRunStatus(String testRunStatus) {
		this.testRunStatus = testRunStatus;
	}

	public String getTestRunStatus() {
		return testRunStatus;
	}

}
