/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.quality.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 测试用例表 * <!-- end-user-doc -->
 */
public class QualityTestCase implements Serializable{

	public static final int DELETE_YES = 1;

	public static final int DELETE_NO = 1;

	public static final String RESULT_NA = "0";

	public static final String RESULT_PASS = "1";

	public static final String RESULT_BLOCK = "3";

	public static final String RESULT_FAULT = "2";

	/**
	 * 创建人名称
	 */
	private String caseOpenedName;

	/**
	 * 执行人名称
	 */
	private String caseLastRunnerName;

	/**
	 * <!-- begin-user-doc --> 用例编号 * <!-- end-user-doc -->
	 */
	private Integer caseId;

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	private Integer productId;

	/**
	 * <!-- begin-user-doc --> 模块ID * <!-- end-user-doc -->
	 */
	private Integer moduleId;

	/**
	 * <!-- begin-user-doc --> CASE_PATH * <!-- end-user-doc -->
	 */
	private Integer casePath;

	/**
	 * <!-- begin-user-doc --> 需求ID * <!-- end-user-doc -->
	 */
	private Integer storyId;

	/**
	 * <!-- begin-user-doc --> 需求版本 * <!-- end-user-doc -->
	 */
	private Integer storyVersion;

	/**
	 * <!-- begin-user-doc --> 用例标题 * <!-- end-user-doc -->
	 */
	private String caseTitle;

	/**
	 * <!-- begin-user-doc --> 前置条件 * <!-- end-user-doc -->
	 */
	private String casePrecondition;

	/**
	 * <!-- begin-user-doc --> 关键词 * <!-- end-user-doc -->
	 */
	private String caseKeywords;

	/**
	 * <!-- begin-user-doc --> 优先级 * <!-- end-user-doc -->
	 */
	private Integer priority;

	/**
	 * <!-- begin-user-doc --> 用例类型 * <!-- end-user-doc -->
	 */
	private String caseType;

	/**
	 * <!-- begin-user-doc --> 适用阶段 * <!-- end-user-doc -->
	 */
	private String caseStage;

	/**
	 * <!-- begin-user-doc --> 执行方式 * <!-- end-user-doc -->
	 */
	private String caseRunway;

	/**
	 * <!-- begin-user-doc --> 由谁编写 * <!-- end-user-doc -->
	 */
	private String caseScriptedBy;

	/**
	 * <!-- begin-user-doc --> 编写日期 * <!-- end-user-doc -->
	 */
	private Date caseScriptedDate;

	/**
	 * <!-- begin-user-doc --> SCRIPTSTATUS * <!-- end-user-doc -->
	 */
	private String scriptStatus;

	/**
	 * <!-- begin-user-doc --> SCRIPTLOCATION * <!-- end-user-doc -->
	 */
	private String scriptLocation;

	/**
	 * <!-- begin-user-doc --> 用例状态 * <!-- end-user-doc -->
	 */
	private String caseStatus;

	/**
	 * <!-- begin-user-doc --> 执行频率 * <!-- end-user-doc -->
	 */
	private String caseFrequency;

	/**
	 * <!-- begin-user-doc --> 排序 * <!-- end-user-doc -->
	 */
	private Integer caseOrder;

	/**
	 * <!-- begin-user-doc --> 由谁创建 * <!-- end-user-doc -->
	 */
	private String caseOpenedBy;

	/**
	 * <!-- begin-user-doc --> 创建日期 * <!-- end-user-doc -->
	 */
	private Date caseOpenedDate;

	/**
	 * <!-- begin-user-doc --> 最后修改者 * <!-- end-user-doc -->
	 */
	private String caseLastEditedBy;

	/**
	 * <!-- begin-user-doc --> 最后修改日期 * <!-- end-user-doc -->
	 */
	private Date caseLastEditedDate;

	/**
	 * <!-- begin-user-doc --> 关联用例版本 * <!-- end-user-doc -->
	 */
	private Integer caseVersion;

	/**
	 * <!-- begin-user-doc --> 相关用例 * <!-- end-user-doc -->
	 */
	private Integer linkCase;

	/**
	 * <!-- begin-user-doc --> 来源Bug * <!-- end-user-doc -->
	 */
	private Integer caseFromBug;

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	private Integer deleted;

	/**
	 * <!-- begin-user-doc --> 最后执行人 * <!-- end-user-doc -->
	 */
	private String caseLastRunner;

	/**
	 * <!-- begin-user-doc --> 最后执行时间 * <!-- end-user-doc -->
	 */
	private Date caseLastRunDate;

	/**
	 * <!-- begin-user-doc --> 用例执行结果 * <!-- end-user-doc -->
	 */
	private String caseLastRunResult;

	private String moduleName;

	private String storyTitle;

	private String caseFromBugTitle;

	private String linkCaseTile;

	/**
	 * <!-- begin-user-doc --> 独立编号 * <!-- end-user-doc -->
	 */
	private Integer no;

	/**
	 * <!-- begin-user-doc --> 用例编号 * <!-- end-user-doc -->
	 */
	public void setCaseId(Integer caseId) {
		this.caseId = caseId;
	}

	public Integer getCaseId() {
		return caseId;
	}

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getProductId() {
		return productId;
	}

	/**
	 * <!-- begin-user-doc --> 模块ID * <!-- end-user-doc -->
	 */
	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	/**
	 * <!-- begin-user-doc --> CASE_PATH * <!-- end-user-doc -->
	 */
	public void setCasePath(Integer casePath) {
		this.casePath = casePath;
	}

	public Integer getCasePath() {
		return casePath;
	}

	/**
	 * <!-- begin-user-doc --> 需求ID * <!-- end-user-doc -->
	 */
	public void setStoryId(Integer storyId) {
		this.storyId = storyId;
	}

	public Integer getStoryId() {
		return storyId;
	}

	/**
	 * <!-- begin-user-doc --> 需求版本 * <!-- end-user-doc -->
	 */
	public void setStoryVersion(Integer storyVersion) {
		this.storyVersion = storyVersion;
	}

	public Integer getStoryVersion() {
		return storyVersion;
	}

	/**
	 * <!-- begin-user-doc --> 用例标题 * <!-- end-user-doc -->
	 */
	public void setCaseTitle(String caseTitle) {
		this.caseTitle = caseTitle;
	}

	public String getCaseTitle() {
		return caseTitle;
	}

	/**
	 * <!-- begin-user-doc --> 前置条件 * <!-- end-user-doc -->
	 */
	public void setCasePrecondition(String casePrecondition) {
		this.casePrecondition = casePrecondition;
	}

	public String getCasePrecondition() {
		return casePrecondition;
	}

	/**
	 * <!-- begin-user-doc --> 关键词 * <!-- end-user-doc -->
	 */
	public void setCaseKeywords(String caseKeywords) {
		this.caseKeywords = caseKeywords;
	}

	public String getCaseKeywords() {
		return caseKeywords;
	}

	/**
	 * <!-- begin-user-doc --> 优先级 * <!-- end-user-doc -->
	 */
	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Integer getPriority() {
		return priority;
	}

	/**
	 * <!-- begin-user-doc --> 用例类型 * <!-- end-user-doc -->
	 */
	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public String getCaseType() {
		return caseType;
	}

	/**
	 * <!-- begin-user-doc --> 适用阶段 * <!-- end-user-doc -->
	 */
	public void setCaseStage(String caseStage) {
		this.caseStage = caseStage;
	}

	public String getCaseStage() {
		return caseStage;
	}

	/**
	 * <!-- begin-user-doc --> 执行方式 * <!-- end-user-doc -->
	 */
	public void setCaseRunway(String caseRunway) {
		this.caseRunway = caseRunway;
	}

	public String getCaseRunway() {
		return caseRunway;
	}

	/**
	 * <!-- begin-user-doc --> 由谁编写 * <!-- end-user-doc -->
	 */
	public void setCaseScriptedBy(String caseScriptedBy) {
		this.caseScriptedBy = caseScriptedBy;
	}

	public String getCaseScriptedBy() {
		return caseScriptedBy;
	}

	/**
	 * <!-- begin-user-doc --> 编写日期 * <!-- end-user-doc -->
	 */
	public void setCaseScriptedDate(Date caseScriptedDate) {
		this.caseScriptedDate = caseScriptedDate;
	}

	public Date getCaseScriptedDate() {
		return caseScriptedDate;
	}

	/**
	 * <!-- begin-user-doc --> SCRIPTSTATUS * <!-- end-user-doc -->
	 */
	public void setScriptStatus(String scriptStatus) {
		this.scriptStatus = scriptStatus;
	}

	public String getScriptStatus() {
		return scriptStatus;
	}

	/**
	 * <!-- begin-user-doc --> SCRIPTLOCATION * <!-- end-user-doc -->
	 */
	public void setScriptLocation(String scriptLocation) {
		this.scriptLocation = scriptLocation;
	}

	public String getScriptLocation() {
		return scriptLocation;
	}

	/**
	 * <!-- begin-user-doc --> 用例状态 * <!-- end-user-doc -->
	 */
	public void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}

	public String getCaseStatus() {
		return caseStatus;
	}

	/**
	 * <!-- begin-user-doc --> 执行频率 * <!-- end-user-doc -->
	 */
	public void setCaseFrequency(String caseFrequency) {
		this.caseFrequency = caseFrequency;
	}

	public String getCaseFrequency() {
		return caseFrequency;
	}

	/**
	 * <!-- begin-user-doc --> 排序 * <!-- end-user-doc -->
	 */
	public void setCaseOrder(Integer caseOrder) {
		this.caseOrder = caseOrder;
	}

	public Integer getCaseOrder() {
		return caseOrder;
	}

	/**
	 * <!-- begin-user-doc --> 由谁创建 * <!-- end-user-doc -->
	 */
	public void setCaseOpenedBy(String caseOpenedBy) {
		this.caseOpenedBy = caseOpenedBy;
	}

	public String getCaseOpenedBy() {
		return caseOpenedBy;
	}

	/**
	 * <!-- begin-user-doc --> 创建日期 * <!-- end-user-doc -->
	 */
	public void setCaseOpenedDate(Date caseOpenedDate) {
		this.caseOpenedDate = caseOpenedDate;
	}

	public Date getCaseOpenedDate() {
		return caseOpenedDate;
	}

	/**
	 * <!-- begin-user-doc --> 最后修改者 * <!-- end-user-doc -->
	 */
	public void setCaseLastEditedBy(String caseLastEditedBy) {
		this.caseLastEditedBy = caseLastEditedBy;
	}

	public String getCaseLastEditedBy() {
		return caseLastEditedBy;
	}

	/**
	 * <!-- begin-user-doc --> 最后修改日期 * <!-- end-user-doc -->
	 */
	public void setCaseLastEditedDate(Date caseLastEditedDate) {
		this.caseLastEditedDate = caseLastEditedDate;
	}

	public Date getCaseLastEditedDate() {
		return caseLastEditedDate;
	}

	/**
	 * <!-- begin-user-doc --> 关联用例版本 * <!-- end-user-doc -->
	 */
	public void setCaseVersion(Integer caseVersion) {
		this.caseVersion = caseVersion;
	}

	public Integer getCaseVersion() {
		return caseVersion;
	}

	/**
	 * <!-- begin-user-doc --> 相关用例 * <!-- end-user-doc -->
	 */
	public void setLinkCase(Integer linkCase) {
		this.linkCase = linkCase;
	}

	public Integer getLinkCase() {
		return linkCase;
	}

	/**
	 * <!-- begin-user-doc --> 来源Bug * <!-- end-user-doc -->
	 */
	public void setCaseFromBug(Integer caseFromBug) {
		this.caseFromBug = caseFromBug;
	}

	public Integer getCaseFromBug() {
		return caseFromBug;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Integer getDeleted() {
		return deleted;
	}

	/**
	 * <!-- begin-user-doc --> 最后执行人 * <!-- end-user-doc -->
	 */
	public void setCaseLastRunner(String caseLastRunner) {
		this.caseLastRunner = caseLastRunner;
	}

	public String getCaseLastRunner() {
		return caseLastRunner;
	}

	/**
	 * <!-- begin-user-doc --> 最后执行时间 * <!-- end-user-doc -->
	 */
	public void setCaseLastRunDate(Date caseLastRunDate) {
		this.caseLastRunDate = caseLastRunDate;
	}

	public Date getCaseLastRunDate() {
		return caseLastRunDate;
	}

	/**
	 * <!-- begin-user-doc --> 用例执行结果 * <!-- end-user-doc -->
	 */
	public void setCaseLastRunResult(String caseLastRunResult) {
		this.caseLastRunResult = caseLastRunResult;
	}

	public String getCaseOpenedName() {
		return caseOpenedName;
	}

	public void setCaseOpenedName(String caseOpenedName) {
		this.caseOpenedName = caseOpenedName;
	}

	public String getCaseLastRunnerName() {
		return caseLastRunnerName;
	}

	public void setCaseLastRunnerName(String caseLastRunnerName) {
		this.caseLastRunnerName = caseLastRunnerName;
	}

	public String getCaseLastRunResult() {
		return caseLastRunResult;
	}

	/**
	 * <!-- begin-user-doc --> 独立编号 * <!-- end-user-doc -->
	 */
	public void setNo(Integer no) {
		this.no = no;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getStoryTitle() {
		return storyTitle;
	}

	public void setStoryTitle(String storyTitle) {
		this.storyTitle = storyTitle;
	}

	public String getCaseFromBugTitle() {
		return caseFromBugTitle;
	}

	public void setCaseFromBugTitle(String caseFromBugTitle) {
		this.caseFromBugTitle = caseFromBugTitle;
	}

	public String getLinkCaseTile() {
		return linkCaseTile;
	}

	public void setLinkCaseTile(String linkCaseTile) {
		this.linkCaseTile = linkCaseTile;
	}

	public Integer getNo() {
		return no;
	}

}
