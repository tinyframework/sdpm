/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.quality.dao.constant;

import org.tinygroup.sdpm.quality.dao.pojo.QualityTestCase;
import org.tinygroup.tinysqldsl.base.Column;
import org.tinygroup.tinysqldsl.base.Table;

/**
 * <!-- begin-user-doc --> 测试用例表 * <!-- end-user-doc -->
 */
public class QualityTestCaseTable extends Table {

	public static final QualityTestCaseTable QUALITY_TEST_CASETABLE = new QualityTestCaseTable();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public static final QualityTestCaseTable QUALITY_TEST_CASE_TABLE = new QualityTestCaseTable();

	/**
	 * <!-- begin-user-doc --> 用例编号 * <!-- end-user-doc -->
	 */
	public final Column CASE_ID = new Column(this, "case_id");
	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	public final Column PRODUCT_ID = new Column(this, "product_id");
	/**
	 * <!-- begin-user-doc --> 模块ID * <!-- end-user-doc -->
	 */
	public final Column MODULE_ID = new Column(this, "module_id");
	/**
	 * <!-- begin-user-doc --> CASE_PATH * <!-- end-user-doc -->
	 */
	public final Column CASE_PATH = new Column(this, "case_path");
	/**
	 * <!-- begin-user-doc --> 需求ID * <!-- end-user-doc -->
	 */
	public final Column STORY_ID = new Column(this, "story_id");
	/**
	 * <!-- begin-user-doc --> 需求版本 * <!-- end-user-doc -->
	 */
	public final Column STORY_VERSION = new Column(this, "story_version");
	/**
	 * <!-- begin-user-doc --> 用例标题 * <!-- end-user-doc -->
	 */
	public final Column CASE_TITLE = new Column(this, "case_title");
	/**
	 * <!-- begin-user-doc --> 前置条件 * <!-- end-user-doc -->
	 */
	public final Column CASE_PRECONDITION = new Column(this,
			"case_precondition");
	/**
	 * <!-- begin-user-doc --> 关键词 * <!-- end-user-doc -->
	 */
	public final Column CASE_KEYWORDS = new Column(this, "case_keywords");
	/**
	 * <!-- begin-user-doc --> 优先级 * <!-- end-user-doc -->
	 */
	public final Column PRIORITY = new Column(this, "priority");
	/**
	 * <!-- begin-user-doc --> 用例类型 * <!-- end-user-doc -->
	 */
	public final Column CASE_TYPE = new Column(this, "case_type");
	/**
	 * <!-- begin-user-doc --> 适用阶段 * <!-- end-user-doc -->
	 */
	public final Column CASE_STAGE = new Column(this, "case_stage");
	/**
	 * <!-- begin-user-doc --> 执行方式 * <!-- end-user-doc -->
	 */
	public final Column CASE_RUNWAY = new Column(this, "case_runway");
	/**
	 * <!-- begin-user-doc --> 由谁编写 * <!-- end-user-doc -->
	 */
	public final Column CASE_SCRIPTED_BY = new Column(this, "case_scripted_by");
	/**
	 * <!-- begin-user-doc --> 编写日期 * <!-- end-user-doc -->
	 */
	public final Column CASE_SCRIPTED_DATE = new Column(this,
			"case_scripted_date");
	/**
	 * <!-- begin-user-doc --> SCRIPTSTATUS * <!-- end-user-doc -->
	 */
	public final Column SCRIPT_STATUS = new Column(this, "script_status");
	/**
	 * <!-- begin-user-doc --> SCRIPTLOCATION * <!-- end-user-doc -->
	 */
	public final Column SCRIPT_LOCATION = new Column(this, "script_location");
	/**
	 * <!-- begin-user-doc --> 用例状态 * <!-- end-user-doc -->
	 */
	public final Column CASE_STATUS = new Column(this, "case_status");
	/**
	 * <!-- begin-user-doc --> 执行频率 * <!-- end-user-doc -->
	 */
	public final Column CASE_FREQUENCY = new Column(this, "case_frequency");
	/**
	 * <!-- begin-user-doc --> 排序 * <!-- end-user-doc -->
	 */
	public final Column CASE_ORDER = new Column(this, "case_order");
	/**
	 * <!-- begin-user-doc --> 由谁创建 * <!-- end-user-doc -->
	 */
	public final Column CASE_OPENED_BY = new Column(this, "case_opened_by");
	/**
	 * <!-- begin-user-doc --> 创建日期 * <!-- end-user-doc -->
	 */
	public final Column CASE_OPENED_DATE = new Column(this, "case_opened_date");
	/**
	 * <!-- begin-user-doc --> 最后修改者 * <!-- end-user-doc -->
	 */
	public final Column CASE_LAST_EDITED_BY = new Column(this,
			"case_last_edited_by");
	/**
	 * <!-- begin-user-doc --> 最后修改日期 * <!-- end-user-doc -->
	 */
	public final Column CASE_LAST_EDITED_DATE = new Column(this,
			"case_last_edited_date");
	/**
	 * <!-- begin-user-doc --> 关联用例版本 * <!-- end-user-doc -->
	 */
	public final Column CASE_VERSION = new Column(this, "case_version");
	/**
	 * <!-- begin-user-doc --> 相关用例 * <!-- end-user-doc -->
	 */
	public final Column LINK_CASE = new Column(this, "link_case");
	/**
	 * <!-- begin-user-doc --> 来源Bug * <!-- end-user-doc -->
	 */
	public final Column CASE_FROM_BUG = new Column(this, "case_from_bug");
	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public final Column DELETED = new Column(this, "deleted");
	/**
	 * <!-- begin-user-doc --> 最后执行人 * <!-- end-user-doc -->
	 */
	public final Column CASE_LAST_RUNNER = new Column(this, "case_last_runner");
	/**
	 * <!-- begin-user-doc --> 最后执行时间 * <!-- end-user-doc -->
	 */
	public final Column CASE_LAST_RUN_DATE = new Column(this,
			"case_last_run_date");
	/**
	 * <!-- begin-user-doc --> 用例执行结果 * <!-- end-user-doc -->
	 */
	public final Column CASE_LAST_RUN_RESULT = new Column(this,
			"case_last_run_result");
	/**
	 * <!-- begin-user-doc --> 独立编号 * <!-- end-user-doc -->
	 */
	public final Column NO = new Column(this, "no");

	public QualityTestCaseTable() {
		super("quality_test_case");
	}

	public boolean isAutoGeneratedKeys() {
		return true;
	}

	public Class getPojoType() {
		return QualityTestCase.class;
	}

}
