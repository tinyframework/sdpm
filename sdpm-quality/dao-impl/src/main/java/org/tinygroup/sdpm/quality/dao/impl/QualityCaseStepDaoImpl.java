/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.quality.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.quality.dao.constant.QualityCaseStepTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.quality.dao.constant.QualityCaseStepTable.QUALITY_CASE_STEPTABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.select;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.quality.dao.pojo.QualityCaseStep;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.quality.dao.QualityCaseStepDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class QualityCaseStepDaoImpl extends TinyDslDaoSupport implements
		QualityCaseStepDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public QualityCaseStep add(QualityCaseStep qualityCaseStep) {
		return getDslTemplate().insertAndReturnKey(qualityCaseStep,
				new InsertGenerateCallback<QualityCaseStep>() {
					public Insert generate(QualityCaseStep t) {
						Insert insert = insertInto(QUALITY_CASE_STEP_TABLE)
								.values(QUALITY_CASE_STEP_TABLE.CASE_STEP_ID
										.value(t.getCaseStepId()),
										QUALITY_CASE_STEP_TABLE.CASE_ID.value(t
												.getCaseId()),
										QUALITY_CASE_STEP_TABLE.CASE_VERSION
												.value(t.getCaseVersion()),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_DESC
												.value(t.getCaseStepDesc()),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_EXPECT
												.value(t.getCaseStepExpect())

								);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(QualityCaseStep qualityCaseStep) {
		if (qualityCaseStep == null || qualityCaseStep.getCaseStepId() == null) {
			return 0;
		}
		return getDslTemplate().update(qualityCaseStep,
				new UpdateGenerateCallback<QualityCaseStep>() {
					public Update generate(QualityCaseStep t) {
						Update update = update(QUALITY_CASE_STEP_TABLE).set(
								QUALITY_CASE_STEP_TABLE.CASE_ID.value(t
										.getCaseId()),
								QUALITY_CASE_STEP_TABLE.CASE_VERSION.value(t
										.getCaseVersion()),
								QUALITY_CASE_STEP_TABLE.CASE_STEP_DESC.value(t
										.getCaseStepDesc()),
								QUALITY_CASE_STEP_TABLE.CASE_STEP_EXPECT
										.value(t.getCaseStepExpect())).where(
								QUALITY_CASE_STEP_TABLE.CASE_STEP_ID.eq(t
										.getCaseStepId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(QUALITY_CASE_STEP_TABLE).where(
								QUALITY_CASE_STEP_TABLE.CASE_STEP_ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(QUALITY_CASE_STEP_TABLE).where(
								QUALITY_CASE_STEP_TABLE.CASE_STEP_ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public QualityCaseStep getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, QualityCaseStep.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(QUALITY_CASE_STEP_TABLE).where(
								QUALITY_CASE_STEP_TABLE.CASE_STEP_ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<QualityCaseStep> query(QualityCaseStep qualityCaseStep,
			final OrderBy... orderArgs) {
		if (qualityCaseStep == null) {
			qualityCaseStep = new QualityCaseStep();
		}
		return getDslTemplate().query(qualityCaseStep,
				new SelectGenerateCallback<QualityCaseStep>() {
					@SuppressWarnings("rawtypes")
					public Select generate(QualityCaseStep t) {
						Select select = selectFrom(QUALITY_CASE_STEP_TABLE)
								.where(and(
										QUALITY_CASE_STEP_TABLE.CASE_STEP_ID
												.eq(t.getCaseStepId()),
										QUALITY_CASE_STEP_TABLE.CASE_ID.eq(t
												.getCaseId()),
										QUALITY_CASE_STEP_TABLE.CASE_VERSION
												.eq(t.getCaseVersion()),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_DESC
												.eq(t.getCaseStepDesc()),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_EXPECT
												.eq(t.getCaseStepExpect())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<QualityCaseStep> queryPager(int start, int limit,
			QualityCaseStep qualityCaseStep, final OrderBy... orderArgs) {
		if (qualityCaseStep == null) {
			qualityCaseStep = new QualityCaseStep();
		}
		return getDslTemplate().queryPager(start, limit, qualityCaseStep,
				false, new SelectGenerateCallback<QualityCaseStep>() {
					public Select generate(QualityCaseStep t) {
						Select select = Select
								.selectFrom(QUALITY_CASE_STEP_TABLE)
								.where(and(
										QUALITY_CASE_STEP_TABLE.CASE_STEP_ID
												.eq(t.getCaseStepId()),
										QUALITY_CASE_STEP_TABLE.CASE_ID.eq(t
												.getCaseId()),
										QUALITY_CASE_STEP_TABLE.CASE_VERSION
												.eq(t.getCaseVersion()),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_DESC
												.eq(t.getCaseStepDesc()),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_EXPECT
												.eq(t.getCaseStepExpect())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<QualityCaseStep> qualityCaseStep) {
		if (CollectionUtil.isEmpty(qualityCaseStep)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, qualityCaseStep,
				new InsertGenerateCallback<QualityCaseStep>() {

					public Insert generate(QualityCaseStep t) {
						return insertInto(QUALITY_CASE_STEP_TABLE).values(
								QUALITY_CASE_STEP_TABLE.CASE_STEP_ID.value(t
										.getCaseStepId()),
								QUALITY_CASE_STEP_TABLE.CASE_ID.value(t
										.getCaseId()),
								QUALITY_CASE_STEP_TABLE.CASE_VERSION.value(t
										.getCaseVersion()),
								QUALITY_CASE_STEP_TABLE.CASE_STEP_DESC.value(t
										.getCaseStepDesc()),
								QUALITY_CASE_STEP_TABLE.CASE_STEP_EXPECT
										.value(t.getCaseStepExpect())

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<QualityCaseStep> qualityCaseSteps) {
		return batchInsert(true, qualityCaseSteps);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<QualityCaseStep> qualityCaseStep) {
		if (CollectionUtil.isEmpty(qualityCaseStep)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(qualityCaseStep,
				new UpdateGenerateCallback<QualityCaseStep>() {
					public Update generate(QualityCaseStep t) {
						return update(QUALITY_CASE_STEP_TABLE).set(
								QUALITY_CASE_STEP_TABLE.CASE_ID.value(t
										.getCaseId()),
								QUALITY_CASE_STEP_TABLE.CASE_VERSION.value(t
										.getCaseVersion()),
								QUALITY_CASE_STEP_TABLE.CASE_STEP_DESC.value(t
										.getCaseStepDesc()),
								QUALITY_CASE_STEP_TABLE.CASE_STEP_EXPECT
										.value(t.getCaseStepExpect())

						).where(QUALITY_CASE_STEP_TABLE.CASE_STEP_ID.eq(t
								.getCaseStepId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<QualityCaseStep> qualityCaseStep) {
		if (CollectionUtil.isEmpty(qualityCaseStep)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(qualityCaseStep,
				new DeleteGenerateCallback<QualityCaseStep>() {
					public Delete generate(QualityCaseStep t) {
						return delete(QUALITY_CASE_STEP_TABLE)
								.where(and(
										QUALITY_CASE_STEP_TABLE.CASE_STEP_ID
												.eq(t.getCaseStepId()),
										QUALITY_CASE_STEP_TABLE.CASE_ID.eq(t
												.getCaseId()),
										QUALITY_CASE_STEP_TABLE.CASE_VERSION
												.eq(t.getCaseVersion()),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_DESC
												.eq(t.getCaseStepDesc()),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_EXPECT
												.eq(t.getCaseStepExpect())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = null;
			if (orderBies[i] != null) {
				tempElement = orderBies[i].getOrderByElement();
			}
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	public Integer getMaxVersion(Integer testCaseId) {
		Select select = select(QUALITY_CASE_STEPTABLE.CASE_VERSION.max()).from(
				QUALITY_CASE_STEPTABLE).where(
				QUALITY_CASE_STEPTABLE.CASE_ID.eq(testCaseId));
		return getDslSession().fetchOneResult(select, Integer.class);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<QualityCaseStep> qualityCaseStep) {
		if (CollectionUtil.isEmpty(qualityCaseStep)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, qualityCaseStep,
				new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(QUALITY_CASE_STEP_TABLE)
								.values(QUALITY_CASE_STEP_TABLE.CASE_ID
										.value(new JdbcNamedParameter("caseId")),
										QUALITY_CASE_STEP_TABLE.CASE_VERSION
												.value(new JdbcNamedParameter(
														"caseVersion")),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_DESC
												.value(new JdbcNamedParameter(
														"caseStepDesc")),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_EXPECT
												.value(new JdbcNamedParameter(
														"caseStepExpect"))

								);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<QualityCaseStep> qualityCaseStep) {
		if (CollectionUtil.isEmpty(qualityCaseStep)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(qualityCaseStep,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(QUALITY_CASE_STEP_TABLE)
								.set(QUALITY_CASE_STEP_TABLE.CASE_ID
										.value(new JdbcNamedParameter("caseId")),
										QUALITY_CASE_STEP_TABLE.CASE_VERSION
												.value(new JdbcNamedParameter(
														"caseVersion")),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_DESC
												.value(new JdbcNamedParameter(
														"caseStepDesc")),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_EXPECT
												.value(new JdbcNamedParameter(
														"caseStepExpect"))

								)
								.where(QUALITY_CASE_STEP_TABLE.CASE_STEP_ID
										.eq(new JdbcNamedParameter("caseStepId")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<QualityCaseStep> qualityCaseStep) {
		if (CollectionUtil.isEmpty(qualityCaseStep)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(qualityCaseStep,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(QUALITY_CASE_STEP_TABLE)
								.where(and(
										QUALITY_CASE_STEP_TABLE.CASE_ID
												.eq(new JdbcNamedParameter(
														"caseId")),
										QUALITY_CASE_STEP_TABLE.CASE_VERSION
												.eq(new JdbcNamedParameter(
														"caseVersion")),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_DESC
												.eq(new JdbcNamedParameter(
														"caseStepDesc")),
										QUALITY_CASE_STEP_TABLE.CASE_STEP_EXPECT
												.eq(new JdbcNamedParameter(
														"caseStepExpect"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<QualityCaseStep> qualityCaseStep) {
		return preparedBatchInsert(true, qualityCaseStep);
	}

}
