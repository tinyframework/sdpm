/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 任务 * <!-- end-user-doc -->
 */
public class ProjectTask implements Serializable{

	public static final String DELETE_YES = "1";

	public static final String DELETE_NO = "0";

	public static final String WAIT = "1";

	public static final String DOING = "2";

	public static final String DONE = "3";

	public static final String PAUSE = "4";

	public static final String CANCEL = "5";

	public static final String CLOSE = "6";

	/**
	 * <!-- begin-user-doc --> 任务id * <!-- end-user-doc -->
	 */
	private Integer taskId;

	/**
	 * <!-- begin-user-doc --> 编号 * <!-- end-user-doc -->
	 */
	private Integer taskNo;

	/**
	 * <!-- begin-user-doc --> 任务所属项目 * <!-- end-user-doc -->
	 */
	private Integer taskProject;

	/**
	 * <!-- begin-user-doc --> 任务相关需求 * <!-- end-user-doc -->
	 */
	private Integer taskStory;

	/**
	 * <!-- begin-user-doc --> 需求版本 * 需求变更后自动增长 <!-- end-user-doc -->
	 */
	private Integer taskStoryVersion;

	/**
	 * <!-- begin-user-doc --> 模块id * <!-- end-user-doc -->
	 */
	private Integer taskModule;

	/**
	 * <!-- begin-user-doc --> 来源bug * <!-- end-user-doc -->
	 */
	private Integer taskFromBug;

	/**
	 * <!-- begin-user-doc --> 任务名称 * <!-- end-user-doc -->
	 */
	private String taskName;

	/**
	 * <!-- begin-user-doc --> 任务类型 * <!-- end-user-doc -->
	 */
	private String taskType;

	/**
	 * <!-- begin-user-doc --> 任务优先级 * 1，2，4，5 递增 <!-- end-user-doc -->
	 */
	private String taskPri;

	/**
	 * <!-- begin-user-doc --> 最初预计 * <!-- end-user-doc -->
	 */
	private Float taskEstimate;

	/**
	 * <!-- begin-user-doc --> 总消耗 * <!-- end-user-doc -->
	 */
	private Float taskConsumed;

	/**
	 * <!-- begin-user-doc --> 预计剩余 * <!-- end-user-doc -->
	 */
	private Float taskLeft;

	/**
	 * <!-- begin-user-doc --> 截止日期 * <!-- end-user-doc -->
	 */
	private Date taskDeadLine;

	/**
	 * <!-- begin-user-doc --> 任务状态 * 0-等待，1-未开始，2-进行中，3-已完成，4-已暂停，5-已取消,6-已关闭
	 * <!-- end-user-doc -->
	 */
	private String taskStatus;

	/**
	 * <!-- begin-user-doc --> 抄送给 * <!-- end-user-doc -->
	 */
	private String taskMailto;

	/**
	 * <!-- begin-user-doc --> 任务描述 * <!-- end-user-doc -->
	 */
	private String taskDesc;

	/**
	 * <!-- begin-user-doc --> 由谁创建 * <!-- end-user-doc -->
	 */
	private String taskOpenBy;

	/**
	 * <!-- begin-user-doc --> 创建日期 * <!-- end-user-doc -->
	 */
	private Date taskOpenedDate;

	/**
	 * <!-- begin-user-doc --> 指派给 * <!-- end-user-doc -->
	 */
	private String taskAssignedTo;

	/**
	 * <!-- begin-user-doc --> 指派日期 * <!-- end-user-doc -->
	 */
	private Date taskAssignedDate;

	/**
	 * <!-- begin-user-doc --> 预计开始 * <!-- end-user-doc -->
	 */
	private Date taskEstStared;

	/**
	 * <!-- begin-user-doc --> 实际开始 * <!-- end-user-doc -->
	 */
	private Date taskRealStarted;

	/**
	 * <!-- begin-user-doc --> 由谁完成 * <!-- end-user-doc -->
	 */
	private String taskFinishedBy;

	/**
	 * <!-- begin-user-doc --> 完成时间 * <!-- end-user-doc -->
	 */
	private Date taskFinishedDate;

	/**
	 * <!-- begin-user-doc --> 由谁取消 * <!-- end-user-doc -->
	 */
	private String taskCanceledBy;

	/**
	 * <!-- begin-user-doc --> 取消时间 * <!-- end-user-doc -->
	 */
	private Date taskCanceledDate;

	/**
	 * <!-- begin-user-doc --> 由谁关闭 * <!-- end-user-doc -->
	 */
	private String taskClosedBy;

	/**
	 * <!-- begin-user-doc --> 关闭时间 * <!-- end-user-doc -->
	 */
	private Date taskCloseDate;

	/**
	 * <!-- begin-user-doc --> 关闭原因 * <!-- end-user-doc -->
	 */
	private String taskClosedReason;

	/**
	 * <!-- begin-user-doc --> 最后修改 * <!-- end-user-doc -->
	 */
	private String taskLastEditedBy;

	/**
	 * <!-- begin-user-doc --> 最后修改日期 * <!-- end-user-doc -->
	 */
	private Date taskLastEditedDate;

	/**
	 * <!-- begin-user-doc --> 已删除 * 0-未删除，1-已删除 <!-- end-user-doc -->
	 */
	private String taskDeleted;

	public ProjectTask() {
		this.taskDeleted = DELETE_NO;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * <!-- begin-user-doc --> Bug关联任务 * <!-- end-user-doc -->
	 */
	private Integer taskRelationBug;

	/**
	 * 用于显示关联项目名称
	 */
	private String projectName;

	/**
	 * 关联需求及需求描述表
	 */
	private String storySpec;

	/**
	 * <!-- begin-user-doc --> 任务id * <!-- end-user-doc -->
	 */
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public Integer getTaskId() {
		return taskId;
	}

	/**
	 * <!-- begin-user-doc --> 编号 * <!-- end-user-doc -->
	 */
	public void setTaskNo(Integer taskNo) {
		this.taskNo = taskNo;
	}

	public Integer getTaskNo() {
		return taskNo;
	}

	/**
	 * <!-- begin-user-doc --> 任务所属项目 * <!-- end-user-doc -->
	 */
	public void setTaskProject(Integer taskProject) {
		this.taskProject = taskProject;
	}

	public Integer getTaskProject() {
		return taskProject;
	}

	/**
	 * <!-- begin-user-doc --> 任务相关需求 * <!-- end-user-doc -->
	 */
	public void setTaskStory(Integer taskStory) {
		this.taskStory = taskStory;
	}

	public Integer getTaskStory() {
		return taskStory;
	}

	/**
	 * <!-- begin-user-doc --> 需求版本 * 需求变更后自动增长 <!-- end-user-doc -->
	 */
	public void setTaskStoryVersion(Integer taskStoryVersion) {
		this.taskStoryVersion = taskStoryVersion;
	}

	public Integer getTaskStoryVersion() {
		return taskStoryVersion;
	}

	/**
	 * <!-- begin-user-doc --> 模块id * <!-- end-user-doc -->
	 */
	public void setTaskModule(Integer taskModule) {
		this.taskModule = taskModule;
	}

	public String getStorySpec() {
		return storySpec;
	}

	public void setStorySpec(String storySpec) {
		this.storySpec = storySpec;
	}

	public Integer getTaskModule() {
		return taskModule;
	}

	/**
	 * <!-- begin-user-doc --> 来源bug * <!-- end-user-doc -->
	 */
	public void setTaskFromBug(Integer taskFromBug) {
		this.taskFromBug = taskFromBug;
	}

	public Integer getTaskFromBug() {
		return taskFromBug;
	}

	/**
	 * <!-- begin-user-doc --> 任务名称 * <!-- end-user-doc -->
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskName() {
		return taskName;
	}

	/**
	 * <!-- begin-user-doc --> 任务类型 * <!-- end-user-doc -->
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getTaskType() {
		return taskType;
	}

	/**
	 * <!-- begin-user-doc --> 任务优先级 * 1，2，4，5 递增 <!-- end-user-doc -->
	 */
	public void setTaskPri(String taskPri) {
		this.taskPri = taskPri;
	}

	public String getTaskPri() {
		return taskPri;
	}

	/**
	 * <!-- begin-user-doc --> 最初预计 * <!-- end-user-doc -->
	 */
	public void setTaskEstimate(Float taskEstimate) {
		this.taskEstimate = taskEstimate;
	}

	public Float getTaskEstimate() {
		return taskEstimate;
	}

	/**
	 * <!-- begin-user-doc --> 总消耗 * <!-- end-user-doc -->
	 */
	public void setTaskConsumed(Float taskConsumed) {
		this.taskConsumed = taskConsumed;
	}

	public Float getTaskConsumed() {
		return taskConsumed;
	}

	/**
	 * <!-- begin-user-doc --> 预计剩余 * <!-- end-user-doc -->
	 */
	public void setTaskLeft(Float taskLeft) {
		this.taskLeft = taskLeft;
	}

	public Float getTaskLeft() {
		return taskLeft;
	}

	/**
	 * <!-- begin-user-doc --> 截止日期 * <!-- end-user-doc -->
	 */
	public void setTaskDeadLine(Date taskDeadLine) {
		this.taskDeadLine = taskDeadLine;
	}

	public Date getTaskDeadLine() {
		return taskDeadLine;
	}

	/**
	 * <!-- begin-user-doc --> 任务状态 * 0-等待，1-未开始，2-进行中，3-已完成，4-已暂停，5-已取消,6-已关闭
	 * <!-- end-user-doc -->
	 */
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	/**
	 * <!-- begin-user-doc --> 抄送给 * <!-- end-user-doc -->
	 */
	public void setTaskMailto(String taskMailto) {
		this.taskMailto = taskMailto;
	}

	public String getTaskMailto() {
		return taskMailto;
	}

	/**
	 * <!-- begin-user-doc --> 任务描述 * <!-- end-user-doc -->
	 */
	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}

	public String getTaskDesc() {
		return taskDesc;
	}

	/**
	 * <!-- begin-user-doc --> 由谁创建 * <!-- end-user-doc -->
	 */
	public void setTaskOpenBy(String taskOpenBy) {
		this.taskOpenBy = taskOpenBy;
	}

	public String getTaskOpenBy() {
		return taskOpenBy;
	}

	/**
	 * <!-- begin-user-doc --> 创建日期 * <!-- end-user-doc -->
	 */
	public void setTaskOpenedDate(Date taskOpenedDate) {
		this.taskOpenedDate = taskOpenedDate;
	}

	public Date getTaskOpenedDate() {
		return taskOpenedDate;
	}

	/**
	 * <!-- begin-user-doc --> 指派给 * <!-- end-user-doc -->
	 */
	public void setTaskAssignedTo(String taskAssignedTo) {
		this.taskAssignedTo = taskAssignedTo;
	}

	public String getTaskAssignedTo() {
		return taskAssignedTo;
	}

	/**
	 * <!-- begin-user-doc --> 指派日期 * <!-- end-user-doc -->
	 */
	public void setTaskAssignedDate(Date taskAssignedDate) {
		this.taskAssignedDate = taskAssignedDate;
	}

	public Date getTaskAssignedDate() {
		return taskAssignedDate;
	}

	/**
	 * <!-- begin-user-doc --> 预计开始 * <!-- end-user-doc -->
	 */
	public void setTaskEstStared(Date taskEstStared) {
		this.taskEstStared = taskEstStared;
	}

	public Date getTaskEstStared() {
		return taskEstStared;
	}

	/**
	 * <!-- begin-user-doc --> 实际开始 * <!-- end-user-doc -->
	 */
	public void setTaskRealStarted(Date taskRealStarted) {
		this.taskRealStarted = taskRealStarted;
	}

	public Date getTaskRealStarted() {
		return taskRealStarted;
	}

	/**
	 * <!-- begin-user-doc --> 由谁完成 * <!-- end-user-doc -->
	 */
	public void setTaskFinishedBy(String taskFinishedBy) {
		this.taskFinishedBy = taskFinishedBy;
	}

	public String getTaskFinishedBy() {
		return taskFinishedBy;
	}

	/**
	 * <!-- begin-user-doc --> 完成时间 * <!-- end-user-doc -->
	 */
	public void setTaskFinishedDate(Date taskFinishedDate) {
		this.taskFinishedDate = taskFinishedDate;
	}

	public Date getTaskFinishedDate() {
		return taskFinishedDate;
	}

	/**
	 * <!-- begin-user-doc --> 由谁取消 * <!-- end-user-doc -->
	 */
	public void setTaskCanceledBy(String taskCanceledBy) {
		this.taskCanceledBy = taskCanceledBy;
	}

	public String getTaskCanceledBy() {
		return taskCanceledBy;
	}

	/**
	 * <!-- begin-user-doc --> 取消时间 * <!-- end-user-doc -->
	 */
	public void setTaskCanceledDate(Date taskCanceledDate) {
		this.taskCanceledDate = taskCanceledDate;
	}

	public Date getTaskCanceledDate() {
		return taskCanceledDate;
	}

	/**
	 * <!-- begin-user-doc --> 由谁关闭 * <!-- end-user-doc -->
	 */
	public void setTaskClosedBy(String taskClosedBy) {
		this.taskClosedBy = taskClosedBy;
	}

	public String getTaskClosedBy() {
		return taskClosedBy;
	}

	/**
	 * <!-- begin-user-doc --> 关闭时间 * <!-- end-user-doc -->
	 */
	public void setTaskCloseDate(Date taskCloseDate) {
		this.taskCloseDate = taskCloseDate;
	}

	public Date getTaskCloseDate() {
		return taskCloseDate;
	}

	/**
	 * <!-- begin-user-doc --> 关闭原因 * <!-- end-user-doc -->
	 */
	public void setTaskClosedReason(String taskClosedReason) {
		this.taskClosedReason = taskClosedReason;
	}

	public String getTaskClosedReason() {
		return taskClosedReason;
	}

	/**
	 * <!-- begin-user-doc --> 最后修改 * <!-- end-user-doc -->
	 */
	public void setTaskLastEditedBy(String taskLastEditedBy) {
		this.taskLastEditedBy = taskLastEditedBy;
	}

	public String getTaskLastEditedBy() {
		return taskLastEditedBy;
	}

	/**
	 * <!-- begin-user-doc --> 最后修改日期 * <!-- end-user-doc -->
	 */
	public void setTaskLastEditedDate(Date taskLastEditedDate) {
		this.taskLastEditedDate = taskLastEditedDate;
	}

	public Date getTaskLastEditedDate() {
		return taskLastEditedDate;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * 0-未删除，1-已删除 <!-- end-user-doc -->
	 */
	public void setTaskDeleted(String taskDeleted) {
		this.taskDeleted = taskDeleted;
	}

	public String getTaskDeleted() {
		return taskDeleted;
	}

	/**
	 * <!-- begin-user-doc --> Bug关联任务 * <!-- end-user-doc -->
	 */
	public void setTaskRelationBug(Integer taskRelationBug) {
		this.taskRelationBug = taskRelationBug;
	}

	public Integer getTaskRelationBug() {
		return taskRelationBug;
	}

}
