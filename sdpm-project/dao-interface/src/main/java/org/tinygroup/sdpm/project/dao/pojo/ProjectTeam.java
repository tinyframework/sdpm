/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 团队 * <!-- end-user-doc -->
 */
public class ProjectTeam implements Serializable{

	/**
	 * <!-- begin-user-doc --> 逻辑ID * <!-- end-user-doc -->
	 */
	private Integer id;

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	private Integer productId;

	/**
	 * <!-- begin-user-doc --> 项目id * <!-- end-user-doc -->
	 */
	private Integer projectId;

	/**
	 * <!-- begin-user-doc --> 用户 * <!-- end-user-doc -->
	 */
	private String teamUserId;

	/**
	 * <!-- begin-user-doc --> 角色 * <!-- end-user-doc -->
	 */
	private String teamRole;

	/**
	 * <!-- begin-user-doc --> 加盟日 * <!-- end-user-doc -->
	 */
	private Date teamJoin;

	/**
	 * <!-- begin-user-doc --> 可用工时 * <!-- end-user-doc -->
	 */
	private Float teamDays;

	/**
	 * <!-- begin-user-doc --> 可用工时 * <!-- end-user-doc -->
	 */
	private Float teamHours;

	/**
	 * 成员角色
	 */
	private String roleName;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * <!-- begin-user-doc --> 逻辑ID * <!-- end-user-doc -->
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getProductId() {
		return productId;
	}

	/**
	 * <!-- begin-user-doc --> 项目id * <!-- end-user-doc -->
	 */
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	/**
	 * <!-- begin-user-doc --> 用户 * <!-- end-user-doc -->
	 */
	public void setTeamUserId(String teamUserId) {
		this.teamUserId = teamUserId;
	}

	public String getTeamUserId() {
		return teamUserId;
	}

	/**
	 * <!-- begin-user-doc --> 角色 * <!-- end-user-doc -->
	 */
	public void setTeamRole(String teamRole) {
		this.teamRole = teamRole;
	}

	public String getTeamRole() {
		return teamRole;
	}

	/**
	 * <!-- begin-user-doc --> 加盟日 * <!-- end-user-doc -->
	 */
	public void setTeamJoin(Date teamJoin) {
		this.teamJoin = teamJoin;
	}

	public Date getTeamJoin() {
		return teamJoin;
	}

	/**
	 * <!-- begin-user-doc --> 可用工时 * <!-- end-user-doc -->
	 */
	public void setTeamDays(Float teamDays) {
		this.teamDays = teamDays;
	}

	public Float getTeamDays() {
		return teamDays;
	}

	/**
	 * <!-- begin-user-doc --> 可用工时 * <!-- end-user-doc -->
	 */
	public void setTeamHours(Float teamHours) {
		this.teamHours = teamHours;
	}

	public Float getTeamHours() {
		return teamHours;
	}

}
