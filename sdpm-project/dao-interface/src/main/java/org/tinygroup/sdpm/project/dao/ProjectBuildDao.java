/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao;

import java.util.List;

import org.tinygroup.jdbctemplatedslsession.daosupport.BaseDao;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.sdpm.product.dao.pojo.ProductStory;
import org.tinygroup.sdpm.project.dao.pojo.ProjectBuild;
import org.tinygroup.tinysqldsl.Pager;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
public interface ProjectBuildDao extends BaseDao<ProjectBuild, Integer> {

	/**
	 * 根据产品id进行软删删除
	 * 
	 * @param productId
	 * @return
	 */
	Integer deleteBuildByProductId(Integer productId);

	Integer softDelete(ProjectBuild build);

	int[] batchUpdateDel(List<ProjectBuild> builds);

	Pager<ProductStory> findBuildStoryList(int start, int limit,
			Integer buildId, OrderBy... orderArgs);

	List<ProjectBuild> getBuildByKeys(String... ids);

	List<ProjectBuild> getBuildByProducts(Integer... ids);

	/**
	 * 根据输入名称和产品查询
	 * 
	 * @param condition
	 * @param productId
	 * @return
	 */
	List<ProjectBuild> buildInCondition(String condition, Integer limit,
			Integer productId, Integer projectId);

}
