/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.pojo;

import java.io.Serializable;

/**
 * <!-- begin-user-doc --> 项目需求 * <!-- end-user-doc -->
 */
public class ProjectStory implements Serializable{

	/**
	 * <!-- begin-user-doc --> 逻辑ID * <!-- end-user-doc -->
	 */
	private Integer id;

	/**
	 * <!-- begin-user-doc --> 项目id * <!-- end-user-doc -->
	 */
	private Integer projectId;

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	private Integer productId;

	/**
	 * <!-- begin-user-doc --> 需求ID * <!-- end-user-doc -->
	 */
	private Integer storyId;

	/**
	 * <!-- begin-user-doc --> 需求版本 * <!-- end-user-doc -->
	 */
	private Integer storyVersion;

	/**
	 * <!-- begin-user-doc --> 逻辑ID * <!-- end-user-doc -->
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc --> 项目id * <!-- end-user-doc -->
	 */
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getProductId() {
		return productId;
	}

	/**
	 * <!-- begin-user-doc --> 需求ID * <!-- end-user-doc -->
	 */
	public void setStoryId(Integer storyId) {
		this.storyId = storyId;
	}

	public Integer getStoryId() {
		return storyId;
	}

	/**
	 * <!-- begin-user-doc --> 需求版本 * <!-- end-user-doc -->
	 */
	public void setStoryVersion(Integer storyVersion) {
		this.storyVersion = storyVersion;
	}

	public Integer getStoryVersion() {
		return storyVersion;
	}

}
