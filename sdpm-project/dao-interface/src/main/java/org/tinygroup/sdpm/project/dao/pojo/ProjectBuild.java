/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 版本 * <!-- end-user-doc -->
 */
public class ProjectBuild implements Serializable{

	public static final String DELETE_YES = "1";

	public static final String DELETE_NO = "0";

	private String productName;

	/**
	 * <!-- begin-user-doc --> 版本id * <!-- end-user-doc -->
	 */
	private Integer buildId;

	/**
	 * <!-- begin-user-doc --> 产品 * <!-- end-user-doc -->
	 */
	private Integer buildProduct;

	/**
	 * <!-- begin-user-doc --> 项目 * <!-- end-user-doc -->
	 */
	private Integer buildProject;

	/**
	 * <!-- begin-user-doc --> 版本名称 * <!-- end-user-doc -->
	 */
	private String buildName;

	/**
	 * <!-- begin-user-doc --> 源码地址 * <!-- end-user-doc -->
	 */
	private String buildScmPath;

	/**
	 * <!-- begin-user-doc --> 下载地址 * <!-- end-user-doc -->
	 */
	private String buildFilePath;

	/**
	 * <!-- begin-user-doc --> 打包日期 * <!-- end-user-doc -->
	 */
	private Date buildDate;

	/**
	 * <!-- begin-user-doc --> 已解决需求 * <!-- end-user-doc -->
	 */
	private String buildStories;

	/**
	 * <!-- begin-user-doc --> 已解决bug * <!-- end-user-doc -->
	 */
	private String buildBugs;

	/**
	 * <!-- begin-user-doc --> 构建者 * <!-- end-user-doc -->
	 */
	private String buildBuilder;

	/**
	 * <!-- begin-user-doc --> 描述 * <!-- end-user-doc -->
	 */
	private String buildDesc;

	/**
	 * <!-- begin-user-doc --> 已删除 * 0-未删除，1-删除 <!-- end-user-doc -->
	 */
	private String buildDeleted;

	public ProjectBuild() {
		super();
		this.buildDeleted = DELETE_NO;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * <!-- begin-user-doc --> 版本id * <!-- end-user-doc -->
	 */
	public void setBuildId(Integer buildId) {
		this.buildId = buildId;
	}

	public Integer getBuildId() {
		return buildId;
	}

	/**
	 * <!-- begin-user-doc --> 产品 * <!-- end-user-doc -->
	 */
	public void setBuildProduct(Integer buildProduct) {
		this.buildProduct = buildProduct;
	}

	public Integer getBuildProduct() {
		return buildProduct;
	}

	/**
	 * <!-- begin-user-doc --> 项目 * <!-- end-user-doc -->
	 */
	public void setBuildProject(Integer buildProject) {
		this.buildProject = buildProject;
	}

	public Integer getBuildProject() {
		return buildProject;
	}

	/**
	 * <!-- begin-user-doc --> 版本名称 * <!-- end-user-doc -->
	 */
	public void setBuildName(String buildName) {
		this.buildName = buildName;
	}

	public String getBuildName() {
		return buildName;
	}

	/**
	 * <!-- begin-user-doc --> 源码地址 * <!-- end-user-doc -->
	 */
	public void setBuildScmPath(String buildScmPath) {
		this.buildScmPath = buildScmPath;
	}

	public String getBuildScmPath() {
		return buildScmPath;
	}

	/**
	 * <!-- begin-user-doc --> 下载地址 * <!-- end-user-doc -->
	 */
	public void setBuildFilePath(String buildFilePath) {
		this.buildFilePath = buildFilePath;
	}

	public String getBuildFilePath() {
		return buildFilePath;
	}

	/**
	 * <!-- begin-user-doc --> 打包日期 * <!-- end-user-doc -->
	 */
	public void setBuildDate(Date buildDate) {
		this.buildDate = buildDate;
	}

	public Date getBuildDate() {
		return buildDate;
	}

	/**
	 * <!-- begin-user-doc --> 已解决需求 * <!-- end-user-doc -->
	 */
	public void setBuildStories(String buildStories) {
		this.buildStories = buildStories;
	}

	public String getBuildStories() {
		return buildStories;
	}

	/**
	 * <!-- begin-user-doc --> 已解决bug * <!-- end-user-doc -->
	 */
	public void setBuildBugs(String buildBugs) {
		this.buildBugs = buildBugs;
	}

	public String getBuildBugs() {
		return buildBugs;
	}

	/**
	 * <!-- begin-user-doc --> 构建者 * <!-- end-user-doc -->
	 */
	public void setBuildBuilder(String buildBuilder) {
		this.buildBuilder = buildBuilder;
	}

	public String getBuildBuilder() {
		return buildBuilder;
	}

	/**
	 * <!-- begin-user-doc --> 描述 * <!-- end-user-doc -->
	 */
	public void setBuildDesc(String buildDesc) {
		this.buildDesc = buildDesc;
	}

	public String getBuildDesc() {
		return buildDesc;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * 0-未删除，1-删除 <!-- end-user-doc -->
	 */
	public void setBuildDeleted(String buildDeleted) {
		this.buildDeleted = buildDeleted;
	}

	public String getBuildDeleted() {
		return buildDeleted;
	}

}
