/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.pojo;

import java.util.Date;

/**
 * <!-- begin-user-doc --> 任务预计 * <!-- end-user-doc -->
 */
public class ProjectTaskestimate {

	/**
	 * <!-- begin-user-doc --> 任务预计id * <!-- end-user-doc -->
	 */
	private Integer taskestimateId;

	/**
	 * <!-- begin-user-doc --> 任务id * <!-- end-user-doc -->
	 */
	private Integer taskId;

	/**
	 * <!-- begin-user-doc --> 任务预计时间 * <!-- end-user-doc -->
	 */
	private Date taskestimateDate;

	/**
	 * <!-- begin-user-doc --> 任务预计剩余 * <!-- end-user-doc -->
	 */
	private Float taskestimateLeft;

	/**
	 * <!-- begin-user-doc --> 任务预计消耗 * <!-- end-user-doc -->
	 */
	private Float taskestimateConsumed;

	/**
	 * <!-- begin-user-doc --> 操作人帐号 * <!-- end-user-doc -->
	 */
	private String taskestimateAccount;

	/**
	 * <!-- begin-user-doc --> 备用字段 * <!-- end-user-doc -->
	 */
	private String taskestimateWork;

	/**
	 * <!-- begin-user-doc --> 任务预计id * <!-- end-user-doc -->
	 */
	public void setTaskestimateId(Integer taskestimateId) {
		this.taskestimateId = taskestimateId;
	}

	public Integer getTaskestimateId() {
		return taskestimateId;
	}

	/**
	 * <!-- begin-user-doc --> 任务id * <!-- end-user-doc -->
	 */
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public Integer getTaskId() {
		return taskId;
	}

	/**
	 * <!-- begin-user-doc --> 任务预计时间 * <!-- end-user-doc -->
	 */
	public void setTaskestimateDate(Date taskestimateDate) {
		this.taskestimateDate = taskestimateDate;
	}

	public Date getTaskestimateDate() {
		return taskestimateDate;
	}

	/**
	 * <!-- begin-user-doc --> 任务预计剩余 * <!-- end-user-doc -->
	 */
	public void setTaskestimateLeft(Float taskestimateLeft) {
		this.taskestimateLeft = taskestimateLeft;
	}

	public Float getTaskestimateLeft() {
		return taskestimateLeft;
	}

	/**
	 * <!-- begin-user-doc --> 任务预计消耗 * <!-- end-user-doc -->
	 */
	public void setTaskestimateConsumed(Float taskestimateConsumed) {
		this.taskestimateConsumed = taskestimateConsumed;
	}

	public Float getTaskestimateConsumed() {
		return taskestimateConsumed;
	}

	/**
	 * <!-- begin-user-doc --> 操作人帐号 * <!-- end-user-doc -->
	 */
	public void setTaskestimateAccount(String taskestimateAccount) {
		this.taskestimateAccount = taskestimateAccount;
	}

	public String getTaskestimateAccount() {
		return taskestimateAccount;
	}

	/**
	 * <!-- begin-user-doc --> 备用字段 * <!-- end-user-doc -->
	 */
	public void setTaskestimateWork(String taskestimateWork) {
		this.taskestimateWork = taskestimateWork;
	}

	public String getTaskestimateWork() {
		return taskestimateWork;
	}

}
