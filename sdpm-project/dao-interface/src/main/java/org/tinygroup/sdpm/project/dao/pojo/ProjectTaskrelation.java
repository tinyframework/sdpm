/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.pojo;

/**
 * <!-- begin-user-doc --> TASKRELATION * <!-- end-user-doc -->
 */
public class ProjectTaskrelation {

	/**
	 * <!-- begin-user-doc --> 逻辑ID * <!-- end-user-doc -->
	 */
	private Integer id;

	/**
	 * <!-- begin-user-doc --> 前置任务 * <!-- end-user-doc -->
	 */
	private Integer perTask;

	/**
	 * <!-- begin-user-doc --> 前置条件 * 0-开始后，1-完成后 <!-- end-user-doc -->
	 */
	private String taskrelationConditon;

	/**
	 * <!-- begin-user-doc --> 后置任务 * <!-- end-user-doc -->
	 */
	private Integer afterTask;

	/**
	 * <!-- begin-user-doc --> 动作 * 0-才能开始，1-才能完成 <!-- end-user-doc -->
	 */
	private String taskrelationAction;

	/**
	 * <!-- begin-user-doc --> 逻辑ID * <!-- end-user-doc -->
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc --> 前置任务 * <!-- end-user-doc -->
	 */
	public void setPerTask(Integer perTask) {
		this.perTask = perTask;
	}

	public Integer getPerTask() {
		return perTask;
	}

	/**
	 * <!-- begin-user-doc --> 前置条件 * 0-开始后，1-完成后 <!-- end-user-doc -->
	 */
	public void setTaskrelationConditon(String taskrelationConditon) {
		this.taskrelationConditon = taskrelationConditon;
	}

	public String getTaskrelationConditon() {
		return taskrelationConditon;
	}

	/**
	 * <!-- begin-user-doc --> 后置任务 * <!-- end-user-doc -->
	 */
	public void setAfterTask(Integer afterTask) {
		this.afterTask = afterTask;
	}

	public Integer getAfterTask() {
		return afterTask;
	}

	/**
	 * <!-- begin-user-doc --> 动作 * 0-才能开始，1-才能完成 <!-- end-user-doc -->
	 */
	public void setTaskrelationAction(String taskrelationAction) {
		this.taskrelationAction = taskrelationAction;
	}

	public String getTaskrelationAction() {
		return taskrelationAction;
	}

}
