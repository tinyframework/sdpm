/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.constant;

import org.tinygroup.sdpm.project.dao.pojo.Project;
import org.tinygroup.tinysqldsl.base.Column;
import org.tinygroup.tinysqldsl.base.Table;

/**
 * <!-- begin-user-doc --> 项目 * <!-- end-user-doc -->
 */
public class ProjectTable extends Table {

	public static final ProjectTable PROJECTTABLE = new ProjectTable();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public static final ProjectTable PROJECT_TABLE = new ProjectTable();

	/**
	 * <!-- begin-user-doc --> 项目id * <!-- end-user-doc -->
	 */
	public final Column PROJECT_ID = new Column(this, "project_id");
	/**
	 * <!-- begin-user-doc --> 是否作为目录 * 0-false,1-true <!-- end-user-doc -->
	 */
	public final Column PROJECT_IS_CAT = new Column(this, "project_is_cat");
	/**
	 * <!-- begin-user-doc --> 目录id * <!-- end-user-doc -->
	 */
	public final Column PROJECT_CAT_ID = new Column(this, "project_cat_id");
	/**
	 * <!-- begin-user-doc --> 项目类型 * 0-长期项目，1-短期项目，2-运维项目 <!-- end-user-doc -->
	 */
	public final Column PROJECT_TYPE = new Column(this, "project_type");
	/**
	 * <!-- begin-user-doc --> 项目名称 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_NAME = new Column(this, "project_name");
	/**
	 * <!-- begin-user-doc --> 项目代号 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_CODE = new Column(this, "project_code");
	/**
	 * <!-- begin-user-doc --> 项目开始日期 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_BEGIN = new Column(this, "project_begin");
	/**
	 * <!-- begin-user-doc --> 项目结束日期 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_END = new Column(this, "project_end");
	/**
	 * <!-- begin-user-doc --> 可用工作日 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_DAYS = new Column(this, "project_days");
	/**
	 * <!-- begin-user-doc --> 项目状态 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_STATUS = new Column(this, "project_status");
	/**
	 * <!-- begin-user-doc --> 项目所处阶段 * 0-未开始，1-进行中，2-已挂起，3-已完成 <!--
	 * end-user-doc -->
	 */
	public final Column PROJECT_STATGE = new Column(this, "project_statge");
	/**
	 * <!-- begin-user-doc --> 优先级 * 1，2，3，4 递增 <!-- end-user-doc -->
	 */
	public final Column PROJECT_PRI = new Column(this, "project_pri");
	/**
	 * <!-- begin-user-doc --> 项目描述 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_DESC = new Column(this, "project_desc");
	/**
	 * <!-- begin-user-doc --> 由谁创建 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_OPENED_BY = new Column(this,
			"project_opened_by");
	/**
	 * <!-- begin-user-doc --> 创建日期 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_OPENED_DATE = new Column(this,
			"project_opened_date");
	/**
	 * <!-- begin-user-doc --> 项目创建版本 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_OPENED_VERSION = new Column(this,
			"project_opened_version");
	/**
	 * <!-- begin-user-doc --> 项目由谁关闭 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_CLOSE_BY = new Column(this, "project_close_by");
	/**
	 * <!-- begin-user-doc --> 项目关闭日期 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_CLOSE_DATE = new Column(this,
			"project_close_date");
	/**
	 * <!-- begin-user-doc --> 项目由谁取消 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_CANCELED_BY = new Column(this,
			"project_canceled_by");
	/**
	 * <!-- begin-user-doc --> 项目取消日期 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_CANCELED_DATE = new Column(this,
			"project_canceled_date");
	/**
	 * <!-- begin-user-doc --> 产品负责人 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_PO = new Column(this, "project_po");
	/**
	 * <!-- begin-user-doc --> 项目负责人 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_PM = new Column(this, "project_pm");
	/**
	 * <!-- begin-user-doc --> 测试负责人 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_QD = new Column(this, "project_qd");
	/**
	 * <!-- begin-user-doc --> 项目发布负责人 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_RD = new Column(this, "project_rd");
	/**
	 * <!-- begin-user-doc --> 团队成员 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_TEAM = new Column(this, "project_team");
	/**
	 * <!-- begin-user-doc --> 访问控制 * 0-open,1-private,2-custom <!--
	 * end-user-doc -->
	 */
	public final Column PROJECT_ACL = new Column(this, "project_acl");
	/**
	 * <!-- begin-user-doc --> 分组白名单 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_WHITE_LIST = new Column(this,
			"project_white_list");
	/**
	 * <!-- begin-user-doc --> 项目排序 * <!-- end-user-doc -->
	 */
	public final Column PROJECT_ORDER = new Column(this, "project_order");
	/**
	 * <!-- begin-user-doc --> 已删除 * 0-未删除，1-删除 <!-- end-user-doc -->
	 */
	public final Column PROJECT_DELETED = new Column(this, "project_deleted");

	public ProjectTable() {
		super("project");
	}

	public boolean isAutoGeneratedKeys() {
		return true;
	}

	public Class getPojoType() {
		return Project.class;
	}

}
