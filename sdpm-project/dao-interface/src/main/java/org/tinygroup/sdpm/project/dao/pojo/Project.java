/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 项目 * <!-- end-user-doc -->
 */
public class Project implements Serializable{

	public static final String DELETE_YES = "1";

	public static final String DELETE_NO = "0";

	public static final String WAIT = "1";

	public static final String DOING = "2";

	public static final String HANGUP = "3";

	public static final String FINISH = "4";

	public static final String ACL_OPEN = "0";

	public static final String ACL_PRIVATE = "1";

	public static final String ACL_CUSTOM = "2";

	/**
	 * 项目下任务完成进度
	 */
	private float percent;

	/**
	 * 该项目燃尽图的字符串
	 */
	private String burnValue;

	/**
	 * 总预计
	 */
	private float estimate;

	/**
	 * 总花费
	 */
	private float consumed;

	/**
	 * 总剩余
	 */
	private float allLeft;

	/**
	 * <!-- begin-user-doc --> 项目id * <!-- end-user-doc -->
	 */
	private Integer projectId;

	/**
	 * <!-- begin-user-doc --> 是否作为目录 * 0-false,1-true <!-- end-user-doc -->
	 */
	private String projectIsCat;

	/**
	 * <!-- begin-user-doc --> 目录id * <!-- end-user-doc -->
	 */
	private Integer projectCatId;

	/**
	 * <!-- begin-user-doc --> 项目类型 * 0-长期项目，1-短期项目，2-运维项目 <!-- end-user-doc -->
	 */
	private String projectType;

	/**
	 * <!-- begin-user-doc --> 项目名称 * <!-- end-user-doc -->
	 */
	private String projectName;

	/**
	 * <!-- begin-user-doc --> 项目代号 * <!-- end-user-doc -->
	 */
	private String projectCode;

	/**
	 * <!-- begin-user-doc --> 项目开始日期 * <!-- end-user-doc -->
	 */
	private Date projectBegin;

	/**
	 * <!-- begin-user-doc --> 项目结束日期 * <!-- end-user-doc -->
	 */
	private Date projectEnd;

	/**
	 * <!-- begin-user-doc --> 可用工作日 * <!-- end-user-doc -->
	 */
	private Integer projectDays;

	/**
	 * <!-- begin-user-doc --> 项目状态 * <!-- end-user-doc -->
	 */
	private String projectStatus;

	/**
	 * <!-- begin-user-doc --> 项目所处阶段 * 0-未开始，1-进行中，2-已挂起，3-已完成 <!--
	 * end-user-doc -->
	 */
	private String projectStatge;

	/**
	 * <!-- begin-user-doc --> 优先级 * 1，2，3，4 递增 <!-- end-user-doc -->
	 */
	private String projectPri;

	/**
	 * <!-- begin-user-doc --> 项目描述 * <!-- end-user-doc -->
	 */
	private String projectDesc;

	/**
	 * <!-- begin-user-doc --> 由谁创建 * <!-- end-user-doc -->
	 */
	private String projectOpenedBy;

	/**
	 * <!-- begin-user-doc --> 创建日期 * <!-- end-user-doc -->
	 */
	private Date projectOpenedDate;

	/**
	 * <!-- begin-user-doc --> 项目创建版本 * <!-- end-user-doc -->
	 */
	private String projectOpenedVersion;

	/**
	 * <!-- begin-user-doc --> 项目由谁关闭 * <!-- end-user-doc -->
	 */
	private String projectCloseBy;

	/**
	 * <!-- begin-user-doc --> 项目关闭日期 * <!-- end-user-doc -->
	 */
	private Date projectCloseDate;

	/**
	 * <!-- begin-user-doc --> 项目由谁取消 * <!-- end-user-doc -->
	 */
	private String projectCanceledBy;

	/**
	 * <!-- begin-user-doc --> 项目取消日期 * <!-- end-user-doc -->
	 */
	private Date projectCanceledDate;

	/**
	 * <!-- begin-user-doc --> 产品负责人 * <!-- end-user-doc -->
	 */
	private String projectPo;

	/**
	 * <!-- begin-user-doc --> 项目负责人 * <!-- end-user-doc -->
	 */
	private String projectPm;

	/**
	 * <!-- begin-user-doc --> 测试负责人 * <!-- end-user-doc -->
	 */
	private String projectQd;

	/**
	 * <!-- begin-user-doc --> 项目发布负责人 * <!-- end-user-doc -->
	 */
	private String projectRd;

	/**
	 * <!-- begin-user-doc --> 团队成员 * <!-- end-user-doc -->
	 */
	private String projectTeam;

	/**
	 * <!-- begin-user-doc --> 访问控制 * 0-open,1-private,2-custom <!--
	 * end-user-doc -->
	 */
	private String projectAcl;

	/**
	 * <!-- begin-user-doc --> 分组白名单 * <!-- end-user-doc -->
	 */
	private String projectWhiteList;

	/**
	 * <!-- begin-user-doc --> 项目排序 * <!-- end-user-doc -->
	 */
	private Integer projectOrder;

	/**
	 * <!-- begin-user-doc --> 已删除 * 0-未删除，1-删除 <!-- end-user-doc -->
	 */
	private String projectDeleted;

	public Project() {
		setProjectDeleted(DELETE_NO);
	}

	public float getAllLeft() {
		return allLeft;
	}

	public void setAllLeft(float allLeft) {
		this.allLeft = allLeft;
	}

	public float getEstimate() {
		return estimate;
	}

	public void setEstimate(float estimate) {
		this.estimate = estimate;
	}

	public float getConsumed() {
		return consumed;
	}

	public void setConsumed(float consumed) {
		this.consumed = consumed;
	}

	public String getBurnValue() {
		return burnValue;
	}

	public void setBurnValue(String burnValue) {
		this.burnValue = burnValue;
	}

	public float getPercent() {
		return percent;
	}

	public void setPercent(float percent) {
		this.percent = percent;
	}

	/**
	 * <!-- begin-user-doc --> 项目id * <!-- end-user-doc -->
	 */
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	/**
	 * <!-- begin-user-doc --> 是否作为目录 * 0-false,1-true <!-- end-user-doc -->
	 */
	public void setProjectIsCat(String projectIsCat) {
		this.projectIsCat = projectIsCat;
	}

	public String getProjectIsCat() {
		return projectIsCat;
	}

	/**
	 * <!-- begin-user-doc --> 目录id * <!-- end-user-doc -->
	 */
	public void setProjectCatId(Integer projectCatId) {
		this.projectCatId = projectCatId;
	}

	public Integer getProjectCatId() {
		return projectCatId;
	}

	/**
	 * <!-- begin-user-doc --> 项目类型 * 0-长期项目，1-短期项目，2-运维项目 <!-- end-user-doc -->
	 */
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getProjectType() {
		return projectType;
	}

	/**
	 * <!-- begin-user-doc --> 项目名称 * <!-- end-user-doc -->
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectName() {
		return projectName;
	}

	/**
	 * <!-- begin-user-doc --> 项目代号 * <!-- end-user-doc -->
	 */
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getProjectCode() {
		return projectCode;
	}

	/**
	 * <!-- begin-user-doc --> 项目开始日期 * <!-- end-user-doc -->
	 */
	public void setProjectBegin(Date projectBegin) {
		this.projectBegin = projectBegin;
	}

	public Date getProjectBegin() {
		return projectBegin;
	}

	/**
	 * <!-- begin-user-doc --> 项目结束日期 * <!-- end-user-doc -->
	 */
	public void setProjectEnd(Date projectEnd) {
		this.projectEnd = projectEnd;
	}

	public Date getProjectEnd() {
		return projectEnd;
	}

	/**
	 * <!-- begin-user-doc --> 可用工作日 * <!-- end-user-doc -->
	 */
	public void setProjectDays(Integer projectDays) {
		this.projectDays = projectDays;
	}

	public Integer getProjectDays() {
		return projectDays;
	}

	/**
	 * <!-- begin-user-doc --> 项目状态 * <!-- end-user-doc -->
	 */
	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}

	public String getProjectStatus() {
		return projectStatus;
	}

	/**
	 * <!-- begin-user-doc --> 项目所处阶段 * 0-未开始，1-进行中，2-已挂起，3-已完成 <!--
	 * end-user-doc -->
	 */
	public void setProjectStatge(String projectStatge) {
		this.projectStatge = projectStatge;
	}

	public String getProjectStatge() {
		return projectStatge;
	}

	/**
	 * <!-- begin-user-doc --> 优先级 * 1，2，3，4 递增 <!-- end-user-doc -->
	 */
	public void setProjectPri(String projectPri) {
		this.projectPri = projectPri;
	}

	public String getProjectPri() {
		return projectPri;
	}

	/**
	 * <!-- begin-user-doc --> 项目描述 * <!-- end-user-doc -->
	 */
	public void setProjectDesc(String projectDesc) {
		this.projectDesc = projectDesc;
	}

	public String getProjectDesc() {
		return projectDesc;
	}

	/**
	 * <!-- begin-user-doc --> 由谁创建 * <!-- end-user-doc -->
	 */
	public void setProjectOpenedBy(String projectOpenedBy) {
		this.projectOpenedBy = projectOpenedBy;
	}

	public String getProjectOpenedBy() {
		return projectOpenedBy;
	}

	/**
	 * <!-- begin-user-doc --> 创建日期 * <!-- end-user-doc -->
	 */
	public void setProjectOpenedDate(Date projectOpenedDate) {
		this.projectOpenedDate = projectOpenedDate;
	}

	public Date getProjectOpenedDate() {
		return projectOpenedDate;
	}

	/**
	 * <!-- begin-user-doc --> 项目创建版本 * <!-- end-user-doc -->
	 */
	public void setProjectOpenedVersion(String projectOpenedVersion) {
		this.projectOpenedVersion = projectOpenedVersion;
	}

	public String getProjectOpenedVersion() {
		return projectOpenedVersion;
	}

	/**
	 * <!-- begin-user-doc --> 项目由谁关闭 * <!-- end-user-doc -->
	 */
	public void setProjectCloseBy(String projectCloseBy) {
		this.projectCloseBy = projectCloseBy;
	}

	public String getProjectCloseBy() {
		return projectCloseBy;
	}

	/**
	 * <!-- begin-user-doc --> 项目关闭日期 * <!-- end-user-doc -->
	 */
	public void setProjectCloseDate(Date projectCloseDate) {
		this.projectCloseDate = projectCloseDate;
	}

	public Date getProjectCloseDate() {
		return projectCloseDate;
	}

	/**
	 * <!-- begin-user-doc --> 项目由谁取消 * <!-- end-user-doc -->
	 */
	public void setProjectCanceledBy(String projectCanceledBy) {
		this.projectCanceledBy = projectCanceledBy;
	}

	public String getProjectCanceledBy() {
		return projectCanceledBy;
	}

	/**
	 * <!-- begin-user-doc --> 项目取消日期 * <!-- end-user-doc -->
	 */
	public void setProjectCanceledDate(Date projectCanceledDate) {
		this.projectCanceledDate = projectCanceledDate;
	}

	public Date getProjectCanceledDate() {
		return projectCanceledDate;
	}

	/**
	 * <!-- begin-user-doc --> 产品负责人 * <!-- end-user-doc -->
	 */
	public void setProjectPo(String projectPo) {
		this.projectPo = projectPo;
	}

	public String getProjectPo() {
		return projectPo;
	}

	/**
	 * <!-- begin-user-doc --> 项目负责人 * <!-- end-user-doc -->
	 */
	public void setProjectPm(String projectPm) {
		this.projectPm = projectPm;
	}

	public String getProjectPm() {
		return projectPm;
	}

	/**
	 * <!-- begin-user-doc --> 测试负责人 * <!-- end-user-doc -->
	 */
	public void setProjectQd(String projectQd) {
		this.projectQd = projectQd;
	}

	public String getProjectQd() {
		return projectQd;
	}

	/**
	 * <!-- begin-user-doc --> 项目发布负责人 * <!-- end-user-doc -->
	 */
	public void setProjectRd(String projectRd) {
		this.projectRd = projectRd;
	}

	public String getProjectRd() {
		return projectRd;
	}

	/**
	 * <!-- begin-user-doc --> 团队成员 * <!-- end-user-doc -->
	 */
	public void setProjectTeam(String projectTeam) {
		this.projectTeam = projectTeam;
	}

	public String getProjectTeam() {
		return projectTeam;
	}

	/**
	 * <!-- begin-user-doc --> 访问控制 * 0-open,1-private,2-custom <!--
	 * end-user-doc -->
	 */
	public void setProjectAcl(String projectAcl) {
		this.projectAcl = projectAcl;
	}

	public String getProjectAcl() {
		return projectAcl;
	}

	/**
	 * <!-- begin-user-doc --> 分组白名单 * <!-- end-user-doc -->
	 */
	public void setProjectWhiteList(String projectWhiteList) {
		this.projectWhiteList = projectWhiteList;
	}

	public String getProjectWhiteList() {
		return projectWhiteList;
	}

	/**
	 * <!-- begin-user-doc --> 项目排序 * <!-- end-user-doc -->
	 */
	public void setProjectOrder(Integer projectOrder) {
		this.projectOrder = projectOrder;
	}

	public Integer getProjectOrder() {
		return projectOrder;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * 0-未删除，1-删除 <!-- end-user-doc -->
	 */
	public void setProjectDeleted(String projectDeleted) {
		this.projectDeleted = projectDeleted;
	}

	public String getProjectDeleted() {
		return projectDeleted;
	}

}
