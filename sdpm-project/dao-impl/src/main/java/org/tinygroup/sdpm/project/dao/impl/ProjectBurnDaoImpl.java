/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.project.dao.constant.ProjectBurnTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.project.dao.constant.ProjectBurnTable.PROJECT_BURNTABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.project.dao.pojo.ProjectBurn;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.project.dao.ProjectBurnDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class ProjectBurnDaoImpl extends TinyDslDaoSupport implements
		ProjectBurnDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ProjectBurn add(ProjectBurn projectBurn) {
		return getDslTemplate().insertAndReturnKey(projectBurn,
				new InsertGenerateCallback<ProjectBurn>() {
					public Insert generate(ProjectBurn t) {
						Insert insert = insertInto(PROJECT_BURN_TABLE).values(
								PROJECT_BURN_TABLE.ID.value(t.getId()),
								PROJECT_BURN_TABLE.PROJECT_ID.value(t
										.getProjectId()),
								PROJECT_BURN_TABLE.BURN_DATE.value(t
										.getBurnDate()),
								PROJECT_BURN_TABLE.BURN_LEFT.value(t
										.getBurnLeft()),
								PROJECT_BURN_TABLE.BURN_CONSUMED.value(t
										.getBurnConsumed())

						);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(ProjectBurn projectBurn) {
		if (projectBurn == null || projectBurn.getId() == null) {
			return 0;
		}
		return getDslTemplate().update(projectBurn,
				new UpdateGenerateCallback<ProjectBurn>() {
					public Update generate(ProjectBurn t) {
						Update update = update(PROJECT_BURN_TABLE).set(
								PROJECT_BURN_TABLE.PROJECT_ID.value(t
										.getProjectId()),
								PROJECT_BURN_TABLE.BURN_DATE.value(t
										.getBurnDate()),
								PROJECT_BURN_TABLE.BURN_LEFT.value(t
										.getBurnLeft()),
								PROJECT_BURN_TABLE.BURN_CONSUMED.value(t
										.getBurnConsumed())).where(
								PROJECT_BURN_TABLE.ID.eq(t.getId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(PROJECT_BURN_TABLE).where(
								PROJECT_BURN_TABLE.ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(PROJECT_BURN_TABLE).where(
								PROJECT_BURN_TABLE.ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ProjectBurn getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, ProjectBurn.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(PROJECT_BURN_TABLE).where(
								PROJECT_BURN_TABLE.ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<ProjectBurn> query(ProjectBurn projectBurn,
			final OrderBy... orderArgs) {
		if (projectBurn == null) {
			projectBurn = new ProjectBurn();
		}
		return getDslTemplate().query(projectBurn,
				new SelectGenerateCallback<ProjectBurn>() {
					@SuppressWarnings("rawtypes")
					public Select generate(ProjectBurn t) {
						Select select = selectFrom(PROJECT_BURN_TABLE).where(
								and(PROJECT_BURN_TABLE.ID.eq(t.getId()),
										PROJECT_BURN_TABLE.PROJECT_ID.eq(t
												.getProjectId()),
										PROJECT_BURN_TABLE.BURN_DATE.eq(t
												.getBurnDate()),
										PROJECT_BURN_TABLE.BURN_LEFT.eq(t
												.getBurnLeft()),
										PROJECT_BURN_TABLE.BURN_CONSUMED.eq(t
												.getBurnConsumed())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<ProjectBurn> queryPager(int start, int limit,
			ProjectBurn projectBurn, final OrderBy... orderArgs) {
		if (projectBurn == null) {
			projectBurn = new ProjectBurn();
		}
		return getDslTemplate().queryPager(start, limit, projectBurn, false,
				new SelectGenerateCallback<ProjectBurn>() {
					public Select generate(ProjectBurn t) {
						Select select = Select.selectFrom(PROJECT_BURN_TABLE)
								.where(and(PROJECT_BURN_TABLE.ID.eq(t.getId()),
										PROJECT_BURN_TABLE.PROJECT_ID.eq(t
												.getProjectId()),
										PROJECT_BURN_TABLE.BURN_DATE.eq(t
												.getBurnDate()),
										PROJECT_BURN_TABLE.BURN_LEFT.eq(t
												.getBurnLeft()),
										PROJECT_BURN_TABLE.BURN_CONSUMED.eq(t
												.getBurnConsumed())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<ProjectBurn> projectBurn) {
		if (CollectionUtil.isEmpty(projectBurn)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, projectBurn,
				new InsertGenerateCallback<ProjectBurn>() {

					public Insert generate(ProjectBurn t) {
						return insertInto(PROJECT_BURN_TABLE).values(
								PROJECT_BURN_TABLE.ID.value(t.getId()),
								PROJECT_BURN_TABLE.PROJECT_ID.value(t
										.getProjectId()),
								PROJECT_BURN_TABLE.BURN_DATE.value(t
										.getBurnDate()),
								PROJECT_BURN_TABLE.BURN_LEFT.value(t
										.getBurnLeft()),
								PROJECT_BURN_TABLE.BURN_CONSUMED.value(t
										.getBurnConsumed())

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<ProjectBurn> projectBurns) {
		return batchInsert(true, projectBurns);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<ProjectBurn> projectBurn) {
		if (CollectionUtil.isEmpty(projectBurn)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(projectBurn,
				new UpdateGenerateCallback<ProjectBurn>() {
					public Update generate(ProjectBurn t) {
						return update(PROJECT_BURN_TABLE).set(
								PROJECT_BURN_TABLE.PROJECT_ID.value(t
										.getProjectId()),
								PROJECT_BURN_TABLE.BURN_DATE.value(t
										.getBurnDate()),
								PROJECT_BURN_TABLE.BURN_LEFT.value(t
										.getBurnLeft()),
								PROJECT_BURN_TABLE.BURN_CONSUMED.value(t
										.getBurnConsumed())

						).where(PROJECT_BURN_TABLE.ID.eq(t.getId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<ProjectBurn> projectBurn) {
		if (CollectionUtil.isEmpty(projectBurn)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(projectBurn,
				new DeleteGenerateCallback<ProjectBurn>() {
					public Delete generate(ProjectBurn t) {
						return delete(PROJECT_BURN_TABLE).where(
								and(PROJECT_BURN_TABLE.ID.eq(t.getId()),
										PROJECT_BURN_TABLE.PROJECT_ID.eq(t
												.getProjectId()),
										PROJECT_BURN_TABLE.BURN_DATE.eq(t
												.getBurnDate()),
										PROJECT_BURN_TABLE.BURN_LEFT.eq(t
												.getBurnLeft()),
										PROJECT_BURN_TABLE.BURN_CONSUMED.eq(t
												.getBurnConsumed())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = orderBies[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<ProjectBurn> projectBurn) {
		if (CollectionUtil.isEmpty(projectBurn)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, projectBurn,
				new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(PROJECT_BURN_TABLE).values(
								PROJECT_BURN_TABLE.PROJECT_ID
										.value(new JdbcNamedParameter(
												"projectId")),
								PROJECT_BURN_TABLE.BURN_DATE
										.value(new JdbcNamedParameter(
												"burnDate")),
								PROJECT_BURN_TABLE.BURN_LEFT
										.value(new JdbcNamedParameter(
												"burnLeft")),
								PROJECT_BURN_TABLE.BURN_CONSUMED
										.value(new JdbcNamedParameter(
												"burnConsumed"))

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<ProjectBurn> projectBurn) {
		if (CollectionUtil.isEmpty(projectBurn)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(projectBurn,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(PROJECT_BURN_TABLE).set(
								PROJECT_BURN_TABLE.PROJECT_ID
										.value(new JdbcNamedParameter(
												"projectId")),
								PROJECT_BURN_TABLE.BURN_DATE
										.value(new JdbcNamedParameter(
												"burnDate")),
								PROJECT_BURN_TABLE.BURN_LEFT
										.value(new JdbcNamedParameter(
												"burnLeft")),
								PROJECT_BURN_TABLE.BURN_CONSUMED
										.value(new JdbcNamedParameter(
												"burnConsumed"))

						).where(PROJECT_BURN_TABLE.ID
								.eq(new JdbcNamedParameter("id")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<ProjectBurn> projectBurn) {
		if (CollectionUtil.isEmpty(projectBurn)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(projectBurn,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(PROJECT_BURN_TABLE)
								.where(and(
										PROJECT_BURN_TABLE.PROJECT_ID
												.eq(new JdbcNamedParameter(
														"projectId")),
										PROJECT_BURN_TABLE.BURN_DATE
												.eq(new JdbcNamedParameter(
														"burnDate")),
										PROJECT_BURN_TABLE.BURN_LEFT
												.eq(new JdbcNamedParameter(
														"burnLeft")),
										PROJECT_BURN_TABLE.BURN_CONSUMED
												.eq(new JdbcNamedParameter(
														"burnConsumed"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<ProjectBurn> projectBurn) {
		return preparedBatchInsert(true, projectBurn);
	}

}
