/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.tinysqldsl.formitem.SubSelect.subSelect;
import static org.tinygroup.sdpm.project.dao.constant.ProjectProductTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.product.dao.constant.ProductTable.PRODUCTTABLE;
import static org.tinygroup.sdpm.project.dao.constant.ProjectProductTable.PROJECT_PRODUCTTABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.project.dao.pojo.ProjectProduct;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.project.dao.ProjectProductDao;

import org.tinygroup.sdpm.project.dao.pojo.Project;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.sdpm.product.dao.pojo.Product;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class ProjectProductDaoImpl extends TinyDslDaoSupport implements
		ProjectProductDao {

	public int deleteByProjectId(Integer projectId) {
		Delete delete = delete(PROJECT_PRODUCTTABLE).where(
				PROJECT_PRODUCTTABLE.PROJECT_ID.eq(projectId));
		return getDslSession().execute(delete);
	}

	public List<Product> findLinkProductByProjectId(Integer projectId) {
		Select subSelect = Select.select(PROJECT_PRODUCTTABLE.PRODUCT_ID)
				.from(PROJECT_PRODUCTTABLE)
				.where(PROJECT_PRODUCTTABLE.PROJECT_ID.eq(projectId));
		Select select = selectFrom(PRODUCTTABLE).where(
				and(PRODUCTTABLE.PRODUCT_ID.inExpression(subSelect(subSelect)),
						PRODUCTTABLE.DELETED.eq(Project.DELETE_NO)));
		return getDslSession().fetchList(select, Product.class);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ProjectProduct add(ProjectProduct projectProduct) {
		return getDslTemplate().insertAndReturnKey(projectProduct,
				new InsertGenerateCallback<ProjectProduct>() {
					public Insert generate(ProjectProduct t) {
						Insert insert = insertInto(PROJECT_PRODUCT_TABLE)
								.values(PROJECT_PRODUCT_TABLE.ID.value(t
										.getId()),
										PROJECT_PRODUCT_TABLE.PROJECT_ID
												.value(t.getProjectId()),
										PROJECT_PRODUCT_TABLE.PRODUCT_ID
												.value(t.getProductId())

								);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(ProjectProduct projectProduct) {
		if (projectProduct == null || projectProduct.getId() == null) {
			return 0;
		}
		return getDslTemplate().update(projectProduct,
				new UpdateGenerateCallback<ProjectProduct>() {
					public Update generate(ProjectProduct t) {
						Update update = update(PROJECT_PRODUCT_TABLE).set(
								PROJECT_PRODUCT_TABLE.PROJECT_ID.value(t
										.getProjectId()),
								PROJECT_PRODUCT_TABLE.PRODUCT_ID.value(t
										.getProductId())).where(
								PROJECT_PRODUCT_TABLE.ID.eq(t.getId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(PROJECT_PRODUCT_TABLE).where(
								PROJECT_PRODUCT_TABLE.ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(PROJECT_PRODUCT_TABLE).where(
								PROJECT_PRODUCT_TABLE.ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ProjectProduct getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, ProjectProduct.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(PROJECT_PRODUCT_TABLE).where(
								PROJECT_PRODUCT_TABLE.ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<ProjectProduct> query(ProjectProduct projectProduct,
			final OrderBy... orderArgs) {
		if (projectProduct == null) {
			projectProduct = new ProjectProduct();
		}
		return getDslTemplate().query(projectProduct,
				new SelectGenerateCallback<ProjectProduct>() {
					@SuppressWarnings("rawtypes")
					public Select generate(ProjectProduct t) {
						Select select = selectFrom(PROJECT_PRODUCT_TABLE)
								.where(and(PROJECT_PRODUCT_TABLE.ID.eq(t
										.getId()),
										PROJECT_PRODUCT_TABLE.PROJECT_ID.eq(t
												.getProjectId()),
										PROJECT_PRODUCT_TABLE.PRODUCT_ID.eq(t
												.getProductId())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<ProjectProduct> queryPager(int start, int limit,
			ProjectProduct projectProduct, final OrderBy... orderArgs) {
		if (projectProduct == null) {
			projectProduct = new ProjectProduct();
		}
		return getDslTemplate().queryPager(start, limit, projectProduct, false,
				new SelectGenerateCallback<ProjectProduct>() {
					public Select generate(ProjectProduct t) {
						Select select = Select
								.selectFrom(PROJECT_PRODUCT_TABLE)
								.where(and(PROJECT_PRODUCT_TABLE.ID.eq(t
										.getId()),
										PROJECT_PRODUCT_TABLE.PROJECT_ID.eq(t
												.getProjectId()),
										PROJECT_PRODUCT_TABLE.PRODUCT_ID.eq(t
												.getProductId())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<ProjectProduct> projectProduct) {
		if (CollectionUtil.isEmpty(projectProduct)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, projectProduct,
				new InsertGenerateCallback<ProjectProduct>() {

					public Insert generate(ProjectProduct t) {
						return insertInto(PROJECT_PRODUCT_TABLE).values(
								PROJECT_PRODUCT_TABLE.ID.value(t.getId()),
								PROJECT_PRODUCT_TABLE.PROJECT_ID.value(t
										.getProjectId()),
								PROJECT_PRODUCT_TABLE.PRODUCT_ID.value(t
										.getProductId())

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<ProjectProduct> projectProducts) {
		return batchInsert(true, projectProducts);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<ProjectProduct> projectProduct) {
		if (CollectionUtil.isEmpty(projectProduct)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(projectProduct,
				new UpdateGenerateCallback<ProjectProduct>() {
					public Update generate(ProjectProduct t) {
						return update(PROJECT_PRODUCT_TABLE).set(
								PROJECT_PRODUCT_TABLE.PROJECT_ID.value(t
										.getProjectId()),
								PROJECT_PRODUCT_TABLE.PRODUCT_ID.value(t
										.getProductId())

						).where(PROJECT_PRODUCT_TABLE.ID.eq(t.getId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<ProjectProduct> projectProduct) {
		if (CollectionUtil.isEmpty(projectProduct)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(projectProduct,
				new DeleteGenerateCallback<ProjectProduct>() {
					public Delete generate(ProjectProduct t) {
						return delete(PROJECT_PRODUCT_TABLE).where(
								and(PROJECT_PRODUCT_TABLE.ID.eq(t.getId()),
										PROJECT_PRODUCT_TABLE.PROJECT_ID.eq(t
												.getProjectId()),
										PROJECT_PRODUCT_TABLE.PRODUCT_ID.eq(t
												.getProductId())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderByArgs) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderByArgs != null && i < orderByArgs.length; i++) {
			OrderByElement tempElement = orderByArgs[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<ProjectProduct> projectProduct) {
		if (CollectionUtil.isEmpty(projectProduct)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, projectProduct,
				new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(PROJECT_PRODUCT_TABLE).values(
								PROJECT_PRODUCT_TABLE.PROJECT_ID
										.value(new JdbcNamedParameter(
												"projectId")),
								PROJECT_PRODUCT_TABLE.PRODUCT_ID
										.value(new JdbcNamedParameter(
												"productId"))

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<ProjectProduct> projectProduct) {
		if (CollectionUtil.isEmpty(projectProduct)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(projectProduct,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(PROJECT_PRODUCT_TABLE).set(
								PROJECT_PRODUCT_TABLE.PROJECT_ID
										.value(new JdbcNamedParameter(
												"projectId")),
								PROJECT_PRODUCT_TABLE.PRODUCT_ID
										.value(new JdbcNamedParameter(
												"productId"))

						).where(PROJECT_PRODUCT_TABLE.ID
								.eq(new JdbcNamedParameter("id")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<ProjectProduct> projectProduct) {
		if (CollectionUtil.isEmpty(projectProduct)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(projectProduct,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(PROJECT_PRODUCT_TABLE)
								.where(and(
										PROJECT_PRODUCT_TABLE.PROJECT_ID
												.eq(new JdbcNamedParameter(
														"projectId")),
										PROJECT_PRODUCT_TABLE.PRODUCT_ID
												.eq(new JdbcNamedParameter(
														"productId"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<ProjectProduct> projectProduct) {
		return preparedBatchInsert(true, projectProduct);
	}

}
