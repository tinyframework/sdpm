/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.project.dao.constant.ProjectTaskestimateTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.project.dao.constant.ProjectTaskestimateTable.PROJECT_TASKESTIMATETABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.project.dao.pojo.ProjectTaskestimate;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.project.dao.ProjectTaskestimateDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class ProjectTaskestimateDaoImpl extends TinyDslDaoSupport implements
		ProjectTaskestimateDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ProjectTaskestimate add(ProjectTaskestimate projectTaskestimate) {
		return getDslTemplate().insertAndReturnKey(projectTaskestimate,
				new InsertGenerateCallback<ProjectTaskestimate>() {
					public Insert generate(ProjectTaskestimate t) {
						Insert insert = insertInto(PROJECT_TASKESTIMATE_TABLE)
								.values(PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ID
										.value(t.getTaskestimateId()),
										PROJECT_TASKESTIMATE_TABLE.TASK_ID
												.value(t.getTaskId()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_DATE
												.value(t.getTaskestimateDate()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_LEFT
												.value(t.getTaskestimateLeft()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_CONSUMED.value(t
												.getTaskestimateConsumed()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ACCOUNT.value(t
												.getTaskestimateAccount()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_WORK
												.value(t.getTaskestimateWork())

								);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(ProjectTaskestimate projectTaskestimate) {
		if (projectTaskestimate == null
				|| projectTaskestimate.getTaskestimateId() == null) {
			return 0;
		}
		return getDslTemplate().update(projectTaskestimate,
				new UpdateGenerateCallback<ProjectTaskestimate>() {
					public Update generate(ProjectTaskestimate t) {
						Update update = update(PROJECT_TASKESTIMATE_TABLE)
								.set(PROJECT_TASKESTIMATE_TABLE.TASK_ID.value(t
										.getTaskId()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_DATE
												.value(t.getTaskestimateDate()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_LEFT
												.value(t.getTaskestimateLeft()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_CONSUMED.value(t
												.getTaskestimateConsumed()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ACCOUNT.value(t
												.getTaskestimateAccount()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_WORK
												.value(t.getTaskestimateWork()))
								.where(PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ID
										.eq(t.getTaskestimateId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(PROJECT_TASKESTIMATE_TABLE).where(
								PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ID
										.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(PROJECT_TASKESTIMATE_TABLE).where(
								PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ID
										.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ProjectTaskestimate getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, ProjectTaskestimate.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(PROJECT_TASKESTIMATE_TABLE).where(
								PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ID
										.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<ProjectTaskestimate> query(
			ProjectTaskestimate projectTaskestimate, final OrderBy... orderArgs) {
		if (projectTaskestimate == null) {
			projectTaskestimate = new ProjectTaskestimate();
		}
		return getDslTemplate().query(projectTaskestimate,
				new SelectGenerateCallback<ProjectTaskestimate>() {
					@SuppressWarnings("rawtypes")
					public Select generate(ProjectTaskestimate t) {
						Select select = selectFrom(PROJECT_TASKESTIMATE_TABLE)
								.where(and(
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ID
												.eq(t.getTaskestimateId()),
										PROJECT_TASKESTIMATE_TABLE.TASK_ID.eq(t
												.getTaskId()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_DATE
												.eq(t.getTaskestimateDate()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_LEFT
												.eq(t.getTaskestimateLeft()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_CONSUMED
												.eq(t.getTaskestimateConsumed()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ACCOUNT
												.eq(t.getTaskestimateAccount()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_WORK
												.eq(t.getTaskestimateWork())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<ProjectTaskestimate> queryPager(int start, int limit,
			ProjectTaskestimate projectTaskestimate, final OrderBy... orderArgs) {
		if (projectTaskestimate == null) {
			projectTaskestimate = new ProjectTaskestimate();
		}
		return getDslTemplate().queryPager(start, limit, projectTaskestimate,
				false, new SelectGenerateCallback<ProjectTaskestimate>() {
					public Select generate(ProjectTaskestimate t) {
						Select select = Select
								.selectFrom(PROJECT_TASKESTIMATE_TABLE)
								.where(and(
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ID
												.eq(t.getTaskestimateId()),
										PROJECT_TASKESTIMATE_TABLE.TASK_ID.eq(t
												.getTaskId()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_DATE
												.eq(t.getTaskestimateDate()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_LEFT
												.eq(t.getTaskestimateLeft()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_CONSUMED
												.eq(t.getTaskestimateConsumed()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ACCOUNT
												.eq(t.getTaskestimateAccount()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_WORK
												.eq(t.getTaskestimateWork())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<ProjectTaskestimate> projectTaskestimate) {
		if (CollectionUtil.isEmpty(projectTaskestimate)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys,
				projectTaskestimate,
				new InsertGenerateCallback<ProjectTaskestimate>() {

					public Insert generate(ProjectTaskestimate t) {
						return insertInto(PROJECT_TASKESTIMATE_TABLE)
								.values(PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ID
										.value(t.getTaskestimateId()),
										PROJECT_TASKESTIMATE_TABLE.TASK_ID
												.value(t.getTaskId()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_DATE
												.value(t.getTaskestimateDate()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_LEFT
												.value(t.getTaskestimateLeft()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_CONSUMED.value(t
												.getTaskestimateConsumed()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ACCOUNT.value(t
												.getTaskestimateAccount()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_WORK
												.value(t.getTaskestimateWork())

								);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<ProjectTaskestimate> projectTaskestimates) {
		return batchInsert(true, projectTaskestimates);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<ProjectTaskestimate> projectTaskestimate) {
		if (CollectionUtil.isEmpty(projectTaskestimate)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(projectTaskestimate,
				new UpdateGenerateCallback<ProjectTaskestimate>() {
					public Update generate(ProjectTaskestimate t) {
						return update(PROJECT_TASKESTIMATE_TABLE)
								.set(PROJECT_TASKESTIMATE_TABLE.TASK_ID.value(t
										.getTaskId()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_DATE
												.value(t.getTaskestimateDate()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_LEFT
												.value(t.getTaskestimateLeft()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_CONSUMED.value(t
												.getTaskestimateConsumed()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ACCOUNT.value(t
												.getTaskestimateAccount()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_WORK
												.value(t.getTaskestimateWork())

								)
								.where(PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ID
										.eq(t.getTaskestimateId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<ProjectTaskestimate> projectTaskestimate) {
		if (CollectionUtil.isEmpty(projectTaskestimate)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(projectTaskestimate,
				new DeleteGenerateCallback<ProjectTaskestimate>() {
					public Delete generate(ProjectTaskestimate t) {
						return delete(PROJECT_TASKESTIMATE_TABLE)
								.where(and(
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ID
												.eq(t.getTaskestimateId()),
										PROJECT_TASKESTIMATE_TABLE.TASK_ID.eq(t
												.getTaskId()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_DATE
												.eq(t.getTaskestimateDate()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_LEFT
												.eq(t.getTaskestimateLeft()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_CONSUMED
												.eq(t.getTaskestimateConsumed()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ACCOUNT
												.eq(t.getTaskestimateAccount()),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_WORK
												.eq(t.getTaskestimateWork())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = orderBies[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<ProjectTaskestimate> projectTaskestimate) {
		if (CollectionUtil.isEmpty(projectTaskestimate)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys,
				projectTaskestimate, new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(PROJECT_TASKESTIMATE_TABLE)
								.values(PROJECT_TASKESTIMATE_TABLE.TASK_ID
										.value(new JdbcNamedParameter("taskId")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_DATE
												.value(new JdbcNamedParameter(
														"taskestimateDate")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_LEFT
												.value(new JdbcNamedParameter(
														"taskestimateLeft")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_CONSUMED
												.value(new JdbcNamedParameter(
														"taskestimateConsumed")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ACCOUNT
												.value(new JdbcNamedParameter(
														"taskestimateAccount")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_WORK
												.value(new JdbcNamedParameter(
														"taskestimateWork"))

								);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(
			List<ProjectTaskestimate> projectTaskestimate) {
		if (CollectionUtil.isEmpty(projectTaskestimate)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(projectTaskestimate,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(PROJECT_TASKESTIMATE_TABLE)
								.set(PROJECT_TASKESTIMATE_TABLE.TASK_ID
										.value(new JdbcNamedParameter("taskId")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_DATE
												.value(new JdbcNamedParameter(
														"taskestimateDate")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_LEFT
												.value(new JdbcNamedParameter(
														"taskestimateLeft")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_CONSUMED
												.value(new JdbcNamedParameter(
														"taskestimateConsumed")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ACCOUNT
												.value(new JdbcNamedParameter(
														"taskestimateAccount")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_WORK
												.value(new JdbcNamedParameter(
														"taskestimateWork"))

								)
								.where(PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ID
										.eq(new JdbcNamedParameter(
												"taskestimateId")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(
			List<ProjectTaskestimate> projectTaskestimate) {
		if (CollectionUtil.isEmpty(projectTaskestimate)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(projectTaskestimate,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(PROJECT_TASKESTIMATE_TABLE)
								.where(and(
										PROJECT_TASKESTIMATE_TABLE.TASK_ID
												.eq(new JdbcNamedParameter(
														"taskId")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_DATE
												.eq(new JdbcNamedParameter(
														"taskestimateDate")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_LEFT
												.eq(new JdbcNamedParameter(
														"taskestimateLeft")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_CONSUMED
												.eq(new JdbcNamedParameter(
														"taskestimateConsumed")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_ACCOUNT
												.eq(new JdbcNamedParameter(
														"taskestimateAccount")),
										PROJECT_TASKESTIMATE_TABLE.TASKESTIMATE_WORK
												.eq(new JdbcNamedParameter(
														"taskestimateWork"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(
			List<ProjectTaskestimate> projectTaskestimate) {
		return preparedBatchInsert(true, projectTaskestimate);
	}

}
