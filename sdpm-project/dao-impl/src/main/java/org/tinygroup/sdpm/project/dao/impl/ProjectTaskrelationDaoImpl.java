/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.project.dao.constant.ProjectTaskrelationTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.project.dao.constant.ProjectTaskrelationTable.PROJECT_TASKRELATIONTABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.project.dao.pojo.ProjectTaskrelation;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.project.dao.ProjectTaskrelationDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class ProjectTaskrelationDaoImpl extends TinyDslDaoSupport implements
		ProjectTaskrelationDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ProjectTaskrelation add(ProjectTaskrelation projectTaskrelation) {
		return getDslTemplate().insertAndReturnKey(projectTaskrelation,
				new InsertGenerateCallback<ProjectTaskrelation>() {
					public Insert generate(ProjectTaskrelation t) {
						Insert insert = insertInto(PROJECT_TASKRELATION_TABLE)
								.values(PROJECT_TASKRELATION_TABLE.ID.value(t
										.getId()),
										PROJECT_TASKRELATION_TABLE.PER_TASK
												.value(t.getPerTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_CONDITON.value(t
												.getTaskrelationConditon()),
										PROJECT_TASKRELATION_TABLE.AFTER_TASK
												.value(t.getAfterTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_ACTION.value(t
												.getTaskrelationAction())

								);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(ProjectTaskrelation projectTaskrelation) {
		if (projectTaskrelation == null || projectTaskrelation.getId() == null) {
			return 0;
		}
		return getDslTemplate().update(projectTaskrelation,
				new UpdateGenerateCallback<ProjectTaskrelation>() {
					public Update generate(ProjectTaskrelation t) {
						Update update = update(PROJECT_TASKRELATION_TABLE)
								.set(PROJECT_TASKRELATION_TABLE.PER_TASK
										.value(t.getPerTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_CONDITON.value(t
												.getTaskrelationConditon()),
										PROJECT_TASKRELATION_TABLE.AFTER_TASK
												.value(t.getAfterTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_ACTION.value(t
												.getTaskrelationAction()))
								.where(PROJECT_TASKRELATION_TABLE.ID.eq(t
										.getId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(PROJECT_TASKRELATION_TABLE).where(
								PROJECT_TASKRELATION_TABLE.ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(PROJECT_TASKRELATION_TABLE).where(
								PROJECT_TASKRELATION_TABLE.ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ProjectTaskrelation getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, ProjectTaskrelation.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(PROJECT_TASKRELATION_TABLE).where(
								PROJECT_TASKRELATION_TABLE.ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<ProjectTaskrelation> query(
			ProjectTaskrelation projectTaskrelation, final OrderBy... orderArgs) {
		if (projectTaskrelation == null) {
			projectTaskrelation = new ProjectTaskrelation();
		}
		return getDslTemplate().query(projectTaskrelation,
				new SelectGenerateCallback<ProjectTaskrelation>() {
					@SuppressWarnings("rawtypes")
					public Select generate(ProjectTaskrelation t) {
						Select select = selectFrom(PROJECT_TASKRELATION_TABLE)
								.where(and(
										PROJECT_TASKRELATION_TABLE.ID.eq(t
												.getId()),
										PROJECT_TASKRELATION_TABLE.PER_TASK
												.eq(t.getPerTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_CONDITON
												.eq(t.getTaskrelationConditon()),
										PROJECT_TASKRELATION_TABLE.AFTER_TASK
												.eq(t.getAfterTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_ACTION
												.eq(t.getTaskrelationAction())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<ProjectTaskrelation> queryPager(int start, int limit,
			ProjectTaskrelation projectTaskrelation, final OrderBy... orderArgs) {
		if (projectTaskrelation == null) {
			projectTaskrelation = new ProjectTaskrelation();
		}
		return getDslTemplate().queryPager(start, limit, projectTaskrelation,
				false, new SelectGenerateCallback<ProjectTaskrelation>() {
					public Select generate(ProjectTaskrelation t) {
						Select select = Select
								.selectFrom(PROJECT_TASKRELATION_TABLE)
								.where(and(
										PROJECT_TASKRELATION_TABLE.ID.eq(t
												.getId()),
										PROJECT_TASKRELATION_TABLE.PER_TASK
												.eq(t.getPerTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_CONDITON
												.eq(t.getTaskrelationConditon()),
										PROJECT_TASKRELATION_TABLE.AFTER_TASK
												.eq(t.getAfterTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_ACTION
												.eq(t.getTaskrelationAction())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<ProjectTaskrelation> projectTaskrelation) {
		if (CollectionUtil.isEmpty(projectTaskrelation)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys,
				projectTaskrelation,
				new InsertGenerateCallback<ProjectTaskrelation>() {

					public Insert generate(ProjectTaskrelation t) {
						return insertInto(PROJECT_TASKRELATION_TABLE)
								.values(PROJECT_TASKRELATION_TABLE.ID.value(t
										.getId()),
										PROJECT_TASKRELATION_TABLE.PER_TASK
												.value(t.getPerTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_CONDITON.value(t
												.getTaskrelationConditon()),
										PROJECT_TASKRELATION_TABLE.AFTER_TASK
												.value(t.getAfterTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_ACTION.value(t
												.getTaskrelationAction())

								);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<ProjectTaskrelation> projectTaskrelations) {
		return batchInsert(true, projectTaskrelations);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<ProjectTaskrelation> projectTaskrelation) {
		if (CollectionUtil.isEmpty(projectTaskrelation)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(projectTaskrelation,
				new UpdateGenerateCallback<ProjectTaskrelation>() {
					public Update generate(ProjectTaskrelation t) {
						return update(PROJECT_TASKRELATION_TABLE)
								.set(PROJECT_TASKRELATION_TABLE.PER_TASK
										.value(t.getPerTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_CONDITON.value(t
												.getTaskrelationConditon()),
										PROJECT_TASKRELATION_TABLE.AFTER_TASK
												.value(t.getAfterTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_ACTION.value(t
												.getTaskrelationAction())

								).where(PROJECT_TASKRELATION_TABLE.ID.eq(t
										.getId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<ProjectTaskrelation> projectTaskrelation) {
		if (CollectionUtil.isEmpty(projectTaskrelation)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(projectTaskrelation,
				new DeleteGenerateCallback<ProjectTaskrelation>() {
					public Delete generate(ProjectTaskrelation t) {
						return delete(PROJECT_TASKRELATION_TABLE)
								.where(and(
										PROJECT_TASKRELATION_TABLE.ID.eq(t
												.getId()),
										PROJECT_TASKRELATION_TABLE.PER_TASK
												.eq(t.getPerTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_CONDITON
												.eq(t.getTaskrelationConditon()),
										PROJECT_TASKRELATION_TABLE.AFTER_TASK
												.eq(t.getAfterTask()),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_ACTION
												.eq(t.getTaskrelationAction())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = orderBies[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<ProjectTaskrelation> projectTaskrelation) {
		if (CollectionUtil.isEmpty(projectTaskrelation)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys,
				projectTaskrelation, new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(PROJECT_TASKRELATION_TABLE)
								.values(PROJECT_TASKRELATION_TABLE.PER_TASK
										.value(new JdbcNamedParameter("perTask")),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_CONDITON
												.value(new JdbcNamedParameter(
														"taskrelationConditon")),
										PROJECT_TASKRELATION_TABLE.AFTER_TASK
												.value(new JdbcNamedParameter(
														"afterTask")),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_ACTION
												.value(new JdbcNamedParameter(
														"taskrelationAction"))

								);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(
			List<ProjectTaskrelation> projectTaskrelation) {
		if (CollectionUtil.isEmpty(projectTaskrelation)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(projectTaskrelation,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(PROJECT_TASKRELATION_TABLE)
								.set(PROJECT_TASKRELATION_TABLE.PER_TASK
										.value(new JdbcNamedParameter("perTask")),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_CONDITON
												.value(new JdbcNamedParameter(
														"taskrelationConditon")),
										PROJECT_TASKRELATION_TABLE.AFTER_TASK
												.value(new JdbcNamedParameter(
														"afterTask")),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_ACTION
												.value(new JdbcNamedParameter(
														"taskrelationAction"))

								).where(PROJECT_TASKRELATION_TABLE.ID
										.eq(new JdbcNamedParameter("id")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(
			List<ProjectTaskrelation> projectTaskrelation) {
		if (CollectionUtil.isEmpty(projectTaskrelation)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(projectTaskrelation,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(PROJECT_TASKRELATION_TABLE)
								.where(and(
										PROJECT_TASKRELATION_TABLE.PER_TASK
												.eq(new JdbcNamedParameter(
														"perTask")),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_CONDITON
												.eq(new JdbcNamedParameter(
														"taskrelationConditon")),
										PROJECT_TASKRELATION_TABLE.AFTER_TASK
												.eq(new JdbcNamedParameter(
														"afterTask")),
										PROJECT_TASKRELATION_TABLE.TASKRELATION_ACTION
												.eq(new JdbcNamedParameter(
														"taskrelationAction"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(
			List<ProjectTaskrelation> projectTaskrelation) {
		return preparedBatchInsert(true, projectTaskrelation);
	}

}
