/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.or;
import static org.tinygroup.tinysqldsl.formitem.SubSelect.subSelect;
import static org.tinygroup.sdpm.project.dao.constant.ProjectTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.tinygroup.sdpm.project.dao.constant.ProjectStoryTable.PROJECT_STORYTABLE;
import static org.tinygroup.sdpm.project.dao.constant.ProjectTable.PROJECTTABLE;
import static org.tinygroup.sdpm.project.dao.constant.ProjectTaskTable.PROJECT_TASKTABLE;
import static org.tinygroup.sdpm.project.dao.constant.ProjectTeamTable.PROJECT_TEAMTABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.select;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.Join;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.tinysqldsl.selectitem.FragmentSelectItemSql;
import org.tinygroup.sdpm.project.dao.pojo.Project;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.tinysqldsl.base.Condition;
import org.tinygroup.sdpm.project.dao.ProjectDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class ProjectDaoImpl extends TinyDslDaoSupport implements ProjectDao {

	public Pager<Project> findPageWithStatistics(int start, int limit,
			Project project, final Integer[] projectIds, final OrderBy... args) {
		if (project == null) {
			project = new Project();
		}
		return getDslTemplate().queryPager(start, limit, project, false,
				new SelectGenerateCallback<Project>() {
					public Select generate(Project t) {
						Select select = MysqlSelect
								.select(PROJECTTABLE.PROJECT_ID,
										PROJECTTABLE.PROJECT_NAME,
										PROJECTTABLE.PROJECT_CODE,
										PROJECTTABLE.PROJECT_CLOSE_DATE,
										PROJECTTABLE.PROJECT_STATUS,
										PROJECT_TASKTABLE.TASK_ESTIMATE.sum()
												.as("estimate"),
										PROJECT_TASKTABLE.TASK_CONSUMED.sum()
												.as("consumed"),
										PROJECT_TASKTABLE.TASK_LEFT.sum().as(
												"allLeft"),
										FragmentSelectItemSql
												.fragmentSelect("SUM(project_task.task_consumed)/(SUM(project_task.task_consumed)+SUM(project_task.task_left)) as percent"))
								.from(PROJECTTABLE)
								.join(Join.leftJoin(PROJECT_TASKTABLE,
										PROJECT_TASKTABLE.TASK_PROJECT
												.eq(PROJECTTABLE.PROJECT_ID)))
								.where(and(PROJECTTABLE.PROJECT_ID
										.in(projectIds),
										PROJECTTABLE.PROJECT_ID.eq(t
												.getProjectId())))
								.groupBy(PROJECTTABLE.PROJECT_ID,
										PROJECTTABLE.PROJECT_NAME,
										PROJECTTABLE.PROJECT_CODE,
										PROJECTTABLE.PROJECT_CLOSE_DATE,
										PROJECTTABLE.PROJECT_STATUS);
						return addOrderByElements(select, args);
					}
				});
	}

	public List<Project> findListByRelatedUser(Project project) {
		if (project == null) {
			return new ArrayList<Project>();
		}
		Select select = selectFrom(PROJECTTABLE).where(
				and(PROJECTTABLE.PROJECT_DELETED.eq(Project.DELETE_NO),
						or(PROJECTTABLE.PROJECT_OPENED_BY.eq(project
								.getProjectOpenedBy()), PROJECTTABLE.PROJECT_PO
								.eq(project.getProjectPo()),
								PROJECTTABLE.PROJECT_PM.eq(project
										.getProjectPm()),
								PROJECTTABLE.PROJECT_QD.eq(project
										.getProjectQd()),
								PROJECTTABLE.PROJECT_RD.eq(project
										.getProjectRd()))));
		return getDslSession().fetchList(select, Project.class);
	}

	public List<Project> findListWithStatistics(Project project,
			Date startDate, Date endDate) {
		Condition condition = null;
		if (startDate != null) {
			condition = and(PROJECTTABLE.PROJECT_OPENED_DATE.gte(startDate),
					PROJECTTABLE.PROJECT_OPENED_DATE.lte(endDate));
		}
		if (endDate != null) {
			condition = condition == null ? PROJECTTABLE.PROJECT_OPENED_DATE
					.lte(endDate) : and(condition,
					PROJECTTABLE.PROJECT_OPENED_DATE.lte(endDate));
		}
		if (project == null) {
			project = new Project();
		}
		Select select = MysqlSelect
				.select(PROJECTTABLE.PROJECT_ID,
						PROJECTTABLE.PROJECT_NAME,
						PROJECTTABLE.PROJECT_CODE,
						PROJECTTABLE.PROJECT_CLOSE_DATE,
						PROJECTTABLE.PROJECT_STATUS,
						PROJECT_TASKTABLE.TASK_ESTIMATE.sum().as("estimate"),
						PROJECT_TASKTABLE.TASK_CONSUMED.sum().as("consumed"),
						PROJECT_TASKTABLE.TASK_LEFT.sum().as("allLeft"),
						FragmentSelectItemSql
								.fragmentSelect("SUM(project_task.task_consumed)/(SUM(project_task.task_consumed)+SUM(project_task.task_left)) as percent"))
				.from(PROJECTTABLE)
				.join(Join.leftJoin(PROJECT_TASKTABLE,
						PROJECT_TASKTABLE.TASK_PROJECT
								.equal(PROJECTTABLE.PROJECT_ID)))
				.where(and(condition,
						PROJECTTABLE.PROJECT_DELETED.eq(Project.DELETE_NO),
						PROJECTTABLE.PROJECT_ID.eq(project.getProjectId())))
				.groupBy(PROJECTTABLE.PROJECT_ID, PROJECTTABLE.PROJECT_NAME,
						PROJECTTABLE.PROJECT_CODE,
						PROJECTTABLE.PROJECT_CLOSE_DATE,
						PROJECTTABLE.PROJECT_STATUS);
		return getDslSession().fetchList(select, Project.class);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Project add(Project project) {
		return getDslTemplate().insertAndReturnKey(project,
				new InsertGenerateCallback<Project>() {
					public Insert generate(Project t) {
						Insert insert = insertInto(PROJECT_TABLE)
								.values(PROJECT_TABLE.PROJECT_ID.value(t
										.getProjectId()),
										PROJECT_TABLE.PROJECT_IS_CAT.value(t
												.getProjectIsCat()),
										PROJECT_TABLE.PROJECT_CAT_ID.value(t
												.getProjectCatId()),
										PROJECT_TABLE.PROJECT_TYPE.value(t
												.getProjectType()),
										PROJECT_TABLE.PROJECT_NAME.value(t
												.getProjectName()),
										PROJECT_TABLE.PROJECT_CODE.value(t
												.getProjectCode()),
										PROJECT_TABLE.PROJECT_BEGIN.value(t
												.getProjectBegin()),
										PROJECT_TABLE.PROJECT_END.value(t
												.getProjectEnd()),
										PROJECT_TABLE.PROJECT_DAYS.value(t
												.getProjectDays()),
										PROJECT_TABLE.PROJECT_STATUS.value(t
												.getProjectStatus()),
										PROJECT_TABLE.PROJECT_STATGE.value(t
												.getProjectStatge()),
										PROJECT_TABLE.PROJECT_PRI.value(t
												.getProjectPri()),
										PROJECT_TABLE.PROJECT_DESC.value(t
												.getProjectDesc()),
										PROJECT_TABLE.PROJECT_OPENED_BY.value(t
												.getProjectOpenedBy()),
										PROJECT_TABLE.PROJECT_OPENED_DATE
												.value(t.getProjectOpenedDate()),
										PROJECT_TABLE.PROJECT_OPENED_VERSION.value(t
												.getProjectOpenedVersion()),
										PROJECT_TABLE.PROJECT_CLOSE_BY.value(t
												.getProjectCloseBy()),
										PROJECT_TABLE.PROJECT_CLOSE_DATE
												.value(t.getProjectCloseDate()),
										PROJECT_TABLE.PROJECT_CANCELED_BY
												.value(t.getProjectCanceledBy()),
										PROJECT_TABLE.PROJECT_CANCELED_DATE.value(t
												.getProjectCanceledDate()),
										PROJECT_TABLE.PROJECT_PO.value(t
												.getProjectPo()),
										PROJECT_TABLE.PROJECT_PM.value(t
												.getProjectPm()),
										PROJECT_TABLE.PROJECT_QD.value(t
												.getProjectQd()),
										PROJECT_TABLE.PROJECT_RD.value(t
												.getProjectRd()),
										PROJECT_TABLE.PROJECT_TEAM.value(t
												.getProjectTeam()),
										PROJECT_TABLE.PROJECT_ACL.value(t
												.getProjectAcl()),
										PROJECT_TABLE.PROJECT_WHITE_LIST
												.value(t.getProjectWhiteList()),
										PROJECT_TABLE.PROJECT_ORDER.value(t
												.getProjectOrder()),
										PROJECT_TABLE.PROJECT_DELETED.value(t
												.getProjectDeleted())

								);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(Project project) {
		if (project == null || project.getProjectId() == null) {
			return 0;
		}
		return getDslTemplate().update(project,
				new UpdateGenerateCallback<Project>() {
					public Update generate(Project t) {
						Update update = update(PROJECT_TABLE)
								.set(PROJECT_TABLE.PROJECT_IS_CAT.value(t
										.getProjectIsCat()),
										PROJECT_TABLE.PROJECT_CAT_ID.value(t
												.getProjectCatId()),
										PROJECT_TABLE.PROJECT_TYPE.value(t
												.getProjectType()),
										PROJECT_TABLE.PROJECT_NAME.value(t
												.getProjectName()),
										PROJECT_TABLE.PROJECT_CODE.value(t
												.getProjectCode()),
										PROJECT_TABLE.PROJECT_BEGIN.value(t
												.getProjectBegin()),
										PROJECT_TABLE.PROJECT_END.value(t
												.getProjectEnd()),
										PROJECT_TABLE.PROJECT_DAYS.value(t
												.getProjectDays()),
										PROJECT_TABLE.PROJECT_STATUS.value(t
												.getProjectStatus()),
										PROJECT_TABLE.PROJECT_STATGE.value(t
												.getProjectStatge()),
										PROJECT_TABLE.PROJECT_PRI.value(t
												.getProjectPri()),
										PROJECT_TABLE.PROJECT_DESC.value(t
												.getProjectDesc()),
										PROJECT_TABLE.PROJECT_OPENED_BY.value(t
												.getProjectOpenedBy()),
										PROJECT_TABLE.PROJECT_OPENED_DATE
												.value(t.getProjectOpenedDate()),
										PROJECT_TABLE.PROJECT_OPENED_VERSION.value(t
												.getProjectOpenedVersion()),
										PROJECT_TABLE.PROJECT_CLOSE_BY.value(t
												.getProjectCloseBy()),
										PROJECT_TABLE.PROJECT_CLOSE_DATE
												.value(t.getProjectCloseDate()),
										PROJECT_TABLE.PROJECT_CANCELED_BY
												.value(t.getProjectCanceledBy()),
										PROJECT_TABLE.PROJECT_CANCELED_DATE.value(t
												.getProjectCanceledDate()),
										PROJECT_TABLE.PROJECT_PO.value(t
												.getProjectPo()),
										PROJECT_TABLE.PROJECT_PM.value(t
												.getProjectPm()),
										PROJECT_TABLE.PROJECT_QD.value(t
												.getProjectQd()),
										PROJECT_TABLE.PROJECT_RD.value(t
												.getProjectRd()),
										PROJECT_TABLE.PROJECT_TEAM.value(t
												.getProjectTeam()),
										PROJECT_TABLE.PROJECT_ACL.value(t
												.getProjectAcl()),
										PROJECT_TABLE.PROJECT_WHITE_LIST
												.value(t.getProjectWhiteList()),
										PROJECT_TABLE.PROJECT_ORDER.value(t
												.getProjectOrder()),
										PROJECT_TABLE.PROJECT_DELETED.value(t
												.getProjectDeleted())).where(
										PROJECT_TABLE.PROJECT_ID.eq(t
												.getProjectId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(PROJECT_TABLE).where(
								PROJECT_TABLE.PROJECT_ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(PROJECT_TABLE).where(
								PROJECT_TABLE.PROJECT_ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Project getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, Project.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(PROJECT_TABLE).where(
								PROJECT_TABLE.PROJECT_ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<Project> query(Project project, final OrderBy... orderArgs) {
		if (project == null) {
			project = new Project();
		}
		return getDslTemplate().query(project,
				new SelectGenerateCallback<Project>() {
					@SuppressWarnings("rawtypes")
					public Select generate(Project t) {
						Select select = selectFrom(PROJECT_TABLE)
								.where(and(
										PROJECT_TABLE.PROJECT_ID.eq(t
												.getProjectId()),
										PROJECT_TABLE.PROJECT_IS_CAT.eq(t
												.getProjectIsCat()),
										PROJECT_TABLE.PROJECT_CAT_ID.eq(t
												.getProjectCatId()),
										PROJECT_TABLE.PROJECT_TYPE.eq(t
												.getProjectType()),
										PROJECT_TABLE.PROJECT_NAME.eq(t
												.getProjectName()),
										PROJECT_TABLE.PROJECT_CODE.eq(t
												.getProjectCode()),
										PROJECT_TABLE.PROJECT_BEGIN.eq(t
												.getProjectBegin()),
										PROJECT_TABLE.PROJECT_END.eq(t
												.getProjectEnd()),
										PROJECT_TABLE.PROJECT_DAYS.eq(t
												.getProjectDays()),
										PROJECT_TABLE.PROJECT_STATUS.eq(t
												.getProjectStatus()),
										PROJECT_TABLE.PROJECT_STATGE.eq(t
												.getProjectStatge()),
										PROJECT_TABLE.PROJECT_PRI.eq(t
												.getProjectPri()),
										PROJECT_TABLE.PROJECT_DESC.eq(t
												.getProjectDesc()),
										PROJECT_TABLE.PROJECT_OPENED_BY.eq(t
												.getProjectOpenedBy()),
										PROJECT_TABLE.PROJECT_OPENED_DATE.eq(t
												.getProjectOpenedDate()),
										PROJECT_TABLE.PROJECT_OPENED_VERSION
												.eq(t.getProjectOpenedVersion()),
										PROJECT_TABLE.PROJECT_CLOSE_BY.eq(t
												.getProjectCloseBy()),
										PROJECT_TABLE.PROJECT_CLOSE_DATE.eq(t
												.getProjectCloseDate()),
										PROJECT_TABLE.PROJECT_CANCELED_BY.eq(t
												.getProjectCanceledBy()),
										PROJECT_TABLE.PROJECT_CANCELED_DATE
												.eq(t.getProjectCanceledDate()),
										PROJECT_TABLE.PROJECT_PO.eq(t
												.getProjectPo()),
										PROJECT_TABLE.PROJECT_PM.eq(t
												.getProjectPm()),
										PROJECT_TABLE.PROJECT_QD.eq(t
												.getProjectQd()),
										PROJECT_TABLE.PROJECT_RD.eq(t
												.getProjectRd()),
										PROJECT_TABLE.PROJECT_TEAM.eq(t
												.getProjectTeam()),
										PROJECT_TABLE.PROJECT_ACL.eq(t
												.getProjectAcl()),
										PROJECT_TABLE.PROJECT_WHITE_LIST.eq(t
												.getProjectWhiteList()),
										PROJECT_TABLE.PROJECT_ORDER.eq(t
												.getProjectOrder()),
										PROJECT_TABLE.PROJECT_DELETED.eq(t
												.getProjectDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<Project> queryPager(int start, int limit, Project project,
			final OrderBy... orderArgs) {
		if (project == null) {
			project = new Project();
		}
		return getDslTemplate().queryPager(start, limit, project, false,
				new SelectGenerateCallback<Project>() {
					public Select generate(Project t) {
						Select select = Select
								.selectFrom(PROJECT_TABLE)
								.where(and(
										PROJECT_TABLE.PROJECT_ID.eq(t
												.getProjectId()),
										PROJECT_TABLE.PROJECT_IS_CAT.eq(t
												.getProjectIsCat()),
										PROJECT_TABLE.PROJECT_CAT_ID.eq(t
												.getProjectCatId()),
										PROJECT_TABLE.PROJECT_TYPE.eq(t
												.getProjectType()),
										PROJECT_TABLE.PROJECT_NAME.eq(t
												.getProjectName()),
										PROJECT_TABLE.PROJECT_CODE.eq(t
												.getProjectCode()),
										PROJECT_TABLE.PROJECT_BEGIN.eq(t
												.getProjectBegin()),
										PROJECT_TABLE.PROJECT_END.eq(t
												.getProjectEnd()),
										PROJECT_TABLE.PROJECT_DAYS.eq(t
												.getProjectDays()),
										PROJECT_TABLE.PROJECT_STATUS.eq(t
												.getProjectStatus()),
										PROJECT_TABLE.PROJECT_STATGE.eq(t
												.getProjectStatge()),
										PROJECT_TABLE.PROJECT_PRI.eq(t
												.getProjectPri()),
										PROJECT_TABLE.PROJECT_DESC.eq(t
												.getProjectDesc()),
										PROJECT_TABLE.PROJECT_OPENED_BY.eq(t
												.getProjectOpenedBy()),
										PROJECT_TABLE.PROJECT_OPENED_DATE.eq(t
												.getProjectOpenedDate()),
										PROJECT_TABLE.PROJECT_OPENED_VERSION
												.eq(t.getProjectOpenedVersion()),
										PROJECT_TABLE.PROJECT_CLOSE_BY.eq(t
												.getProjectCloseBy()),
										PROJECT_TABLE.PROJECT_CLOSE_DATE.eq(t
												.getProjectCloseDate()),
										PROJECT_TABLE.PROJECT_CANCELED_BY.eq(t
												.getProjectCanceledBy()),
										PROJECT_TABLE.PROJECT_CANCELED_DATE
												.eq(t.getProjectCanceledDate()),
										PROJECT_TABLE.PROJECT_PO.eq(t
												.getProjectPo()),
										PROJECT_TABLE.PROJECT_PM.eq(t
												.getProjectPm()),
										PROJECT_TABLE.PROJECT_QD.eq(t
												.getProjectQd()),
										PROJECT_TABLE.PROJECT_RD.eq(t
												.getProjectRd()),
										PROJECT_TABLE.PROJECT_TEAM.eq(t
												.getProjectTeam()),
										PROJECT_TABLE.PROJECT_ACL.eq(t
												.getProjectAcl()),
										PROJECT_TABLE.PROJECT_WHITE_LIST.eq(t
												.getProjectWhiteList()),
										PROJECT_TABLE.PROJECT_ORDER.eq(t
												.getProjectOrder()),
										PROJECT_TABLE.PROJECT_DELETED.eq(t
												.getProjectDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys, List<Project> project) {
		if (CollectionUtil.isEmpty(project)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, project,
				new InsertGenerateCallback<Project>() {

					public Insert generate(Project t) {
						return insertInto(PROJECT_TABLE)
								.values(PROJECT_TABLE.PROJECT_ID.value(t
										.getProjectId()),
										PROJECT_TABLE.PROJECT_IS_CAT.value(t
												.getProjectIsCat()),
										PROJECT_TABLE.PROJECT_CAT_ID.value(t
												.getProjectCatId()),
										PROJECT_TABLE.PROJECT_TYPE.value(t
												.getProjectType()),
										PROJECT_TABLE.PROJECT_NAME.value(t
												.getProjectName()),
										PROJECT_TABLE.PROJECT_CODE.value(t
												.getProjectCode()),
										PROJECT_TABLE.PROJECT_BEGIN.value(t
												.getProjectBegin()),
										PROJECT_TABLE.PROJECT_END.value(t
												.getProjectEnd()),
										PROJECT_TABLE.PROJECT_DAYS.value(t
												.getProjectDays()),
										PROJECT_TABLE.PROJECT_STATUS.value(t
												.getProjectStatus()),
										PROJECT_TABLE.PROJECT_STATGE.value(t
												.getProjectStatge()),
										PROJECT_TABLE.PROJECT_PRI.value(t
												.getProjectPri()),
										PROJECT_TABLE.PROJECT_DESC.value(t
												.getProjectDesc()),
										PROJECT_TABLE.PROJECT_OPENED_BY.value(t
												.getProjectOpenedBy()),
										PROJECT_TABLE.PROJECT_OPENED_DATE
												.value(t.getProjectOpenedDate()),
										PROJECT_TABLE.PROJECT_OPENED_VERSION.value(t
												.getProjectOpenedVersion()),
										PROJECT_TABLE.PROJECT_CLOSE_BY.value(t
												.getProjectCloseBy()),
										PROJECT_TABLE.PROJECT_CLOSE_DATE
												.value(t.getProjectCloseDate()),
										PROJECT_TABLE.PROJECT_CANCELED_BY
												.value(t.getProjectCanceledBy()),
										PROJECT_TABLE.PROJECT_CANCELED_DATE.value(t
												.getProjectCanceledDate()),
										PROJECT_TABLE.PROJECT_PO.value(t
												.getProjectPo()),
										PROJECT_TABLE.PROJECT_PM.value(t
												.getProjectPm()),
										PROJECT_TABLE.PROJECT_QD.value(t
												.getProjectQd()),
										PROJECT_TABLE.PROJECT_RD.value(t
												.getProjectRd()),
										PROJECT_TABLE.PROJECT_TEAM.value(t
												.getProjectTeam()),
										PROJECT_TABLE.PROJECT_ACL.value(t
												.getProjectAcl()),
										PROJECT_TABLE.PROJECT_WHITE_LIST
												.value(t.getProjectWhiteList()),
										PROJECT_TABLE.PROJECT_ORDER.value(t
												.getProjectOrder()),
										PROJECT_TABLE.PROJECT_DELETED.value(t
												.getProjectDeleted())

								);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<Project> projects) {
		return batchInsert(true, projects);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<Project> project) {
		if (CollectionUtil.isEmpty(project)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(project,
				new UpdateGenerateCallback<Project>() {
					public Update generate(Project t) {
						return update(PROJECT_TABLE)
								.set(PROJECT_TABLE.PROJECT_IS_CAT.value(t
										.getProjectIsCat()),
										PROJECT_TABLE.PROJECT_CAT_ID.value(t
												.getProjectCatId()),
										PROJECT_TABLE.PROJECT_TYPE.value(t
												.getProjectType()),
										PROJECT_TABLE.PROJECT_NAME.value(t
												.getProjectName()),
										PROJECT_TABLE.PROJECT_CODE.value(t
												.getProjectCode()),
										PROJECT_TABLE.PROJECT_BEGIN.value(t
												.getProjectBegin()),
										PROJECT_TABLE.PROJECT_END.value(t
												.getProjectEnd()),
										PROJECT_TABLE.PROJECT_DAYS.value(t
												.getProjectDays()),
										PROJECT_TABLE.PROJECT_STATUS.value(t
												.getProjectStatus()),
										PROJECT_TABLE.PROJECT_STATGE.value(t
												.getProjectStatge()),
										PROJECT_TABLE.PROJECT_PRI.value(t
												.getProjectPri()),
										PROJECT_TABLE.PROJECT_DESC.value(t
												.getProjectDesc()),
										PROJECT_TABLE.PROJECT_OPENED_BY.value(t
												.getProjectOpenedBy()),
										PROJECT_TABLE.PROJECT_OPENED_DATE
												.value(t.getProjectOpenedDate()),
										PROJECT_TABLE.PROJECT_OPENED_VERSION.value(t
												.getProjectOpenedVersion()),
										PROJECT_TABLE.PROJECT_CLOSE_BY.value(t
												.getProjectCloseBy()),
										PROJECT_TABLE.PROJECT_CLOSE_DATE
												.value(t.getProjectCloseDate()),
										PROJECT_TABLE.PROJECT_CANCELED_BY
												.value(t.getProjectCanceledBy()),
										PROJECT_TABLE.PROJECT_CANCELED_DATE.value(t
												.getProjectCanceledDate()),
										PROJECT_TABLE.PROJECT_PO.value(t
												.getProjectPo()),
										PROJECT_TABLE.PROJECT_PM.value(t
												.getProjectPm()),
										PROJECT_TABLE.PROJECT_QD.value(t
												.getProjectQd()),
										PROJECT_TABLE.PROJECT_RD.value(t
												.getProjectRd()),
										PROJECT_TABLE.PROJECT_TEAM.value(t
												.getProjectTeam()),
										PROJECT_TABLE.PROJECT_ACL.value(t
												.getProjectAcl()),
										PROJECT_TABLE.PROJECT_WHITE_LIST
												.value(t.getProjectWhiteList()),
										PROJECT_TABLE.PROJECT_ORDER.value(t
												.getProjectOrder()),
										PROJECT_TABLE.PROJECT_DELETED.value(t
												.getProjectDeleted())

								).where(PROJECT_TABLE.PROJECT_ID.eq(t
										.getProjectId()));
					}
				});
	}

	public int[] batchSoftDelete(List<Project> projects) {
		if (CollectionUtil.isEmpty(projects)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(projects,
				new NoParamUpdateGenerateCallback() {

					public Update generate() {
						return update(PROJECTTABLE)
								.set(PROJECTTABLE.PROJECT_DELETED
										.value(new JdbcNamedParameter(
												"projectDeleted")))
								.where(PROJECTTABLE.PROJECT_ID
										.eq(new JdbcNamedParameter("projectId")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<Project> project) {
		if (CollectionUtil.isEmpty(project)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(project,
				new DeleteGenerateCallback<Project>() {
					public Delete generate(Project t) {
						return delete(PROJECT_TABLE)
								.where(and(
										PROJECT_TABLE.PROJECT_ID.eq(t
												.getProjectId()),
										PROJECT_TABLE.PROJECT_IS_CAT.eq(t
												.getProjectIsCat()),
										PROJECT_TABLE.PROJECT_CAT_ID.eq(t
												.getProjectCatId()),
										PROJECT_TABLE.PROJECT_TYPE.eq(t
												.getProjectType()),
										PROJECT_TABLE.PROJECT_NAME.eq(t
												.getProjectName()),
										PROJECT_TABLE.PROJECT_CODE.eq(t
												.getProjectCode()),
										PROJECT_TABLE.PROJECT_BEGIN.eq(t
												.getProjectBegin()),
										PROJECT_TABLE.PROJECT_END.eq(t
												.getProjectEnd()),
										PROJECT_TABLE.PROJECT_DAYS.eq(t
												.getProjectDays()),
										PROJECT_TABLE.PROJECT_STATUS.eq(t
												.getProjectStatus()),
										PROJECT_TABLE.PROJECT_STATGE.eq(t
												.getProjectStatge()),
										PROJECT_TABLE.PROJECT_PRI.eq(t
												.getProjectPri()),
										PROJECT_TABLE.PROJECT_DESC.eq(t
												.getProjectDesc()),
										PROJECT_TABLE.PROJECT_OPENED_BY.eq(t
												.getProjectOpenedBy()),
										PROJECT_TABLE.PROJECT_OPENED_DATE.eq(t
												.getProjectOpenedDate()),
										PROJECT_TABLE.PROJECT_OPENED_VERSION
												.eq(t.getProjectOpenedVersion()),
										PROJECT_TABLE.PROJECT_CLOSE_BY.eq(t
												.getProjectCloseBy()),
										PROJECT_TABLE.PROJECT_CLOSE_DATE.eq(t
												.getProjectCloseDate()),
										PROJECT_TABLE.PROJECT_CANCELED_BY.eq(t
												.getProjectCanceledBy()),
										PROJECT_TABLE.PROJECT_CANCELED_DATE
												.eq(t.getProjectCanceledDate()),
										PROJECT_TABLE.PROJECT_PO.eq(t
												.getProjectPo()),
										PROJECT_TABLE.PROJECT_PM.eq(t
												.getProjectPm()),
										PROJECT_TABLE.PROJECT_QD.eq(t
												.getProjectQd()),
										PROJECT_TABLE.PROJECT_RD.eq(t
												.getProjectRd()),
										PROJECT_TABLE.PROJECT_TEAM.eq(t
												.getProjectTeam()),
										PROJECT_TABLE.PROJECT_ACL.eq(t
												.getProjectAcl()),
										PROJECT_TABLE.PROJECT_WHITE_LIST.eq(t
												.getProjectWhiteList()),
										PROJECT_TABLE.PROJECT_ORDER.eq(t
												.getProjectOrder()),
										PROJECT_TABLE.PROJECT_DELETED.eq(t
												.getProjectDeleted())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderArgs) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderArgs != null && i < orderArgs.length; i++) {
			OrderByElement tempElement = orderArgs[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	public List<Project> getProjectByStoryId(Integer storyId) {
		Select select = selectFrom(PROJECTTABLE).where(
				PROJECTTABLE.PROJECT_ID.inExpression(
						subSelect(select(PROJECT_STORYTABLE.PROJECT_ID).from(
								PROJECT_STORYTABLE).where(
								PROJECT_STORYTABLE.STORY_ID.eq(storyId)))).and(
						PROJECTTABLE.PROJECT_DELETED.eq(Project.DELETE_NO)));
		return getDslSession().fetchList(select, Project.class);
	}

	public List<Project> findByIds(Integer... ids) {
		Select select = selectFrom(PROJECTTABLE).where(
				PROJECTTABLE.PROJECT_ID.in(ids));
		return getDslSession().fetchList(select, Project.class);
	}

	public List<Project> findListByTeamUserId(String userId, String acl) {
		Select select = selectFrom(PROJECTTABLE).where(
				and(PROJECTTABLE.PROJECT_ID.inExpression(subSelect(select(
						PROJECT_TEAMTABLE.PROJECT_ID).from(PROJECT_TEAMTABLE)
						.where(PROJECT_TEAMTABLE.TEAM_USER_ID.eq(userId)))),
						PROJECTTABLE.PROJECT_ACL.eq(acl),
						PROJECTTABLE.PROJECT_DELETED.eq(Project.DELETE_NO),
						PROJECTTABLE.PROJECT_STATGE.neq("3")));
		return getDslSession().fetchList(select, Project.class);
	}

	public List<Project> projectInCondition(String condition, Integer limit,
			Integer... ids) {
		Select select = MysqlSelect
				.selectFrom(PROJECTTABLE)
				.where(and(PROJECTTABLE.PROJECT_DELETED.eq(0),
						PROJECTTABLE.PROJECT_ID.in(ids),
						PROJECTTABLE.PROJECT_NAME.like(condition)))
				.limit(0, limit);
		return getDslSession().fetchList(select, Project.class);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<Project> project) {
		if (CollectionUtil.isEmpty(project)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, project,
				new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(PROJECT_TABLE).values(
								PROJECT_TABLE.PROJECT_IS_CAT
										.value(new JdbcNamedParameter(
												"projectIsCat")),
								PROJECT_TABLE.PROJECT_CAT_ID
										.value(new JdbcNamedParameter(
												"projectCatId")),
								PROJECT_TABLE.PROJECT_TYPE
										.value(new JdbcNamedParameter(
												"projectType")),
								PROJECT_TABLE.PROJECT_NAME
										.value(new JdbcNamedParameter(
												"projectName")),
								PROJECT_TABLE.PROJECT_CODE
										.value(new JdbcNamedParameter(
												"projectCode")),
								PROJECT_TABLE.PROJECT_BEGIN
										.value(new JdbcNamedParameter(
												"projectBegin")),
								PROJECT_TABLE.PROJECT_END
										.value(new JdbcNamedParameter(
												"projectEnd")),
								PROJECT_TABLE.PROJECT_DAYS
										.value(new JdbcNamedParameter(
												"projectDays")),
								PROJECT_TABLE.PROJECT_STATUS
										.value(new JdbcNamedParameter(
												"projectStatus")),
								PROJECT_TABLE.PROJECT_STATGE
										.value(new JdbcNamedParameter(
												"projectStatge")),
								PROJECT_TABLE.PROJECT_PRI
										.value(new JdbcNamedParameter(
												"projectPri")),
								PROJECT_TABLE.PROJECT_DESC
										.value(new JdbcNamedParameter(
												"projectDesc")),
								PROJECT_TABLE.PROJECT_OPENED_BY
										.value(new JdbcNamedParameter(
												"projectOpenedBy")),
								PROJECT_TABLE.PROJECT_OPENED_DATE
										.value(new JdbcNamedParameter(
												"projectOpenedDate")),
								PROJECT_TABLE.PROJECT_OPENED_VERSION
										.value(new JdbcNamedParameter(
												"projectOpenedVersion")),
								PROJECT_TABLE.PROJECT_CLOSE_BY
										.value(new JdbcNamedParameter(
												"projectCloseBy")),
								PROJECT_TABLE.PROJECT_CLOSE_DATE
										.value(new JdbcNamedParameter(
												"projectCloseDate")),
								PROJECT_TABLE.PROJECT_CANCELED_BY
										.value(new JdbcNamedParameter(
												"projectCanceledBy")),
								PROJECT_TABLE.PROJECT_CANCELED_DATE
										.value(new JdbcNamedParameter(
												"projectCanceledDate")),
								PROJECT_TABLE.PROJECT_PO
										.value(new JdbcNamedParameter(
												"projectPo")),
								PROJECT_TABLE.PROJECT_PM
										.value(new JdbcNamedParameter(
												"projectPm")),
								PROJECT_TABLE.PROJECT_QD
										.value(new JdbcNamedParameter(
												"projectQd")),
								PROJECT_TABLE.PROJECT_RD
										.value(new JdbcNamedParameter(
												"projectRd")),
								PROJECT_TABLE.PROJECT_TEAM
										.value(new JdbcNamedParameter(
												"projectTeam")),
								PROJECT_TABLE.PROJECT_ACL
										.value(new JdbcNamedParameter(
												"projectAcl")),
								PROJECT_TABLE.PROJECT_WHITE_LIST
										.value(new JdbcNamedParameter(
												"projectWhiteList")),
								PROJECT_TABLE.PROJECT_ORDER
										.value(new JdbcNamedParameter(
												"projectOrder")),
								PROJECT_TABLE.PROJECT_DELETED
										.value(new JdbcNamedParameter(
												"projectDeleted"))

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<Project> project) {
		if (CollectionUtil.isEmpty(project)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(project,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(PROJECT_TABLE).set(
								PROJECT_TABLE.PROJECT_IS_CAT
										.value(new JdbcNamedParameter(
												"projectIsCat")),
								PROJECT_TABLE.PROJECT_CAT_ID
										.value(new JdbcNamedParameter(
												"projectCatId")),
								PROJECT_TABLE.PROJECT_TYPE
										.value(new JdbcNamedParameter(
												"projectType")),
								PROJECT_TABLE.PROJECT_NAME
										.value(new JdbcNamedParameter(
												"projectName")),
								PROJECT_TABLE.PROJECT_CODE
										.value(new JdbcNamedParameter(
												"projectCode")),
								PROJECT_TABLE.PROJECT_BEGIN
										.value(new JdbcNamedParameter(
												"projectBegin")),
								PROJECT_TABLE.PROJECT_END
										.value(new JdbcNamedParameter(
												"projectEnd")),
								PROJECT_TABLE.PROJECT_DAYS
										.value(new JdbcNamedParameter(
												"projectDays")),
								PROJECT_TABLE.PROJECT_STATUS
										.value(new JdbcNamedParameter(
												"projectStatus")),
								PROJECT_TABLE.PROJECT_STATGE
										.value(new JdbcNamedParameter(
												"projectStatge")),
								PROJECT_TABLE.PROJECT_PRI
										.value(new JdbcNamedParameter(
												"projectPri")),
								PROJECT_TABLE.PROJECT_DESC
										.value(new JdbcNamedParameter(
												"projectDesc")),
								PROJECT_TABLE.PROJECT_OPENED_BY
										.value(new JdbcNamedParameter(
												"projectOpenedBy")),
								PROJECT_TABLE.PROJECT_OPENED_DATE
										.value(new JdbcNamedParameter(
												"projectOpenedDate")),
								PROJECT_TABLE.PROJECT_OPENED_VERSION
										.value(new JdbcNamedParameter(
												"projectOpenedVersion")),
								PROJECT_TABLE.PROJECT_CLOSE_BY
										.value(new JdbcNamedParameter(
												"projectCloseBy")),
								PROJECT_TABLE.PROJECT_CLOSE_DATE
										.value(new JdbcNamedParameter(
												"projectCloseDate")),
								PROJECT_TABLE.PROJECT_CANCELED_BY
										.value(new JdbcNamedParameter(
												"projectCanceledBy")),
								PROJECT_TABLE.PROJECT_CANCELED_DATE
										.value(new JdbcNamedParameter(
												"projectCanceledDate")),
								PROJECT_TABLE.PROJECT_PO
										.value(new JdbcNamedParameter(
												"projectPo")),
								PROJECT_TABLE.PROJECT_PM
										.value(new JdbcNamedParameter(
												"projectPm")),
								PROJECT_TABLE.PROJECT_QD
										.value(new JdbcNamedParameter(
												"projectQd")),
								PROJECT_TABLE.PROJECT_RD
										.value(new JdbcNamedParameter(
												"projectRd")),
								PROJECT_TABLE.PROJECT_TEAM
										.value(new JdbcNamedParameter(
												"projectTeam")),
								PROJECT_TABLE.PROJECT_ACL
										.value(new JdbcNamedParameter(
												"projectAcl")),
								PROJECT_TABLE.PROJECT_WHITE_LIST
										.value(new JdbcNamedParameter(
												"projectWhiteList")),
								PROJECT_TABLE.PROJECT_ORDER
										.value(new JdbcNamedParameter(
												"projectOrder")),
								PROJECT_TABLE.PROJECT_DELETED
										.value(new JdbcNamedParameter(
												"projectDeleted"))

						).where(PROJECT_TABLE.PROJECT_ID
								.eq(new JdbcNamedParameter("projectId")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<Project> project) {
		if (CollectionUtil.isEmpty(project)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(project,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(PROJECT_TABLE)
								.where(and(
										PROJECT_TABLE.PROJECT_IS_CAT
												.eq(new JdbcNamedParameter(
														"projectIsCat")),
										PROJECT_TABLE.PROJECT_CAT_ID
												.eq(new JdbcNamedParameter(
														"projectCatId")),
										PROJECT_TABLE.PROJECT_TYPE
												.eq(new JdbcNamedParameter(
														"projectType")),
										PROJECT_TABLE.PROJECT_NAME
												.eq(new JdbcNamedParameter(
														"projectName")),
										PROJECT_TABLE.PROJECT_CODE
												.eq(new JdbcNamedParameter(
														"projectCode")),
										PROJECT_TABLE.PROJECT_BEGIN
												.eq(new JdbcNamedParameter(
														"projectBegin")),
										PROJECT_TABLE.PROJECT_END
												.eq(new JdbcNamedParameter(
														"projectEnd")),
										PROJECT_TABLE.PROJECT_DAYS
												.eq(new JdbcNamedParameter(
														"projectDays")),
										PROJECT_TABLE.PROJECT_STATUS
												.eq(new JdbcNamedParameter(
														"projectStatus")),
										PROJECT_TABLE.PROJECT_STATGE
												.eq(new JdbcNamedParameter(
														"projectStatge")),
										PROJECT_TABLE.PROJECT_PRI
												.eq(new JdbcNamedParameter(
														"projectPri")),
										PROJECT_TABLE.PROJECT_DESC
												.eq(new JdbcNamedParameter(
														"projectDesc")),
										PROJECT_TABLE.PROJECT_OPENED_BY
												.eq(new JdbcNamedParameter(
														"projectOpenedBy")),
										PROJECT_TABLE.PROJECT_OPENED_DATE
												.eq(new JdbcNamedParameter(
														"projectOpenedDate")),
										PROJECT_TABLE.PROJECT_OPENED_VERSION
												.eq(new JdbcNamedParameter(
														"projectOpenedVersion")),
										PROJECT_TABLE.PROJECT_CLOSE_BY
												.eq(new JdbcNamedParameter(
														"projectCloseBy")),
										PROJECT_TABLE.PROJECT_CLOSE_DATE
												.eq(new JdbcNamedParameter(
														"projectCloseDate")),
										PROJECT_TABLE.PROJECT_CANCELED_BY
												.eq(new JdbcNamedParameter(
														"projectCanceledBy")),
										PROJECT_TABLE.PROJECT_CANCELED_DATE
												.eq(new JdbcNamedParameter(
														"projectCanceledDate")),
										PROJECT_TABLE.PROJECT_PO
												.eq(new JdbcNamedParameter(
														"projectPo")),
										PROJECT_TABLE.PROJECT_PM
												.eq(new JdbcNamedParameter(
														"projectPm")),
										PROJECT_TABLE.PROJECT_QD
												.eq(new JdbcNamedParameter(
														"projectQd")),
										PROJECT_TABLE.PROJECT_RD
												.eq(new JdbcNamedParameter(
														"projectRd")),
										PROJECT_TABLE.PROJECT_TEAM
												.eq(new JdbcNamedParameter(
														"projectTeam")),
										PROJECT_TABLE.PROJECT_ACL
												.eq(new JdbcNamedParameter(
														"projectAcl")),
										PROJECT_TABLE.PROJECT_WHITE_LIST
												.eq(new JdbcNamedParameter(
														"projectWhiteList")),
										PROJECT_TABLE.PROJECT_ORDER
												.eq(new JdbcNamedParameter(
														"projectOrder")),
										PROJECT_TABLE.PROJECT_DELETED
												.eq(new JdbcNamedParameter(
														"projectDeleted"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<Project> project) {
		return preparedBatchInsert(true, project);
	}

}
