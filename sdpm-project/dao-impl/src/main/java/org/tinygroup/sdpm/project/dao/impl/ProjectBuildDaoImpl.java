/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.project.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.project.dao.constant.ProjectBuildTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.product.dao.constant.ProductStoryTable.PRODUCT_STORYTABLE;
import static org.tinygroup.sdpm.product.dao.constant.ProductTable.PRODUCTTABLE;
import static org.tinygroup.sdpm.project.dao.constant.ProjectBuildTable.PROJECT_BUILDTABLE;
import static org.tinygroup.sdpm.project.dao.constant.ProjectTable.PROJECTTABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.select;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.Join;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.project.dao.pojo.ProjectBuild;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.tinysqldsl.base.Condition;
import org.tinygroup.tinysqldsl.base.FragmentSql;
import org.tinygroup.sdpm.project.dao.ProjectBuildDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.sdpm.product.dao.pojo.ProductStory;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class ProjectBuildDaoImpl extends TinyDslDaoSupport implements
		ProjectBuildDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ProjectBuild add(ProjectBuild projectBuild) {
		return getDslTemplate().insertAndReturnKey(projectBuild,
				new InsertGenerateCallback<ProjectBuild>() {
					public Insert generate(ProjectBuild t) {
						Insert insert = insertInto(PROJECT_BUILD_TABLE).values(
								PROJECT_BUILD_TABLE.BUILD_ID.value(t
										.getBuildId()),
								PROJECT_BUILD_TABLE.BUILD_PRODUCT.value(t
										.getBuildProduct()),
								PROJECT_BUILD_TABLE.BUILD_PROJECT.value(t
										.getBuildProject()),
								PROJECT_BUILD_TABLE.BUILD_NAME.value(t
										.getBuildName()),
								PROJECT_BUILD_TABLE.BUILD_SCM_PATH.value(t
										.getBuildScmPath()),
								PROJECT_BUILD_TABLE.BUILD_FILE_PATH.value(t
										.getBuildFilePath()),
								PROJECT_BUILD_TABLE.BUILD_DATE.value(t
										.getBuildDate()),
								PROJECT_BUILD_TABLE.BUILD_STORIES.value(t
										.getBuildStories()),
								PROJECT_BUILD_TABLE.BUILD_BUGS.value(t
										.getBuildBugs()),
								PROJECT_BUILD_TABLE.BUILD_BUILDER.value(t
										.getBuildBuilder()),
								PROJECT_BUILD_TABLE.BUILD_DESC.value(t
										.getBuildDesc()),
								PROJECT_BUILD_TABLE.BUILD_DELETED.value(t
										.getBuildDeleted())

						);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(ProjectBuild projectBuild) {
		if (projectBuild == null || projectBuild.getBuildId() == null) {
			return 0;
		}
		return getDslTemplate().update(projectBuild,
				new UpdateGenerateCallback<ProjectBuild>() {
					public Update generate(ProjectBuild t) {
						Update update = update(PROJECT_BUILD_TABLE).set(
								PROJECT_BUILD_TABLE.BUILD_PRODUCT.value(t
										.getBuildProduct()),
								PROJECT_BUILD_TABLE.BUILD_PROJECT.value(t
										.getBuildProject()),
								PROJECT_BUILD_TABLE.BUILD_NAME.value(t
										.getBuildName()),
								PROJECT_BUILD_TABLE.BUILD_SCM_PATH.value(t
										.getBuildScmPath()),
								PROJECT_BUILD_TABLE.BUILD_FILE_PATH.value(t
										.getBuildFilePath()),
								PROJECT_BUILD_TABLE.BUILD_DATE.value(t
										.getBuildDate()),
								PROJECT_BUILD_TABLE.BUILD_STORIES.value(t
										.getBuildStories()),
								PROJECT_BUILD_TABLE.BUILD_BUGS.value(t
										.getBuildBugs()),
								PROJECT_BUILD_TABLE.BUILD_BUILDER.value(t
										.getBuildBuilder()),
								PROJECT_BUILD_TABLE.BUILD_DESC.value(t
										.getBuildDesc()),
								PROJECT_BUILD_TABLE.BUILD_DELETED.value(t
										.getBuildDeleted()))
								.where(PROJECT_BUILD_TABLE.BUILD_ID.eq(t
										.getBuildId()));
						return update;
					}
				});
	}

	public Integer deleteBuildByProductId(Integer productId) {
		if (productId == null) {
			return 0;
		}
		Update update = update(PROJECT_BUILDTABLE)
				.set(PROJECT_BUILDTABLE.BUILD_DELETED
						.value(ProjectBuild.DELETE_YES)).where(
						PROJECT_BUILDTABLE.BUILD_PRODUCT.eq(productId));
		return getDslSession().execute(update);
	}

	public Integer softDelete(ProjectBuild build) {
		Update update = update(PROJECT_BUILDTABLE)
				.set(PROJECT_BUILDTABLE.BUILD_DELETED.value(build
						.getBuildDeleted())).where(
						PROJECT_BUILDTABLE.BUILD_ID.eq(build.getBuildId()));
		return getDslSession().execute(update);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(PROJECT_BUILD_TABLE).where(
								PROJECT_BUILD_TABLE.BUILD_ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(PROJECT_BUILD_TABLE).where(
								PROJECT_BUILD_TABLE.BUILD_ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ProjectBuild getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, ProjectBuild.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(PROJECT_BUILD_TABLE).where(
								PROJECT_BUILD_TABLE.BUILD_ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<ProjectBuild> query(ProjectBuild projectBuild,
			final OrderBy... orderArgs) {
		if (projectBuild == null) {
			projectBuild = new ProjectBuild();
		}
		return getDslTemplate().query(projectBuild,
				new SelectGenerateCallback<ProjectBuild>() {
					@SuppressWarnings("rawtypes")
					public Select generate(ProjectBuild t) {
						Select select = selectFrom(PROJECT_BUILD_TABLE).where(
								and(PROJECT_BUILD_TABLE.BUILD_ID.eq(t
										.getBuildId()),
										PROJECT_BUILD_TABLE.BUILD_PRODUCT.eq(t
												.getBuildProduct()),
										PROJECT_BUILD_TABLE.BUILD_PROJECT.eq(t
												.getBuildProject()),
										PROJECT_BUILD_TABLE.BUILD_NAME.eq(t
												.getBuildName()),
										PROJECT_BUILD_TABLE.BUILD_SCM_PATH.eq(t
												.getBuildScmPath()),
										PROJECT_BUILD_TABLE.BUILD_FILE_PATH
												.eq(t.getBuildFilePath()),
										PROJECT_BUILD_TABLE.BUILD_DATE.eq(t
												.getBuildDate()),
										PROJECT_BUILD_TABLE.BUILD_STORIES.eq(t
												.getBuildStories()),
										PROJECT_BUILD_TABLE.BUILD_BUGS.eq(t
												.getBuildBugs()),
										PROJECT_BUILD_TABLE.BUILD_BUILDER.eq(t
												.getBuildBuilder()),
										PROJECT_BUILD_TABLE.BUILD_DESC.eq(t
												.getBuildDesc()),
										PROJECT_BUILD_TABLE.BUILD_DELETED.eq(t
												.getBuildDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<ProjectBuild> queryPager(int start, int limit,
			ProjectBuild projectBuild, final OrderBy... orderArgs) {
		if (projectBuild == null) {
			projectBuild = new ProjectBuild();
		}
		return getDslTemplate().queryPager(start, limit, projectBuild, false,
				new SelectGenerateCallback<ProjectBuild>() {
					public Select generate(ProjectBuild t) {
						Select select = Select.selectFrom(PROJECT_BUILD_TABLE)
								.where(and(PROJECT_BUILD_TABLE.BUILD_ID.eq(t
										.getBuildId()),
										PROJECT_BUILD_TABLE.BUILD_PRODUCT.eq(t
												.getBuildProduct()),
										PROJECT_BUILD_TABLE.BUILD_PROJECT.eq(t
												.getBuildProject()),
										PROJECT_BUILD_TABLE.BUILD_NAME.eq(t
												.getBuildName()),
										PROJECT_BUILD_TABLE.BUILD_SCM_PATH.eq(t
												.getBuildScmPath()),
										PROJECT_BUILD_TABLE.BUILD_FILE_PATH
												.eq(t.getBuildFilePath()),
										PROJECT_BUILD_TABLE.BUILD_DATE.eq(t
												.getBuildDate()),
										PROJECT_BUILD_TABLE.BUILD_STORIES.eq(t
												.getBuildStories()),
										PROJECT_BUILD_TABLE.BUILD_BUGS.eq(t
												.getBuildBugs()),
										PROJECT_BUILD_TABLE.BUILD_BUILDER.eq(t
												.getBuildBuilder()),
										PROJECT_BUILD_TABLE.BUILD_DESC.eq(t
												.getBuildDesc()),
										PROJECT_BUILD_TABLE.BUILD_DELETED.eq(t
												.getBuildDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<ProjectBuild> projectBuild) {
		if (CollectionUtil.isEmpty(projectBuild)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, projectBuild,
				new InsertGenerateCallback<ProjectBuild>() {

					public Insert generate(ProjectBuild t) {
						return insertInto(PROJECT_BUILD_TABLE).values(
								PROJECT_BUILD_TABLE.BUILD_ID.value(t
										.getBuildId()),
								PROJECT_BUILD_TABLE.BUILD_PRODUCT.value(t
										.getBuildProduct()),
								PROJECT_BUILD_TABLE.BUILD_PROJECT.value(t
										.getBuildProject()),
								PROJECT_BUILD_TABLE.BUILD_NAME.value(t
										.getBuildName()),
								PROJECT_BUILD_TABLE.BUILD_SCM_PATH.value(t
										.getBuildScmPath()),
								PROJECT_BUILD_TABLE.BUILD_FILE_PATH.value(t
										.getBuildFilePath()),
								PROJECT_BUILD_TABLE.BUILD_DATE.value(t
										.getBuildDate()),
								PROJECT_BUILD_TABLE.BUILD_STORIES.value(t
										.getBuildStories()),
								PROJECT_BUILD_TABLE.BUILD_BUGS.value(t
										.getBuildBugs()),
								PROJECT_BUILD_TABLE.BUILD_BUILDER.value(t
										.getBuildBuilder()),
								PROJECT_BUILD_TABLE.BUILD_DESC.value(t
										.getBuildDesc()),
								PROJECT_BUILD_TABLE.BUILD_DELETED.value(t
										.getBuildDeleted())

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<ProjectBuild> projectBuilds) {
		return batchInsert(true, projectBuilds);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<ProjectBuild> projectBuild) {
		if (CollectionUtil.isEmpty(projectBuild)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(projectBuild,
				new UpdateGenerateCallback<ProjectBuild>() {
					public Update generate(ProjectBuild t) {
						return update(PROJECT_BUILD_TABLE).set(
								PROJECT_BUILD_TABLE.BUILD_PRODUCT.value(t
										.getBuildProduct()),
								PROJECT_BUILD_TABLE.BUILD_PROJECT.value(t
										.getBuildProject()),
								PROJECT_BUILD_TABLE.BUILD_NAME.value(t
										.getBuildName()),
								PROJECT_BUILD_TABLE.BUILD_SCM_PATH.value(t
										.getBuildScmPath()),
								PROJECT_BUILD_TABLE.BUILD_FILE_PATH.value(t
										.getBuildFilePath()),
								PROJECT_BUILD_TABLE.BUILD_DATE.value(t
										.getBuildDate()),
								PROJECT_BUILD_TABLE.BUILD_STORIES.value(t
										.getBuildStories()),
								PROJECT_BUILD_TABLE.BUILD_BUGS.value(t
										.getBuildBugs()),
								PROJECT_BUILD_TABLE.BUILD_BUILDER.value(t
										.getBuildBuilder()),
								PROJECT_BUILD_TABLE.BUILD_DESC.value(t
										.getBuildDesc()),
								PROJECT_BUILD_TABLE.BUILD_DELETED.value(t
										.getBuildDeleted())

						)
								.where(PROJECT_BUILD_TABLE.BUILD_ID.eq(t
										.getBuildId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<ProjectBuild> projectBuild) {
		if (CollectionUtil.isEmpty(projectBuild)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(projectBuild,
				new DeleteGenerateCallback<ProjectBuild>() {
					public Delete generate(ProjectBuild t) {
						return delete(PROJECT_BUILD_TABLE).where(
								and(PROJECT_BUILD_TABLE.BUILD_ID.eq(t
										.getBuildId()),
										PROJECT_BUILD_TABLE.BUILD_PRODUCT.eq(t
												.getBuildProduct()),
										PROJECT_BUILD_TABLE.BUILD_PROJECT.eq(t
												.getBuildProject()),
										PROJECT_BUILD_TABLE.BUILD_NAME.eq(t
												.getBuildName()),
										PROJECT_BUILD_TABLE.BUILD_SCM_PATH.eq(t
												.getBuildScmPath()),
										PROJECT_BUILD_TABLE.BUILD_FILE_PATH
												.eq(t.getBuildFilePath()),
										PROJECT_BUILD_TABLE.BUILD_DATE.eq(t
												.getBuildDate()),
										PROJECT_BUILD_TABLE.BUILD_STORIES.eq(t
												.getBuildStories()),
										PROJECT_BUILD_TABLE.BUILD_BUGS.eq(t
												.getBuildBugs()),
										PROJECT_BUILD_TABLE.BUILD_BUILDER.eq(t
												.getBuildBuilder()),
										PROJECT_BUILD_TABLE.BUILD_DESC.eq(t
												.getBuildDesc()),
										PROJECT_BUILD_TABLE.BUILD_DELETED.eq(t
												.getBuildDeleted())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = orderBies[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	public int[] batchUpdateDel(List<ProjectBuild> builds) {
		if (CollectionUtil.isEmpty(builds)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(builds,
				new NoParamUpdateGenerateCallback() {

					public Update generate() {
						return update(PROJECT_BUILDTABLE).set(
								PROJECT_BUILDTABLE.BUILD_DELETED
										.value(new JdbcNamedParameter(
												"buildDeleted"))).where(
								PROJECT_BUILDTABLE.BUILD_ID
										.eq(new JdbcNamedParameter("buildId")));
					}
				});

	}

	public Pager<ProductStory> findBuildStoryList(int start, int limit,
			Integer buildId, final OrderBy... orderBies) {
		Select select = select(PROJECT_BUILDTABLE.BUILD_STORIES).from(
				PROJECT_BUILDTABLE).where(
				PROJECT_BUILDTABLE.BUILD_ID.eq(buildId));
		ProjectBuild test = getDslSession().fetchOneResult(select,
				ProjectBuild.class);
		final String[] storys = test.getBuildStories().split(",");
		ProductStory productStory = new ProductStory();
		return getDslTemplate().queryPager(start, limit, productStory, false,
				new SelectGenerateCallback<ProductStory>() {

					public Select generate(ProductStory t) {
						Select select = MysqlSelect.selectFrom(
								PRODUCT_STORYTABLE).where(
								PRODUCT_STORYTABLE.STORY_ID.in(storys));
						return addOrderByElements(select, orderBies);
					}
				});

	}

	public List<ProjectBuild> getBuildByKeys(String... ids) {
		Select select = selectFrom(PROJECT_BUILDTABLE).where(
				and(PROJECT_BUILDTABLE.BUILD_DELETED.eq("0"),
						PROJECT_BUILDTABLE.BUILD_ID.in(ids)));
		return getDslSession().fetchList(select, ProjectBuild.class);
	}

	public List<ProjectBuild> getBuildByProducts(Integer... ids) {
		Select select = selectFrom(PROJECT_BUILDTABLE).where(
				and(PROJECT_BUILDTABLE.BUILD_DELETED.eq("0"),
						PROJECT_BUILDTABLE.BUILD_PRODUCT.in(ids)));
		return getDslSession().fetchList(select, ProjectBuild.class);
	}

	public List<ProjectBuild> buildInCondition(String condition, Integer limit,
			Integer productId, Integer projectId) {
		Condition con = null;
		if (productId != null) {
			con = PROJECT_BUILDTABLE.BUILD_PRODUCT.eq(productId);
		}
		if (projectId != null) {
			con = con == null ? PROJECT_BUILDTABLE.BUILD_PROJECT.eq(projectId)
					: and(con, PROJECT_BUILDTABLE.BUILD_PROJECT.eq(projectId));
		}
		Select select = MysqlSelect
				.select(FragmentSql
						.fragmentSelect("CONCAT (build_name,'-',project_name) as buildName"),
						PROJECT_BUILDTABLE.BUILD_ID)
				.from(PROJECT_BUILDTABLE)
				.where(and(PROJECT_BUILDTABLE.BUILD_NAME.like(condition), con,
						PROJECT_BUILDTABLE.BUILD_DELETED.eq(0)))
				.join(Join.leftJoin(PROJECTTABLE,
						PROJECT_BUILDTABLE.BUILD_PROJECT
								.eq(PROJECTTABLE.PROJECT_ID)))
				.orderBy(OrderByElement.desc(PROJECT_BUILDTABLE.BUILD_DATE))
				.limit(0, limit);
		return getDslSession().fetchList(select, ProjectBuild.class);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<ProjectBuild> projectBuild) {
		if (CollectionUtil.isEmpty(projectBuild)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, projectBuild,
				new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(PROJECT_BUILD_TABLE).values(
								PROJECT_BUILD_TABLE.BUILD_PRODUCT
										.value(new JdbcNamedParameter(
												"buildProduct")),
								PROJECT_BUILD_TABLE.BUILD_PROJECT
										.value(new JdbcNamedParameter(
												"buildProject")),
								PROJECT_BUILD_TABLE.BUILD_NAME
										.value(new JdbcNamedParameter(
												"buildName")),
								PROJECT_BUILD_TABLE.BUILD_SCM_PATH
										.value(new JdbcNamedParameter(
												"buildScmPath")),
								PROJECT_BUILD_TABLE.BUILD_FILE_PATH
										.value(new JdbcNamedParameter(
												"buildFilePath")),
								PROJECT_BUILD_TABLE.BUILD_DATE
										.value(new JdbcNamedParameter(
												"buildDate")),
								PROJECT_BUILD_TABLE.BUILD_STORIES
										.value(new JdbcNamedParameter(
												"buildStories")),
								PROJECT_BUILD_TABLE.BUILD_BUGS
										.value(new JdbcNamedParameter(
												"buildBugs")),
								PROJECT_BUILD_TABLE.BUILD_BUILDER
										.value(new JdbcNamedParameter(
												"buildBuilder")),
								PROJECT_BUILD_TABLE.BUILD_DESC
										.value(new JdbcNamedParameter(
												"buildDesc")),
								PROJECT_BUILD_TABLE.BUILD_DELETED
										.value(new JdbcNamedParameter(
												"buildDeleted"))

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<ProjectBuild> projectBuild) {
		if (CollectionUtil.isEmpty(projectBuild)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(projectBuild,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(PROJECT_BUILD_TABLE).set(
								PROJECT_BUILD_TABLE.BUILD_PRODUCT
										.value(new JdbcNamedParameter(
												"buildProduct")),
								PROJECT_BUILD_TABLE.BUILD_PROJECT
										.value(new JdbcNamedParameter(
												"buildProject")),
								PROJECT_BUILD_TABLE.BUILD_NAME
										.value(new JdbcNamedParameter(
												"buildName")),
								PROJECT_BUILD_TABLE.BUILD_SCM_PATH
										.value(new JdbcNamedParameter(
												"buildScmPath")),
								PROJECT_BUILD_TABLE.BUILD_FILE_PATH
										.value(new JdbcNamedParameter(
												"buildFilePath")),
								PROJECT_BUILD_TABLE.BUILD_DATE
										.value(new JdbcNamedParameter(
												"buildDate")),
								PROJECT_BUILD_TABLE.BUILD_STORIES
										.value(new JdbcNamedParameter(
												"buildStories")),
								PROJECT_BUILD_TABLE.BUILD_BUGS
										.value(new JdbcNamedParameter(
												"buildBugs")),
								PROJECT_BUILD_TABLE.BUILD_BUILDER
										.value(new JdbcNamedParameter(
												"buildBuilder")),
								PROJECT_BUILD_TABLE.BUILD_DESC
										.value(new JdbcNamedParameter(
												"buildDesc")),
								PROJECT_BUILD_TABLE.BUILD_DELETED
										.value(new JdbcNamedParameter(
												"buildDeleted"))

						).where(PROJECT_BUILD_TABLE.BUILD_ID
								.eq(new JdbcNamedParameter("buildId")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<ProjectBuild> projectBuild) {
		if (CollectionUtil.isEmpty(projectBuild)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(projectBuild,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(PROJECT_BUILD_TABLE).where(
								and(PROJECT_BUILD_TABLE.BUILD_PRODUCT
										.eq(new JdbcNamedParameter(
												"buildProduct")),
										PROJECT_BUILD_TABLE.BUILD_PROJECT
												.eq(new JdbcNamedParameter(
														"buildProject")),
										PROJECT_BUILD_TABLE.BUILD_NAME
												.eq(new JdbcNamedParameter(
														"buildName")),
										PROJECT_BUILD_TABLE.BUILD_SCM_PATH
												.eq(new JdbcNamedParameter(
														"buildScmPath")),
										PROJECT_BUILD_TABLE.BUILD_FILE_PATH
												.eq(new JdbcNamedParameter(
														"buildFilePath")),
										PROJECT_BUILD_TABLE.BUILD_DATE
												.eq(new JdbcNamedParameter(
														"buildDate")),
										PROJECT_BUILD_TABLE.BUILD_STORIES
												.eq(new JdbcNamedParameter(
														"buildStories")),
										PROJECT_BUILD_TABLE.BUILD_BUGS
												.eq(new JdbcNamedParameter(
														"buildBugs")),
										PROJECT_BUILD_TABLE.BUILD_BUILDER
												.eq(new JdbcNamedParameter(
														"buildBuilder")),
										PROJECT_BUILD_TABLE.BUILD_DESC
												.eq(new JdbcNamedParameter(
														"buildDesc")),
										PROJECT_BUILD_TABLE.BUILD_DELETED
												.eq(new JdbcNamedParameter(
														"buildDeleted"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<ProjectBuild> projectBuild) {
		return preparedBatchInsert(true, projectBuild);
	}

}
