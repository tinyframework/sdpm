/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.pojo;

import java.io.Serializable;

/**
 * <!-- begin-user-doc --> 数据字典 * <!-- end-user-doc -->
 */
public class SystemDict implements Serializable{

	public static final int DELETE_YES = 1;

	public static final int DELETE_NO = 0;

	/**
	 * <!-- begin-user-doc --> 字典项ID * <!-- end-user-doc -->
	 */
	private Integer dictId;

	/**
	 * <!-- begin-user-doc --> 字典key值 * <!-- end-user-doc -->
	 */
	private String dictKey;

	/**
	 * <!-- begin-user-doc --> 字典value值 * <!-- end-user-doc -->
	 */
	private String dictValue;

	/**
	 * <!-- begin-user-doc --> 字典项序号 * <!-- end-user-doc -->
	 */
	private Integer dictSort;

	/**
	 * <!-- begin-user-doc --> 模块ID * <!-- end-user-doc -->
	 */
	private Integer moduleId;

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	private Integer deleted;

	/**
	 * <!-- begin-user-doc --> 字典项ID * <!-- end-user-doc -->
	 */
	public void setDictId(Integer dictId) {
		this.dictId = dictId;
	}

	public Integer getDictId() {
		return dictId;
	}

	/**
	 * <!-- begin-user-doc --> 字典key值 * <!-- end-user-doc -->
	 */
	public void setDictKey(String dictKey) {
		this.dictKey = dictKey;
	}

	public String getDictKey() {
		return dictKey;
	}

	/**
	 * <!-- begin-user-doc --> 字典value值 * <!-- end-user-doc -->
	 */
	public void setDictValue(String dictValue) {
		this.dictValue = dictValue;
	}

	public String getDictValue() {
		return dictValue;
	}

	/**
	 * <!-- begin-user-doc --> 字典项序号 * <!-- end-user-doc -->
	 */
	public void setDictSort(Integer dictSort) {
		this.dictSort = dictSort;
	}

	public Integer getDictSort() {
		return dictSort;
	}

	/**
	 * <!-- begin-user-doc --> 模块ID * <!-- end-user-doc -->
	 */
	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Integer getDeleted() {
		return deleted;
	}

}
