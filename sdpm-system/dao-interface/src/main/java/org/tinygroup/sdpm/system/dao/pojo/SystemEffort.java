/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 日志表 * <!-- end-user-doc -->
 */
public class SystemEffort implements Serializable{

	/**
	 * <!-- begin-user-doc --> 日志编号 * <!-- end-user-doc -->
	 */
	private Integer effortId;

	/**
	 * <!-- begin-user-doc --> 耗时 * <!-- end-user-doc -->
	 */
	private Float effortConsumed;

	/**
	 * <!-- begin-user-doc --> 剩余 * <!-- end-user-doc -->
	 */
	private Float effortLeft;

	/**
	 * <!-- begin-user-doc --> 日期 * <!-- end-user-doc -->
	 */
	private Date effortDate;

	/**
	 * <!-- begin-user-doc --> 对象ID * <!-- end-user-doc -->
	 */
	private Integer effortObjectId;

	/**
	 * <!-- begin-user-doc --> 日志对象 * <!-- end-user-doc -->
	 */
	private String effortObjectType;

	/**
	 * 日志星期
	 */
	private Integer effortWeek;

	/**
	 * <!-- begin-user-doc --> 工作内容 * <!-- end-user-doc -->
	 */
	private String effortWork;

	/**
	 * <!-- begin-user-doc --> 登记人 * <!-- end-user-doc -->
	 */
	private String effortAccount;

	/**
	 * 以String形式显示星期
	 */
	private String effortWeekDay;

	/**
	 * <!-- begin-user-doc --> 所属项目 * <!-- end-user-doc -->
	 */
	private Integer effortProject;

	/**
	 * <!-- begin-user-doc --> 所属产品 * <!-- end-user-doc -->
	 */
	private String effortProduct;

	/**
	 * <!-- begin-user-doc --> 开始 * <!-- end-user-doc -->
	 */
	private String effortBegin;

	/**
	 * <!-- begin-user-doc --> 已关闭 * <!-- end-user-doc -->
	 */
	private String effortEnd;

	/**
	 * <!-- begin-user-doc --> 日志编号 * <!-- end-user-doc -->
	 */
	public void setEffortId(Integer effortId) {
		this.effortId = effortId;
	}

	public Integer getEffortId() {
		return effortId;
	}

	/**
	 * <!-- begin-user-doc --> 耗时 * <!-- end-user-doc -->
	 */
	public void setEffortConsumed(Float effortConsumed) {
		this.effortConsumed = effortConsumed;
	}

	public Float getEffortConsumed() {
		return effortConsumed;
	}

	/**
	 * <!-- begin-user-doc --> 剩余 * <!-- end-user-doc -->
	 */
	public void setEffortLeft(Float effortLeft) {
		this.effortLeft = effortLeft;
	}

	public Float getEffortLeft() {
		return effortLeft;
	}

	/**
	 * <!-- begin-user-doc --> 日期 * <!-- end-user-doc -->
	 */
	public void setEffortDate(Date effortDate) {
		this.effortDate = effortDate;
	}

	public Date getEffortDate() {
		return effortDate;
	}

	/**
	 * <!-- begin-user-doc --> 对象ID * <!-- end-user-doc -->
	 */
	public void setEffortObjectId(Integer effortObjectId) {
		this.effortObjectId = effortObjectId;
	}

	public Integer getEffortObjectId() {
		return effortObjectId;
	}

	/**
	 * <!-- begin-user-doc --> 日志对象 * <!-- end-user-doc -->
	 */
	public void setEffortObjectType(String effortObjectType) {
		this.effortObjectType = effortObjectType;
	}

	public String getEffortObjectType() {
		return effortObjectType;
	}

	/**
	 * <!-- begin-user-doc --> 工作内容 * <!-- end-user-doc -->
	 */
	public void setEffortWork(String effortWork) {
		this.effortWork = effortWork;
	}

	public String getEffortWork() {
		return effortWork;
	}

	/**
	 * <!-- begin-user-doc --> 登记人 * <!-- end-user-doc -->
	 */
	public void setEffortAccount(String effortAccount) {
		this.effortAccount = effortAccount;
	}

	public Integer getEffortWeek() {
		return effortWeek;
	}

	public void setEffortWeek(Integer effortWeek) {
		this.effortWeek = effortWeek;
	}

	public String getEffortWeekDay() {
		return effortWeekDay;
	}

	public void setEffortWeekDay(String effortWeekDay) {
		this.effortWeekDay = effortWeekDay;
	}

	public String getEffortAccount() {
		return effortAccount;
	}

	/**
	 * <!-- begin-user-doc --> 所属项目 * <!-- end-user-doc -->
	 */
	public void setEffortProject(Integer effortProject) {
		this.effortProject = effortProject;
	}

	public Integer getEffortProject() {
		return effortProject;
	}

	/**
	 * <!-- begin-user-doc --> 所属产品 * <!-- end-user-doc -->
	 */
	public void setEffortProduct(String effortProduct) {
		this.effortProduct = effortProduct;
	}

	public String getEffortProduct() {
		return effortProduct;
	}

	/**
	 * <!-- begin-user-doc --> 开始 * <!-- end-user-doc -->
	 */
	public void setEffortBegin(String effortBegin) {
		this.effortBegin = effortBegin;
	}

	public String getEffortBegin() {
		return effortBegin;
	}

	/**
	 * <!-- begin-user-doc --> 已关闭 * <!-- end-user-doc -->
	 */
	public void setEffortEnd(String effortEnd) {
		this.effortEnd = effortEnd;
	}

	public String getEffortEnd() {
		return effortEnd;
	}

}
