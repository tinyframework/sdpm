/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.pojo;

import java.util.Date;

/**
 * <!-- begin-user-doc --> 邮件队列 * <!-- end-user-doc -->
 */
public class SystemMailqueue {

	/**
	 * <!-- begin-user-doc --> 邮件队列ID * <!-- end-user-doc -->
	 */
	private Integer mailqueueId;

	/**
	 * <!-- begin-user-doc --> 发送给 * <!-- end-user-doc -->
	 */
	private String mailqueueToList;

	/**
	 * <!-- begin-user-doc --> 抄送给 * <!-- end-user-doc -->
	 */
	private String mailqueueCcList;

	/**
	 * <!-- begin-user-doc --> 邮箱队列主题 * <!-- end-user-doc -->
	 */
	private String mailqueueSubject;

	/**
	 * <!-- begin-user-doc --> 队列主体 * <!-- end-user-doc -->
	 */
	private String mailqueueBody;

	/**
	 * <!-- begin-user-doc --> 添加日期 * <!-- end-user-doc -->
	 */
	private Date mailqueueAddedDate;

	/**
	 * <!-- begin-user-doc --> 邮箱队列由谁添加 * <!-- end-user-doc -->
	 */
	private String mailqueueAddedBy;

	/**
	 * <!-- begin-user-doc --> 传送时间 * <!-- end-user-doc -->
	 */
	private Date mailqueueSendTime;

	/**
	 * <!-- begin-user-doc --> 队列状态 * <!-- end-user-doc -->
	 */
	private String mailqueueStatus;

	/**
	 * <!-- begin-user-doc --> 传送失败理由 * <!-- end-user-doc -->
	 */
	private String mailqueueFailReason;

	/**
	 * <!-- begin-user-doc --> 邮件队列ID * <!-- end-user-doc -->
	 */
	public void setMailqueueId(Integer mailqueueId) {
		this.mailqueueId = mailqueueId;
	}

	public Integer getMailqueueId() {
		return mailqueueId;
	}

	/**
	 * <!-- begin-user-doc --> 发送给 * <!-- end-user-doc -->
	 */
	public void setMailqueueToList(String mailqueueToList) {
		this.mailqueueToList = mailqueueToList;
	}

	public String getMailqueueToList() {
		return mailqueueToList;
	}

	/**
	 * <!-- begin-user-doc --> 抄送给 * <!-- end-user-doc -->
	 */
	public void setMailqueueCcList(String mailqueueCcList) {
		this.mailqueueCcList = mailqueueCcList;
	}

	public String getMailqueueCcList() {
		return mailqueueCcList;
	}

	/**
	 * <!-- begin-user-doc --> 邮箱队列主题 * <!-- end-user-doc -->
	 */
	public void setMailqueueSubject(String mailqueueSubject) {
		this.mailqueueSubject = mailqueueSubject;
	}

	public String getMailqueueSubject() {
		return mailqueueSubject;
	}

	/**
	 * <!-- begin-user-doc --> 队列主体 * <!-- end-user-doc -->
	 */
	public void setMailqueueBody(String mailqueueBody) {
		this.mailqueueBody = mailqueueBody;
	}

	public String getMailqueueBody() {
		return mailqueueBody;
	}

	/**
	 * <!-- begin-user-doc --> 添加日期 * <!-- end-user-doc -->
	 */
	public void setMailqueueAddedDate(Date mailqueueAddedDate) {
		this.mailqueueAddedDate = mailqueueAddedDate;
	}

	public Date getMailqueueAddedDate() {
		return mailqueueAddedDate;
	}

	/**
	 * <!-- begin-user-doc --> 邮箱队列由谁添加 * <!-- end-user-doc -->
	 */
	public void setMailqueueAddedBy(String mailqueueAddedBy) {
		this.mailqueueAddedBy = mailqueueAddedBy;
	}

	public String getMailqueueAddedBy() {
		return mailqueueAddedBy;
	}

	/**
	 * <!-- begin-user-doc --> 传送时间 * <!-- end-user-doc -->
	 */
	public void setMailqueueSendTime(Date mailqueueSendTime) {
		this.mailqueueSendTime = mailqueueSendTime;
	}

	public Date getMailqueueSendTime() {
		return mailqueueSendTime;
	}

	/**
	 * <!-- begin-user-doc --> 队列状态 * <!-- end-user-doc -->
	 */
	public void setMailqueueStatus(String mailqueueStatus) {
		this.mailqueueStatus = mailqueueStatus;
	}

	public String getMailqueueStatus() {
		return mailqueueStatus;
	}

	/**
	 * <!-- begin-user-doc --> 传送失败理由 * <!-- end-user-doc -->
	 */
	public void setMailqueueFailReason(String mailqueueFailReason) {
		this.mailqueueFailReason = mailqueueFailReason;
	}

	public String getMailqueueFailReason() {
		return mailqueueFailReason;
	}

}
