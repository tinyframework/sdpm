/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 附件表 * <!-- end-user-doc -->
 */
public class SystemProfile implements Serializable{

	public static final String DELETE_YES = "1";

	public static final String DELETE_NO = "0";

	/**
	 * <!-- begin-user-doc --> 附件ID * <!-- end-user-doc -->
	 */
	private Integer fileId;

	/**
	 * <!-- begin-user-doc --> 附件地址 * <!-- end-user-doc -->
	 */
	private String filePathname;

	/**
	 * <!-- begin-user-doc --> 附件名 * <!-- end-user-doc -->
	 */
	private String fileTitle;

	/**
	 * <!-- begin-user-doc --> 附件扩展名 * <!-- end-user-doc -->
	 */
	private String fileExtension;

	/**
	 * <!-- begin-user-doc --> 附件大小 * <!-- end-user-doc -->
	 */
	private Integer fileSize;

	/**
	 * <!-- begin-user-doc --> 附件文件类型 * <!-- end-user-doc -->
	 */
	private String fileObjectType;

	/**
	 * <!-- begin-user-doc --> 附件对象ID * <!-- end-user-doc -->
	 */
	private Integer fileObjectId;

	/**
	 * <!-- begin-user-doc --> 由谁添加 * <!-- end-user-doc -->
	 */
	private String fileAddedBy;

	/**
	 * <!-- begin-user-doc --> 附件添加日期 * <!-- end-user-doc -->
	 */
	private Date fileAddedDate;

	/**
	 * <!-- begin-user-doc --> 下载次数 * <!-- end-user-doc -->
	 */
	private Integer fileDownloads;

	/**
	 * <!-- begin-user-doc --> file_extra * <!-- end-user-doc -->
	 */
	private String fileExtra;

	/**
	 * <!-- begin-user-doc --> 是否删除 * <!-- end-user-doc -->
	 */
	private String fileDeleted;

	public SystemProfile() {
		this.fileDeleted = DELETE_NO;
	}

	public SystemProfile(String filePathname, String fileTitle,
			String fileExtension, Integer fileSize, String fileObjectType,
			Integer fileObjectId, String fileAddedBy, Date fileAddedDate) {
		this.filePathname = filePathname;
		this.fileTitle = fileTitle;
		this.fileExtension = fileExtension;
		this.fileSize = fileSize;
		this.fileObjectType = fileObjectType;
		this.fileObjectId = fileObjectId;
		this.fileAddedBy = fileAddedBy;
		this.fileAddedDate = fileAddedDate;
		this.fileDeleted = DELETE_NO;
	}

	/**
	 * <!-- begin-user-doc --> 附件ID * <!-- end-user-doc -->
	 */
	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public Integer getFileId() {
		return fileId;
	}

	/**
	 * <!-- begin-user-doc --> 附件地址 * <!-- end-user-doc -->
	 */
	public void setFilePathname(String filePathname) {
		this.filePathname = filePathname;
	}

	public String getFilePathname() {
		return filePathname;
	}

	/**
	 * <!-- begin-user-doc --> 附件名 * <!-- end-user-doc -->
	 */
	public void setFileTitle(String fileTitle) {
		this.fileTitle = fileTitle;
	}

	public String getFileTitle() {
		return fileTitle;
	}

	/**
	 * <!-- begin-user-doc --> 附件扩展名 * <!-- end-user-doc -->
	 */
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	/**
	 * <!-- begin-user-doc --> 附件大小 * <!-- end-user-doc -->
	 */
	public void setFileSize(Integer fileSize) {
		this.fileSize = fileSize;
	}

	public Integer getFileSize() {
		return fileSize;
	}

	/**
	 * <!-- begin-user-doc --> 附件文件类型 * <!-- end-user-doc -->
	 */
	public void setFileObjectType(String fileObjectType) {
		this.fileObjectType = fileObjectType;
	}

	public String getFileObjectType() {
		return fileObjectType;
	}

	/**
	 * <!-- begin-user-doc --> 附件对象ID * <!-- end-user-doc -->
	 */
	public void setFileObjectId(Integer fileObjectId) {
		this.fileObjectId = fileObjectId;
	}

	public Integer getFileObjectId() {
		return fileObjectId;
	}

	/**
	 * <!-- begin-user-doc --> 由谁添加 * <!-- end-user-doc -->
	 */
	public void setFileAddedBy(String fileAddedBy) {
		this.fileAddedBy = fileAddedBy;
	}

	public String getFileAddedBy() {
		return fileAddedBy;
	}

	/**
	 * <!-- begin-user-doc --> 附件添加日期 * <!-- end-user-doc -->
	 */
	public void setFileAddedDate(Date fileAddedDate) {
		this.fileAddedDate = fileAddedDate;
	}

	public Date getFileAddedDate() {
		return fileAddedDate;
	}

	/**
	 * <!-- begin-user-doc --> 下载次数 * <!-- end-user-doc -->
	 */
	public void setFileDownloads(Integer fileDownloads) {
		this.fileDownloads = fileDownloads;
	}

	public Integer getFileDownloads() {
		return fileDownloads;
	}

	/**
	 * <!-- begin-user-doc --> file_extra * <!-- end-user-doc -->
	 */
	public void setFileExtra(String fileExtra) {
		this.fileExtra = fileExtra;
	}

	public String getFileExtra() {
		return fileExtra;
	}

	/**
	 * <!-- begin-user-doc --> 是否删除 * <!-- end-user-doc -->
	 */
	public void setFileDeleted(String fileDeleted) {
		this.fileDeleted = fileDeleted;
	}

	public String getFileDeleted() {
		return fileDeleted;
	}

}
