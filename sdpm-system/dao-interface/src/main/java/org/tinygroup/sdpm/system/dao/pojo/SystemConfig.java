/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.pojo;

import java.io.Serializable;

/**
 * <!-- begin-user-doc --> 模块配置表 * <!-- end-user-doc -->
 */
public class SystemConfig implements Serializable{

	public static final int DELETE_YES = 1;

	public static final int DELETE_NO = 0;

	public static final String SEARCH_CONFIG = "searchConfig";

	/**
	 * <!-- begin-user-doc --> 配置ID * <!-- end-user-doc -->
	 */
	private Integer configId;

	/**
	 * <!-- begin-user-doc --> 配置创建人 * <!-- end-user-doc -->
	 */
	private String configOwner;

	/**
	 * <!-- begin-user-doc --> 配置模块 * <!-- end-user-doc -->
	 */
	private String configModule;

	/**
	 * <!-- begin-user-doc --> 配置部分 * <!-- end-user-doc -->
	 */
	private String configSection;

	/**
	 * <!-- begin-user-doc --> 配置的关键词 * <!-- end-user-doc -->
	 */
	private String configKey;

	/**
	 * <!-- begin-user-doc --> 配置的值 * <!-- end-user-doc -->
	 */
	private String configValue;

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	private Integer deleted;

	/**
	 * <!-- begin-user-doc --> 配置ID * <!-- end-user-doc -->
	 */
	public void setConfigId(Integer configId) {
		this.configId = configId;
	}

	public Integer getConfigId() {
		return configId;
	}

	/**
	 * <!-- begin-user-doc --> 配置创建人 * <!-- end-user-doc -->
	 */
	public void setConfigOwner(String configOwner) {
		this.configOwner = configOwner;
	}

	public String getConfigOwner() {
		return configOwner;
	}

	/**
	 * <!-- begin-user-doc --> 配置模块 * <!-- end-user-doc -->
	 */
	public void setConfigModule(String configModule) {
		this.configModule = configModule;
	}

	public String getConfigModule() {
		return configModule;
	}

	/**
	 * <!-- begin-user-doc --> 配置部分 * <!-- end-user-doc -->
	 */
	public void setConfigSection(String configSection) {
		this.configSection = configSection;
	}

	public String getConfigSection() {
		return configSection;
	}

	/**
	 * <!-- begin-user-doc --> 配置的关键词 * <!-- end-user-doc -->
	 */
	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public String getConfigKey() {
		return configKey;
	}

	/**
	 * <!-- begin-user-doc --> 配置的值 * <!-- end-user-doc -->
	 */
	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public String getConfigValue() {
		return configValue;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Integer getDeleted() {
		return deleted;
	}

}
