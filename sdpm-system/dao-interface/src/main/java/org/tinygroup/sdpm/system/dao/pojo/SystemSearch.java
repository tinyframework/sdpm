/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.pojo;

import java.util.Date;

/**
 * <!-- begin-user-doc --> 模块搜索表 * <!-- end-user-doc -->
 */
public class SystemSearch {

	public static final int DELETE_YES = 1;

	public static final int DELETE_NO = 0;

	/**
	 * <!-- begin-user-doc --> 搜索ID * <!-- end-user-doc -->
	 */
	private Integer searchId;

	/**
	 * <!-- begin-user-doc --> 搜索对象类型 * <!-- end-user-doc -->
	 */
	private String searchObjectType;

	/**
	 * <!-- begin-user-doc --> 搜索对象ID * <!-- end-user-doc -->
	 */
	private Integer searchObjectId;

	/**
	 * <!-- begin-user-doc --> 搜索名称 * <!-- end-user-doc -->
	 */
	private String searchTitle;

	/**
	 * <!-- begin-user-doc --> 搜索内容 * <!-- end-user-doc -->
	 */
	private String searchContent;

	/**
	 * <!-- begin-user-doc --> 搜索添加日期 * <!-- end-user-doc -->
	 */
	private Date searchAddedDate;

	/**
	 * <!-- begin-user-doc --> 搜索编辑日期 * <!-- end-user-doc -->
	 */
	private Date searchEditedDate;

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	private Integer deleted;

	/**
	 * <!-- begin-user-doc --> 搜索ID * <!-- end-user-doc -->
	 */
	public void setSearchId(Integer searchId) {
		this.searchId = searchId;
	}

	public Integer getSearchId() {
		return searchId;
	}

	/**
	 * <!-- begin-user-doc --> 搜索对象类型 * <!-- end-user-doc -->
	 */
	public void setSearchObjectType(String searchObjectType) {
		this.searchObjectType = searchObjectType;
	}

	public String getSearchObjectType() {
		return searchObjectType;
	}

	/**
	 * <!-- begin-user-doc --> 搜索对象ID * <!-- end-user-doc -->
	 */
	public void setSearchObjectId(Integer searchObjectId) {
		this.searchObjectId = searchObjectId;
	}

	public Integer getSearchObjectId() {
		return searchObjectId;
	}

	/**
	 * <!-- begin-user-doc --> 搜索名称 * <!-- end-user-doc -->
	 */
	public void setSearchTitle(String searchTitle) {
		this.searchTitle = searchTitle;
	}

	public String getSearchTitle() {
		return searchTitle;
	}

	/**
	 * <!-- begin-user-doc --> 搜索内容 * <!-- end-user-doc -->
	 */
	public void setSearchContent(String searchContent) {
		this.searchContent = searchContent;
	}

	public String getSearchContent() {
		return searchContent;
	}

	/**
	 * <!-- begin-user-doc --> 搜索添加日期 * <!-- end-user-doc -->
	 */
	public void setSearchAddedDate(Date searchAddedDate) {
		this.searchAddedDate = searchAddedDate;
	}

	public Date getSearchAddedDate() {
		return searchAddedDate;
	}

	/**
	 * <!-- begin-user-doc --> 搜索编辑日期 * <!-- end-user-doc -->
	 */
	public void setSearchEditedDate(Date searchEditedDate) {
		this.searchEditedDate = searchEditedDate;
	}

	public Date getSearchEditedDate() {
		return searchEditedDate;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Integer getDeleted() {
		return deleted;
	}

}
