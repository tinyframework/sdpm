/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 假期表 * <!-- end-user-doc -->
 */
public class Holiday implements Serializable{

	public static final int DELETE_YES = 1;

	public static final int DELETE_NO = 0;

	/**
	 * <!-- begin-user-doc --> 假期ID * <!-- end-user-doc -->
	 */
	private Integer holidayId;

	/**
	 * <!-- begin-user-doc --> 假期名 * <!-- end-user-doc -->
	 */
	private String holidayName;

	/**
	 * <!-- begin-user-doc --> 假期创建人 * <!-- end-user-doc -->
	 */
	private String holidayAccount;

	/**
	 * <!-- begin-user-doc --> 假期日期 * <!-- end-user-doc -->
	 */
	private String holidayDate;

	/**
	 * <!-- begin-user-doc --> 假期类型 * <!-- end-user-doc -->
	 */
	private String holidayType;

	/**
	 * <!-- begin-user-doc --> 删除标志位 * <!-- end-user-doc -->
	 */
	private Integer holidayDeleted;

	/**
	 * <!-- begin-user-doc --> 公司ID * <!-- end-user-doc -->
	 */
	private Integer companyId;

	/**
	 * <!-- begin-user-doc --> 假期描述 * <!-- end-user-doc -->
	 */
	private String holidayDetail;

	/**
	 * <!-- begin-user-doc --> 假期备注 * <!-- end-user-doc -->
	 */
	private String hoilidayRemark;

	/**
	 * 动作
	 */
	private String actionAction;

	/**
	 * 操作人ID
	 */
	private String actionActor;

	/**
	 * 操作时间
	 */
	private Date actionDate;

	public String getActionAction() {
		return actionAction;
	}

	public void setActionAction(String actionAction) {
		this.actionAction = actionAction;
	}

	public String getActionActor() {
		return actionActor;
	}

	public void setActionActor(String actionActor) {
		this.actionActor = actionActor;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	/**
	 * <!-- begin-user-doc --> 假期ID * <!-- end-user-doc -->
	 */
	public void setHolidayId(Integer holidayId) {
		this.holidayId = holidayId;
	}

	public Integer getHolidayId() {
		return holidayId;
	}

	/**
	 * <!-- begin-user-doc --> 假期名 * <!-- end-user-doc -->
	 */
	public void setHolidayName(String holidayName) {
		this.holidayName = holidayName;
	}

	public String getHolidayName() {
		return holidayName;
	}

	/**
	 * <!-- begin-user-doc --> 假期创建人 * <!-- end-user-doc -->
	 */
	public void setHolidayAccount(String holidayAccount) {
		this.holidayAccount = holidayAccount;
	}

	public String getHolidayAccount() {
		return holidayAccount;
	}

	/**
	 * <!-- begin-user-doc --> 假期日期 * <!-- end-user-doc -->
	 */
	public void setHolidayDate(String holidayDate) {
		this.holidayDate = holidayDate;
	}

	public String getHolidayDate() {
		return holidayDate;
	}

	/**
	 * <!-- begin-user-doc --> 假期类型 * <!-- end-user-doc -->
	 */
	public void setHolidayType(String holidayType) {
		this.holidayType = holidayType;
	}

	public String getHolidayType() {
		return holidayType;
	}

	/**
	 * <!-- begin-user-doc --> 删除标志位 * <!-- end-user-doc -->
	 */
	public void setHolidayDeleted(Integer holidayDeleted) {
		this.holidayDeleted = holidayDeleted;
	}

	public Integer getHolidayDeleted() {
		return holidayDeleted;
	}

	/**
	 * <!-- begin-user-doc --> 公司ID * <!-- end-user-doc -->
	 */
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	/**
	 * <!-- begin-user-doc --> 假期描述 * <!-- end-user-doc -->
	 */
	public void setHolidayDetail(String holidayDetail) {
		this.holidayDetail = holidayDetail;
	}

	public String getHolidayDetail() {
		return holidayDetail;
	}

	/**
	 * <!-- begin-user-doc --> 假期备注 * <!-- end-user-doc -->
	 */
	public void setHoilidayRemark(String hoilidayRemark) {
		this.hoilidayRemark = hoilidayRemark;
	}

	public String getHoilidayRemark() {
		return hoilidayRemark;
	}

}
