/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 系统日志表 * <!-- end-user-doc -->
 */
public class SystemAction implements Serializable, Comparable<SystemAction> {

	/**
	 * <!-- begin-user-doc --> 系统日志ID * <!-- end-user-doc -->
	 */
	private Integer actionId;

	/**
	 * <!-- begin-user-doc --> 对象类型 * <!-- end-user-doc -->
	 */
	private String actionObjectType;

	/**
	 * <!-- begin-user-doc --> 对象ID * <!-- end-user-doc -->
	 */
	private String actionObjectId;

	/**
	 * <!-- begin-user-doc --> 所属项目 * <!-- end-user-doc -->
	 */
	private String actionProject;

	/**
	 * <!-- begin-user-doc --> 所属产品 * <!-- end-user-doc -->
	 */
	private String actionProduct;

	/**
	 * <!-- begin-user-doc --> 操作者 * <!-- end-user-doc -->
	 */
	private String actionActor;

	/**
	 * <!-- begin-user-doc --> 系统日志日期 * <!-- end-user-doc -->
	 */
	private Date actionDate;

	/**
	 * <!-- begin-user-doc --> 注释 * <!-- end-user-doc -->
	 */
	private String actionComment;

	/**
	 * <!-- begin-user-doc --> EXTRA * <!-- end-user-doc -->
	 */
	private String actionExtra;

	/**
	 * <!-- begin-user-doc --> 是否已读 * <!-- end-user-doc -->
	 */
	private String actionRead;

	/**
	 * <!-- begin-user-doc --> ACTION_efforted * <!-- end-user-doc -->
	 */
	private Integer actionEfforted;

	/**
	 * 创建者名称
	 */
	private String actorName;

	/**
	 * 对象名称
	 */
	private String objectName;

	private String url;

	/**
	 * 所在星期几
	 */
	private String actionWeekDay;

	public String getActorName() {
		return actorName;
	}

	public void setActorName(String actorName) {
		this.actorName = actorName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	/**
	 * <!-- begin-user-doc --> 动作 * <!-- end-user-doc -->
	 */
	private String actionAction;

	/**
	 * <!-- begin-user-doc --> 系统日志ID * <!-- end-user-doc -->
	 */
	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}

	public Integer getActionId() {
		return actionId;
	}

	/**
	 * <!-- begin-user-doc --> 对象类型 * <!-- end-user-doc -->
	 */
	public void setActionObjectType(String actionObjectType) {
		this.actionObjectType = actionObjectType;
	}

	public String getActionObjectType() {
		return actionObjectType;
	}

	/**
	 * <!-- begin-user-doc --> 对象ID * <!-- end-user-doc -->
	 */
	public void setActionObjectId(String actionObjectId) {
		this.actionObjectId = actionObjectId;
	}

	public String getActionObjectId() {
		return actionObjectId;
	}

	/**
	 * <!-- begin-user-doc --> 所属项目 * <!-- end-user-doc -->
	 */
	public void setActionProject(String actionProject) {
		this.actionProject = actionProject;
	}

	public String getActionProject() {
		return actionProject;
	}

	/**
	 * <!-- begin-user-doc --> 所属产品 * <!-- end-user-doc -->
	 */
	public void setActionProduct(String actionProduct) {
		this.actionProduct = actionProduct;
	}

	public String getActionProduct() {
		return actionProduct;
	}

	/**
	 * <!-- begin-user-doc --> 操作者 * <!-- end-user-doc -->
	 */
	public void setActionActor(String actionActor) {
		this.actionActor = actionActor;
	}

	public String getActionActor() {
		return actionActor;
	}

	/**
	 * <!-- begin-user-doc --> 系统日志日期 * <!-- end-user-doc -->
	 */
	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public Date getActionDate() {
		return actionDate;
	}

	/**
	 * <!-- begin-user-doc --> 注释 * <!-- end-user-doc -->
	 */
	public void setActionComment(String actionComment) {
		this.actionComment = actionComment;
	}

	public String getActionComment() {
		return actionComment;
	}

	/**
	 * <!-- begin-user-doc --> EXTRA * <!-- end-user-doc -->
	 */
	public void setActionExtra(String actionExtra) {
		this.actionExtra = actionExtra;
	}

	public String getActionExtra() {
		return actionExtra;
	}

	/**
	 * <!-- begin-user-doc --> 是否已读 * <!-- end-user-doc -->
	 */
	public void setActionRead(String actionRead) {
		this.actionRead = actionRead;
	}

	public String getActionRead() {
		return actionRead;
	}

	/**
	 * <!-- begin-user-doc --> ACTION_efforted * <!-- end-user-doc -->
	 */
	public void setActionEfforted(Integer actionEfforted) {
		this.actionEfforted = actionEfforted;
	}

	public Integer getActionEfforted() {
		return actionEfforted;
	}

	/**
	 * <!-- begin-user-doc --> 动作 * <!-- end-user-doc -->
	 */
	public void setActionAction(String actionAction) {
		this.actionAction = actionAction;
	}

	public String getActionWeekDay() {
		return actionWeekDay;
	}

	public void setActionWeekDay(String actionWeekDay) {
		this.actionWeekDay = actionWeekDay;
	}

	public int compareTo(SystemAction o) {
		if (o == null)
			return 0;
		else {
			return this.actionDate.compareTo(o.getActionDate());
		}
	}

	public String getActionAction() {
		return actionAction;
	}

}
