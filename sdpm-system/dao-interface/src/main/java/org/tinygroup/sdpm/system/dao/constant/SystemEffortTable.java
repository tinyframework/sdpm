/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.constant;

import org.tinygroup.sdpm.system.dao.pojo.SystemEffort;
import org.tinygroup.tinysqldsl.base.Column;
import org.tinygroup.tinysqldsl.base.Table;

/**
 * <!-- begin-user-doc --> 日志表 * <!-- end-user-doc -->
 */
public class SystemEffortTable extends Table {

	public static final SystemEffortTable SYSTEM_EFFORTTABLE = new SystemEffortTable();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public static final SystemEffortTable SYSTEM_EFFORT_TABLE = new SystemEffortTable();

	/**
	 * <!-- begin-user-doc --> 日志编号 * <!-- end-user-doc -->
	 */
	public final Column EFFORT_ID = new Column(this, "effort_id");
	/**
	 * <!-- begin-user-doc --> 耗时 * <!-- end-user-doc -->
	 */
	public final Column EFFORT_CONSUMED = new Column(this, "effort_consumed");
	/**
	 * <!-- begin-user-doc --> 剩余 * <!-- end-user-doc -->
	 */
	public final Column EFFORT_LEFT = new Column(this, "effort_left");
	/**
	 * <!-- begin-user-doc --> 日期 * <!-- end-user-doc -->
	 */
	public final Column EFFORT_DATE = new Column(this, "effort_date");
	/**
	 * <!-- begin-user-doc --> 对象ID * <!-- end-user-doc -->
	 */
	public final Column EFFORT_OBJECT_ID = new Column(this, "effort_object_id");
	/**
	 * <!-- begin-user-doc --> 日志对象 * <!-- end-user-doc -->
	 */
	public final Column EFFORT_OBJECT_TYPE = new Column(this,
			"effort_object_type");
	/**
	 * <!-- begin-user-doc --> 工作内容 * <!-- end-user-doc -->
	 */
	public final Column EFFORT_WORK = new Column(this, "effort_work");
	/**
	 * <!-- begin-user-doc --> 登记人 * <!-- end-user-doc -->
	 */
	public final Column EFFORT_ACCOUNT = new Column(this, "effort_account");
	/**
	 * <!-- begin-user-doc --> 所属项目 * <!-- end-user-doc -->
	 */
	public final Column EFFORT_PROJECT = new Column(this, "effort_project");
	/**
	 * <!-- begin-user-doc --> 所属产品 * <!-- end-user-doc -->
	 */
	public final Column EFFORT_PRODUCT = new Column(this, "effort_product");
	/**
	 * <!-- begin-user-doc --> 开始 * <!-- end-user-doc -->
	 */
	public final Column EFFORT_BEGIN = new Column(this, "effort_begin");
	/**
	 * <!-- begin-user-doc --> 已关闭 * <!-- end-user-doc -->
	 */
	public final Column EFFORT_END = new Column(this, "effort_end");

	public SystemEffortTable() {
		super("system_effort");
	}

	public boolean isAutoGeneratedKeys() {
		return true;
	}

	public Class getPojoType() {
		return SystemEffort.class;
	}

}
