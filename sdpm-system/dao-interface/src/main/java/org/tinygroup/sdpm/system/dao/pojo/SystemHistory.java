/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.pojo;

import java.io.Serializable;

/**
 * <!-- begin-user-doc --> 操作历史表 * <!-- end-user-doc -->
 */
public class SystemHistory implements Serializable{

	/**
	 * <!-- begin-user-doc --> 操作历史ID * <!-- end-user-doc -->
	 */
	private Integer historyId;

	/**
	 * <!-- begin-user-doc --> 操作ID * <!-- end-user-doc -->
	 */
	private Integer historyAction;

	/**
	 * <!-- begin-user-doc --> 所属领域 * <!-- end-user-doc -->
	 */
	private String historyField;

	/**
	 * <!-- begin-user-doc --> 当前历史 * <!-- end-user-doc -->
	 */
	private String historyNew;

	/**
	 * <!-- begin-user-doc --> 上一条历史 * <!-- end-user-doc -->
	 */
	private String historyOld;

	/**
	 * <!-- begin-user-doc --> 对比 * <!-- end-user-doc -->
	 */
	private String historyDiff;

	/**
	 * <!-- begin-user-doc --> 操作历史ID * <!-- end-user-doc -->
	 */
	public void setHistoryId(Integer historyId) {
		this.historyId = historyId;
	}

	public Integer getHistoryId() {
		return historyId;
	}

	/**
	 * <!-- begin-user-doc --> 操作ID * <!-- end-user-doc -->
	 */
	public void setHistoryAction(Integer historyAction) {
		this.historyAction = historyAction;
	}

	public Integer getHistoryAction() {
		return historyAction;
	}

	/**
	 * <!-- begin-user-doc --> 所属领域 * <!-- end-user-doc -->
	 */
	public void setHistoryField(String historyField) {
		this.historyField = historyField;
	}

	public String getHistoryField() {
		return historyField;
	}

	/**
	 * <!-- begin-user-doc --> 当前历史 * <!-- end-user-doc -->
	 */
	public void setHistoryNew(String historyNew) {
		this.historyNew = historyNew;
	}

	public String getHistoryNew() {
		return historyNew;
	}

	/**
	 * <!-- begin-user-doc --> 上一条历史 * <!-- end-user-doc -->
	 */
	public void setHistoryOld(String historyOld) {
		this.historyOld = historyOld;
	}

	public String getHistoryOld() {
		return historyOld;
	}

	/**
	 * <!-- begin-user-doc --> 对比 * <!-- end-user-doc -->
	 */
	public void setHistoryDiff(String historyDiff) {
		this.historyDiff = historyDiff;
	}

	public String getHistoryDiff() {
		return historyDiff;
	}

}
