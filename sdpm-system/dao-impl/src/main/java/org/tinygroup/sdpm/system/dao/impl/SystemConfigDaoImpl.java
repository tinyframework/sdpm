/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.system.dao.constant.SystemConfigTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.system.dao.constant.SystemConfigTable.SYSTEM_CONFIGTABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.system.dao.pojo.SystemConfig;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.system.dao.SystemConfigDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class SystemConfigDaoImpl extends TinyDslDaoSupport implements
		SystemConfigDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public SystemConfig add(SystemConfig systemConfig) {
		return getDslTemplate().insertAndReturnKey(systemConfig,
				new InsertGenerateCallback<SystemConfig>() {
					public Insert generate(SystemConfig t) {
						Insert insert = insertInto(SYSTEM_CONFIG_TABLE).values(
								SYSTEM_CONFIG_TABLE.CONFIG_ID.value(t
										.getConfigId()),
								SYSTEM_CONFIG_TABLE.CONFIG_OWNER.value(t
										.getConfigOwner()),
								SYSTEM_CONFIG_TABLE.CONFIG_MODULE.value(t
										.getConfigModule()),
								SYSTEM_CONFIG_TABLE.CONFIG_SECTION.value(t
										.getConfigSection()),
								SYSTEM_CONFIG_TABLE.CONFIG_KEY.value(t
										.getConfigKey()),
								SYSTEM_CONFIG_TABLE.CONFIG_VALUE.value(t
										.getConfigValue()),
								SYSTEM_CONFIG_TABLE.DELETED.value(t
										.getDeleted())

						);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(SystemConfig systemConfig) {
		if (systemConfig == null || systemConfig.getConfigId() == null) {
			return 0;
		}
		return getDslTemplate().update(systemConfig,
				new UpdateGenerateCallback<SystemConfig>() {
					public Update generate(SystemConfig t) {
						Update update = update(SYSTEM_CONFIG_TABLE).set(
								SYSTEM_CONFIG_TABLE.CONFIG_OWNER.value(t
										.getConfigOwner()),
								SYSTEM_CONFIG_TABLE.CONFIG_MODULE.value(t
										.getConfigModule()),
								SYSTEM_CONFIG_TABLE.CONFIG_SECTION.value(t
										.getConfigSection()),
								SYSTEM_CONFIG_TABLE.CONFIG_KEY.value(t
										.getConfigKey()),
								SYSTEM_CONFIG_TABLE.CONFIG_VALUE.value(t
										.getConfigValue()),
								SYSTEM_CONFIG_TABLE.DELETED.value(t
										.getDeleted())).where(
								SYSTEM_CONFIG_TABLE.CONFIG_ID.eq(t
										.getConfigId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(SYSTEM_CONFIG_TABLE).where(
								SYSTEM_CONFIG_TABLE.CONFIG_ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(SYSTEM_CONFIG_TABLE).where(
								SYSTEM_CONFIG_TABLE.CONFIG_ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public SystemConfig getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, SystemConfig.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(SYSTEM_CONFIG_TABLE).where(
								SYSTEM_CONFIG_TABLE.CONFIG_ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<SystemConfig> query(SystemConfig systemConfig,
			final OrderBy... orderArgs) {
		if (systemConfig == null) {
			systemConfig = new SystemConfig();
		}
		return getDslTemplate().query(systemConfig,
				new SelectGenerateCallback<SystemConfig>() {
					@SuppressWarnings("rawtypes")
					public Select generate(SystemConfig t) {
						Select select = selectFrom(SYSTEM_CONFIG_TABLE).where(
								and(SYSTEM_CONFIG_TABLE.CONFIG_ID.eq(t
										.getConfigId()),
										SYSTEM_CONFIG_TABLE.CONFIG_OWNER.eq(t
												.getConfigOwner()),
										SYSTEM_CONFIG_TABLE.CONFIG_MODULE.eq(t
												.getConfigModule()),
										SYSTEM_CONFIG_TABLE.CONFIG_SECTION.eq(t
												.getConfigSection()),
										SYSTEM_CONFIG_TABLE.CONFIG_KEY.eq(t
												.getConfigKey()),
										SYSTEM_CONFIG_TABLE.CONFIG_VALUE.eq(t
												.getConfigValue()),
										SYSTEM_CONFIG_TABLE.DELETED.eq(t
												.getDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<SystemConfig> queryPager(int start, int limit,
			SystemConfig systemConfig, final OrderBy... orderArgs) {
		if (systemConfig == null) {
			systemConfig = new SystemConfig();
		}
		return getDslTemplate().queryPager(start, limit, systemConfig, false,
				new SelectGenerateCallback<SystemConfig>() {
					public Select generate(SystemConfig t) {
						Select select = Select.selectFrom(SYSTEM_CONFIG_TABLE)
								.where(and(SYSTEM_CONFIG_TABLE.CONFIG_ID.eq(t
										.getConfigId()),
										SYSTEM_CONFIG_TABLE.CONFIG_OWNER.eq(t
												.getConfigOwner()),
										SYSTEM_CONFIG_TABLE.CONFIG_MODULE.eq(t
												.getConfigModule()),
										SYSTEM_CONFIG_TABLE.CONFIG_SECTION.eq(t
												.getConfigSection()),
										SYSTEM_CONFIG_TABLE.CONFIG_KEY.eq(t
												.getConfigKey()),
										SYSTEM_CONFIG_TABLE.CONFIG_VALUE.eq(t
												.getConfigValue()),
										SYSTEM_CONFIG_TABLE.DELETED.eq(t
												.getDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<SystemConfig> systemConfig) {
		if (CollectionUtil.isEmpty(systemConfig)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, systemConfig,
				new InsertGenerateCallback<SystemConfig>() {

					public Insert generate(SystemConfig t) {
						return insertInto(SYSTEM_CONFIG_TABLE).values(
								SYSTEM_CONFIG_TABLE.CONFIG_ID.value(t
										.getConfigId()),
								SYSTEM_CONFIG_TABLE.CONFIG_OWNER.value(t
										.getConfigOwner()),
								SYSTEM_CONFIG_TABLE.CONFIG_MODULE.value(t
										.getConfigModule()),
								SYSTEM_CONFIG_TABLE.CONFIG_SECTION.value(t
										.getConfigSection()),
								SYSTEM_CONFIG_TABLE.CONFIG_KEY.value(t
										.getConfigKey()),
								SYSTEM_CONFIG_TABLE.CONFIG_VALUE.value(t
										.getConfigValue()),
								SYSTEM_CONFIG_TABLE.DELETED.value(t
										.getDeleted())

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<SystemConfig> systemConfigs) {
		return batchInsert(true, systemConfigs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<SystemConfig> systemConfig) {
		if (CollectionUtil.isEmpty(systemConfig)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(systemConfig,
				new UpdateGenerateCallback<SystemConfig>() {
					public Update generate(SystemConfig t) {
						return update(SYSTEM_CONFIG_TABLE).set(
								SYSTEM_CONFIG_TABLE.CONFIG_OWNER.value(t
										.getConfigOwner()),
								SYSTEM_CONFIG_TABLE.CONFIG_MODULE.value(t
										.getConfigModule()),
								SYSTEM_CONFIG_TABLE.CONFIG_SECTION.value(t
										.getConfigSection()),
								SYSTEM_CONFIG_TABLE.CONFIG_KEY.value(t
										.getConfigKey()),
								SYSTEM_CONFIG_TABLE.CONFIG_VALUE.value(t
										.getConfigValue()),
								SYSTEM_CONFIG_TABLE.DELETED.value(t
										.getDeleted())

						).where(SYSTEM_CONFIG_TABLE.CONFIG_ID.eq(t
								.getConfigId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<SystemConfig> systemConfig) {
		if (CollectionUtil.isEmpty(systemConfig)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(systemConfig,
				new DeleteGenerateCallback<SystemConfig>() {
					public Delete generate(SystemConfig t) {
						return delete(SYSTEM_CONFIG_TABLE).where(
								and(SYSTEM_CONFIG_TABLE.CONFIG_ID.eq(t
										.getConfigId()),
										SYSTEM_CONFIG_TABLE.CONFIG_OWNER.eq(t
												.getConfigOwner()),
										SYSTEM_CONFIG_TABLE.CONFIG_MODULE.eq(t
												.getConfigModule()),
										SYSTEM_CONFIG_TABLE.CONFIG_SECTION.eq(t
												.getConfigSection()),
										SYSTEM_CONFIG_TABLE.CONFIG_KEY.eq(t
												.getConfigKey()),
										SYSTEM_CONFIG_TABLE.CONFIG_VALUE.eq(t
												.getConfigValue()),
										SYSTEM_CONFIG_TABLE.DELETED.eq(t
												.getDeleted())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = orderBies[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	public SystemConfig getConfigBySection(String section) {
		Select select = selectFrom(SYSTEM_CONFIGTABLE).where(
				SYSTEM_CONFIGTABLE.CONFIG_SECTION.eq(section));
		return getDslSession().fetchOneResult(select, SystemConfig.class);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<SystemConfig> systemConfig) {
		if (CollectionUtil.isEmpty(systemConfig)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, systemConfig,
				new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(SYSTEM_CONFIG_TABLE)
								.values(SYSTEM_CONFIG_TABLE.CONFIG_OWNER
										.value(new JdbcNamedParameter(
												"configOwner")),
										SYSTEM_CONFIG_TABLE.CONFIG_MODULE
												.value(new JdbcNamedParameter(
														"configModule")),
										SYSTEM_CONFIG_TABLE.CONFIG_SECTION
												.value(new JdbcNamedParameter(
														"configSection")),
										SYSTEM_CONFIG_TABLE.CONFIG_KEY
												.value(new JdbcNamedParameter(
														"configKey")),
										SYSTEM_CONFIG_TABLE.CONFIG_VALUE
												.value(new JdbcNamedParameter(
														"configValue")),
										SYSTEM_CONFIG_TABLE.DELETED
												.value(new JdbcNamedParameter(
														"deleted"))

								);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<SystemConfig> systemConfig) {
		if (CollectionUtil.isEmpty(systemConfig)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(systemConfig,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(SYSTEM_CONFIG_TABLE)
								.set(SYSTEM_CONFIG_TABLE.CONFIG_OWNER
										.value(new JdbcNamedParameter(
												"configOwner")),
										SYSTEM_CONFIG_TABLE.CONFIG_MODULE
												.value(new JdbcNamedParameter(
														"configModule")),
										SYSTEM_CONFIG_TABLE.CONFIG_SECTION
												.value(new JdbcNamedParameter(
														"configSection")),
										SYSTEM_CONFIG_TABLE.CONFIG_KEY
												.value(new JdbcNamedParameter(
														"configKey")),
										SYSTEM_CONFIG_TABLE.CONFIG_VALUE
												.value(new JdbcNamedParameter(
														"configValue")),
										SYSTEM_CONFIG_TABLE.DELETED
												.value(new JdbcNamedParameter(
														"deleted"))

								)
								.where(SYSTEM_CONFIG_TABLE.CONFIG_ID
										.eq(new JdbcNamedParameter("configId")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<SystemConfig> systemConfig) {
		if (CollectionUtil.isEmpty(systemConfig)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(systemConfig,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(SYSTEM_CONFIG_TABLE).where(
								and(SYSTEM_CONFIG_TABLE.CONFIG_OWNER
										.eq(new JdbcNamedParameter(
												"configOwner")),
										SYSTEM_CONFIG_TABLE.CONFIG_MODULE
												.eq(new JdbcNamedParameter(
														"configModule")),
										SYSTEM_CONFIG_TABLE.CONFIG_SECTION
												.eq(new JdbcNamedParameter(
														"configSection")),
										SYSTEM_CONFIG_TABLE.CONFIG_KEY
												.eq(new JdbcNamedParameter(
														"configKey")),
										SYSTEM_CONFIG_TABLE.CONFIG_VALUE
												.eq(new JdbcNamedParameter(
														"configValue")),
										SYSTEM_CONFIG_TABLE.DELETED
												.eq(new JdbcNamedParameter(
														"deleted"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<SystemConfig> systemConfig) {
		return preparedBatchInsert(true, systemConfig);
	}

}
