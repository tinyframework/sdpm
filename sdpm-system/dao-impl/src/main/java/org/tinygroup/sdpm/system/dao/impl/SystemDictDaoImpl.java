/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.system.dao.constant.SystemDictTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.system.dao.constant.SystemDictTable.SYSTEM_DICTTABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.SimpleDslSession;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.system.dao.pojo.SystemDict;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.system.dao.SystemDictDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class SystemDictDaoImpl extends TinyDslDaoSupport implements
		SystemDictDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public SystemDict add(SystemDict systemDict) {
		return getDslTemplate().insertAndReturnKey(systemDict,
				new InsertGenerateCallback<SystemDict>() {
					public Insert generate(SystemDict t) {
						Insert insert = insertInto(SYSTEM_DICT_TABLE)
								.values(SYSTEM_DICT_TABLE.DICT_ID.value(t
										.getDictId()),
										SYSTEM_DICT_TABLE.DICT_KEY.value(t
												.getDictKey()),
										SYSTEM_DICT_TABLE.DICT_VALUE.value(t
												.getDictValue()),
										SYSTEM_DICT_TABLE.DICT_SORT.value(t
												.getDictSort()),
										SYSTEM_DICT_TABLE.MODULE_ID.value(t
												.getModuleId()),
										SYSTEM_DICT_TABLE.DELETED.value(t
												.getDeleted())

								);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(SystemDict systemDict) {
		if (systemDict == null || systemDict.getDictId() == null) {
			return 0;
		}
		return getDslTemplate().update(systemDict,
				new UpdateGenerateCallback<SystemDict>() {
					public Update generate(SystemDict t) {
						Update update = update(SYSTEM_DICT_TABLE)
								.set(SYSTEM_DICT_TABLE.DICT_KEY.value(t
										.getDictKey()),
										SYSTEM_DICT_TABLE.DICT_VALUE.value(t
												.getDictValue()),
										SYSTEM_DICT_TABLE.DICT_SORT.value(t
												.getDictSort()),
										SYSTEM_DICT_TABLE.MODULE_ID.value(t
												.getModuleId()),
										SYSTEM_DICT_TABLE.DELETED.value(t
												.getDeleted())).where(
										SYSTEM_DICT_TABLE.DICT_ID.eq(t
												.getDictId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(SYSTEM_DICT_TABLE).where(
								SYSTEM_DICT_TABLE.DICT_ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(SYSTEM_DICT_TABLE).where(
								SYSTEM_DICT_TABLE.DICT_ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public SystemDict getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, SystemDict.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(SYSTEM_DICT_TABLE).where(
								SYSTEM_DICT_TABLE.DICT_ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<SystemDict> query(SystemDict systemDict,
			final OrderBy... orderArgs) {
		if (systemDict == null) {
			systemDict = new SystemDict();
		}
		return getDslTemplate().query(systemDict,
				new SelectGenerateCallback<SystemDict>() {
					@SuppressWarnings("rawtypes")
					public Select generate(SystemDict t) {
						Select select = selectFrom(SYSTEM_DICT_TABLE)
								.where(and(SYSTEM_DICT_TABLE.DICT_ID.eq(t
										.getDictId()),
										SYSTEM_DICT_TABLE.DICT_KEY.eq(t
												.getDictKey()),
										SYSTEM_DICT_TABLE.DICT_VALUE.eq(t
												.getDictValue()),
										SYSTEM_DICT_TABLE.DICT_SORT.eq(t
												.getDictSort()),
										SYSTEM_DICT_TABLE.MODULE_ID.eq(t
												.getModuleId()),
										SYSTEM_DICT_TABLE.DELETED.eq(t
												.getDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<SystemDict> queryPager(int start, int limit,
			SystemDict systemDict, final OrderBy... orderArgs) {
		if (systemDict == null) {
			systemDict = new SystemDict();
		}
		return getDslTemplate().queryPager(start, limit, systemDict, false,
				new SelectGenerateCallback<SystemDict>() {
					public Select generate(SystemDict t) {
						Select select = Select.selectFrom(SYSTEM_DICT_TABLE)
								.where(and(SYSTEM_DICT_TABLE.DICT_ID.eq(t
										.getDictId()),
										SYSTEM_DICT_TABLE.DICT_KEY.eq(t
												.getDictKey()),
										SYSTEM_DICT_TABLE.DICT_VALUE.eq(t
												.getDictValue()),
										SYSTEM_DICT_TABLE.DICT_SORT.eq(t
												.getDictSort()),
										SYSTEM_DICT_TABLE.MODULE_ID.eq(t
												.getModuleId()),
										SYSTEM_DICT_TABLE.DELETED.eq(t
												.getDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<SystemDict> systemDict) {
		if (CollectionUtil.isEmpty(systemDict)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, systemDict,
				new InsertGenerateCallback<SystemDict>() {

					public Insert generate(SystemDict t) {
						return insertInto(SYSTEM_DICT_TABLE)
								.values(SYSTEM_DICT_TABLE.DICT_ID.value(t
										.getDictId()),
										SYSTEM_DICT_TABLE.DICT_KEY.value(t
												.getDictKey()),
										SYSTEM_DICT_TABLE.DICT_VALUE.value(t
												.getDictValue()),
										SYSTEM_DICT_TABLE.DICT_SORT.value(t
												.getDictSort()),
										SYSTEM_DICT_TABLE.MODULE_ID.value(t
												.getModuleId()),
										SYSTEM_DICT_TABLE.DELETED.value(t
												.getDeleted())

								);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<SystemDict> systemDicts) {
		return batchInsert(true, systemDicts);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<SystemDict> systemDict) {
		if (CollectionUtil.isEmpty(systemDict)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(systemDict,
				new UpdateGenerateCallback<SystemDict>() {
					public Update generate(SystemDict t) {
						return update(SYSTEM_DICT_TABLE)
								.set(SYSTEM_DICT_TABLE.DICT_KEY.value(t
										.getDictKey()),
										SYSTEM_DICT_TABLE.DICT_VALUE.value(t
												.getDictValue()),
										SYSTEM_DICT_TABLE.DICT_SORT.value(t
												.getDictSort()),
										SYSTEM_DICT_TABLE.MODULE_ID.value(t
												.getModuleId()),
										SYSTEM_DICT_TABLE.DELETED.value(t
												.getDeleted())

								).where(SYSTEM_DICT_TABLE.DICT_ID.eq(t
										.getDictId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<SystemDict> systemDict) {
		if (CollectionUtil.isEmpty(systemDict)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(systemDict,
				new DeleteGenerateCallback<SystemDict>() {
					public Delete generate(SystemDict t) {
						return delete(SYSTEM_DICT_TABLE)
								.where(and(SYSTEM_DICT_TABLE.DICT_ID.eq(t
										.getDictId()),
										SYSTEM_DICT_TABLE.DICT_KEY.eq(t
												.getDictKey()),
										SYSTEM_DICT_TABLE.DICT_VALUE.eq(t
												.getDictValue()),
										SYSTEM_DICT_TABLE.DICT_SORT.eq(t
												.getDictSort()),
										SYSTEM_DICT_TABLE.MODULE_ID.eq(t
												.getModuleId()),
										SYSTEM_DICT_TABLE.DELETED.eq(t
												.getDeleted())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = null;
			if (orderBies[i] != null) {
				tempElement = orderBies[i].getOrderByElement();
			}
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	public void deleteAll() {
		SimpleDslSession simpleDslSession = (SimpleDslSession) this
				.getDslSession();
		simpleDslSession.getJdbcTemplate()
				.execute("truncate table system_dict");
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<SystemDict> systemDict) {
		if (CollectionUtil.isEmpty(systemDict)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, systemDict,
				new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(SYSTEM_DICT_TABLE)
								.values(SYSTEM_DICT_TABLE.DICT_KEY
										.value(new JdbcNamedParameter("dictKey")),
										SYSTEM_DICT_TABLE.DICT_VALUE
												.value(new JdbcNamedParameter(
														"dictValue")),
										SYSTEM_DICT_TABLE.DICT_SORT
												.value(new JdbcNamedParameter(
														"dictSort")),
										SYSTEM_DICT_TABLE.MODULE_ID
												.value(new JdbcNamedParameter(
														"moduleId")),
										SYSTEM_DICT_TABLE.DELETED
												.value(new JdbcNamedParameter(
														"deleted"))

								);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<SystemDict> systemDict) {
		if (CollectionUtil.isEmpty(systemDict)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(systemDict,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(SYSTEM_DICT_TABLE)
								.set(SYSTEM_DICT_TABLE.DICT_KEY
										.value(new JdbcNamedParameter("dictKey")),
										SYSTEM_DICT_TABLE.DICT_VALUE
												.value(new JdbcNamedParameter(
														"dictValue")),
										SYSTEM_DICT_TABLE.DICT_SORT
												.value(new JdbcNamedParameter(
														"dictSort")),
										SYSTEM_DICT_TABLE.MODULE_ID
												.value(new JdbcNamedParameter(
														"moduleId")),
										SYSTEM_DICT_TABLE.DELETED
												.value(new JdbcNamedParameter(
														"deleted"))

								).where(SYSTEM_DICT_TABLE.DICT_ID
										.eq(new JdbcNamedParameter("dictId")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<SystemDict> systemDict) {
		if (CollectionUtil.isEmpty(systemDict)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(systemDict,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(SYSTEM_DICT_TABLE).where(
								and(SYSTEM_DICT_TABLE.DICT_KEY
										.eq(new JdbcNamedParameter("dictKey")),
										SYSTEM_DICT_TABLE.DICT_VALUE
												.eq(new JdbcNamedParameter(
														"dictValue")),
										SYSTEM_DICT_TABLE.DICT_SORT
												.eq(new JdbcNamedParameter(
														"dictSort")),
										SYSTEM_DICT_TABLE.MODULE_ID
												.eq(new JdbcNamedParameter(
														"moduleId")),
										SYSTEM_DICT_TABLE.DELETED
												.eq(new JdbcNamedParameter(
														"deleted"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<SystemDict> systemDict) {
		return preparedBatchInsert(true, systemDict);
	}

}
