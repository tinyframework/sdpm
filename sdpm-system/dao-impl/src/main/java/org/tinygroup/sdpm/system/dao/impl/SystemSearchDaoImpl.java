/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.system.dao.constant.SystemSearchTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.system.dao.constant.SystemSearchTable.SYSTEM_SEARCHTABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.system.dao.pojo.SystemSearch;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.system.dao.SystemSearchDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class SystemSearchDaoImpl extends TinyDslDaoSupport implements
		SystemSearchDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public SystemSearch add(SystemSearch systemSearch) {
		return getDslTemplate().insertAndReturnKey(systemSearch,
				new InsertGenerateCallback<SystemSearch>() {
					public Insert generate(SystemSearch t) {
						Insert insert = insertInto(SYSTEM_SEARCH_TABLE).values(
								SYSTEM_SEARCH_TABLE.SEARCH_ID.value(t
										.getSearchId()),
								SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_TYPE.value(t
										.getSearchObjectType()),
								SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_ID.value(t
										.getSearchObjectId()),
								SYSTEM_SEARCH_TABLE.SEARCH_TITLE.value(t
										.getSearchTitle()),
								SYSTEM_SEARCH_TABLE.SEARCH_CONTENT.value(t
										.getSearchContent()),
								SYSTEM_SEARCH_TABLE.SEARCH_ADDED_DATE.value(t
										.getSearchAddedDate()),
								SYSTEM_SEARCH_TABLE.SEARCH_EDITED_DATE.value(t
										.getSearchEditedDate()),
								SYSTEM_SEARCH_TABLE.DELETED.value(t
										.getDeleted())

						);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(SystemSearch systemSearch) {
		if (systemSearch == null || systemSearch.getSearchId() == null) {
			return 0;
		}
		return getDslTemplate().update(systemSearch,
				new UpdateGenerateCallback<SystemSearch>() {
					public Update generate(SystemSearch t) {
						Update update = update(SYSTEM_SEARCH_TABLE).set(
								SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_TYPE.value(t
										.getSearchObjectType()),
								SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_ID.value(t
										.getSearchObjectId()),
								SYSTEM_SEARCH_TABLE.SEARCH_TITLE.value(t
										.getSearchTitle()),
								SYSTEM_SEARCH_TABLE.SEARCH_CONTENT.value(t
										.getSearchContent()),
								SYSTEM_SEARCH_TABLE.SEARCH_ADDED_DATE.value(t
										.getSearchAddedDate()),
								SYSTEM_SEARCH_TABLE.SEARCH_EDITED_DATE.value(t
										.getSearchEditedDate()),
								SYSTEM_SEARCH_TABLE.DELETED.value(t
										.getDeleted())).where(
								SYSTEM_SEARCH_TABLE.SEARCH_ID.eq(t
										.getSearchId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(SYSTEM_SEARCH_TABLE).where(
								SYSTEM_SEARCH_TABLE.SEARCH_ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(SYSTEM_SEARCH_TABLE).where(
								SYSTEM_SEARCH_TABLE.SEARCH_ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public SystemSearch getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, SystemSearch.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(SYSTEM_SEARCH_TABLE).where(
								SYSTEM_SEARCH_TABLE.SEARCH_ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<SystemSearch> query(SystemSearch systemSearch,
			final OrderBy... orderArgs) {
		if (systemSearch == null) {
			systemSearch = new SystemSearch();
		}
		return getDslTemplate().query(systemSearch,
				new SelectGenerateCallback<SystemSearch>() {
					@SuppressWarnings("rawtypes")
					public Select generate(SystemSearch t) {
						Select select = selectFrom(SYSTEM_SEARCH_TABLE).where(
								and(SYSTEM_SEARCH_TABLE.SEARCH_ID.eq(t
										.getSearchId()),
										SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_TYPE
												.eq(t.getSearchObjectType()),
										SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_ID
												.eq(t.getSearchObjectId()),
										SYSTEM_SEARCH_TABLE.SEARCH_TITLE.eq(t
												.getSearchTitle()),
										SYSTEM_SEARCH_TABLE.SEARCH_CONTENT.eq(t
												.getSearchContent()),
										SYSTEM_SEARCH_TABLE.SEARCH_ADDED_DATE
												.eq(t.getSearchAddedDate()),
										SYSTEM_SEARCH_TABLE.SEARCH_EDITED_DATE
												.eq(t.getSearchEditedDate()),
										SYSTEM_SEARCH_TABLE.DELETED.eq(t
												.getDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<SystemSearch> queryPager(int start, int limit,
			SystemSearch systemSearch, final OrderBy... orderArgs) {
		if (systemSearch == null) {
			systemSearch = new SystemSearch();
		}
		return getDslTemplate().queryPager(start, limit, systemSearch, false,
				new SelectGenerateCallback<SystemSearch>() {
					public Select generate(SystemSearch t) {
						Select select = Select.selectFrom(SYSTEM_SEARCH_TABLE)
								.where(and(SYSTEM_SEARCH_TABLE.SEARCH_ID.eq(t
										.getSearchId()),
										SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_TYPE
												.eq(t.getSearchObjectType()),
										SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_ID
												.eq(t.getSearchObjectId()),
										SYSTEM_SEARCH_TABLE.SEARCH_TITLE.eq(t
												.getSearchTitle()),
										SYSTEM_SEARCH_TABLE.SEARCH_CONTENT.eq(t
												.getSearchContent()),
										SYSTEM_SEARCH_TABLE.SEARCH_ADDED_DATE
												.eq(t.getSearchAddedDate()),
										SYSTEM_SEARCH_TABLE.SEARCH_EDITED_DATE
												.eq(t.getSearchEditedDate()),
										SYSTEM_SEARCH_TABLE.DELETED.eq(t
												.getDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<SystemSearch> systemSearch) {
		if (CollectionUtil.isEmpty(systemSearch)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, systemSearch,
				new InsertGenerateCallback<SystemSearch>() {

					public Insert generate(SystemSearch t) {
						return insertInto(SYSTEM_SEARCH_TABLE).values(
								SYSTEM_SEARCH_TABLE.SEARCH_ID.value(t
										.getSearchId()),
								SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_TYPE.value(t
										.getSearchObjectType()),
								SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_ID.value(t
										.getSearchObjectId()),
								SYSTEM_SEARCH_TABLE.SEARCH_TITLE.value(t
										.getSearchTitle()),
								SYSTEM_SEARCH_TABLE.SEARCH_CONTENT.value(t
										.getSearchContent()),
								SYSTEM_SEARCH_TABLE.SEARCH_ADDED_DATE.value(t
										.getSearchAddedDate()),
								SYSTEM_SEARCH_TABLE.SEARCH_EDITED_DATE.value(t
										.getSearchEditedDate()),
								SYSTEM_SEARCH_TABLE.DELETED.value(t
										.getDeleted())

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<SystemSearch> systemSearchs) {
		return batchInsert(true, systemSearchs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<SystemSearch> systemSearch) {
		if (CollectionUtil.isEmpty(systemSearch)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(systemSearch,
				new UpdateGenerateCallback<SystemSearch>() {
					public Update generate(SystemSearch t) {
						return update(SYSTEM_SEARCH_TABLE).set(
								SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_TYPE.value(t
										.getSearchObjectType()),
								SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_ID.value(t
										.getSearchObjectId()),
								SYSTEM_SEARCH_TABLE.SEARCH_TITLE.value(t
										.getSearchTitle()),
								SYSTEM_SEARCH_TABLE.SEARCH_CONTENT.value(t
										.getSearchContent()),
								SYSTEM_SEARCH_TABLE.SEARCH_ADDED_DATE.value(t
										.getSearchAddedDate()),
								SYSTEM_SEARCH_TABLE.SEARCH_EDITED_DATE.value(t
										.getSearchEditedDate()),
								SYSTEM_SEARCH_TABLE.DELETED.value(t
										.getDeleted())

						).where(SYSTEM_SEARCH_TABLE.SEARCH_ID.eq(t
								.getSearchId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<SystemSearch> systemSearch) {
		if (CollectionUtil.isEmpty(systemSearch)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(systemSearch,
				new DeleteGenerateCallback<SystemSearch>() {
					public Delete generate(SystemSearch t) {
						return delete(SYSTEM_SEARCH_TABLE).where(
								and(SYSTEM_SEARCH_TABLE.SEARCH_ID.eq(t
										.getSearchId()),
										SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_TYPE
												.eq(t.getSearchObjectType()),
										SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_ID
												.eq(t.getSearchObjectId()),
										SYSTEM_SEARCH_TABLE.SEARCH_TITLE.eq(t
												.getSearchTitle()),
										SYSTEM_SEARCH_TABLE.SEARCH_CONTENT.eq(t
												.getSearchContent()),
										SYSTEM_SEARCH_TABLE.SEARCH_ADDED_DATE
												.eq(t.getSearchAddedDate()),
										SYSTEM_SEARCH_TABLE.SEARCH_EDITED_DATE
												.eq(t.getSearchEditedDate()),
										SYSTEM_SEARCH_TABLE.DELETED.eq(t
												.getDeleted())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = orderBies[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<SystemSearch> systemSearch) {
		if (CollectionUtil.isEmpty(systemSearch)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, systemSearch,
				new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(SYSTEM_SEARCH_TABLE)
								.values(SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_TYPE
										.value(new JdbcNamedParameter(
												"searchObjectType")),
										SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_ID
												.value(new JdbcNamedParameter(
														"searchObjectId")),
										SYSTEM_SEARCH_TABLE.SEARCH_TITLE
												.value(new JdbcNamedParameter(
														"searchTitle")),
										SYSTEM_SEARCH_TABLE.SEARCH_CONTENT
												.value(new JdbcNamedParameter(
														"searchContent")),
										SYSTEM_SEARCH_TABLE.SEARCH_ADDED_DATE
												.value(new JdbcNamedParameter(
														"searchAddedDate")),
										SYSTEM_SEARCH_TABLE.SEARCH_EDITED_DATE
												.value(new JdbcNamedParameter(
														"searchEditedDate")),
										SYSTEM_SEARCH_TABLE.DELETED
												.value(new JdbcNamedParameter(
														"deleted"))

								);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<SystemSearch> systemSearch) {
		if (CollectionUtil.isEmpty(systemSearch)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(systemSearch,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(SYSTEM_SEARCH_TABLE)
								.set(SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_TYPE
										.value(new JdbcNamedParameter(
												"searchObjectType")),
										SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_ID
												.value(new JdbcNamedParameter(
														"searchObjectId")),
										SYSTEM_SEARCH_TABLE.SEARCH_TITLE
												.value(new JdbcNamedParameter(
														"searchTitle")),
										SYSTEM_SEARCH_TABLE.SEARCH_CONTENT
												.value(new JdbcNamedParameter(
														"searchContent")),
										SYSTEM_SEARCH_TABLE.SEARCH_ADDED_DATE
												.value(new JdbcNamedParameter(
														"searchAddedDate")),
										SYSTEM_SEARCH_TABLE.SEARCH_EDITED_DATE
												.value(new JdbcNamedParameter(
														"searchEditedDate")),
										SYSTEM_SEARCH_TABLE.DELETED
												.value(new JdbcNamedParameter(
														"deleted"))

								)
								.where(SYSTEM_SEARCH_TABLE.SEARCH_ID
										.eq(new JdbcNamedParameter("searchId")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<SystemSearch> systemSearch) {
		if (CollectionUtil.isEmpty(systemSearch)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(systemSearch,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(SYSTEM_SEARCH_TABLE).where(
								and(SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_TYPE
										.eq(new JdbcNamedParameter(
												"searchObjectType")),
										SYSTEM_SEARCH_TABLE.SEARCH_OBJECT_ID
												.eq(new JdbcNamedParameter(
														"searchObjectId")),
										SYSTEM_SEARCH_TABLE.SEARCH_TITLE
												.eq(new JdbcNamedParameter(
														"searchTitle")),
										SYSTEM_SEARCH_TABLE.SEARCH_CONTENT
												.eq(new JdbcNamedParameter(
														"searchContent")),
										SYSTEM_SEARCH_TABLE.SEARCH_ADDED_DATE
												.eq(new JdbcNamedParameter(
														"searchAddedDate")),
										SYSTEM_SEARCH_TABLE.SEARCH_EDITED_DATE
												.eq(new JdbcNamedParameter(
														"searchEditedDate")),
										SYSTEM_SEARCH_TABLE.DELETED
												.eq(new JdbcNamedParameter(
														"deleted"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<SystemSearch> systemSearch) {
		return preparedBatchInsert(true, systemSearch);
	}

}
