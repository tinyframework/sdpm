/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.system.dao.constant.SystemHistoryTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.system.dao.constant.SystemHistoryTable.SYSTEM_HISTORYTABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.system.dao.pojo.SystemHistory;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.system.dao.SystemHistoryDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class SystemHistoryDaoImpl extends TinyDslDaoSupport implements
		SystemHistoryDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public SystemHistory add(SystemHistory systemHistory) {
		return getDslTemplate().insertAndReturnKey(systemHistory,
				new InsertGenerateCallback<SystemHistory>() {
					public Insert generate(SystemHistory t) {
						Insert insert = insertInto(SYSTEM_HISTORY_TABLE)
								.values(SYSTEM_HISTORY_TABLE.HISTORY_ID.value(t
										.getHistoryId()),
										SYSTEM_HISTORY_TABLE.HISTORY_ACTION
												.value(t.getHistoryAction()),
										SYSTEM_HISTORY_TABLE.HISTORY_FIELD
												.value(t.getHistoryField()),
										SYSTEM_HISTORY_TABLE.HISTORY_NEW
												.value(t.getHistoryNew()),
										SYSTEM_HISTORY_TABLE.HISTORY_OLD
												.value(t.getHistoryOld()),
										SYSTEM_HISTORY_TABLE.HISTORY_DIFF
												.value(t.getHistoryDiff())

								);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(SystemHistory systemHistory) {
		if (systemHistory == null || systemHistory.getHistoryId() == null) {
			return 0;
		}
		return getDslTemplate().update(systemHistory,
				new UpdateGenerateCallback<SystemHistory>() {
					public Update generate(SystemHistory t) {
						Update update = update(SYSTEM_HISTORY_TABLE).set(
								SYSTEM_HISTORY_TABLE.HISTORY_ACTION.value(t
										.getHistoryAction()),
								SYSTEM_HISTORY_TABLE.HISTORY_FIELD.value(t
										.getHistoryField()),
								SYSTEM_HISTORY_TABLE.HISTORY_NEW.value(t
										.getHistoryNew()),
								SYSTEM_HISTORY_TABLE.HISTORY_OLD.value(t
										.getHistoryOld()),
								SYSTEM_HISTORY_TABLE.HISTORY_DIFF.value(t
										.getHistoryDiff())).where(
								SYSTEM_HISTORY_TABLE.HISTORY_ID.eq(t
										.getHistoryId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(SYSTEM_HISTORY_TABLE).where(
								SYSTEM_HISTORY_TABLE.HISTORY_ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(SYSTEM_HISTORY_TABLE).where(
								SYSTEM_HISTORY_TABLE.HISTORY_ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public SystemHistory getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, SystemHistory.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(SYSTEM_HISTORY_TABLE).where(
								SYSTEM_HISTORY_TABLE.HISTORY_ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<SystemHistory> query(SystemHistory systemHistory,
			final OrderBy... orderArgs) {
		if (systemHistory == null) {
			systemHistory = new SystemHistory();
		}
		return getDslTemplate().query(systemHistory,
				new SelectGenerateCallback<SystemHistory>() {
					@SuppressWarnings("rawtypes")
					public Select generate(SystemHistory t) {
						Select select = selectFrom(SYSTEM_HISTORY_TABLE).where(
								and(SYSTEM_HISTORY_TABLE.HISTORY_ID.eq(t
										.getHistoryId()),
										SYSTEM_HISTORY_TABLE.HISTORY_ACTION
												.eq(t.getHistoryAction()),
										SYSTEM_HISTORY_TABLE.HISTORY_FIELD.eq(t
												.getHistoryField()),
										SYSTEM_HISTORY_TABLE.HISTORY_NEW.eq(t
												.getHistoryNew()),
										SYSTEM_HISTORY_TABLE.HISTORY_OLD.eq(t
												.getHistoryOld()),
										SYSTEM_HISTORY_TABLE.HISTORY_DIFF.eq(t
												.getHistoryDiff())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<SystemHistory> queryPager(int start, int limit,
			SystemHistory systemHistory, final OrderBy... orderArgs) {
		if (systemHistory == null) {
			systemHistory = new SystemHistory();
		}
		return getDslTemplate().queryPager(start, limit, systemHistory, false,
				new SelectGenerateCallback<SystemHistory>() {
					public Select generate(SystemHistory t) {
						Select select = Select.selectFrom(SYSTEM_HISTORY_TABLE)
								.where(and(SYSTEM_HISTORY_TABLE.HISTORY_ID.eq(t
										.getHistoryId()),
										SYSTEM_HISTORY_TABLE.HISTORY_ACTION
												.eq(t.getHistoryAction()),
										SYSTEM_HISTORY_TABLE.HISTORY_FIELD.eq(t
												.getHistoryField()),
										SYSTEM_HISTORY_TABLE.HISTORY_NEW.eq(t
												.getHistoryNew()),
										SYSTEM_HISTORY_TABLE.HISTORY_OLD.eq(t
												.getHistoryOld()),
										SYSTEM_HISTORY_TABLE.HISTORY_DIFF.eq(t
												.getHistoryDiff())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<SystemHistory> systemHistory) {
		if (CollectionUtil.isEmpty(systemHistory)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, systemHistory,
				new InsertGenerateCallback<SystemHistory>() {

					public Insert generate(SystemHistory t) {
						return insertInto(SYSTEM_HISTORY_TABLE).values(
								SYSTEM_HISTORY_TABLE.HISTORY_ID.value(t
										.getHistoryId()),
								SYSTEM_HISTORY_TABLE.HISTORY_ACTION.value(t
										.getHistoryAction()),
								SYSTEM_HISTORY_TABLE.HISTORY_FIELD.value(t
										.getHistoryField()),
								SYSTEM_HISTORY_TABLE.HISTORY_NEW.value(t
										.getHistoryNew()),
								SYSTEM_HISTORY_TABLE.HISTORY_OLD.value(t
										.getHistoryOld()),
								SYSTEM_HISTORY_TABLE.HISTORY_DIFF.value(t
										.getHistoryDiff())

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<SystemHistory> systemHistorys) {
		return batchInsert(true, systemHistorys);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<SystemHistory> systemHistory) {
		if (CollectionUtil.isEmpty(systemHistory)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(systemHistory,
				new UpdateGenerateCallback<SystemHistory>() {
					public Update generate(SystemHistory t) {
						return update(SYSTEM_HISTORY_TABLE).set(
								SYSTEM_HISTORY_TABLE.HISTORY_ACTION.value(t
										.getHistoryAction()),
								SYSTEM_HISTORY_TABLE.HISTORY_FIELD.value(t
										.getHistoryField()),
								SYSTEM_HISTORY_TABLE.HISTORY_NEW.value(t
										.getHistoryNew()),
								SYSTEM_HISTORY_TABLE.HISTORY_OLD.value(t
										.getHistoryOld()),
								SYSTEM_HISTORY_TABLE.HISTORY_DIFF.value(t
										.getHistoryDiff())

						).where(SYSTEM_HISTORY_TABLE.HISTORY_ID.eq(t
								.getHistoryId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<SystemHistory> systemHistory) {
		if (CollectionUtil.isEmpty(systemHistory)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(systemHistory,
				new DeleteGenerateCallback<SystemHistory>() {
					public Delete generate(SystemHistory t) {
						return delete(SYSTEM_HISTORY_TABLE).where(
								and(SYSTEM_HISTORY_TABLE.HISTORY_ID.eq(t
										.getHistoryId()),
										SYSTEM_HISTORY_TABLE.HISTORY_ACTION
												.eq(t.getHistoryAction()),
										SYSTEM_HISTORY_TABLE.HISTORY_FIELD.eq(t
												.getHistoryField()),
										SYSTEM_HISTORY_TABLE.HISTORY_NEW.eq(t
												.getHistoryNew()),
										SYSTEM_HISTORY_TABLE.HISTORY_OLD.eq(t
												.getHistoryOld()),
										SYSTEM_HISTORY_TABLE.HISTORY_DIFF.eq(t
												.getHistoryDiff())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = orderBies[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<SystemHistory> systemHistory) {
		if (CollectionUtil.isEmpty(systemHistory)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, systemHistory,
				new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(SYSTEM_HISTORY_TABLE).values(
								SYSTEM_HISTORY_TABLE.HISTORY_ACTION
										.value(new JdbcNamedParameter(
												"historyAction")),
								SYSTEM_HISTORY_TABLE.HISTORY_FIELD
										.value(new JdbcNamedParameter(
												"historyField")),
								SYSTEM_HISTORY_TABLE.HISTORY_NEW
										.value(new JdbcNamedParameter(
												"historyNew")),
								SYSTEM_HISTORY_TABLE.HISTORY_OLD
										.value(new JdbcNamedParameter(
												"historyOld")),
								SYSTEM_HISTORY_TABLE.HISTORY_DIFF
										.value(new JdbcNamedParameter(
												"historyDiff"))

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<SystemHistory> systemHistory) {
		if (CollectionUtil.isEmpty(systemHistory)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(systemHistory,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(SYSTEM_HISTORY_TABLE).set(
								SYSTEM_HISTORY_TABLE.HISTORY_ACTION
										.value(new JdbcNamedParameter(
												"historyAction")),
								SYSTEM_HISTORY_TABLE.HISTORY_FIELD
										.value(new JdbcNamedParameter(
												"historyField")),
								SYSTEM_HISTORY_TABLE.HISTORY_NEW
										.value(new JdbcNamedParameter(
												"historyNew")),
								SYSTEM_HISTORY_TABLE.HISTORY_OLD
										.value(new JdbcNamedParameter(
												"historyOld")),
								SYSTEM_HISTORY_TABLE.HISTORY_DIFF
										.value(new JdbcNamedParameter(
												"historyDiff"))

						).where(SYSTEM_HISTORY_TABLE.HISTORY_ID
								.eq(new JdbcNamedParameter("historyId")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<SystemHistory> systemHistory) {
		if (CollectionUtil.isEmpty(systemHistory)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(systemHistory,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(SYSTEM_HISTORY_TABLE).where(
								and(SYSTEM_HISTORY_TABLE.HISTORY_ACTION
										.eq(new JdbcNamedParameter(
												"historyAction")),
										SYSTEM_HISTORY_TABLE.HISTORY_FIELD
												.eq(new JdbcNamedParameter(
														"historyField")),
										SYSTEM_HISTORY_TABLE.HISTORY_NEW
												.eq(new JdbcNamedParameter(
														"historyNew")),
										SYSTEM_HISTORY_TABLE.HISTORY_OLD
												.eq(new JdbcNamedParameter(
														"historyOld")),
										SYSTEM_HISTORY_TABLE.HISTORY_DIFF
												.eq(new JdbcNamedParameter(
														"historyDiff"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<SystemHistory> systemHistory) {
		return preparedBatchInsert(true, systemHistory);
	}

}
