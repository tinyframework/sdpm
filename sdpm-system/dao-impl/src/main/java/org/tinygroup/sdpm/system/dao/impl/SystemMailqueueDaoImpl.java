/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.system.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.system.dao.constant.SystemMailqueueTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.system.dao.constant.SystemMailqueueTable.SYSTEM_MAILQUEUETABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.system.dao.pojo.SystemMailqueue;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.system.dao.SystemMailqueueDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class SystemMailqueueDaoImpl extends TinyDslDaoSupport implements
		SystemMailqueueDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public SystemMailqueue add(SystemMailqueue systemMailqueue) {
		return getDslTemplate().insertAndReturnKey(systemMailqueue,
				new InsertGenerateCallback<SystemMailqueue>() {
					public Insert generate(SystemMailqueue t) {
						Insert insert = insertInto(SYSTEM_MAILQUEUE_TABLE)
								.values(SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ID
										.value(t.getMailqueueId()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_TO_LIST
												.value(t.getMailqueueToList()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_CC_LIST
												.value(t.getMailqueueCcList()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SUBJECT
												.value(t.getMailqueueSubject()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_BODY
												.value(t.getMailqueueBody()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_DATE.value(t
												.getMailqueueAddedDate()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_BY
												.value(t.getMailqueueAddedBy()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SEND_TIME
												.value(t.getMailqueueSendTime()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_STATUS
												.value(t.getMailqueueStatus()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_FAIL_REASON.value(t
												.getMailqueueFailReason())

								);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(SystemMailqueue systemMailqueue) {
		if (systemMailqueue == null || systemMailqueue.getMailqueueId() == null) {
			return 0;
		}
		return getDslTemplate().update(systemMailqueue,
				new UpdateGenerateCallback<SystemMailqueue>() {
					public Update generate(SystemMailqueue t) {
						Update update = update(SYSTEM_MAILQUEUE_TABLE).set(
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_TO_LIST
										.value(t.getMailqueueToList()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_CC_LIST
										.value(t.getMailqueueCcList()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SUBJECT
										.value(t.getMailqueueSubject()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_BODY.value(t
										.getMailqueueBody()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_DATE
										.value(t.getMailqueueAddedDate()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_BY
										.value(t.getMailqueueAddedBy()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SEND_TIME
										.value(t.getMailqueueSendTime()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_STATUS.value(t
										.getMailqueueStatus()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_FAIL_REASON
										.value(t.getMailqueueFailReason()))
								.where(SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ID.eq(t
										.getMailqueueId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(SYSTEM_MAILQUEUE_TABLE).where(
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(SYSTEM_MAILQUEUE_TABLE).where(
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public SystemMailqueue getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, SystemMailqueue.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(SYSTEM_MAILQUEUE_TABLE).where(
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<SystemMailqueue> query(SystemMailqueue systemMailqueue,
			final OrderBy... orderArgs) {
		if (systemMailqueue == null) {
			systemMailqueue = new SystemMailqueue();
		}
		return getDslTemplate().query(systemMailqueue,
				new SelectGenerateCallback<SystemMailqueue>() {
					@SuppressWarnings("rawtypes")
					public Select generate(SystemMailqueue t) {
						Select select = selectFrom(SYSTEM_MAILQUEUE_TABLE)
								.where(and(
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ID
												.eq(t.getMailqueueId()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_TO_LIST
												.eq(t.getMailqueueToList()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_CC_LIST
												.eq(t.getMailqueueCcList()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SUBJECT
												.eq(t.getMailqueueSubject()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_BODY
												.eq(t.getMailqueueBody()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_DATE
												.eq(t.getMailqueueAddedDate()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_BY
												.eq(t.getMailqueueAddedBy()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SEND_TIME
												.eq(t.getMailqueueSendTime()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_STATUS
												.eq(t.getMailqueueStatus()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_FAIL_REASON
												.eq(t.getMailqueueFailReason())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<SystemMailqueue> queryPager(int start, int limit,
			SystemMailqueue systemMailqueue, final OrderBy... orderArgs) {
		if (systemMailqueue == null) {
			systemMailqueue = new SystemMailqueue();
		}
		return getDslTemplate().queryPager(start, limit, systemMailqueue,
				false, new SelectGenerateCallback<SystemMailqueue>() {
					public Select generate(SystemMailqueue t) {
						Select select = Select
								.selectFrom(SYSTEM_MAILQUEUE_TABLE)
								.where(and(
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ID
												.eq(t.getMailqueueId()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_TO_LIST
												.eq(t.getMailqueueToList()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_CC_LIST
												.eq(t.getMailqueueCcList()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SUBJECT
												.eq(t.getMailqueueSubject()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_BODY
												.eq(t.getMailqueueBody()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_DATE
												.eq(t.getMailqueueAddedDate()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_BY
												.eq(t.getMailqueueAddedBy()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SEND_TIME
												.eq(t.getMailqueueSendTime()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_STATUS
												.eq(t.getMailqueueStatus()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_FAIL_REASON
												.eq(t.getMailqueueFailReason())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<SystemMailqueue> systemMailqueue) {
		if (CollectionUtil.isEmpty(systemMailqueue)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, systemMailqueue,
				new InsertGenerateCallback<SystemMailqueue>() {

					public Insert generate(SystemMailqueue t) {
						return insertInto(SYSTEM_MAILQUEUE_TABLE).values(
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ID.value(t
										.getMailqueueId()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_TO_LIST
										.value(t.getMailqueueToList()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_CC_LIST
										.value(t.getMailqueueCcList()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SUBJECT
										.value(t.getMailqueueSubject()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_BODY.value(t
										.getMailqueueBody()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_DATE
										.value(t.getMailqueueAddedDate()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_BY
										.value(t.getMailqueueAddedBy()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SEND_TIME
										.value(t.getMailqueueSendTime()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_STATUS.value(t
										.getMailqueueStatus()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_FAIL_REASON
										.value(t.getMailqueueFailReason())

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<SystemMailqueue> systemMailqueues) {
		return batchInsert(true, systemMailqueues);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<SystemMailqueue> systemMailqueue) {
		if (CollectionUtil.isEmpty(systemMailqueue)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(systemMailqueue,
				new UpdateGenerateCallback<SystemMailqueue>() {
					public Update generate(SystemMailqueue t) {
						return update(SYSTEM_MAILQUEUE_TABLE).set(
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_TO_LIST
										.value(t.getMailqueueToList()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_CC_LIST
										.value(t.getMailqueueCcList()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SUBJECT
										.value(t.getMailqueueSubject()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_BODY.value(t
										.getMailqueueBody()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_DATE
										.value(t.getMailqueueAddedDate()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_BY
										.value(t.getMailqueueAddedBy()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SEND_TIME
										.value(t.getMailqueueSendTime()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_STATUS.value(t
										.getMailqueueStatus()),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_FAIL_REASON
										.value(t.getMailqueueFailReason())

						).where(SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ID.eq(t
								.getMailqueueId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<SystemMailqueue> systemMailqueue) {
		if (CollectionUtil.isEmpty(systemMailqueue)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(systemMailqueue,
				new DeleteGenerateCallback<SystemMailqueue>() {
					public Delete generate(SystemMailqueue t) {
						return delete(SYSTEM_MAILQUEUE_TABLE)
								.where(and(
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ID
												.eq(t.getMailqueueId()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_TO_LIST
												.eq(t.getMailqueueToList()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_CC_LIST
												.eq(t.getMailqueueCcList()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SUBJECT
												.eq(t.getMailqueueSubject()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_BODY
												.eq(t.getMailqueueBody()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_DATE
												.eq(t.getMailqueueAddedDate()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_BY
												.eq(t.getMailqueueAddedBy()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SEND_TIME
												.eq(t.getMailqueueSendTime()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_STATUS
												.eq(t.getMailqueueStatus()),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_FAIL_REASON
												.eq(t.getMailqueueFailReason())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = orderBies[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<SystemMailqueue> systemMailqueue) {
		if (CollectionUtil.isEmpty(systemMailqueue)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, systemMailqueue,
				new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(SYSTEM_MAILQUEUE_TABLE).values(
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_TO_LIST
										.value(new JdbcNamedParameter(
												"mailqueueToList")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_CC_LIST
										.value(new JdbcNamedParameter(
												"mailqueueCcList")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SUBJECT
										.value(new JdbcNamedParameter(
												"mailqueueSubject")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_BODY
										.value(new JdbcNamedParameter(
												"mailqueueBody")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_DATE
										.value(new JdbcNamedParameter(
												"mailqueueAddedDate")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_BY
										.value(new JdbcNamedParameter(
												"mailqueueAddedBy")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SEND_TIME
										.value(new JdbcNamedParameter(
												"mailqueueSendTime")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_STATUS
										.value(new JdbcNamedParameter(
												"mailqueueStatus")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_FAIL_REASON
										.value(new JdbcNamedParameter(
												"mailqueueFailReason"))

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<SystemMailqueue> systemMailqueue) {
		if (CollectionUtil.isEmpty(systemMailqueue)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(systemMailqueue,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(SYSTEM_MAILQUEUE_TABLE).set(
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_TO_LIST
										.value(new JdbcNamedParameter(
												"mailqueueToList")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_CC_LIST
										.value(new JdbcNamedParameter(
												"mailqueueCcList")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SUBJECT
										.value(new JdbcNamedParameter(
												"mailqueueSubject")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_BODY
										.value(new JdbcNamedParameter(
												"mailqueueBody")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_DATE
										.value(new JdbcNamedParameter(
												"mailqueueAddedDate")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_BY
										.value(new JdbcNamedParameter(
												"mailqueueAddedBy")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SEND_TIME
										.value(new JdbcNamedParameter(
												"mailqueueSendTime")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_STATUS
										.value(new JdbcNamedParameter(
												"mailqueueStatus")),
								SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_FAIL_REASON
										.value(new JdbcNamedParameter(
												"mailqueueFailReason"))

						).where(SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ID
								.eq(new JdbcNamedParameter("mailqueueId")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<SystemMailqueue> systemMailqueue) {
		if (CollectionUtil.isEmpty(systemMailqueue)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(systemMailqueue,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(SYSTEM_MAILQUEUE_TABLE)
								.where(and(
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_TO_LIST
												.eq(new JdbcNamedParameter(
														"mailqueueToList")),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_CC_LIST
												.eq(new JdbcNamedParameter(
														"mailqueueCcList")),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SUBJECT
												.eq(new JdbcNamedParameter(
														"mailqueueSubject")),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_BODY
												.eq(new JdbcNamedParameter(
														"mailqueueBody")),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_DATE
												.eq(new JdbcNamedParameter(
														"mailqueueAddedDate")),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_ADDED_BY
												.eq(new JdbcNamedParameter(
														"mailqueueAddedBy")),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_SEND_TIME
												.eq(new JdbcNamedParameter(
														"mailqueueSendTime")),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_STATUS
												.eq(new JdbcNamedParameter(
														"mailqueueStatus")),
										SYSTEM_MAILQUEUE_TABLE.MAILQUEUE_FAIL_REASON
												.eq(new JdbcNamedParameter(
														"mailqueueFailReason"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<SystemMailqueue> systemMailqueue) {
		return preparedBatchInsert(true, systemMailqueue);
	}

}
