/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.pojo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 客户请求表 * <!-- end-user-doc -->
 */
public class ServiceRequest implements Serializable {

	public static final Integer DELETE_YES = 1;

	public static final Integer DELETE_NO = 0;

	public static final Integer CREATED = 0;

	public static final Integer DOING = 1;

	public static final Integer REJECTED = 2;

	public static final Integer TOPRODUCT = 3;

	public static final Integer PLANNED = 4;

	public static final Integer POSTPONED = 5;

	public static final Integer FINISHED = 6;

	public static final Integer RELEASED = 7;

	public static final Integer RETURNVISIT = 8;

	public static final Integer REOPEN = 9;

	public static final Integer CLOSE = 10;

	private String clientName;

	/**
	 * <!-- begin-user-doc --> 请求ID * 服务请求ID <!-- end-user-doc -->
	 */
	private Integer clientRequestId;

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	private Integer productId;

	/**
	 * <!-- begin-user-doc --> 模块ID * <!-- end-user-doc -->
	 */
	private Integer moduleId;

	/**
	 * <!-- begin-user-doc --> 请求逻辑编号 * 服务请求逻辑编号 <!-- end-user-doc -->
	 */
	private String requestNo;

	/**
	 * <!-- begin-user-doc --> 请求类型 * 请求类型 Event
	 * type:0-bug(缺陷/纠错,1-req1（适应性）,2-req2（完善或增强）,3-req3（个性化定制）,4-reg4（全新）,
	 * 5-training,6-support,7-question <!-- end-user-doc -->
	 */
	private String requestType;

	/**
	 * <!-- begin-user-doc --> 请求优先级 * 请求优先级 Priority:0-low,1-common,2-urgent
	 * <!-- end-user-doc -->
	 */
	private Integer requestPre;

	/**
	 * <!-- begin-user-doc --> 请求标题 * <!-- end-user-doc -->
	 */
	private String requestTitle;

	/**
	 * <!-- begin-user-doc --> 关键字 * 关键词（分隔符） <!-- end-user-doc -->
	 */
	private String requestKeywords;

	/**
	 * <!-- begin-user-doc --> 请求描述 * <!-- end-user-doc -->
	 */
	private String requestSpec;

	/**
	 * <!-- begin-user-doc --> 异常标记 * 0-正常；1-异常；2-重大异常 <!-- end-user-doc -->
	 */
	private Integer requestIsAbnormal;

	/**
	 * <!-- begin-user-doc --> 客户ID * <!-- end-user-doc -->
	 */
	private Integer clientId;

	/**
	 * <!-- begin-user-doc --> 联系人 * 默认为需求提交人或产品客户其他联系人 <!-- end-user-doc -->
	 */
	private String requester;

	/**
	 * <!-- begin-user-doc --> 服务事件提交人 * 客户自助提交，此字段信息与requester相同，如客服代录入，可以不一致
	 * <!-- end-user-doc -->
	 */
	private String requestSubmitBy;

	/**
	 * <!-- begin-user-doc --> 请求提交时间 * <!-- end-user-doc -->
	 */
	private Date requestSubmitDate;

	/**
	 * <!-- begin-user-doc --> 请求回复时间 * <!-- end-user-doc -->
	 */
	private Date requestReplyDate;

	/**
	 * <!-- begin-user-doc --> 请求承诺完成日期 * <!-- end-user-doc -->
	 */
	private Date requestCommitmentDate;

	/**
	 * <!-- begin-user-doc --> 回访人 * <!-- end-user-doc -->
	 */
	private String requestReviewer;

	/**
	 * <!-- begin-user-doc --> 回访日期 * 请求回访日期 <!-- end-user-doc -->
	 */
	private Date requestReviewDate;

	/**
	 * <!-- begin-user-doc --> 请求最后编辑者 * <!-- end-user-doc -->
	 */
	private String requestLastEditedBy;

	/**
	 * <!-- begin-user-doc --> 请求最后编辑时间 * <!-- end-user-doc -->
	 */
	private Date requestLastEditDate;

	/**
	 * <!-- begin-user-doc --> 请求完成日期 * 支持类需求-实际完成日期 修改类需求-实际发布日期 <!--
	 * end-user-doc -->
	 */
	private BigInteger requestReleaseDate;

	/**
	 * <!-- begin-user-doc --> 关闭人 * <!-- end-user-doc -->
	 */
	private String requestClosedBy;

	/**
	 * <!-- begin-user-doc --> 关闭时间 * <!-- end-user-doc -->
	 */
	private Date requestCloseDate;

	/**
	 * <!-- begin-user-doc --> 请求打开次数 * 默认0，经历一次拒绝/关闭再打开加1 <!-- end-user-doc -->
	 */
	private Integer requestOpenCount;

	/**
	 * <!-- begin-user-doc --> 请求状态 *
	 * 请求状态0-created新建,1-doing处理中，2-rejected拒绝,3-toProduct转出
	 * （回复后用户可见）,4-planned已接受【由产品模块需求纳入计划触发/客服直接处理】
	 * （回复后用户可见），5-postponed挂起，6-finished已完成
	 * 【由产品模块需求阶段】，7-released已发放(用户可见),8-retrunVisit已回访(用户可见),
	 * 9-reopen【回访不通过】(用户可见)，10-close关闭【状态8，并且review结果是pass】 <!-- end-user-doc
	 * -->
	 */
	private Integer requestStatus;

	/**
	 * <!-- begin-user-doc --> 转换为 * 1-"toReq"转为需求，2-"toBug"转为缺陷，3-"toTask"转为任务
	 * <!-- end-user-doc -->
	 */
	private Integer requestTransTo;

	/**
	 * <!-- begin-user-doc --> 转出对象ID * <!-- end-user-doc -->
	 */
	private Integer requestTransId;

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	private Integer deleted;

	/**
	 * <!-- begin-user-doc --> 回复描述 * <!-- end-user-doc -->
	 */
	private String replySpec;

	/**
	 * <!-- begin-user-doc --> 回复者 * 客服 <!-- end-user-doc -->
	 */
	private String replier;

	/**
	 * <!-- begin-user-doc --> 回复时间 * <!-- end-user-doc -->
	 */
	private Date replyDate;

	public ServiceRequest() {
		setDeleted(DELETE_NO);
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	/**
	 * <!-- begin-user-doc --> 请求ID * 服务请求ID <!-- end-user-doc -->
	 */
	public void setClientRequestId(Integer clientRequestId) {
		this.clientRequestId = clientRequestId;
	}

	public Integer getClientRequestId() {
		return clientRequestId;
	}

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getProductId() {
		return productId;
	}

	/**
	 * <!-- begin-user-doc --> 模块ID * <!-- end-user-doc -->
	 */
	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	/**
	 * <!-- begin-user-doc --> 请求逻辑编号 * 服务请求逻辑编号 <!-- end-user-doc -->
	 */
	public void setRequestNo(String requestNo) {
		this.requestNo = requestNo;
	}

	public String getRequestNo() {
		return requestNo;
	}

	/**
	 * <!-- begin-user-doc --> 请求类型 * 请求类型 Event
	 * type:0-bug(缺陷/纠错,1-req1（适应性）,2-req2（完善或增强）,3-req3（个性化定制）,4-reg4（全新）,
	 * 5-training,6-support,7-question <!-- end-user-doc -->
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getRequestType() {
		return requestType;
	}

	/**
	 * <!-- begin-user-doc --> 请求优先级 * 请求优先级 Priority:0-low,1-common,2-urgent
	 * <!-- end-user-doc -->
	 */
	public void setRequestPre(Integer requestPre) {
		this.requestPre = requestPre;
	}

	public Integer getRequestPre() {
		return requestPre;
	}

	/**
	 * <!-- begin-user-doc --> 请求标题 * <!-- end-user-doc -->
	 */
	public void setRequestTitle(String requestTitle) {
		this.requestTitle = requestTitle;
	}

	public String getRequestTitle() {
		return requestTitle;
	}

	/**
	 * <!-- begin-user-doc --> 关键字 * 关键词（分隔符） <!-- end-user-doc -->
	 */
	public void setRequestKeywords(String requestKeywords) {
		this.requestKeywords = requestKeywords;
	}

	public String getRequestKeywords() {
		return requestKeywords;
	}

	/**
	 * <!-- begin-user-doc --> 请求描述 * <!-- end-user-doc -->
	 */
	public void setRequestSpec(String requestSpec) {
		this.requestSpec = requestSpec;
	}

	public String getRequestSpec() {
		return requestSpec;
	}

	/**
	 * <!-- begin-user-doc --> 异常标记 * 0-正常；1-异常；2-重大异常 <!-- end-user-doc -->
	 */
	public void setRequestIsAbnormal(Integer requestIsAbnormal) {
		this.requestIsAbnormal = requestIsAbnormal;
	}

	public Integer getRequestIsAbnormal() {
		return requestIsAbnormal;
	}

	/**
	 * <!-- begin-user-doc --> 客户ID * <!-- end-user-doc -->
	 */
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public Integer getClientId() {
		return clientId;
	}

	/**
	 * <!-- begin-user-doc --> 联系人 * 默认为需求提交人或产品客户其他联系人 <!-- end-user-doc -->
	 */
	public void setRequester(String requester) {
		this.requester = requester;
	}

	public String getRequester() {
		return requester;
	}

	/**
	 * <!-- begin-user-doc --> 服务事件提交人 * 客户自助提交，此字段信息与requester相同，如客服代录入，可以不一致
	 * <!-- end-user-doc -->
	 */
	public void setRequestSubmitBy(String requestSubmitBy) {
		this.requestSubmitBy = requestSubmitBy;
	}

	public String getRequestSubmitBy() {
		return requestSubmitBy;
	}

	/**
	 * <!-- begin-user-doc --> 请求提交时间 * <!-- end-user-doc -->
	 */
	public void setRequestSubmitDate(Date requestSubmitDate) {
		this.requestSubmitDate = requestSubmitDate;
	}

	public Date getRequestSubmitDate() {
		return requestSubmitDate;
	}

	/**
	 * <!-- begin-user-doc --> 请求回复时间 * <!-- end-user-doc -->
	 */
	public void setRequestReplyDate(Date requestReplyDate) {
		this.requestReplyDate = requestReplyDate;
	}

	public Date getRequestReplyDate() {
		return requestReplyDate;
	}

	/**
	 * <!-- begin-user-doc --> 请求承诺完成日期 * <!-- end-user-doc -->
	 */
	public void setRequestCommitmentDate(Date requestCommitmentDate) {
		this.requestCommitmentDate = requestCommitmentDate;
	}

	public Date getRequestCommitmentDate() {
		return requestCommitmentDate;
	}

	/**
	 * <!-- begin-user-doc --> 回访人 * <!-- end-user-doc -->
	 */
	public void setRequestReviewer(String requestReviewer) {
		this.requestReviewer = requestReviewer;
	}

	public String getRequestReviewer() {
		return requestReviewer;
	}

	/**
	 * <!-- begin-user-doc --> 回访日期 * 请求回访日期 <!-- end-user-doc -->
	 */
	public void setRequestReviewDate(Date requestReviewDate) {
		this.requestReviewDate = requestReviewDate;
	}

	public Date getRequestReviewDate() {
		return requestReviewDate;
	}

	/**
	 * <!-- begin-user-doc --> 请求最后编辑者 * <!-- end-user-doc -->
	 */
	public void setRequestLastEditedBy(String requestLastEditedBy) {
		this.requestLastEditedBy = requestLastEditedBy;
	}

	public String getRequestLastEditedBy() {
		return requestLastEditedBy;
	}

	/**
	 * <!-- begin-user-doc --> 请求最后编辑时间 * <!-- end-user-doc -->
	 */
	public void setRequestLastEditDate(Date requestLastEditDate) {
		this.requestLastEditDate = requestLastEditDate;
	}

	public Date getRequestLastEditDate() {
		return requestLastEditDate;
	}

	/**
	 * <!-- begin-user-doc --> 请求完成日期 * 支持类需求-实际完成日期 修改类需求-实际发布日期 <!--
	 * end-user-doc -->
	 */
	public void setRequestReleaseDate(BigInteger requestReleaseDate) {
		this.requestReleaseDate = requestReleaseDate;
	}

	public BigInteger getRequestReleaseDate() {
		return requestReleaseDate;
	}

	/**
	 * <!-- begin-user-doc --> 关闭人 * <!-- end-user-doc -->
	 */
	public void setRequestClosedBy(String requestClosedBy) {
		this.requestClosedBy = requestClosedBy;
	}

	public String getRequestClosedBy() {
		return requestClosedBy;
	}

	/**
	 * <!-- begin-user-doc --> 关闭时间 * <!-- end-user-doc -->
	 */
	public void setRequestCloseDate(Date requestCloseDate) {
		this.requestCloseDate = requestCloseDate;
	}

	public Date getRequestCloseDate() {
		return requestCloseDate;
	}

	/**
	 * <!-- begin-user-doc --> 请求打开次数 * 默认0，经历一次拒绝/关闭再打开加1 <!-- end-user-doc -->
	 */
	public void setRequestOpenCount(Integer requestOpenCount) {
		this.requestOpenCount = requestOpenCount;
	}

	public Integer getRequestOpenCount() {
		return requestOpenCount;
	}

	/**
	 * <!-- begin-user-doc --> 请求状态 *
	 * 请求状态0-created新建,1-doing处理中，2-rejected拒绝,3-toProduct转出
	 * （回复后用户可见）,4-planned已接受【由产品模块需求纳入计划触发/客服直接处理】
	 * （回复后用户可见），5-postponed挂起，6-finished已完成
	 * 【由产品模块需求阶段】，7-released已发放(用户可见),8-retrunVisit已回访(用户可见),
	 * 9-reopen【回访不通过】(用户可见)，10-close关闭【状态8，并且review结果是pass】 <!-- end-user-doc
	 * -->
	 */
	public void setRequestStatus(Integer requestStatus) {
		this.requestStatus = requestStatus;
	}

	public Integer getRequestStatus() {
		return requestStatus;
	}

	/**
	 * <!-- begin-user-doc --> 转换为 * 1-"toReq"转为需求，2-"toBug"转为缺陷，3-"toTask"转为任务
	 * <!-- end-user-doc -->
	 */
	public void setRequestTransTo(Integer requestTransTo) {
		this.requestTransTo = requestTransTo;
	}

	public Integer getRequestTransTo() {
		return requestTransTo;
	}

	/**
	 * <!-- begin-user-doc --> 转出对象ID * <!-- end-user-doc -->
	 */
	public void setRequestTransId(Integer requestTransId) {
		this.requestTransId = requestTransId;
	}

	public Integer getRequestTransId() {
		return requestTransId;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Integer getDeleted() {
		return deleted;
	}

	/**
	 * <!-- begin-user-doc --> 回复描述 * <!-- end-user-doc -->
	 */
	public void setReplySpec(String replySpec) {
		this.replySpec = replySpec;
	}

	public String getReplySpec() {
		return replySpec;
	}

	/**
	 * <!-- begin-user-doc --> 回复者 * 客服 <!-- end-user-doc -->
	 */
	public void setReplier(String replier) {
		this.replier = replier;
	}

	public String getReplier() {
		return replier;
	}

	/**
	 * <!-- begin-user-doc --> 回复时间 * <!-- end-user-doc -->
	 */
	public void setReplyDate(Date replyDate) {
		this.replyDate = replyDate;
	}

	public Date getReplyDate() {
		return replyDate;
	}

}
