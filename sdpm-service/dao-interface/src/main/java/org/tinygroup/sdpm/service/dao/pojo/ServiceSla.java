/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 服务级别协议表 * <!-- end-user-doc -->
 */
public class ServiceSla implements Serializable {

	public static final Integer DELETE_YES = 1;

	public static final Integer DELETE_NO = 0;

	private String clientName;

	private String productName;

	/**
	 * <!-- begin-user-doc --> 服务协议ID * <!-- end-user-doc -->
	 */
	private Integer slaId;

	/**
	 * <!-- begin-user-doc --> 服务级别 * 服务级别Service Level，保留字段，未今后分级服务提供信息； 0基础
	 * 1潜力 2核心 3vip <!-- end-user-doc -->
	 */
	private Integer serviceLevel;

	/**
	 * <!-- begin-user-doc --> 服务期限 * 服务有效期Service Deadline <!-- end-user-doc
	 * -->
	 */
	private Date serviceDeadline;

	/**
	 * <!-- begin-user-doc --> 客户ID * <!-- end-user-doc -->
	 */
	private Integer clientId;

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	private Integer productId;

	/**
	 * <!-- begin-user-doc --> 服务协议标题 * <!-- end-user-doc -->
	 */
	private String slaTitle;

	/**
	 * <!-- begin-user-doc --> 服务协议表述 * <!-- end-user-doc -->
	 */
	private String slaSpec;

	/**
	 * <!-- begin-user-doc --> 服务响应时限 * 单位为小时（不含节假日） <!-- end-user-doc -->
	 */
	private Integer serviceReplyTimeLimit;

	/**
	 * <!-- begin-user-doc --> 需求回访时限 * 单位工作日 <!-- end-user-doc -->
	 */
	private Integer serviceReviewTimeLimit;

	/**
	 * <!-- begin-user-doc --> 服务工时上限 * <!-- end-user-doc -->
	 */
	private Float serviceEffortLimit;

	/**
	 * <!-- begin-user-doc --> 服务请求数上限 * <!-- end-user-doc -->
	 */
	private Integer serviceRequestLimit;

	/**
	 * <!-- begin-user-doc --> 现场服务次数上限 * <!-- end-user-doc -->
	 */
	private Integer serviceTsOnsiteLimit;

	/**
	 * <!-- begin-user-doc --> 服务专员 * 可以多人，间隔符英文, <!-- end-user-doc -->
	 */
	private String serviceSpecialist;

	/**
	 * <!-- begin-user-doc --> 服务级别协议 * 0新建，1生效，2到期，3作废，4关闭 <!-- end-user-doc
	 * -->
	 */
	private Integer slaStatus;

	/**
	 * <!-- begin-user-doc --> 创建人 * <!-- end-user-doc -->
	 */
	private String slaCreatedBy;

	/**
	 * <!-- begin-user-doc --> 创建时间 * <!-- end-user-doc -->
	 */
	private Date slaCreateDate;

	/**
	 * <!-- begin-user-doc --> 批准人 * <!-- end-user-doc -->
	 */
	private String slaReviewedBy;

	/**
	 * <!-- begin-user-doc --> 批准时间 * <!-- end-user-doc -->
	 */
	private Date slaReviewDate;

	/**
	 * <!-- begin-user-doc --> 关闭人 * <!-- end-user-doc -->
	 */
	private String slaClosedBy;

	/**
	 * <!-- begin-user-doc --> 关闭时间 * <!-- end-user-doc -->
	 */
	private Date slaCloseDate;

	/**
	 * <!-- begin-user-doc --> 打开次数 * 默认0，审核通过后编辑再审核+1 <!-- end-user-doc -->
	 */
	private Integer slaOpenCount;

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	private Integer deleted;

	/**
	 * <!-- begin-user-doc --> 产品版本 * <!-- end-user-doc -->
	 */
	private String cilentProductVision;

	public ServiceSla() {
		setDeleted(0);
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * <!-- begin-user-doc --> 服务协议ID * <!-- end-user-doc -->
	 */
	public void setSlaId(Integer slaId) {
		this.slaId = slaId;
	}

	public Integer getSlaId() {
		return slaId;
	}

	/**
	 * <!-- begin-user-doc --> 服务级别 * 服务级别Service Level，保留字段，未今后分级服务提供信息； 0基础
	 * 1潜力 2核心 3vip <!-- end-user-doc -->
	 */
	public void setServiceLevel(Integer serviceLevel) {
		this.serviceLevel = serviceLevel;
	}

	public Integer getServiceLevel() {
		return serviceLevel;
	}

	/**
	 * <!-- begin-user-doc --> 服务期限 * 服务有效期Service Deadline <!-- end-user-doc
	 * -->
	 */
	public void setServiceDeadline(Date serviceDeadline) {
		this.serviceDeadline = serviceDeadline;
	}

	public Date getServiceDeadline() {
		return serviceDeadline;
	}

	/**
	 * <!-- begin-user-doc --> 客户ID * <!-- end-user-doc -->
	 */
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public Integer getClientId() {
		return clientId;
	}

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getProductId() {
		return productId;
	}

	/**
	 * <!-- begin-user-doc --> 服务协议标题 * <!-- end-user-doc -->
	 */
	public void setSlaTitle(String slaTitle) {
		this.slaTitle = slaTitle;
	}

	public String getSlaTitle() {
		return slaTitle;
	}

	/**
	 * <!-- begin-user-doc --> 服务协议表述 * <!-- end-user-doc -->
	 */
	public void setSlaSpec(String slaSpec) {
		this.slaSpec = slaSpec;
	}

	public String getSlaSpec() {
		return slaSpec;
	}

	/**
	 * <!-- begin-user-doc --> 服务响应时限 * 单位为小时（不含节假日） <!-- end-user-doc -->
	 */
	public void setServiceReplyTimeLimit(Integer serviceReplyTimeLimit) {
		this.serviceReplyTimeLimit = serviceReplyTimeLimit;
	}

	public Integer getServiceReplyTimeLimit() {
		return serviceReplyTimeLimit;
	}

	/**
	 * <!-- begin-user-doc --> 需求回访时限 * 单位工作日 <!-- end-user-doc -->
	 */
	public void setServiceReviewTimeLimit(Integer serviceReviewTimeLimit) {
		this.serviceReviewTimeLimit = serviceReviewTimeLimit;
	}

	public Integer getServiceReviewTimeLimit() {
		return serviceReviewTimeLimit;
	}

	/**
	 * <!-- begin-user-doc --> 服务工时上限 * <!-- end-user-doc -->
	 */
	public void setServiceEffortLimit(Float serviceEffortLimit) {
		this.serviceEffortLimit = serviceEffortLimit;
	}

	public Float getServiceEffortLimit() {
		return serviceEffortLimit;
	}

	/**
	 * <!-- begin-user-doc --> 服务请求数上限 * <!-- end-user-doc -->
	 */
	public void setServiceRequestLimit(Integer serviceRequestLimit) {
		this.serviceRequestLimit = serviceRequestLimit;
	}

	public Integer getServiceRequestLimit() {
		return serviceRequestLimit;
	}

	/**
	 * <!-- begin-user-doc --> 现场服务次数上限 * <!-- end-user-doc -->
	 */
	public void setServiceTsOnsiteLimit(Integer serviceTsOnsiteLimit) {
		this.serviceTsOnsiteLimit = serviceTsOnsiteLimit;
	}

	public Integer getServiceTsOnsiteLimit() {
		return serviceTsOnsiteLimit;
	}

	/**
	 * <!-- begin-user-doc --> 服务专员 * 可以多人，间隔符英文, <!-- end-user-doc -->
	 */
	public void setServiceSpecialist(String serviceSpecialist) {
		this.serviceSpecialist = serviceSpecialist;
	}

	public String getServiceSpecialist() {
		return serviceSpecialist;
	}

	/**
	 * <!-- begin-user-doc --> 服务级别协议 * 0新建，1生效，2到期，3作废，4关闭 <!-- end-user-doc
	 * -->
	 */
	public void setSlaStatus(Integer slaStatus) {
		this.slaStatus = slaStatus;
	}

	public Integer getSlaStatus() {
		return slaStatus;
	}

	/**
	 * <!-- begin-user-doc --> 创建人 * <!-- end-user-doc -->
	 */
	public void setSlaCreatedBy(String slaCreatedBy) {
		this.slaCreatedBy = slaCreatedBy;
	}

	public String getSlaCreatedBy() {
		return slaCreatedBy;
	}

	/**
	 * <!-- begin-user-doc --> 创建时间 * <!-- end-user-doc -->
	 */
	public void setSlaCreateDate(Date slaCreateDate) {
		this.slaCreateDate = slaCreateDate;
	}

	public Date getSlaCreateDate() {
		return slaCreateDate;
	}

	/**
	 * <!-- begin-user-doc --> 批准人 * <!-- end-user-doc -->
	 */
	public void setSlaReviewedBy(String slaReviewedBy) {
		this.slaReviewedBy = slaReviewedBy;
	}

	public String getSlaReviewedBy() {
		return slaReviewedBy;
	}

	/**
	 * <!-- begin-user-doc --> 批准时间 * <!-- end-user-doc -->
	 */
	public void setSlaReviewDate(Date slaReviewDate) {
		this.slaReviewDate = slaReviewDate;
	}

	public Date getSlaReviewDate() {
		return slaReviewDate;
	}

	/**
	 * <!-- begin-user-doc --> 关闭人 * <!-- end-user-doc -->
	 */
	public void setSlaClosedBy(String slaClosedBy) {
		this.slaClosedBy = slaClosedBy;
	}

	public String getSlaClosedBy() {
		return slaClosedBy;
	}

	/**
	 * <!-- begin-user-doc --> 关闭时间 * <!-- end-user-doc -->
	 */
	public void setSlaCloseDate(Date slaCloseDate) {
		this.slaCloseDate = slaCloseDate;
	}

	public Date getSlaCloseDate() {
		return slaCloseDate;
	}

	/**
	 * <!-- begin-user-doc --> 打开次数 * 默认0，审核通过后编辑再审核+1 <!-- end-user-doc -->
	 */
	public void setSlaOpenCount(Integer slaOpenCount) {
		this.slaOpenCount = slaOpenCount;
	}

	public Integer getSlaOpenCount() {
		return slaOpenCount;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Integer getDeleted() {
		return deleted;
	}

	/**
	 * <!-- begin-user-doc --> 产品版本 * <!-- end-user-doc -->
	 */
	public void setCilentProductVision(String cilentProductVision) {
		this.cilentProductVision = cilentProductVision;
	}

	public String getCilentProductVision() {
		return cilentProductVision;
	}

}
