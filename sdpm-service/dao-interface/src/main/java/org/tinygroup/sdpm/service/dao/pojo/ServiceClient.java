/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 客户信息表 * <!-- end-user-doc -->
 */
public class ServiceClient implements Serializable{

	public static final Integer DELETE_YES = 1;

	public static final Integer DELETE_NO = 0;

	/**
	 * <!-- begin-user-doc --> 客户ID * <!-- end-user-doc -->
	 */
	private Integer clientId;

	/**
	 * <!-- begin-user-doc --> 客户名称 * <!-- end-user-doc -->
	 */
	private String clientName;

	/**
	 * <!-- begin-user-doc --> 客户描述 * <!-- end-user-doc -->
	 */
	private String clientSpec;

	/**
	 * <!-- begin-user-doc --> 客户编号 * <!-- end-user-doc -->
	 */
	private String clientNO;

	/**
	 * <!-- begin-user-doc --> 客户单位/部门ID * <!-- end-user-doc -->
	 */
	private Integer clientDeptId;

	/**
	 * <!-- begin-user-doc --> 客户登记人 * <!-- end-user-doc -->
	 */
	private String clientCreatedBy;

	/**
	 * <!-- begin-user-doc --> 客户登记时间 * <!-- end-user-doc -->
	 */
	private Date clientCreateDate;

	/**
	 * <!-- begin-user-doc --> 客户状态 * 0-停止；1-活跃 <!-- end-user-doc -->
	 */
	private Integer clientStatus;

	/**
	 * <!-- begin-user-doc --> 客户联系电话 * <!-- end-user-doc -->
	 */
	private String userPhone;

	/**
	 * <!-- begin-user-doc --> 客户联系人 * <!-- end-user-doc -->
	 */
	private String userAccount;

	/**
	 * <!-- begin-user-doc --> 联系人职务 * <!-- end-user-doc -->
	 */
	private String userPost;

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	private Integer deleted;

	public ServiceClient() {
		setDeleted(DELETE_NO);
	}

	/**
	 * <!-- begin-user-doc --> 客户ID * <!-- end-user-doc -->
	 */
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public Integer getClientId() {
		return clientId;
	}

	/**
	 * <!-- begin-user-doc --> 客户名称 * <!-- end-user-doc -->
	 */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientName() {
		return clientName;
	}

	/**
	 * <!-- begin-user-doc --> 客户描述 * <!-- end-user-doc -->
	 */
	public void setClientSpec(String clientSpec) {
		this.clientSpec = clientSpec;
	}

	public String getClientSpec() {
		return clientSpec;
	}

	/**
	 * <!-- begin-user-doc --> 客户编号 * <!-- end-user-doc -->
	 */
	public void setClientNO(String clientNO) {
		this.clientNO = clientNO;
	}

	public String getClientNO() {
		return clientNO;
	}

	/**
	 * <!-- begin-user-doc --> 客户单位/部门ID * <!-- end-user-doc -->
	 */
	public void setClientDeptId(Integer clientDeptId) {
		this.clientDeptId = clientDeptId;
	}

	public Integer getClientDeptId() {
		return clientDeptId;
	}

	/**
	 * <!-- begin-user-doc --> 客户登记人 * <!-- end-user-doc -->
	 */
	public void setClientCreatedBy(String clientCreatedBy) {
		this.clientCreatedBy = clientCreatedBy;
	}

	public String getClientCreatedBy() {
		return clientCreatedBy;
	}

	/**
	 * <!-- begin-user-doc --> 客户登记时间 * <!-- end-user-doc -->
	 */
	public void setClientCreateDate(Date clientCreateDate) {
		this.clientCreateDate = clientCreateDate;
	}

	public Date getClientCreateDate() {
		return clientCreateDate;
	}

	/**
	 * <!-- begin-user-doc --> 客户状态 * 0-停止；1-活跃 <!-- end-user-doc -->
	 */
	public void setClientStatus(Integer clientStatus) {
		this.clientStatus = clientStatus;
	}

	public Integer getClientStatus() {
		return clientStatus;
	}

	/**
	 * <!-- begin-user-doc --> 客户联系电话 * <!-- end-user-doc -->
	 */
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getUserPhone() {
		return userPhone;
	}

	/**
	 * <!-- begin-user-doc --> 客户联系人 * <!-- end-user-doc -->
	 */
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public String getUserAccount() {
		return userAccount;
	}

	/**
	 * <!-- begin-user-doc --> 联系人职务 * <!-- end-user-doc -->
	 */
	public void setUserPost(String userPost) {
		this.userPost = userPost;
	}

	public String getUserPost() {
		return userPost;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Integer getDeleted() {
		return deleted;
	}

}
