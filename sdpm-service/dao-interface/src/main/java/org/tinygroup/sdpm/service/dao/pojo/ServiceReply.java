/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 客服回复表 * <!-- end-user-doc -->
 */
public class ServiceReply implements Serializable {

	/**
	 * <!-- begin-user-doc --> 回复ID * <!-- end-user-doc -->
	 */
	private Integer replyId;

	/**
	 * <!-- begin-user-doc --> 请求ID * 服务请求ID <!-- end-user-doc -->
	 */
	private Integer clientRequestId;

	/**
	 * <!-- begin-user-doc --> 回复意见 * 0-接受；1-拒绝；2-待定 <!-- end-user-doc -->
	 */
	private Integer replyOpinion;

	/**
	 * <!-- begin-user-doc --> 回复描述 * <!-- end-user-doc -->
	 */
	private String replySpec;

	/**
	 * <!-- begin-user-doc --> 承诺日期 * 回复意见为“0-接受，”时，应有承诺日期 <!-- end-user-doc -->
	 */
	private Date replyCommitmentDate;

	/**
	 * <!-- begin-user-doc --> 回复处理人 * 可以是客服或转出后的产品组处理人 <!-- end-user-doc -->
	 */
	private String replyDoBy;

	/**
	 * <!-- begin-user-doc --> 回复处理日期 * <!-- end-user-doc -->
	 */
	private Date replyDoDate;

	/**
	 * <!-- begin-user-doc --> 回复者 * 客服 <!-- end-user-doc -->
	 */
	private String replier;

	/**
	 * <!-- begin-user-doc --> 回复时间 * <!-- end-user-doc -->
	 */
	private Date replyDate;

	/**
	 * <!-- begin-user-doc --> 是否回复 * 0-未回复；1-已回复 <!-- end-user-doc -->
	 */
	private Integer replyDone;

	/**
	 * <!-- begin-user-doc --> 回复ID * <!-- end-user-doc -->
	 */
	public void setReplyId(Integer replyId) {
		this.replyId = replyId;
	}

	public Integer getReplyId() {
		return replyId;
	}

	/**
	 * <!-- begin-user-doc --> 请求ID * 服务请求ID <!-- end-user-doc -->
	 */
	public void setClientRequestId(Integer clientRequestId) {
		this.clientRequestId = clientRequestId;
	}

	public Integer getClientRequestId() {
		return clientRequestId;
	}

	/**
	 * <!-- begin-user-doc --> 回复意见 * 0-接受；1-拒绝；2-待定 <!-- end-user-doc -->
	 */
	public void setReplyOpinion(Integer replyOpinion) {
		this.replyOpinion = replyOpinion;
	}

	public Integer getReplyOpinion() {
		return replyOpinion;
	}

	/**
	 * <!-- begin-user-doc --> 回复描述 * <!-- end-user-doc -->
	 */
	public void setReplySpec(String replySpec) {
		this.replySpec = replySpec;
	}

	public String getReplySpec() {
		return replySpec;
	}

	/**
	 * <!-- begin-user-doc --> 承诺日期 * 回复意见为“0-接受，”时，应有承诺日期 <!-- end-user-doc -->
	 */
	public void setReplyCommitmentDate(Date replyCommitmentDate) {
		this.replyCommitmentDate = replyCommitmentDate;
	}

	public Date getReplyCommitmentDate() {
		return replyCommitmentDate;
	}

	/**
	 * <!-- begin-user-doc --> 回复处理人 * 可以是客服或转出后的产品组处理人 <!-- end-user-doc -->
	 */
	public void setReplyDoBy(String replyDoBy) {
		this.replyDoBy = replyDoBy;
	}

	public String getReplyDoBy() {
		return replyDoBy;
	}

	/**
	 * <!-- begin-user-doc --> 回复处理日期 * <!-- end-user-doc -->
	 */
	public void setReplyDoDate(Date replyDoDate) {
		this.replyDoDate = replyDoDate;
	}

	public Date getReplyDoDate() {
		return replyDoDate;
	}

	/**
	 * <!-- begin-user-doc --> 回复者 * 客服 <!-- end-user-doc -->
	 */
	public void setReplier(String replier) {
		this.replier = replier;
	}

	public String getReplier() {
		return replier;
	}

	/**
	 * <!-- begin-user-doc --> 回复时间 * <!-- end-user-doc -->
	 */
	public void setReplyDate(Date replyDate) {
		this.replyDate = replyDate;
	}

	public Date getReplyDate() {
		return replyDate;
	}

	/**
	 * <!-- begin-user-doc --> 是否回复 * 0-未回复；1-已回复 <!-- end-user-doc -->
	 */
	public void setReplyDone(Integer replyDone) {
		this.replyDone = replyDone;
	}

	public Integer getReplyDone() {
		return replyDone;
	}

}
