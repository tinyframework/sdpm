/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.constant;

import org.tinygroup.sdpm.service.dao.pojo.ServiceClient;
import org.tinygroup.tinysqldsl.base.Column;
import org.tinygroup.tinysqldsl.base.Table;

/**
 * <!-- begin-user-doc --> 客户信息表 * <!-- end-user-doc -->
 */
public class ServiceClientTable extends Table {

	public static final ServiceClientTable SERVICE_CLIENTTABLE = new ServiceClientTable();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public static final ServiceClientTable SERVICE_CLIENT_TABLE = new ServiceClientTable();

	/**
	 * <!-- begin-user-doc --> 客户ID * <!-- end-user-doc -->
	 */
	public final Column CLIENT_ID = new Column(this, "client_id");
	/**
	 * <!-- begin-user-doc --> 客户名称 * <!-- end-user-doc -->
	 */
	public final Column CLIENT_NAME = new Column(this, "client_name");
	/**
	 * <!-- begin-user-doc --> 客户描述 * <!-- end-user-doc -->
	 */
	public final Column CLIENT_SPEC = new Column(this, "client_spec");
	/**
	 * <!-- begin-user-doc --> 客户编号 * <!-- end-user-doc -->
	 */
	public final Column CLIENT_N_O = new Column(this, "client_n_o");
	/**
	 * <!-- begin-user-doc --> 客户单位/部门ID * <!-- end-user-doc -->
	 */
	public final Column CLIENT_DEPT_ID = new Column(this, "client_dept_id");
	/**
	 * <!-- begin-user-doc --> 客户登记人 * <!-- end-user-doc -->
	 */
	public final Column CLIENT_CREATED_BY = new Column(this,
			"client_created_by");
	/**
	 * <!-- begin-user-doc --> 客户登记时间 * <!-- end-user-doc -->
	 */
	public final Column CLIENT_CREATE_DATE = new Column(this,
			"client_create_date");
	/**
	 * <!-- begin-user-doc --> 客户状态 * 0-停止；1-活跃 <!-- end-user-doc -->
	 */
	public final Column CLIENT_STATUS = new Column(this, "client_status");
	/**
	 * <!-- begin-user-doc --> 客户联系电话 * <!-- end-user-doc -->
	 */
	public final Column USER_PHONE = new Column(this, "user_phone");
	/**
	 * <!-- begin-user-doc --> 客户联系人 * <!-- end-user-doc -->
	 */
	public final Column USER_ACCOUNT = new Column(this, "user_account");
	/**
	 * <!-- begin-user-doc --> 联系人职务 * <!-- end-user-doc -->
	 */
	public final Column USER_POST = new Column(this, "user_post");
	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public final Column DELETED = new Column(this, "deleted");

	public ServiceClientTable() {
		super("service_client");
	}

	public boolean isAutoGeneratedKeys() {
		return true;
	}

	public Class getPojoType() {
		return ServiceClient.class;
	}

}
