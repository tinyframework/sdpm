/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.constant;

import org.tinygroup.sdpm.service.dao.pojo.ServiceRequest;
import org.tinygroup.tinysqldsl.base.Column;
import org.tinygroup.tinysqldsl.base.Table;

/**
 * <!-- begin-user-doc --> 客户请求表 * <!-- end-user-doc -->
 */
public class ServiceRequestTable extends Table {

	public static final ServiceRequestTable SERVICE_REQUESTTABLE = new ServiceRequestTable();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public static final ServiceRequestTable SERVICE_REQUEST_TABLE = new ServiceRequestTable();

	/**
	 * <!-- begin-user-doc --> 请求ID * 服务请求ID <!-- end-user-doc -->
	 */
	public final Column CLIENT_REQUEST_ID = new Column(this,
			"client_request_id");
	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	public final Column PRODUCT_ID = new Column(this, "product_id");
	/**
	 * <!-- begin-user-doc --> 模块ID * <!-- end-user-doc -->
	 */
	public final Column MODULE_ID = new Column(this, "module_id");
	/**
	 * <!-- begin-user-doc --> 请求逻辑编号 * 服务请求逻辑编号 <!-- end-user-doc -->
	 */
	public final Column REQUEST_NO = new Column(this, "request_no");
	/**
	 * <!-- begin-user-doc --> 请求类型 * 请求类型 Event
	 * type:0-bug(缺陷/纠错,1-req1（适应性）,2-req2（完善或增强）,3-req3（个性化定制）,4-reg4（全新）,
	 * 5-training,6-support,7-question <!-- end-user-doc -->
	 */
	public final Column REQUEST_TYPE = new Column(this, "request_type");
	/**
	 * <!-- begin-user-doc --> 请求优先级 * 请求优先级 Priority:0-low,1-common,2-urgent
	 * <!-- end-user-doc -->
	 */
	public final Column REQUEST_PRE = new Column(this, "request_pre");
	/**
	 * <!-- begin-user-doc --> 请求标题 * <!-- end-user-doc -->
	 */
	public final Column REQUEST_TITLE = new Column(this, "request_title");
	/**
	 * <!-- begin-user-doc --> 关键字 * 关键词（分隔符） <!-- end-user-doc -->
	 */
	public final Column REQUEST_KEYWORDS = new Column(this, "request_keywords");
	/**
	 * <!-- begin-user-doc --> 请求描述 * <!-- end-user-doc -->
	 */
	public final Column REQUEST_SPEC = new Column(this, "request_spec");
	/**
	 * <!-- begin-user-doc --> 异常标记 * 0-正常；1-异常；2-重大异常 <!-- end-user-doc -->
	 */
	public final Column REQUEST_IS_ABNORMAL = new Column(this,
			"request_is_abnormal");
	/**
	 * <!-- begin-user-doc --> 客户ID * <!-- end-user-doc -->
	 */
	public final Column CLIENT_ID = new Column(this, "client_id");
	/**
	 * <!-- begin-user-doc --> 联系人 * 默认为需求提交人或产品客户其他联系人 <!-- end-user-doc -->
	 */
	public final Column REQUESTER = new Column(this, "requester");
	/**
	 * <!-- begin-user-doc --> 服务事件提交人 * 客户自助提交，此字段信息与requester相同，如客服代录入，可以不一致
	 * <!-- end-user-doc -->
	 */
	public final Column REQUEST_SUBMIT_BY = new Column(this,
			"request_submit_by");
	/**
	 * <!-- begin-user-doc --> 请求提交时间 * <!-- end-user-doc -->
	 */
	public final Column REQUEST_SUBMIT_DATE = new Column(this,
			"request_submit_date");
	/**
	 * <!-- begin-user-doc --> 请求回复时间 * <!-- end-user-doc -->
	 */
	public final Column REQUEST_REPLY_DATE = new Column(this,
			"request_reply_date");
	/**
	 * <!-- begin-user-doc --> 请求承诺完成日期 * <!-- end-user-doc -->
	 */
	public final Column REQUEST_COMMITMENT_DATE = new Column(this,
			"request_commitment_date");
	/**
	 * <!-- begin-user-doc --> 回访人 * <!-- end-user-doc -->
	 */
	public final Column REQUEST_REVIEWER = new Column(this, "request_reviewer");
	/**
	 * <!-- begin-user-doc --> 回访日期 * 请求回访日期 <!-- end-user-doc -->
	 */
	public final Column REQUEST_REVIEW_DATE = new Column(this,
			"request_review_date");
	/**
	 * <!-- begin-user-doc --> 请求最后编辑者 * <!-- end-user-doc -->
	 */
	public final Column REQUEST_LAST_EDITED_BY = new Column(this,
			"request_last_edited_by");
	/**
	 * <!-- begin-user-doc --> 请求最后编辑时间 * <!-- end-user-doc -->
	 */
	public final Column REQUEST_LAST_EDIT_DATE = new Column(this,
			"request_last_edit_date");
	/**
	 * <!-- begin-user-doc --> 请求完成日期 * 支持类需求-实际完成日期 修改类需求-实际发布日期 <!--
	 * end-user-doc -->
	 */
	public final Column REQUEST_RELEASE_DATE = new Column(this,
			"request_release_date");
	/**
	 * <!-- begin-user-doc --> 关闭人 * <!-- end-user-doc -->
	 */
	public final Column REQUEST_CLOSED_BY = new Column(this,
			"request_closed_by");
	/**
	 * <!-- begin-user-doc --> 关闭时间 * <!-- end-user-doc -->
	 */
	public final Column REQUEST_CLOSE_DATE = new Column(this,
			"request_close_date");
	/**
	 * <!-- begin-user-doc --> 请求打开次数 * 默认0，经历一次拒绝/关闭再打开加1 <!-- end-user-doc -->
	 */
	public final Column REQUEST_OPEN_COUNT = new Column(this,
			"request_open_count");
	/**
	 * <!-- begin-user-doc --> 请求状态 *
	 * 请求状态0-created新建,1-doing处理中，2-rejected拒绝,3-toProduct转出
	 * （回复后用户可见）,4-planned已接受【由产品模块需求纳入计划触发/客服直接处理】
	 * （回复后用户可见），5-postponed挂起，6-finished已完成
	 * 【由产品模块需求阶段】，7-released已发放(用户可见),8-retrunVisit已回访(用户可见),
	 * 9-reopen【回访不通过】(用户可见)，10-close关闭【状态8，并且review结果是pass】 <!-- end-user-doc
	 * -->
	 */
	public final Column REQUEST_STATUS = new Column(this, "request_status");
	/**
	 * <!-- begin-user-doc --> 转换为 * 1-"toReq"转为需求，2-"toBug"转为缺陷，3-"toTask"转为任务
	 * <!-- end-user-doc -->
	 */
	public final Column REQUEST_TRANS_TO = new Column(this, "request_trans_to");
	/**
	 * <!-- begin-user-doc --> 转出对象ID * <!-- end-user-doc -->
	 */
	public final Column REQUEST_TRANS_ID = new Column(this, "request_trans_id");
	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public final Column DELETED = new Column(this, "deleted");
	/**
	 * <!-- begin-user-doc --> 回复描述 * <!-- end-user-doc -->
	 */
	public final Column REPLY_SPEC = new Column(this, "reply_spec");
	/**
	 * <!-- begin-user-doc --> 回复者 * 客服 <!-- end-user-doc -->
	 */
	public final Column REPLIER = new Column(this, "replier");
	/**
	 * <!-- begin-user-doc --> 回复时间 * <!-- end-user-doc -->
	 */
	public final Column REPLY_DATE = new Column(this, "reply_date");

	public ServiceRequestTable() {
		super("service_request");
	}

	public boolean isAutoGeneratedKeys() {
		return true;
	}

	public Class getPojoType() {
		return ServiceRequest.class;
	}

}
