/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> faq分类 * <!-- end-user-doc -->
 */
public class ServiceFaqType implements Serializable {

	public static final Integer DELETE_YES = 1;

	public static final Integer DELETE_NO = 0;

	/**
	 * <!-- begin-user-doc --> 问题类型id * <!-- end-user-doc -->
	 */
	private Integer faqTypeId;

	/**
	 * <!-- begin-user-doc --> 问答类型 * <!-- end-user-doc -->
	 */
	private String faqType;

	/**
	 * <!-- begin-user-doc --> 父级问题类型id * <!-- end-user-doc -->
	 */
	private Integer faqParentTypeId;

	/**
	 * <!-- begin-user-doc --> faq类型创建时间 * <!-- end-user-doc -->
	 */
	private Date faqTypeCreatDay;

	/**
	 * <!-- begin-user-doc --> 创建人 * <!-- end-user-doc -->
	 */
	private String faqCreatedBy;

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	private Integer deleted;

	public ServiceFaqType() {
		setDeleted(DELETE_NO);
	}

	/**
	 * <!-- begin-user-doc --> 问题类型id * <!-- end-user-doc -->
	 */
	public void setFaqTypeId(Integer faqTypeId) {
		this.faqTypeId = faqTypeId;
	}

	public Integer getFaqTypeId() {
		return faqTypeId;
	}

	/**
	 * <!-- begin-user-doc --> 问答类型 * <!-- end-user-doc -->
	 */
	public void setFaqType(String faqType) {
		this.faqType = faqType;
	}

	public String getFaqType() {
		return faqType;
	}

	/**
	 * <!-- begin-user-doc --> 父级问题类型id * <!-- end-user-doc -->
	 */
	public void setFaqParentTypeId(Integer faqParentTypeId) {
		this.faqParentTypeId = faqParentTypeId;
	}

	public Integer getFaqParentTypeId() {
		return faqParentTypeId;
	}

	/**
	 * <!-- begin-user-doc --> faq类型创建时间 * <!-- end-user-doc -->
	 */
	public void setFaqTypeCreatDay(Date faqTypeCreatDay) {
		this.faqTypeCreatDay = faqTypeCreatDay;
	}

	public Date getFaqTypeCreatDay() {
		return faqTypeCreatDay;
	}

	/**
	 * <!-- begin-user-doc --> 创建人 * <!-- end-user-doc -->
	 */
	public void setFaqCreatedBy(String faqCreatedBy) {
		this.faqCreatedBy = faqCreatedBy;
	}

	public String getFaqCreatedBy() {
		return faqCreatedBy;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Integer getDeleted() {
		return deleted;
	}

}
