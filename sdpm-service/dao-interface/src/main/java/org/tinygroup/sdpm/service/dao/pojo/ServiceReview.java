/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 客服回访表 * <!-- end-user-doc -->
 */
public class ServiceReview implements Serializable {

	/**
	 * <!-- begin-user-doc --> 回访ID * <!-- end-user-doc -->
	 */
	private Integer reviewId;

	/**
	 * <!-- begin-user-doc --> 请求ID * 服务请求ID <!-- end-user-doc -->
	 */
	private Integer clientRequestId;

	/**
	 * <!-- begin-user-doc --> 回访描述 * <!-- end-user-doc -->
	 */
	private String reviewSpec;

	/**
	 * <!-- begin-user-doc --> 联系人 * 默认为需求提交人或产品客户其他联系人 <!-- end-user-doc -->
	 */
	private String requester;

	/**
	 * <!-- begin-user-doc --> 回访者 * 回访人account，当前操作者 <!-- end-user-doc -->
	 */
	private String reviewer;

	/**
	 * <!-- begin-user-doc --> 回访时间 * <!-- end-user-doc -->
	 */
	private Date reviewDate;

	/**
	 * <!-- begin-user-doc --> 回访结果 * 0-not pass, 1-pass <!-- end-user-doc -->
	 */
	private Integer reviewResult;

	/**
	 * <!-- begin-user-doc --> 回访评分 * 0很不满意，1不满意，2一般，3满意，4非常满意 <!-- end-user-doc
	 * -->
	 */
	private Integer reviewScore;

	/**
	 * <!-- begin-user-doc --> 回访类型 * 0-发放回访，1-拒绝需求回访，2-无修改需求回访 <!--
	 * end-user-doc -->
	 */
	private Integer reviewType;

	/**
	 * <!-- begin-user-doc --> 回访ID * <!-- end-user-doc -->
	 */
	public void setReviewId(Integer reviewId) {
		this.reviewId = reviewId;
	}

	public Integer getReviewId() {
		return reviewId;
	}

	/**
	 * <!-- begin-user-doc --> 请求ID * 服务请求ID <!-- end-user-doc -->
	 */
	public void setClientRequestId(Integer clientRequestId) {
		this.clientRequestId = clientRequestId;
	}

	public Integer getClientRequestId() {
		return clientRequestId;
	}

	/**
	 * <!-- begin-user-doc --> 回访描述 * <!-- end-user-doc -->
	 */
	public void setReviewSpec(String reviewSpec) {
		this.reviewSpec = reviewSpec;
	}

	public String getReviewSpec() {
		return reviewSpec;
	}

	/**
	 * <!-- begin-user-doc --> 联系人 * 默认为需求提交人或产品客户其他联系人 <!-- end-user-doc -->
	 */
	public void setRequester(String requester) {
		this.requester = requester;
	}

	public String getRequester() {
		return requester;
	}

	/**
	 * <!-- begin-user-doc --> 回访者 * 回访人account，当前操作者 <!-- end-user-doc -->
	 */
	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}

	public String getReviewer() {
		return reviewer;
	}

	/**
	 * <!-- begin-user-doc --> 回访时间 * <!-- end-user-doc -->
	 */
	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	public Date getReviewDate() {
		return reviewDate;
	}

	/**
	 * <!-- begin-user-doc --> 回访结果 * 0-not pass, 1-pass <!-- end-user-doc -->
	 */
	public void setReviewResult(Integer reviewResult) {
		this.reviewResult = reviewResult;
	}

	public Integer getReviewResult() {
		return reviewResult;
	}

	/**
	 * <!-- begin-user-doc --> 回访评分 * 0很不满意，1不满意，2一般，3满意，4非常满意 <!-- end-user-doc
	 * -->
	 */
	public void setReviewScore(Integer reviewScore) {
		this.reviewScore = reviewScore;
	}

	public Integer getReviewScore() {
		return reviewScore;
	}

	/**
	 * <!-- begin-user-doc --> 回访类型 * 0-发放回访，1-拒绝需求回访，2-无修改需求回访 <!--
	 * end-user-doc -->
	 */
	public void setReviewType(Integer reviewType) {
		this.reviewType = reviewType;
	}

	public Integer getReviewType() {
		return reviewType;
	}

}
