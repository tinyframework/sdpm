/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.pojo;

import java.io.Serializable;

/**
 * <!-- begin-user-doc --> 客户联系人信息表 * <!-- end-user-doc -->
 */
public class ServiceClientUser implements Serializable{

	public static final Integer DELETE_YES = 1;

	public static final Integer DELETE_NO = 0;

	/**
	 * <!-- begin-user-doc --> 逻辑ID * <!-- end-user-doc -->
	 */
	private Integer id;

	/**
	 * <!-- begin-user-doc --> 客户ID * <!-- end-user-doc -->
	 */
	private Integer clientId;

	/**
	 * <!-- begin-user-doc --> 客户联系人 * <!-- end-user-doc -->
	 */
	private String userAccount;

	/**
	 * <!-- begin-user-doc --> 客户联系电话 * <!-- end-user-doc -->
	 */
	private String userPhone;

	/**
	 * <!-- begin-user-doc --> 联系人职务 * <!-- end-user-doc -->
	 */
	private String userPost;

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	private Integer deleted;

	public ServiceClientUser() {
		setDeleted(DELETE_NO);
	}

	/**
	 * <!-- begin-user-doc --> 逻辑ID * <!-- end-user-doc -->
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc --> 客户ID * <!-- end-user-doc -->
	 */
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public Integer getClientId() {
		return clientId;
	}

	/**
	 * <!-- begin-user-doc --> 客户联系人 * <!-- end-user-doc -->
	 */
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public String getUserAccount() {
		return userAccount;
	}

	/**
	 * <!-- begin-user-doc --> 客户联系电话 * <!-- end-user-doc -->
	 */
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getUserPhone() {
		return userPhone;
	}

	/**
	 * <!-- begin-user-doc --> 联系人职务 * <!-- end-user-doc -->
	 */
	public void setUserPost(String userPost) {
		this.userPost = userPost;
	}

	public String getUserPost() {
		return userPost;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Integer getDeleted() {
		return deleted;
	}

}
