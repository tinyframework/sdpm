/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * <!-- begin-user-doc --> 问答表 * <!-- end-user-doc -->
 */
public class ServiceFaq implements Serializable {

	public static final Integer DELETE_YES = 1;

	public static final Integer DELETE_NO = 0;

	/**
	 * <!-- begin-user-doc --> 问答ID * <!-- end-user-doc -->
	 */
	private Integer faqId;

	/**
	 * <!-- begin-user-doc --> 问题描述 * <!-- end-user-doc -->
	 */
	private String faqQuestion;

	/**
	 * <!-- begin-user-doc --> 解答 * <!-- end-user-doc -->
	 */
	private String faqAnswer;

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	private Integer deleted;

	/**
	 * <!-- begin-user-doc --> 创建时间 * <!-- end-user-doc -->
	 */
	private Date faqCreateDate;

	/**
	 * <!-- begin-user-doc --> 创建人 * <!-- end-user-doc -->
	 */
	private String faqCreatedBy;

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	private Integer productId;

	/**
	 * <!-- begin-user-doc --> 关键字 * <!-- end-user-doc -->
	 */
	private String faqKeywords;

	/**
	 * <!-- begin-user-doc --> 来源ID * <!-- end-user-doc -->
	 */
	private Integer faqSourceId;

	/**
	 * <!-- begin-user-doc --> FAQ来源 * <!-- end-user-doc -->
	 */
	private String faqSource;

	/**
	 * <!-- begin-user-doc --> 回复者 * <!-- end-user-doc -->
	 */
	private String faqRepliedBy;

	/**
	 * <!-- begin-user-doc --> FAQ_REPLY_DATE * <!-- end-user-doc -->
	 */
	private Date faqReplyDate;

	/**
	 * <!-- begin-user-doc --> 问题类型id * <!-- end-user-doc -->
	 */
	private Integer faqTypeId;

	public ServiceFaq() {
		setDeleted(DELETE_NO);
	}

	/**
	 * <!-- begin-user-doc --> 问答ID * <!-- end-user-doc -->
	 */
	public void setFaqId(Integer faqId) {
		this.faqId = faqId;
	}

	public Integer getFaqId() {
		return faqId;
	}

	/**
	 * <!-- begin-user-doc --> 问题描述 * <!-- end-user-doc -->
	 */
	public void setFaqQuestion(String faqQuestion) {
		this.faqQuestion = faqQuestion;
	}

	public String getFaqQuestion() {
		return faqQuestion;
	}

	/**
	 * <!-- begin-user-doc --> 解答 * <!-- end-user-doc -->
	 */
	public void setFaqAnswer(String faqAnswer) {
		this.faqAnswer = faqAnswer;
	}

	public String getFaqAnswer() {
		return faqAnswer;
	}

	/**
	 * <!-- begin-user-doc --> 已删除 * <!-- end-user-doc -->
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Integer getDeleted() {
		return deleted;
	}

	/**
	 * <!-- begin-user-doc --> 创建时间 * <!-- end-user-doc -->
	 */
	public void setFaqCreateDate(Date faqCreateDate) {
		this.faqCreateDate = faqCreateDate;
	}

	public Date getFaqCreateDate() {
		return faqCreateDate;
	}

	/**
	 * <!-- begin-user-doc --> 创建人 * <!-- end-user-doc -->
	 */
	public void setFaqCreatedBy(String faqCreatedBy) {
		this.faqCreatedBy = faqCreatedBy;
	}

	public String getFaqCreatedBy() {
		return faqCreatedBy;
	}

	/**
	 * <!-- begin-user-doc --> 产品ID * <!-- end-user-doc -->
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getProductId() {
		return productId;
	}

	/**
	 * <!-- begin-user-doc --> 关键字 * <!-- end-user-doc -->
	 */
	public void setFaqKeywords(String faqKeywords) {
		this.faqKeywords = faqKeywords;
	}

	public String getFaqKeywords() {
		return faqKeywords;
	}

	/**
	 * <!-- begin-user-doc --> 来源ID * <!-- end-user-doc -->
	 */
	public void setFaqSourceId(Integer faqSourceId) {
		this.faqSourceId = faqSourceId;
	}

	public Integer getFaqSourceId() {
		return faqSourceId;
	}

	/**
	 * <!-- begin-user-doc --> FAQ来源 * <!-- end-user-doc -->
	 */
	public void setFaqSource(String faqSource) {
		this.faqSource = faqSource;
	}

	public String getFaqSource() {
		return faqSource;
	}

	/**
	 * <!-- begin-user-doc --> 回复者 * <!-- end-user-doc -->
	 */
	public void setFaqRepliedBy(String faqRepliedBy) {
		this.faqRepliedBy = faqRepliedBy;
	}

	public String getFaqRepliedBy() {
		return faqRepliedBy;
	}

	/**
	 * <!-- begin-user-doc --> FAQ_REPLY_DATE * <!-- end-user-doc -->
	 */
	public void setFaqReplyDate(Date faqReplyDate) {
		this.faqReplyDate = faqReplyDate;
	}

	public Date getFaqReplyDate() {
		return faqReplyDate;
	}

	/**
	 * <!-- begin-user-doc --> 问题类型id * <!-- end-user-doc -->
	 */
	public void setFaqTypeId(Integer faqTypeId) {
		this.faqTypeId = faqTypeId;
	}

	public Integer getFaqTypeId() {
		return faqTypeId;
	}

}
