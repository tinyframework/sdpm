/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.service.dao.constant.ServiceClientUserTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.service.dao.constant.ServiceClientUserTable.SERVICE_CLIENT_USERTABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.service.dao.pojo.ServiceClientUser;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.service.dao.ServiceClientUserDao;

import org.tinygroup.sdpm.service.dao.pojo.ServiceClient;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class ServiceClientUserDaoImpl extends TinyDslDaoSupport implements
		ServiceClientUserDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ServiceClientUser add(ServiceClientUser serviceClientUser) {
		return getDslTemplate().insertAndReturnKey(serviceClientUser,
				new InsertGenerateCallback<ServiceClientUser>() {
					public Insert generate(ServiceClientUser t) {
						Insert insert = insertInto(SERVICE_CLIENT_USER_TABLE)
								.values(SERVICE_CLIENT_USER_TABLE.ID.value(t
										.getId()),
										SERVICE_CLIENT_USER_TABLE.CLIENT_ID
												.value(t.getClientId()),
										SERVICE_CLIENT_USER_TABLE.USER_ACCOUNT
												.value(t.getUserAccount()),
										SERVICE_CLIENT_USER_TABLE.USER_PHONE
												.value(t.getUserPhone()),
										SERVICE_CLIENT_USER_TABLE.USER_POST
												.value(t.getUserPost()),
										SERVICE_CLIENT_USER_TABLE.DELETED
												.value(t.getDeleted())

								);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(ServiceClientUser serviceClientUser) {
		if (serviceClientUser == null || serviceClientUser.getId() == null) {
			return 0;
		}
		return getDslTemplate().update(serviceClientUser,
				new UpdateGenerateCallback<ServiceClientUser>() {
					public Update generate(ServiceClientUser t) {
						Update update = update(SERVICE_CLIENT_USER_TABLE).set(
								SERVICE_CLIENT_USER_TABLE.CLIENT_ID.value(t
										.getClientId()),
								SERVICE_CLIENT_USER_TABLE.USER_ACCOUNT.value(t
										.getUserAccount()),
								SERVICE_CLIENT_USER_TABLE.USER_PHONE.value(t
										.getUserPhone()),
								SERVICE_CLIENT_USER_TABLE.USER_POST.value(t
										.getUserPost()),
								SERVICE_CLIENT_USER_TABLE.DELETED.value(t
										.getDeleted())).where(
								SERVICE_CLIENT_USER_TABLE.ID.eq(t.getId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(SERVICE_CLIENT_USER_TABLE).where(
								SERVICE_CLIENT_USER_TABLE.ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(SERVICE_CLIENT_USER_TABLE).where(
								SERVICE_CLIENT_USER_TABLE.ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ServiceClientUser getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, ServiceClientUser.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(SERVICE_CLIENT_USER_TABLE).where(
								SERVICE_CLIENT_USER_TABLE.ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<ServiceClientUser> query(ServiceClientUser serviceClientUser,
			final OrderBy... orderArgs) {
		if (serviceClientUser == null) {
			serviceClientUser = new ServiceClientUser();
		}
		return getDslTemplate().query(serviceClientUser,
				new SelectGenerateCallback<ServiceClientUser>() {
					@SuppressWarnings("rawtypes")
					public Select generate(ServiceClientUser t) {
						Select select = selectFrom(SERVICE_CLIENT_USER_TABLE)
								.where(and(SERVICE_CLIENT_USER_TABLE.ID.eq(t
										.getId()),
										SERVICE_CLIENT_USER_TABLE.CLIENT_ID
												.eq(t.getClientId()),
										SERVICE_CLIENT_USER_TABLE.USER_ACCOUNT
												.eq(t.getUserAccount()),
										SERVICE_CLIENT_USER_TABLE.USER_PHONE
												.eq(t.getUserPhone()),
										SERVICE_CLIENT_USER_TABLE.USER_POST
												.eq(t.getUserPost()),
										SERVICE_CLIENT_USER_TABLE.DELETED.eq(t
												.getDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<ServiceClientUser> queryPager(int start, int limit,
			ServiceClientUser serviceClientUser, final OrderBy... orderArgs) {
		if (serviceClientUser == null) {
			serviceClientUser = new ServiceClientUser();
		}
		return getDslTemplate().queryPager(start, limit, serviceClientUser,
				false, new SelectGenerateCallback<ServiceClientUser>() {
					public Select generate(ServiceClientUser t) {
						Select select = Select.selectFrom(
								SERVICE_CLIENT_USER_TABLE).where(
								and(SERVICE_CLIENT_USER_TABLE.ID.eq(t.getId()),
										SERVICE_CLIENT_USER_TABLE.CLIENT_ID
												.eq(t.getClientId()),
										SERVICE_CLIENT_USER_TABLE.USER_ACCOUNT
												.eq(t.getUserAccount()),
										SERVICE_CLIENT_USER_TABLE.USER_PHONE
												.eq(t.getUserPhone()),
										SERVICE_CLIENT_USER_TABLE.USER_POST
												.eq(t.getUserPost()),
										SERVICE_CLIENT_USER_TABLE.DELETED.eq(t
												.getDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<ServiceClientUser> serviceClientUser) {
		if (CollectionUtil.isEmpty(serviceClientUser)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys,
				serviceClientUser,
				new InsertGenerateCallback<ServiceClientUser>() {

					public Insert generate(ServiceClientUser t) {
						return insertInto(SERVICE_CLIENT_USER_TABLE).values(
								SERVICE_CLIENT_USER_TABLE.ID.value(t.getId()),
								SERVICE_CLIENT_USER_TABLE.CLIENT_ID.value(t
										.getClientId()),
								SERVICE_CLIENT_USER_TABLE.USER_ACCOUNT.value(t
										.getUserAccount()),
								SERVICE_CLIENT_USER_TABLE.USER_PHONE.value(t
										.getUserPhone()),
								SERVICE_CLIENT_USER_TABLE.USER_POST.value(t
										.getUserPost()),
								SERVICE_CLIENT_USER_TABLE.DELETED.value(t
										.getDeleted())

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<ServiceClientUser> serviceClientUsers) {
		return batchInsert(true, serviceClientUsers);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<ServiceClientUser> serviceClientUser) {
		if (CollectionUtil.isEmpty(serviceClientUser)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(serviceClientUser,
				new UpdateGenerateCallback<ServiceClientUser>() {
					public Update generate(ServiceClientUser t) {
						return update(SERVICE_CLIENT_USER_TABLE).set(
								SERVICE_CLIENT_USER_TABLE.CLIENT_ID.value(t
										.getClientId()),
								SERVICE_CLIENT_USER_TABLE.USER_ACCOUNT.value(t
										.getUserAccount()),
								SERVICE_CLIENT_USER_TABLE.USER_PHONE.value(t
										.getUserPhone()),
								SERVICE_CLIENT_USER_TABLE.USER_POST.value(t
										.getUserPost()),
								SERVICE_CLIENT_USER_TABLE.DELETED.value(t
										.getDeleted())

						).where(SERVICE_CLIENT_USER_TABLE.ID.eq(t.getId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<ServiceClientUser> serviceClientUser) {
		if (CollectionUtil.isEmpty(serviceClientUser)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(serviceClientUser,
				new DeleteGenerateCallback<ServiceClientUser>() {
					public Delete generate(ServiceClientUser t) {
						return delete(SERVICE_CLIENT_USER_TABLE).where(
								and(SERVICE_CLIENT_USER_TABLE.ID.eq(t.getId()),
										SERVICE_CLIENT_USER_TABLE.CLIENT_ID
												.eq(t.getClientId()),
										SERVICE_CLIENT_USER_TABLE.USER_ACCOUNT
												.eq(t.getUserAccount()),
										SERVICE_CLIENT_USER_TABLE.USER_PHONE
												.eq(t.getUserPhone()),
										SERVICE_CLIENT_USER_TABLE.USER_POST
												.eq(t.getUserPost()),
										SERVICE_CLIENT_USER_TABLE.DELETED.eq(t
												.getDeleted())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = orderBies[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	public Integer softDelete(Integer id) {
		return getDslTemplate().update(id,
				new UpdateGenerateCallback<Integer>() {
					public Update generate(Integer id) {
						Update update = update(SERVICE_CLIENT_USERTABLE).set(
								SERVICE_CLIENT_USERTABLE.DELETED
										.value(ServiceClient.DELETE_YES))
								.where(SERVICE_CLIENT_USERTABLE.ID.eq(id));
						return update;
					}
				});

	}

	public Integer softAllDelete(Integer id) {
		return getDslTemplate().update(id,
				new UpdateGenerateCallback<Integer>() {
					public Update generate(Integer id) {
						Update update = update(SERVICE_CLIENT_USERTABLE).set(
								SERVICE_CLIENT_USERTABLE.DELETED
										.value(ServiceClient.DELETE_YES))
								.where(SERVICE_CLIENT_USERTABLE.CLIENT_ID
										.eq(id));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<ServiceClientUser> serviceClientUser) {
		if (CollectionUtil.isEmpty(serviceClientUser)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys,
				serviceClientUser, new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(SERVICE_CLIENT_USER_TABLE)
								.values(SERVICE_CLIENT_USER_TABLE.CLIENT_ID
										.value(new JdbcNamedParameter(
												"clientId")),
										SERVICE_CLIENT_USER_TABLE.USER_ACCOUNT
												.value(new JdbcNamedParameter(
														"userAccount")),
										SERVICE_CLIENT_USER_TABLE.USER_PHONE
												.value(new JdbcNamedParameter(
														"userPhone")),
										SERVICE_CLIENT_USER_TABLE.USER_POST
												.value(new JdbcNamedParameter(
														"userPost")),
										SERVICE_CLIENT_USER_TABLE.DELETED
												.value(new JdbcNamedParameter(
														"deleted"))

								);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<ServiceClientUser> serviceClientUser) {
		if (CollectionUtil.isEmpty(serviceClientUser)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(serviceClientUser,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(SERVICE_CLIENT_USER_TABLE)
								.set(SERVICE_CLIENT_USER_TABLE.CLIENT_ID
										.value(new JdbcNamedParameter(
												"clientId")),
										SERVICE_CLIENT_USER_TABLE.USER_ACCOUNT
												.value(new JdbcNamedParameter(
														"userAccount")),
										SERVICE_CLIENT_USER_TABLE.USER_PHONE
												.value(new JdbcNamedParameter(
														"userPhone")),
										SERVICE_CLIENT_USER_TABLE.USER_POST
												.value(new JdbcNamedParameter(
														"userPost")),
										SERVICE_CLIENT_USER_TABLE.DELETED
												.value(new JdbcNamedParameter(
														"deleted"))

								).where(SERVICE_CLIENT_USER_TABLE.ID
										.eq(new JdbcNamedParameter("id")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<ServiceClientUser> serviceClientUser) {
		if (CollectionUtil.isEmpty(serviceClientUser)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(serviceClientUser,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(SERVICE_CLIENT_USER_TABLE)
								.where(and(
										SERVICE_CLIENT_USER_TABLE.CLIENT_ID
												.eq(new JdbcNamedParameter(
														"clientId")),
										SERVICE_CLIENT_USER_TABLE.USER_ACCOUNT
												.eq(new JdbcNamedParameter(
														"userAccount")),
										SERVICE_CLIENT_USER_TABLE.USER_PHONE
												.eq(new JdbcNamedParameter(
														"userPhone")),
										SERVICE_CLIENT_USER_TABLE.USER_POST
												.eq(new JdbcNamedParameter(
														"userPost")),
										SERVICE_CLIENT_USER_TABLE.DELETED
												.eq(new JdbcNamedParameter(
														"deleted"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<ServiceClientUser> serviceClientUser) {
		return preparedBatchInsert(true, serviceClientUser);
	}

}
