/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.service.dao.constant.ServiceReviewTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.service.dao.constant.ServiceReviewTable.SERVICE_REVIEWTABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.service.dao.pojo.ServiceReview;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.service.dao.ServiceReviewDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class ServiceReviewDaoImpl extends TinyDslDaoSupport implements
		ServiceReviewDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ServiceReview add(ServiceReview serviceReview) {
		return getDslTemplate().insertAndReturnKey(serviceReview,
				new InsertGenerateCallback<ServiceReview>() {
					public Insert generate(ServiceReview t) {
						Insert insert = insertInto(SERVICE_REVIEW_TABLE)
								.values(SERVICE_REVIEW_TABLE.REVIEW_ID.value(t
										.getReviewId()),
										SERVICE_REVIEW_TABLE.CLIENT_REQUEST_ID
												.value(t.getClientRequestId()),
										SERVICE_REVIEW_TABLE.REVIEW_SPEC
												.value(t.getReviewSpec()),
										SERVICE_REVIEW_TABLE.REQUESTER.value(t
												.getRequester()),
										SERVICE_REVIEW_TABLE.REVIEWER.value(t
												.getReviewer()),
										SERVICE_REVIEW_TABLE.REVIEW_DATE
												.value(t.getReviewDate()),
										SERVICE_REVIEW_TABLE.REVIEW_RESULT
												.value(t.getReviewResult()),
										SERVICE_REVIEW_TABLE.REVIEW_SCORE
												.value(t.getReviewScore()),
										SERVICE_REVIEW_TABLE.REVIEW_TYPE
												.value(t.getReviewType())

								);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(ServiceReview serviceReview) {
		if (serviceReview == null || serviceReview.getReviewId() == null) {
			return 0;
		}
		return getDslTemplate().update(serviceReview,
				new UpdateGenerateCallback<ServiceReview>() {
					public Update generate(ServiceReview t) {
						Update update = update(SERVICE_REVIEW_TABLE).set(
								SERVICE_REVIEW_TABLE.CLIENT_REQUEST_ID.value(t
										.getClientRequestId()),
								SERVICE_REVIEW_TABLE.REVIEW_SPEC.value(t
										.getReviewSpec()),
								SERVICE_REVIEW_TABLE.REQUESTER.value(t
										.getRequester()),
								SERVICE_REVIEW_TABLE.REVIEWER.value(t
										.getReviewer()),
								SERVICE_REVIEW_TABLE.REVIEW_DATE.value(t
										.getReviewDate()),
								SERVICE_REVIEW_TABLE.REVIEW_RESULT.value(t
										.getReviewResult()),
								SERVICE_REVIEW_TABLE.REVIEW_SCORE.value(t
										.getReviewScore()),
								SERVICE_REVIEW_TABLE.REVIEW_TYPE.value(t
										.getReviewType())).where(
								SERVICE_REVIEW_TABLE.REVIEW_ID.eq(t
										.getReviewId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(SERVICE_REVIEW_TABLE).where(
								SERVICE_REVIEW_TABLE.REVIEW_ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(SERVICE_REVIEW_TABLE).where(
								SERVICE_REVIEW_TABLE.REVIEW_ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ServiceReview getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, ServiceReview.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(SERVICE_REVIEW_TABLE).where(
								SERVICE_REVIEW_TABLE.REVIEW_ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<ServiceReview> query(ServiceReview serviceReview,
			final OrderBy... orderArgs) {
		if (serviceReview == null) {
			serviceReview = new ServiceReview();
		}
		return getDslTemplate().query(serviceReview,
				new SelectGenerateCallback<ServiceReview>() {
					@SuppressWarnings("rawtypes")
					public Select generate(ServiceReview t) {
						Select select = selectFrom(SERVICE_REVIEW_TABLE).where(
								and(SERVICE_REVIEW_TABLE.REVIEW_ID.eq(t
										.getReviewId()),
										SERVICE_REVIEW_TABLE.CLIENT_REQUEST_ID
												.eq(t.getClientRequestId()),
										SERVICE_REVIEW_TABLE.REVIEW_SPEC.eq(t
												.getReviewSpec()),
										SERVICE_REVIEW_TABLE.REQUESTER.eq(t
												.getRequester()),
										SERVICE_REVIEW_TABLE.REVIEWER.eq(t
												.getReviewer()),
										SERVICE_REVIEW_TABLE.REVIEW_DATE.eq(t
												.getReviewDate()),
										SERVICE_REVIEW_TABLE.REVIEW_RESULT.eq(t
												.getReviewResult()),
										SERVICE_REVIEW_TABLE.REVIEW_SCORE.eq(t
												.getReviewScore()),
										SERVICE_REVIEW_TABLE.REVIEW_TYPE.eq(t
												.getReviewType())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<ServiceReview> queryPager(int start, int limit,
			ServiceReview serviceReview, final OrderBy... orderArgs) {
		if (serviceReview == null) {
			serviceReview = new ServiceReview();
		}
		return getDslTemplate().queryPager(start, limit, serviceReview, false,
				new SelectGenerateCallback<ServiceReview>() {
					public Select generate(ServiceReview t) {
						Select select = Select.selectFrom(SERVICE_REVIEW_TABLE)
								.where(and(SERVICE_REVIEW_TABLE.REVIEW_ID.eq(t
										.getReviewId()),
										SERVICE_REVIEW_TABLE.CLIENT_REQUEST_ID
												.eq(t.getClientRequestId()),
										SERVICE_REVIEW_TABLE.REVIEW_SPEC.eq(t
												.getReviewSpec()),
										SERVICE_REVIEW_TABLE.REQUESTER.eq(t
												.getRequester()),
										SERVICE_REVIEW_TABLE.REVIEWER.eq(t
												.getReviewer()),
										SERVICE_REVIEW_TABLE.REVIEW_DATE.eq(t
												.getReviewDate()),
										SERVICE_REVIEW_TABLE.REVIEW_RESULT.eq(t
												.getReviewResult()),
										SERVICE_REVIEW_TABLE.REVIEW_SCORE.eq(t
												.getReviewScore()),
										SERVICE_REVIEW_TABLE.REVIEW_TYPE.eq(t
												.getReviewType())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<ServiceReview> serviceReview) {
		if (CollectionUtil.isEmpty(serviceReview)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, serviceReview,
				new InsertGenerateCallback<ServiceReview>() {

					public Insert generate(ServiceReview t) {
						return insertInto(SERVICE_REVIEW_TABLE).values(
								SERVICE_REVIEW_TABLE.REVIEW_ID.value(t
										.getReviewId()),
								SERVICE_REVIEW_TABLE.CLIENT_REQUEST_ID.value(t
										.getClientRequestId()),
								SERVICE_REVIEW_TABLE.REVIEW_SPEC.value(t
										.getReviewSpec()),
								SERVICE_REVIEW_TABLE.REQUESTER.value(t
										.getRequester()),
								SERVICE_REVIEW_TABLE.REVIEWER.value(t
										.getReviewer()),
								SERVICE_REVIEW_TABLE.REVIEW_DATE.value(t
										.getReviewDate()),
								SERVICE_REVIEW_TABLE.REVIEW_RESULT.value(t
										.getReviewResult()),
								SERVICE_REVIEW_TABLE.REVIEW_SCORE.value(t
										.getReviewScore()),
								SERVICE_REVIEW_TABLE.REVIEW_TYPE.value(t
										.getReviewType())

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<ServiceReview> serviceReviews) {
		return batchInsert(true, serviceReviews);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<ServiceReview> serviceReview) {
		if (CollectionUtil.isEmpty(serviceReview)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(serviceReview,
				new UpdateGenerateCallback<ServiceReview>() {
					public Update generate(ServiceReview t) {
						return update(SERVICE_REVIEW_TABLE).set(
								SERVICE_REVIEW_TABLE.CLIENT_REQUEST_ID.value(t
										.getClientRequestId()),
								SERVICE_REVIEW_TABLE.REVIEW_SPEC.value(t
										.getReviewSpec()),
								SERVICE_REVIEW_TABLE.REQUESTER.value(t
										.getRequester()),
								SERVICE_REVIEW_TABLE.REVIEWER.value(t
										.getReviewer()),
								SERVICE_REVIEW_TABLE.REVIEW_DATE.value(t
										.getReviewDate()),
								SERVICE_REVIEW_TABLE.REVIEW_RESULT.value(t
										.getReviewResult()),
								SERVICE_REVIEW_TABLE.REVIEW_SCORE.value(t
										.getReviewScore()),
								SERVICE_REVIEW_TABLE.REVIEW_TYPE.value(t
										.getReviewType())

						).where(SERVICE_REVIEW_TABLE.REVIEW_ID.eq(t
								.getReviewId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<ServiceReview> serviceReview) {
		if (CollectionUtil.isEmpty(serviceReview)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(serviceReview,
				new DeleteGenerateCallback<ServiceReview>() {
					public Delete generate(ServiceReview t) {
						return delete(SERVICE_REVIEW_TABLE).where(
								and(SERVICE_REVIEW_TABLE.REVIEW_ID.eq(t
										.getReviewId()),
										SERVICE_REVIEW_TABLE.CLIENT_REQUEST_ID
												.eq(t.getClientRequestId()),
										SERVICE_REVIEW_TABLE.REVIEW_SPEC.eq(t
												.getReviewSpec()),
										SERVICE_REVIEW_TABLE.REQUESTER.eq(t
												.getRequester()),
										SERVICE_REVIEW_TABLE.REVIEWER.eq(t
												.getReviewer()),
										SERVICE_REVIEW_TABLE.REVIEW_DATE.eq(t
												.getReviewDate()),
										SERVICE_REVIEW_TABLE.REVIEW_RESULT.eq(t
												.getReviewResult()),
										SERVICE_REVIEW_TABLE.REVIEW_SCORE.eq(t
												.getReviewScore()),
										SERVICE_REVIEW_TABLE.REVIEW_TYPE.eq(t
												.getReviewType())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = orderBies[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	public ServiceReview findByRequestId(Integer id) {
		return getDslTemplate().getByKey(id, ServiceReview.class,
				new SelectGenerateCallback<Serializable>() {
					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(SERVICE_REVIEWTABLE).where(
								SERVICE_REVIEWTABLE.CLIENT_REQUEST_ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<ServiceReview> serviceReview) {
		if (CollectionUtil.isEmpty(serviceReview)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, serviceReview,
				new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(SERVICE_REVIEW_TABLE).values(
								SERVICE_REVIEW_TABLE.CLIENT_REQUEST_ID
										.value(new JdbcNamedParameter(
												"clientRequestId")),
								SERVICE_REVIEW_TABLE.REVIEW_SPEC
										.value(new JdbcNamedParameter(
												"reviewSpec")),
								SERVICE_REVIEW_TABLE.REQUESTER
										.value(new JdbcNamedParameter(
												"requester")),
								SERVICE_REVIEW_TABLE.REVIEWER
										.value(new JdbcNamedParameter(
												"reviewer")),
								SERVICE_REVIEW_TABLE.REVIEW_DATE
										.value(new JdbcNamedParameter(
												"reviewDate")),
								SERVICE_REVIEW_TABLE.REVIEW_RESULT
										.value(new JdbcNamedParameter(
												"reviewResult")),
								SERVICE_REVIEW_TABLE.REVIEW_SCORE
										.value(new JdbcNamedParameter(
												"reviewScore")),
								SERVICE_REVIEW_TABLE.REVIEW_TYPE
										.value(new JdbcNamedParameter(
												"reviewType"))

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<ServiceReview> serviceReview) {
		if (CollectionUtil.isEmpty(serviceReview)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(serviceReview,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(SERVICE_REVIEW_TABLE).set(
								SERVICE_REVIEW_TABLE.CLIENT_REQUEST_ID
										.value(new JdbcNamedParameter(
												"clientRequestId")),
								SERVICE_REVIEW_TABLE.REVIEW_SPEC
										.value(new JdbcNamedParameter(
												"reviewSpec")),
								SERVICE_REVIEW_TABLE.REQUESTER
										.value(new JdbcNamedParameter(
												"requester")),
								SERVICE_REVIEW_TABLE.REVIEWER
										.value(new JdbcNamedParameter(
												"reviewer")),
								SERVICE_REVIEW_TABLE.REVIEW_DATE
										.value(new JdbcNamedParameter(
												"reviewDate")),
								SERVICE_REVIEW_TABLE.REVIEW_RESULT
										.value(new JdbcNamedParameter(
												"reviewResult")),
								SERVICE_REVIEW_TABLE.REVIEW_SCORE
										.value(new JdbcNamedParameter(
												"reviewScore")),
								SERVICE_REVIEW_TABLE.REVIEW_TYPE
										.value(new JdbcNamedParameter(
												"reviewType"))

						).where(SERVICE_REVIEW_TABLE.REVIEW_ID
								.eq(new JdbcNamedParameter("reviewId")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<ServiceReview> serviceReview) {
		if (CollectionUtil.isEmpty(serviceReview)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(serviceReview,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(SERVICE_REVIEW_TABLE).where(
								and(SERVICE_REVIEW_TABLE.CLIENT_REQUEST_ID
										.eq(new JdbcNamedParameter(
												"clientRequestId")),
										SERVICE_REVIEW_TABLE.REVIEW_SPEC
												.eq(new JdbcNamedParameter(
														"reviewSpec")),
										SERVICE_REVIEW_TABLE.REQUESTER
												.eq(new JdbcNamedParameter(
														"requester")),
										SERVICE_REVIEW_TABLE.REVIEWER
												.eq(new JdbcNamedParameter(
														"reviewer")),
										SERVICE_REVIEW_TABLE.REVIEW_DATE
												.eq(new JdbcNamedParameter(
														"reviewDate")),
										SERVICE_REVIEW_TABLE.REVIEW_RESULT
												.eq(new JdbcNamedParameter(
														"reviewResult")),
										SERVICE_REVIEW_TABLE.REVIEW_SCORE
												.eq(new JdbcNamedParameter(
														"reviewScore")),
										SERVICE_REVIEW_TABLE.REVIEW_TYPE
												.eq(new JdbcNamedParameter(
														"reviewType"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<ServiceReview> serviceReview) {
		return preparedBatchInsert(true, serviceReview);
	}

}
