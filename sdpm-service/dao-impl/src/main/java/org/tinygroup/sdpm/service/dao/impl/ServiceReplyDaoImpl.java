/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.service.dao.constant.ServiceReplyTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.service.dao.constant.ServiceReplyTable.SERVICE_REPLYTABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.service.dao.pojo.ServiceReply;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.service.dao.ServiceReplyDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class ServiceReplyDaoImpl extends TinyDslDaoSupport implements
		ServiceReplyDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ServiceReply add(ServiceReply serviceReply) {
		return getDslTemplate().insertAndReturnKey(serviceReply,
				new InsertGenerateCallback<ServiceReply>() {
					public Insert generate(ServiceReply t) {
						Insert insert = insertInto(SERVICE_REPLY_TABLE).values(
								SERVICE_REPLY_TABLE.REPLY_ID.value(t
										.getReplyId()),
								SERVICE_REPLY_TABLE.CLIENT_REQUEST_ID.value(t
										.getClientRequestId()),
								SERVICE_REPLY_TABLE.REPLY_OPINION.value(t
										.getReplyOpinion()),
								SERVICE_REPLY_TABLE.REPLY_SPEC.value(t
										.getReplySpec()),
								SERVICE_REPLY_TABLE.REPLY_COMMITMENT_DATE
										.value(t.getReplyCommitmentDate()),
								SERVICE_REPLY_TABLE.REPLY_DO_BY.value(t
										.getReplyDoBy()),
								SERVICE_REPLY_TABLE.REPLY_DO_DATE.value(t
										.getReplyDoDate()),
								SERVICE_REPLY_TABLE.REPLIER.value(t
										.getReplier()),
								SERVICE_REPLY_TABLE.REPLY_DATE.value(t
										.getReplyDate()),
								SERVICE_REPLY_TABLE.REPLY_DONE.value(t
										.getReplyDone())

						);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(ServiceReply serviceReply) {
		if (serviceReply == null || serviceReply.getReplyId() == null) {
			return 0;
		}
		return getDslTemplate().update(serviceReply,
				new UpdateGenerateCallback<ServiceReply>() {
					public Update generate(ServiceReply t) {
						Update update = update(SERVICE_REPLY_TABLE).set(
								SERVICE_REPLY_TABLE.CLIENT_REQUEST_ID.value(t
										.getClientRequestId()),
								SERVICE_REPLY_TABLE.REPLY_OPINION.value(t
										.getReplyOpinion()),
								SERVICE_REPLY_TABLE.REPLY_SPEC.value(t
										.getReplySpec()),
								SERVICE_REPLY_TABLE.REPLY_COMMITMENT_DATE
										.value(t.getReplyCommitmentDate()),
								SERVICE_REPLY_TABLE.REPLY_DO_BY.value(t
										.getReplyDoBy()),
								SERVICE_REPLY_TABLE.REPLY_DO_DATE.value(t
										.getReplyDoDate()),
								SERVICE_REPLY_TABLE.REPLIER.value(t
										.getReplier()),
								SERVICE_REPLY_TABLE.REPLY_DATE.value(t
										.getReplyDate()),
								SERVICE_REPLY_TABLE.REPLY_DONE.value(t
										.getReplyDone()))
								.where(SERVICE_REPLY_TABLE.REPLY_ID.eq(t
										.getReplyId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(SERVICE_REPLY_TABLE).where(
								SERVICE_REPLY_TABLE.REPLY_ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(SERVICE_REPLY_TABLE).where(
								SERVICE_REPLY_TABLE.REPLY_ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ServiceReply getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, ServiceReply.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(SERVICE_REPLY_TABLE).where(
								SERVICE_REPLY_TABLE.REPLY_ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<ServiceReply> query(ServiceReply serviceReply,
			final OrderBy... orderArgs) {
		if (serviceReply == null) {
			serviceReply = new ServiceReply();
		}
		return getDslTemplate().query(serviceReply,
				new SelectGenerateCallback<ServiceReply>() {
					@SuppressWarnings("rawtypes")
					public Select generate(ServiceReply t) {
						Select select = selectFrom(SERVICE_REPLY_TABLE)
								.where(and(
										SERVICE_REPLY_TABLE.REPLY_ID.eq(t
												.getReplyId()),
										SERVICE_REPLY_TABLE.CLIENT_REQUEST_ID
												.eq(t.getClientRequestId()),
										SERVICE_REPLY_TABLE.REPLY_OPINION.eq(t
												.getReplyOpinion()),
										SERVICE_REPLY_TABLE.REPLY_SPEC.eq(t
												.getReplySpec()),
										SERVICE_REPLY_TABLE.REPLY_COMMITMENT_DATE
												.eq(t.getReplyCommitmentDate()),
										SERVICE_REPLY_TABLE.REPLY_DO_BY.eq(t
												.getReplyDoBy()),
										SERVICE_REPLY_TABLE.REPLY_DO_DATE.eq(t
												.getReplyDoDate()),
										SERVICE_REPLY_TABLE.REPLIER.eq(t
												.getReplier()),
										SERVICE_REPLY_TABLE.REPLY_DATE.eq(t
												.getReplyDate()),
										SERVICE_REPLY_TABLE.REPLY_DONE.eq(t
												.getReplyDone())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<ServiceReply> queryPager(int start, int limit,
			ServiceReply serviceReply, final OrderBy... orderArgs) {
		if (serviceReply == null) {
			serviceReply = new ServiceReply();
		}
		return getDslTemplate().queryPager(start, limit, serviceReply, false,
				new SelectGenerateCallback<ServiceReply>() {
					public Select generate(ServiceReply t) {
						Select select = Select
								.selectFrom(SERVICE_REPLY_TABLE)
								.where(and(
										SERVICE_REPLY_TABLE.REPLY_ID.eq(t
												.getReplyId()),
										SERVICE_REPLY_TABLE.CLIENT_REQUEST_ID
												.eq(t.getClientRequestId()),
										SERVICE_REPLY_TABLE.REPLY_OPINION.eq(t
												.getReplyOpinion()),
										SERVICE_REPLY_TABLE.REPLY_SPEC.eq(t
												.getReplySpec()),
										SERVICE_REPLY_TABLE.REPLY_COMMITMENT_DATE
												.eq(t.getReplyCommitmentDate()),
										SERVICE_REPLY_TABLE.REPLY_DO_BY.eq(t
												.getReplyDoBy()),
										SERVICE_REPLY_TABLE.REPLY_DO_DATE.eq(t
												.getReplyDoDate()),
										SERVICE_REPLY_TABLE.REPLIER.eq(t
												.getReplier()),
										SERVICE_REPLY_TABLE.REPLY_DATE.eq(t
												.getReplyDate()),
										SERVICE_REPLY_TABLE.REPLY_DONE.eq(t
												.getReplyDone())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<ServiceReply> serviceReply) {
		if (CollectionUtil.isEmpty(serviceReply)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, serviceReply,
				new InsertGenerateCallback<ServiceReply>() {

					public Insert generate(ServiceReply t) {
						return insertInto(SERVICE_REPLY_TABLE).values(
								SERVICE_REPLY_TABLE.REPLY_ID.value(t
										.getReplyId()),
								SERVICE_REPLY_TABLE.CLIENT_REQUEST_ID.value(t
										.getClientRequestId()),
								SERVICE_REPLY_TABLE.REPLY_OPINION.value(t
										.getReplyOpinion()),
								SERVICE_REPLY_TABLE.REPLY_SPEC.value(t
										.getReplySpec()),
								SERVICE_REPLY_TABLE.REPLY_COMMITMENT_DATE
										.value(t.getReplyCommitmentDate()),
								SERVICE_REPLY_TABLE.REPLY_DO_BY.value(t
										.getReplyDoBy()),
								SERVICE_REPLY_TABLE.REPLY_DO_DATE.value(t
										.getReplyDoDate()),
								SERVICE_REPLY_TABLE.REPLIER.value(t
										.getReplier()),
								SERVICE_REPLY_TABLE.REPLY_DATE.value(t
										.getReplyDate()),
								SERVICE_REPLY_TABLE.REPLY_DONE.value(t
										.getReplyDone())

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<ServiceReply> serviceReplys) {
		return batchInsert(true, serviceReplys);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<ServiceReply> serviceReply) {
		if (CollectionUtil.isEmpty(serviceReply)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(serviceReply,
				new UpdateGenerateCallback<ServiceReply>() {
					public Update generate(ServiceReply t) {
						return update(SERVICE_REPLY_TABLE).set(
								SERVICE_REPLY_TABLE.CLIENT_REQUEST_ID.value(t
										.getClientRequestId()),
								SERVICE_REPLY_TABLE.REPLY_OPINION.value(t
										.getReplyOpinion()),
								SERVICE_REPLY_TABLE.REPLY_SPEC.value(t
										.getReplySpec()),
								SERVICE_REPLY_TABLE.REPLY_COMMITMENT_DATE
										.value(t.getReplyCommitmentDate()),
								SERVICE_REPLY_TABLE.REPLY_DO_BY.value(t
										.getReplyDoBy()),
								SERVICE_REPLY_TABLE.REPLY_DO_DATE.value(t
										.getReplyDoDate()),
								SERVICE_REPLY_TABLE.REPLIER.value(t
										.getReplier()),
								SERVICE_REPLY_TABLE.REPLY_DATE.value(t
										.getReplyDate()),
								SERVICE_REPLY_TABLE.REPLY_DONE.value(t
										.getReplyDone())

						)
								.where(SERVICE_REPLY_TABLE.REPLY_ID.eq(t
										.getReplyId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<ServiceReply> serviceReply) {
		if (CollectionUtil.isEmpty(serviceReply)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(serviceReply,
				new DeleteGenerateCallback<ServiceReply>() {
					public Delete generate(ServiceReply t) {
						return delete(SERVICE_REPLY_TABLE)
								.where(and(
										SERVICE_REPLY_TABLE.REPLY_ID.eq(t
												.getReplyId()),
										SERVICE_REPLY_TABLE.CLIENT_REQUEST_ID
												.eq(t.getClientRequestId()),
										SERVICE_REPLY_TABLE.REPLY_OPINION.eq(t
												.getReplyOpinion()),
										SERVICE_REPLY_TABLE.REPLY_SPEC.eq(t
												.getReplySpec()),
										SERVICE_REPLY_TABLE.REPLY_COMMITMENT_DATE
												.eq(t.getReplyCommitmentDate()),
										SERVICE_REPLY_TABLE.REPLY_DO_BY.eq(t
												.getReplyDoBy()),
										SERVICE_REPLY_TABLE.REPLY_DO_DATE.eq(t
												.getReplyDoDate()),
										SERVICE_REPLY_TABLE.REPLIER.eq(t
												.getReplier()),
										SERVICE_REPLY_TABLE.REPLY_DATE.eq(t
												.getReplyDate()),
										SERVICE_REPLY_TABLE.REPLY_DONE.eq(t
												.getReplyDone())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = orderBies[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<ServiceReply> serviceReply) {
		if (CollectionUtil.isEmpty(serviceReply)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, serviceReply,
				new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(SERVICE_REPLY_TABLE)
								.values(SERVICE_REPLY_TABLE.CLIENT_REQUEST_ID
										.value(new JdbcNamedParameter(
												"clientRequestId")),
										SERVICE_REPLY_TABLE.REPLY_OPINION
												.value(new JdbcNamedParameter(
														"replyOpinion")),
										SERVICE_REPLY_TABLE.REPLY_SPEC
												.value(new JdbcNamedParameter(
														"replySpec")),
										SERVICE_REPLY_TABLE.REPLY_COMMITMENT_DATE
												.value(new JdbcNamedParameter(
														"replyCommitmentDate")),
										SERVICE_REPLY_TABLE.REPLY_DO_BY
												.value(new JdbcNamedParameter(
														"replyDoBy")),
										SERVICE_REPLY_TABLE.REPLY_DO_DATE
												.value(new JdbcNamedParameter(
														"replyDoDate")),
										SERVICE_REPLY_TABLE.REPLIER
												.value(new JdbcNamedParameter(
														"replier")),
										SERVICE_REPLY_TABLE.REPLY_DATE
												.value(new JdbcNamedParameter(
														"replyDate")),
										SERVICE_REPLY_TABLE.REPLY_DONE
												.value(new JdbcNamedParameter(
														"replyDone"))

								);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<ServiceReply> serviceReply) {
		if (CollectionUtil.isEmpty(serviceReply)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(serviceReply,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(SERVICE_REPLY_TABLE)
								.set(SERVICE_REPLY_TABLE.CLIENT_REQUEST_ID
										.value(new JdbcNamedParameter(
												"clientRequestId")),
										SERVICE_REPLY_TABLE.REPLY_OPINION
												.value(new JdbcNamedParameter(
														"replyOpinion")),
										SERVICE_REPLY_TABLE.REPLY_SPEC
												.value(new JdbcNamedParameter(
														"replySpec")),
										SERVICE_REPLY_TABLE.REPLY_COMMITMENT_DATE
												.value(new JdbcNamedParameter(
														"replyCommitmentDate")),
										SERVICE_REPLY_TABLE.REPLY_DO_BY
												.value(new JdbcNamedParameter(
														"replyDoBy")),
										SERVICE_REPLY_TABLE.REPLY_DO_DATE
												.value(new JdbcNamedParameter(
														"replyDoDate")),
										SERVICE_REPLY_TABLE.REPLIER
												.value(new JdbcNamedParameter(
														"replier")),
										SERVICE_REPLY_TABLE.REPLY_DATE
												.value(new JdbcNamedParameter(
														"replyDate")),
										SERVICE_REPLY_TABLE.REPLY_DONE
												.value(new JdbcNamedParameter(
														"replyDone"))

								).where(SERVICE_REPLY_TABLE.REPLY_ID
										.eq(new JdbcNamedParameter("replyId")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<ServiceReply> serviceReply) {
		if (CollectionUtil.isEmpty(serviceReply)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(serviceReply,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(SERVICE_REPLY_TABLE)
								.where(and(
										SERVICE_REPLY_TABLE.CLIENT_REQUEST_ID
												.eq(new JdbcNamedParameter(
														"clientRequestId")),
										SERVICE_REPLY_TABLE.REPLY_OPINION
												.eq(new JdbcNamedParameter(
														"replyOpinion")),
										SERVICE_REPLY_TABLE.REPLY_SPEC
												.eq(new JdbcNamedParameter(
														"replySpec")),
										SERVICE_REPLY_TABLE.REPLY_COMMITMENT_DATE
												.eq(new JdbcNamedParameter(
														"replyCommitmentDate")),
										SERVICE_REPLY_TABLE.REPLY_DO_BY
												.eq(new JdbcNamedParameter(
														"replyDoBy")),
										SERVICE_REPLY_TABLE.REPLY_DO_DATE
												.eq(new JdbcNamedParameter(
														"replyDoDate")),
										SERVICE_REPLY_TABLE.REPLIER
												.eq(new JdbcNamedParameter(
														"replier")),
										SERVICE_REPLY_TABLE.REPLY_DATE
												.eq(new JdbcNamedParameter(
														"replyDate")),
										SERVICE_REPLY_TABLE.REPLY_DONE
												.eq(new JdbcNamedParameter(
														"replyDone"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<ServiceReply> serviceReply) {
		return preparedBatchInsert(true, serviceReply);
	}

}
