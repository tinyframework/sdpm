/**
 *  Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.tinygroup.sdpm.service.dao.impl;

import org.springframework.stereotype.Repository;
import static org.tinygroup.tinysqldsl.base.StatementSqlBuilder.and;
import static org.tinygroup.sdpm.service.dao.constant.ServiceFaqTypeTable.*;
import static org.tinygroup.tinysqldsl.Select.*;
import static org.tinygroup.tinysqldsl.Insert.*;
import static org.tinygroup.tinysqldsl.Delete.*;
import static org.tinygroup.tinysqldsl.Update.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static org.tinygroup.sdpm.service.dao.constant.ServiceFaqTypeTable.SERVICE_FAQ_TYPETABLE;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;
import static org.tinygroup.tinysqldsl.Update.update;
import java.sql.Date;

import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.Pager;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.callback.*;
import org.tinygroup.tinysqldsl.expression.JdbcNamedParameter;
import org.tinygroup.tinysqldsl.extend.MysqlSelect;
import org.tinygroup.tinysqldsl.select.OrderByElement;

import org.tinygroup.sdpm.service.dao.pojo.ServiceFaqType;
import org.tinygroup.tinysqldsl.*;
import org.tinygroup.sdpm.service.dao.ServiceFaqTypeDao;

import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.jdbctemplatedslsession.callback.InsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.DeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.SelectGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.UpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamDeleteGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamInsertGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.callback.NoParamUpdateGenerateCallback;
import org.tinygroup.jdbctemplatedslsession.util.TinyDSLUtil;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
@Repository
public class ServiceFaqTypeDaoImpl extends TinyDslDaoSupport implements
		ServiceFaqTypeDao {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ServiceFaqType add(ServiceFaqType serviceFaqType) {
		return getDslTemplate().insertAndReturnKey(serviceFaqType,
				new InsertGenerateCallback<ServiceFaqType>() {
					public Insert generate(ServiceFaqType t) {
						Insert insert = insertInto(SERVICE_FAQ_TYPE_TABLE)
								.values(SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_ID
										.value(t.getFaqTypeId()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE.value(t
												.getFaqType()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_PARENT_TYPE_ID
												.value(t.getFaqParentTypeId()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_CREAT_DAY
												.value(t.getFaqTypeCreatDay()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_CREATED_BY
												.value(t.getFaqCreatedBy()),
										SERVICE_FAQ_TYPE_TABLE.DELETED.value(t
												.getDeleted())

								);
						return insert;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int edit(ServiceFaqType serviceFaqType) {
		if (serviceFaqType == null || serviceFaqType.getFaqTypeId() == null) {
			return 0;
		}
		return getDslTemplate().update(serviceFaqType,
				new UpdateGenerateCallback<ServiceFaqType>() {
					public Update generate(ServiceFaqType t) {
						Update update = update(SERVICE_FAQ_TYPE_TABLE).set(
								SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE.value(t
										.getFaqType()),
								SERVICE_FAQ_TYPE_TABLE.FAQ_PARENT_TYPE_ID
										.value(t.getFaqParentTypeId()),
								SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_CREAT_DAY
										.value(t.getFaqTypeCreatDay()),
								SERVICE_FAQ_TYPE_TABLE.FAQ_CREATED_BY.value(t
										.getFaqCreatedBy()),
								SERVICE_FAQ_TYPE_TABLE.DELETED.value(t
										.getDeleted())).where(
								SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_ID.eq(t
										.getFaqTypeId()));
						return update;
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKey(Integer pk) {
		if (pk == null) {
			return 0;
		}
		return getDslTemplate().deleteByKey(pk,
				new DeleteGenerateCallback<Serializable>() {
					public Delete generate(Serializable pk) {
						return delete(SERVICE_FAQ_TYPE_TABLE).where(
								SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_ID.eq(pk));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int deleteByKeys(Integer... pks) {
		if (pks == null || pks.length == 0) {
			return 0;
		}
		return getDslTemplate().deleteByKeys(
				new DeleteGenerateCallback<Serializable[]>() {
					public Delete generate(Serializable[] t) {
						return delete(SERVICE_FAQ_TYPE_TABLE).where(
								SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_ID.in(t));
					}
				}, pks);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public ServiceFaqType getByKey(Integer pk) {
		return getDslTemplate().getByKey(pk, ServiceFaqType.class,
				new SelectGenerateCallback<Serializable>() {

					@SuppressWarnings("rawtypes")
					public Select generate(Serializable t) {
						return selectFrom(SERVICE_FAQ_TYPE_TABLE).where(
								SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_ID.eq(t));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public List<ServiceFaqType> query(ServiceFaqType serviceFaqType,
			final OrderBy... orderArgs) {
		if (serviceFaqType == null) {
			serviceFaqType = new ServiceFaqType();
		}
		return getDslTemplate().query(serviceFaqType,
				new SelectGenerateCallback<ServiceFaqType>() {
					@SuppressWarnings("rawtypes")
					public Select generate(ServiceFaqType t) {
						Select select = selectFrom(SERVICE_FAQ_TYPE_TABLE)
								.where(and(
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_ID.eq(t
												.getFaqTypeId()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE.eq(t
												.getFaqType()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_PARENT_TYPE_ID
												.eq(t.getFaqParentTypeId()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_CREAT_DAY
												.eq(t.getFaqTypeCreatDay()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_CREATED_BY
												.eq(t.getFaqCreatedBy()),
										SERVICE_FAQ_TYPE_TABLE.DELETED.eq(t
												.getDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public Pager<ServiceFaqType> queryPager(int start, int limit,
			ServiceFaqType serviceFaqType, final OrderBy... orderArgs) {
		if (serviceFaqType == null) {
			serviceFaqType = new ServiceFaqType();
		}
		return getDslTemplate().queryPager(start, limit, serviceFaqType, false,
				new SelectGenerateCallback<ServiceFaqType>() {
					public Select generate(ServiceFaqType t) {
						Select select = Select
								.selectFrom(SERVICE_FAQ_TYPE_TABLE)
								.where(and(
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_ID.eq(t
												.getFaqTypeId()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE.eq(t
												.getFaqType()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_PARENT_TYPE_ID
												.eq(t.getFaqParentTypeId()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_CREAT_DAY
												.eq(t.getFaqTypeCreatDay()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_CREATED_BY
												.eq(t.getFaqCreatedBy()),
										SERVICE_FAQ_TYPE_TABLE.DELETED.eq(t
												.getDeleted())

								));
						return TinyDSLUtil
								.addOrderByElements(select, orderArgs);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(boolean autoGeneratedKeys,
			List<ServiceFaqType> serviceFaqType) {
		if (CollectionUtil.isEmpty(serviceFaqType)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, serviceFaqType,
				new InsertGenerateCallback<ServiceFaqType>() {

					public Insert generate(ServiceFaqType t) {
						return insertInto(SERVICE_FAQ_TYPE_TABLE).values(
								SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_ID.value(t
										.getFaqTypeId()),
								SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE.value(t
										.getFaqType()),
								SERVICE_FAQ_TYPE_TABLE.FAQ_PARENT_TYPE_ID
										.value(t.getFaqParentTypeId()),
								SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_CREAT_DAY
										.value(t.getFaqTypeCreatDay()),
								SERVICE_FAQ_TYPE_TABLE.FAQ_CREATED_BY.value(t
										.getFaqCreatedBy()),
								SERVICE_FAQ_TYPE_TABLE.DELETED.value(t
										.getDeleted())

						);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchInsert(List<ServiceFaqType> serviceFaqTypes) {
		return batchInsert(true, serviceFaqTypes);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchUpdate(List<ServiceFaqType> serviceFaqType) {
		if (CollectionUtil.isEmpty(serviceFaqType)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(serviceFaqType,
				new UpdateGenerateCallback<ServiceFaqType>() {
					public Update generate(ServiceFaqType t) {
						return update(SERVICE_FAQ_TYPE_TABLE).set(
								SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE.value(t
										.getFaqType()),
								SERVICE_FAQ_TYPE_TABLE.FAQ_PARENT_TYPE_ID
										.value(t.getFaqParentTypeId()),
								SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_CREAT_DAY
										.value(t.getFaqTypeCreatDay()),
								SERVICE_FAQ_TYPE_TABLE.FAQ_CREATED_BY.value(t
										.getFaqCreatedBy()),
								SERVICE_FAQ_TYPE_TABLE.DELETED.value(t
										.getDeleted())

						).where(SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_ID.eq(t
								.getFaqTypeId()));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] batchDelete(List<ServiceFaqType> serviceFaqType) {
		if (CollectionUtil.isEmpty(serviceFaqType)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(serviceFaqType,
				new DeleteGenerateCallback<ServiceFaqType>() {
					public Delete generate(ServiceFaqType t) {
						return delete(SERVICE_FAQ_TYPE_TABLE)
								.where(and(
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_ID.eq(t
												.getFaqTypeId()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE.eq(t
												.getFaqType()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_PARENT_TYPE_ID
												.eq(t.getFaqParentTypeId()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_CREAT_DAY
												.eq(t.getFaqTypeCreatDay()),
										SERVICE_FAQ_TYPE_TABLE.FAQ_CREATED_BY
												.eq(t.getFaqCreatedBy()),
										SERVICE_FAQ_TYPE_TABLE.DELETED.eq(t
												.getDeleted())

								));
					}
				});
	}

	private Select addOrderByElements(Select select, OrderBy... orderBies) {
		List<OrderByElement> orderByElements = new ArrayList<OrderByElement>();
		for (int i = 0; orderBies != null && i < orderBies.length; i++) {
			OrderByElement tempElement = orderBies[i].getOrderByElement();
			if (tempElement != null) {
				orderByElements.add(tempElement);
			}
		}
		if (orderByElements.size() > 0) {
			select.orderBy(orderByElements.toArray(new OrderByElement[0]));
		}
		return select;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchInsert(boolean autoGeneratedKeys,
			List<ServiceFaqType> serviceFaqType) {
		if (CollectionUtil.isEmpty(serviceFaqType)) {
			return new int[0];
		}
		return getDslTemplate().batchInsert(autoGeneratedKeys, serviceFaqType,
				new NoParamInsertGenerateCallback() {

					public Insert generate() {
						return insertInto(SERVICE_FAQ_TYPE_TABLE)
								.values(SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE
										.value(new JdbcNamedParameter("faqType")),
										SERVICE_FAQ_TYPE_TABLE.FAQ_PARENT_TYPE_ID
												.value(new JdbcNamedParameter(
														"faqParentTypeId")),
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_CREAT_DAY
												.value(new JdbcNamedParameter(
														"faqTypeCreatDay")),
										SERVICE_FAQ_TYPE_TABLE.FAQ_CREATED_BY
												.value(new JdbcNamedParameter(
														"faqCreatedBy")),
										SERVICE_FAQ_TYPE_TABLE.DELETED
												.value(new JdbcNamedParameter(
														"deleted"))

								);
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchUpdate(List<ServiceFaqType> serviceFaqType) {
		if (CollectionUtil.isEmpty(serviceFaqType)) {
			return new int[0];
		}
		return getDslTemplate().batchUpdate(serviceFaqType,
				new NoParamUpdateGenerateCallback() {
					public Update generate() {
						return update(SERVICE_FAQ_TYPE_TABLE)
								.set(SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE
										.value(new JdbcNamedParameter("faqType")),
										SERVICE_FAQ_TYPE_TABLE.FAQ_PARENT_TYPE_ID
												.value(new JdbcNamedParameter(
														"faqParentTypeId")),
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_CREAT_DAY
												.value(new JdbcNamedParameter(
														"faqTypeCreatDay")),
										SERVICE_FAQ_TYPE_TABLE.FAQ_CREATED_BY
												.value(new JdbcNamedParameter(
														"faqCreatedBy")),
										SERVICE_FAQ_TYPE_TABLE.DELETED
												.value(new JdbcNamedParameter(
														"deleted"))

								)
								.where(SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_ID
										.eq(new JdbcNamedParameter("faqTypeId")));
					}
				});
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public int[] preparedBatchDelete(List<ServiceFaqType> serviceFaqType) {
		if (CollectionUtil.isEmpty(serviceFaqType)) {
			return new int[0];
		}

		return getDslTemplate().batchDelete(serviceFaqType,
				new NoParamDeleteGenerateCallback() {
					public Delete generate() {
						return delete(SERVICE_FAQ_TYPE_TABLE)
								.where(and(
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE
												.eq(new JdbcNamedParameter(
														"faqType")),
										SERVICE_FAQ_TYPE_TABLE.FAQ_PARENT_TYPE_ID
												.eq(new JdbcNamedParameter(
														"faqParentTypeId")),
										SERVICE_FAQ_TYPE_TABLE.FAQ_TYPE_CREAT_DAY
												.eq(new JdbcNamedParameter(
														"faqTypeCreatDay")),
										SERVICE_FAQ_TYPE_TABLE.FAQ_CREATED_BY
												.eq(new JdbcNamedParameter(
														"faqCreatedBy")),
										SERVICE_FAQ_TYPE_TABLE.DELETED
												.eq(new JdbcNamedParameter(
														"deleted"))

								));
					}
				});
	}

	public int[] preparedBatchInsert(List<ServiceFaqType> serviceFaqType) {
		return preparedBatchInsert(true, serviceFaqType);
	}

}
